﻿// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
//import { Bucket } from '@google-cloud/storage';
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');

var storage = require('@google-cloud/storage');

var serviceAccount = require('./win_key.json');


/*
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://test-project-775ac.firebaseio.com',
  storageBucket: "test-project-775ac.appspot.com"
});
*/


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://ufavolleyreleased.firebaseio.com',
  storageBucket: "ufavolleyreleased.appspot.com"
});



//admin.initializeApp();
const db = admin.firestore();
const realtimeDB = admin.database();

var CURRENT_VERSION = "1.2.1";

async function generatePhotoUrlPromise(docId) {
  let file = admin.storage().bucket().file(docId);
      //usr.imgUrl = file;
      let year = (new Date(Date.now())).getFullYear();
      year += 5;
      let exp = '01-01-' + year;
      return file.getSignedUrl({ action: 'read', expires: exp});
}

exports.searchUsers = functions.https.onCall(async (data, context) => {
  let searchString = data.searchString.toLowerCase();
  let result = [];
  let startAtName = data.startAtName;
  let startAtUid = data.startAtUid;
  if(startAtName === undefined)
    startAtName = null;
  if(startAtUid === undefined)
    startAtUid = null;

  let paginate = startAtName !== null && startAtUid !== null;
  let usersRequest = db.collection('users').orderBy('first_name', 'desc').orderBy('uid', 'desc').where("search", 'array-contains', searchString).where("byAdmin", "==", false);
  if(!paginate){
    usersRequest = usersRequest.limit(200);
  } else {
    usersRequest = usersRequest.startAfter(startAtName, startAtUid).limit(10);
  }
  let users = await usersRequest.get();

  let urlPromises = [];

  users.forEach(userDoc => {
    result.push(getUserObject(userDoc));
    urlPromises.push(generatePhotoUrlPromise(getUserObject(userDoc).docID));
  })

  let photoUrls = await Promise.all(urlPromises);
  for(i = 0; i < photoUrls.length; i++){
    result[i].imgUrl = photoUrls[i][0];
  }

  return result;
})



exports.addSearchString = functions.https.onRequest(async (req, res) => {

 let promises = [];
 let count = 0;
  const collection = await db
    .collection("users")
    .get()
  collection.forEach(doc=> {
    count++;
    let user = getUserObject(doc)
    promises.push(
      doc.ref.update({
        'search' : generateKeywords([user.first_name, user.last_name, user.phone])
        })
    );
  })

  await Promise.all(promises);

  res.status(200).send(count + " users updated");
});

exports.userUpdated = functions.firestore
    .document('users/{uid}')
    .onUpdate(async (change, context) => {
      const userAfter = getUserObject(change.after);
      const userBefore = getUserObject(change.before);

      if(userAfter.first_name !== userBefore.first_name || userAfter.last_name !== userBefore.first_name){
        return db.collection('users').doc(context.params.uid).update({
          'search' : generateKeywords([userAfter.first_name, userAfter.last_name, userAfter.phone])
        })
      }
      return 0;
    })

exports.trainingUpdated = functions.firestore
    .document('trainings/{trId}')
    .onUpdate(async (change, context) =>{
      // Get an object representing the document
      // e.g. {'name': 'Marie', 'age': 66}
      const newValue = change.after.data();

      var trPayload = await getTraining(change.after.id);
      delete trPayload.users
      //console.log("Training updated " + JSON.stringify(trPayload));
      //console.log("Training id is " + context.params.trId);


      let tokens = [];
      var rootRef = realtimeDB.ref();
      var urlRef = rootRef.child("/status");
      await urlRef.once("value", function(snapshot) {
        snapshot.forEach(function(child) {

          //console.log("User with fcm_token: " + child.key + " : "+ JSON.stringify(trPayload));
          tokens.push(child.key);
        });
      });

      var payload = {
        data: {
          training: JSON.stringify(trPayload)
        }
      };

      if(tokens.length > 0){
        await admin.messaging().sendToDevice(tokens, payload).then(function(response) {
                console.log("Successfully sent message:", response);
                console.log(response.results[0].error);
                return 0;
            })
            .catch(function(error) {
                console.log("Error sending message:", error);
            });
        console.log("Tokens send: " + tokens);
        console.log("Training: " + JSON.stringify(payload))
      }

      return 0;
    });


exports.onUserStatusChanged = functions.database.ref('/status/{fcm}').onUpdate(
  async (change, context) => {

    const eventStatus = change.after.val();
    console.log('onUserStatusChanged eventStatus ' + eventStatus.toString() + " " + context.params.fcm);

    const statusSnapshot = await change.after.ref.once('value');
    const status = statusSnapshot.val();
    console.log(status.state, eventStatus);

    if(status.state.toString() === 'offline'){
      console.log('Removing value');
      await realtimeDB.ref('/status/' + context.params.fcm).remove();
    }

    return 0;
  });


  exports.getRatingList = functions.https.onCall(async (data, context) => {
    var users = await db.collection('users').where("rating", ">", 0).orderBy("rating", "desc").get();
    let uids = [];


    for(let i = 0; i < users.docs.length; i++){
      uids.push(getUserObject(users.docs[i]).uid);
    }

    let response = await getUsersV2(uids);

    return response;
  });


  exports.getRatingListV2 = functions.https.onCall(async (data, context) => {

    var level = data.level;

    if(level === null || level === undefined)
      return 0

    var users = await db.collection('users').where("level", "==", level).where("rating", ">", 0).orderBy("rating", "desc").limit(30).get();
    let uids = [];


    for(let i = 0; i < users.docs.length; i++){
      uids.push(getUserObject(users.docs[i]).uid);
    }

    let response = await getUsersV2(uids);

    return response;
  });


  exports.getLeaderBoardData = functions.https.onCall(async (data, context) => {

  var trainingId = data.trainingId;

  var doc = await db.collection("leaderboard").doc(trainingId).get();
  var dataObj = doc.data();



  if(dataObj !== undefined){

    let usersToGetPromises = [];
    for(let i = 0; i < dataObj.groups.length; i++){
      let usersToGet =  dataObj.groups[i].users;
      usersToGetPromises.push(getUsersV2(usersToGet));

    }

    let results = await Promise.all(usersToGetPromises);

    for(let i = 0; i < results.length; i++){
      dataObj.groups[i].users = results[i];
    }


    let objectToReturn = {
      "trainingId" : doc.id,
    }
    Object.assign(objectToReturn, dataObj);
    var dataJson = JSON.stringify(objectToReturn);
    return dataJson;

  } else {
    var tr = getRawTrainingObject(await db.collection('trainings').doc(doc.id).get());
    var usersList = await getUsersV2(tr.uids);

    let objectToReturn = {
      "trainingId" : doc.id,
      "groups" : [{
        "ratingDetails" : {
          "place" : 0,
          "ratingValue" : 0
        },
        "users" : usersList
      }]
    };

    var jsonToReturn = JSON.stringify(objectToReturn);
    return jsonToReturn;

  }
  });


  exports.setLeaderBoardData = functions.https.onCall(async (data, context) => {

    var obj = JSON.parse(data.data);

    var access = ["185IVUsVV0Nku05XvBbhAmN0AV63", "o0nXRP7iiyP5wr3Z6BbrxGigAm82"];
    if(!access.includes(context.auth.uid)){
      console.log("Unauthroized user is trying to set data " + context.auth.uid);
      return -1;
    }


    //вставить сначала get, потом декремент, а после инкремент :))


    var doc = await db.collection("leaderboard").doc(obj.trainingId).get();
    var dataObj = doc.data();
    //декрементим рейтинг тех юзеров, у которых он уже был выставлен (в случае, если мы обновляем лидерборд, т.е. doc !== null)
    let decrementPromises = [];
    if(dataObj !== undefined){

      for(let i = 0; i < dataObj.groups.length; i++){
        if(dataObj.groups[i].ratingDetails.place > 0){
          for(let j = 0; j < dataObj.groups[i].users.length; j++){
            var userRef = db.collection('users').doc(dataObj.groups[i].users[j]);
            decrementPromises.push(userRef.update({
                rating: admin.firestore.FieldValue.increment(-dataObj.groups[i].ratingDetails.ratingValue)
              })
            );
          }
        }
      }
    }


    await Promise.all(decrementPromises);
    //вписываем новые данные в лидерборд и инкрементим рейтинги у юзеров
    var uids = [];
    let incrementPromises = [];
    for(let i = 0; i < obj.groups.length; i++){
      let uidsInGroup = [];
      for(let j = 0; j < obj.groups[i].users.length; j++){

        uidsInGroup.push(obj.groups[i].users[j].uid);

        if(obj.groups[i].ratingDetails.place !== 0){
          uids.push(obj.groups[i].users[j].uid);

          let userRef = db.collection('users').doc(obj.groups[i].users[j].uid);
          incrementPromises.push(userRef.update({
                rating: admin.firestore.FieldValue.increment(obj.groups[i].ratingDetails.ratingValue)
              })
            );
        }
      }
      obj.groups[i].users = uidsInGroup;
    }

    db.collection("leaderboard").doc(obj.trainingId).set({
      groups: obj.groups,
      uids: uids
    });

    await Promise.all(incrementPromises);


    console.log("leaderboard for training: " + obj.trainingId + " is set with " + data.data);

    return 0;

  });

exports.checkVersion = functions.https.onCall(async (data, context) => {

  var ver = data.version;
  console.log(context.auth.uid + " USER APP VERSION IS " + ver);
  if (ver === CURRENT_VERSION) {
    return {
      code: 0,
      msg: "VERSION IS UP TO DATE"
    }
  } else {
    return {
      code: -1,
      msg: "YOU ARE USING OUTDATED VERSION"
    }
  }

});

Date.prototype.addHours = function (h) {
  this.setTime(this.getTime() + (h * 60 * 60 * 1000));
  return this;
}

Date.prototype.addMiuntes = function (h) {
  this.setTime(this.getTime() + (h * 60 * 1000));
  return this;
}

Date.prototype.removeMinutes = function (h) {
  this.setTime(this.getTime() - (h * 60 * 1000));
  return this;
}

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}





function getUserObject(doc) {
  var rating = doc.get('rating');
  if (rating === undefined || rating === null)
    rating = 0;

  var supervisor = doc.get('supervisor');
  if (supervisor === undefined || supervisor === null)
    supervisor = false;

  var banned = doc.get('banned');
  if (banned === undefined || banned === null)
    banned = false;

  var isDebtor = doc.get('isDebtor');
  if(isDebtor === undefined || isDebtor === null)
    isDebtor = false;

  var age = doc.get('age');
  if (age === undefined || age === null)
    age = {'_seconds': 0}

  var usr = {
    admin: doc.get('admin'),
    age: age,
    first_name: doc.get('first_name'),
    last_name: doc.get('last_name'),
    level: doc.get('level'),
    phone: doc.get('phone'),
    sex: doc.get('sex'),
    trainer: doc.get('trainer'),
    uid: doc.get('uid'),
    docID: doc.id,
    editable: doc.get('editable'),
    byAdmin: doc.get('byAdmin'),
    main_admin: doc.get('mainAdmin'),
    supervisor: supervisor,
    banned: banned,
    isDebtor: isDebtor,
    rating: rating
  }

  return usr;
}

function createUserObject(isAdmin, age, first_name, last_name, level, phone, sex, trainer, uid, fcm, editable, byAdmin, supervisor, banned, isDebtor) {

  if(isDebtor === undefined || isDebtor === null){
    isDebtor = false;
  }

  var fcm_var;
  if (Array.isArray(fcm)) {
    fcm_var = fcm;
  } else {
    fcm_var = [fcm];
  }

  var obj = {
    admin: isAdmin,
    age: age,
    first_name: first_name,
    last_name: last_name,
    level: level,
    phone: phone,
    sex: sex,
    trainer: trainer,
    uid: uid,
    fcm: fcm_var,
    editable: editable,
    byAdmin: byAdmin,
    supervisor: supervisor,
    banned: banned,
    isDebtor: isDebtor
  }

  return obj;
}

exports.getEmailsByUids = functions.https.onCall(async (data, context) => {
  var uids = data.uids;
  var promises = [];
  for (var i = 0; i < uids.length; i++) {
    promises.push(admin.auth().getUser(uids[i]));
  }

  var responses = await Promise.all(promises);

  var resp = [];

  for (i = 0; i < responses.length; i++) {
    resp.push({
      uid: responses[i].uid,
      email: responses[i].email
    })
  }

  return {
    response: resp
  }

});



exports.updateTraining = functions.https.onCall(async (data, context) => {

  var trID = data.id;

  var adr = data.adr; //id адреса
  var date = data.date; //timestamp
  var levels = data.levels; //массив id уровней
  var limit = data.limit; //размер лимита
  var price = data.price; //цена
  var title = data.title; //id тайтла
  var trainer_id = data.trainer_id; //массив id тренеров
  var type = data.type; //id типа
  var uids = data.uids; //id списка записавшихся
  var checked = data.checked;
  var participantsMinimum = data.participantsMinimum; //минимум игроков
  var supervisors = data.supervisors;



  //новые поля

  if(supervisors === undefined || supervisors === null) {
    supervisors = [];
  }

  if(participantsMinimum === undefined || participantsMinimum === null) {
    participantsMinimum = 0;
  }
  var duration = data.duration;
  var comment = data.comment;

  if (duration === undefined || duration === null) {
    duration = 0;
  }

  if (comment === undefined || comment === null) {
    comment = "";
  }


  var stamp = new Date();
  stamp.setTime(date);

  var auto = data.auto;

  var toNotify = [];
  var trToUpdate = await db.collection('trainings').doc(trID).get();
  var oldUids = trToUpdate.get('uids');

  for (var i = 0; i < uids.length; i++) {
    if (oldUids.indexOf(uids[i], 0) > (limit - 1) && i <= (limit - 1)) {
      toNotify.push(uids[i]);
    }
  }

  console.log("toNotify array is " + toNotify.toString());

  notifyByUids(toNotify, "Приходите!", "Вы в лимите!", trID, 1, false);

  var training = {
    address: adr,
    date: stamp,
    levels: levels,
    limit: limit,
    price: price,
    reserve: 99999,
    title: title,
    trainer_id: trainer_id,
    type: type,
    uids: uids,
    auto: auto,
    checked: checked,
    duration: duration,
    comment: comment,
    participantsMinimum: participantsMinimum,
    supervisors: supervisors
  };

  return db.collection('trainings').doc(trID).update(training).then((docRef) => {
    return ({
      id: docRef.id,
      code: 0,
      message: 'successfully updated training'
    });
  }).catch(() => {
    return ({
      code: -1,
      message: 'error'
    });
  });


});

exports.addTraining = functions.https.onCall(async (data, context) => {
  var adr = data.adr; //id адреса
  var date = data.date; //timestamp
  var levels = data.levels; //массив id уровней
  var limit = data.limit; //размер лимита
  var price = data.price; //цена
  var title = data.title; //id тайтла
  var trainer_id = data.trainer_id; //массив id тренеров
  var type = data.type; //id типа
  var uids = data.uids; //id списка записавшихся
  var checked = data.checked; //массив отмеченных юзеров
  var participantsMinimum = data.participantsMinimum; //минимум игроков
  var supervisors = data.supervisors;

  //новые поля
  var duration = data.duration;
  var comment = data.comment;

  if(supervisors === undefined || supervisors === null){
    supervisors = [];
  }

  if(participantsMinimum === undefined || participantsMinimum === null) {
    participantsMinimum = 0;
  }

  if (duration === undefined || duration === null) {
    duration = 0;
  }

  if (comment === undefined || comment === null) {
    comment = "";
  }


  var stamp = new Date();
  stamp.setTime(date);

  var auto = data.auto;
  if (auto === undefined || auto === null)
    auto = false;



  var training = {
    address: adr,
    date: stamp,
    levels: levels,
    limit: limit,
    price: price,
    reserve: 99999,
    title: title,
    trainer_id: trainer_id,
    type: type,
    uids: uids,
    auto: auto,
    checked: checked,
    duration: duration,
    comment: comment,
    participantsMinimum: participantsMinimum,
    supervisors: supervisors
  };

  return db.collection('trainings').add(training).then((docRef) => {
    return ({
      id: docRef.id,
      code: 0,
      message: 'successfully added training'
    });
  }).catch(() => {
    return ({
      code: -1,
      message: 'error'
    });
  });



});



exports.getAllUsersOn = functions.https.onRequest(async (req, res) => {

  var lastID = '';

  var usersQuery = db.collection('users').orderBy('first_name', 'desc');
  var users;

  if(lastID === ''){
    users = await usersQuery.limit(100).get();
  } else {
    users = await usersQuery.startAfter(await db.collection('users').doc(lastID).get()).limit(100).get();
  }

  var ret = [];
  var urlPromises = [];

  for (var i = 0; i < users.docs.length; i++) {


    //ПРОВЕРКА НА КАСТ
    //var data = users.docs[i].data();
    //var jstr = JSON.stringify(data);


    var usr = getUserObject(users.docs[i]);



    //здесь проверка на пустых юзеров!
    if (usr.byAdmin === false && (usr.first_name !== '' || usr.last_name !== '')){
      let file = admin.storage().bucket().file(usr.docID);
      //usr.imgUrl = file;
      let year = (new Date(Date.now())).getFullYear();
      year += 5;
      let exp = '01-01-' + year;
      urlPromises.push(file.getSignedUrl({ action: 'read', expires: exp}));
      ret.push(usr);
    }

  }
  //var result = await Promise.all(ret.map(item => item.imgUrl.then(imgUrl => ({... item, imgUrl}))));

  var result = await Promise.all(urlPromises);
  for(i = 0; i < result.length; i++){
    ret[i].imgUrl = result[i][0];
  }


  res.status(200).send(ret);
});


exports.getAllUsersV2 = functions.https.onCall(async (data, context) => {


  var lastID = '';

  if(data.lastID !== undefined)
    lastID = data.lastID;


  let date = new Date(Date.now());
  date.setFullYear( date.getFullYear() - 1);
  date.setMonth(date.getMonth() + 2);

  var usersQuery = db.collection('users').where('last_updated', '>=', date);
  var users;

  if(lastID === ''){
    users = await usersQuery.limit(10000).get();
  } else {
    users = await usersQuery.startAfter(await db.collection('users').doc(lastID).get()).limit(50000).get();
  }

  var ret = [];
  var urlPromises = [];

  for (var i = 0; i < users.docs.length; i++) {


    //ПРОВЕРКА НА КАСТ

    var usr = getUserObject(users.docs[i]);

    //здесь проверка на пустых юзеров!
    if (usr.byAdmin === false && (usr.first_name !== '' || usr.last_name !== '')){
      let file = admin.storage().bucket().file(usr.docID);
      //usr.imgUrl = file;
      let year = (new Date(Date.now())).getFullYear();
      year += 5;
      let exp = '01-01-' + year;
      urlPromises.push(file.getSignedUrl({ action: 'read', expires: exp}));
      ret.push(usr);
    }

  }
  //var result = await Promise.all(ret.map(item => item.imgUrl.then(imgUrl => ({... item, imgUrl}))));


  var result = await Promise.all(urlPromises);
  for(i = 0; i < result.length; i++){
    ret[i].imgUrl = result[i][0];
  }


  return ret;

});


exports.getAllUsers = functions.https.onCall(async (data, context) => {
  let date = new Date(Date.now());
  date.setFullYear( date.getFullYear() - 1);
  date.setMonth(date.getMonth() + 2);

  var users = await db.collection('users').where('last_updated', '>=', date).get();

  var ret = [];
  var urlPromises = [];

  for (var i = 0; i < users.docs.length; i++) {


    //ПРОВЕРКА НА КАСТ

    var usr = getUserObject(users.docs[i]);

    //здесь проверка на пустых юзеров!
    if (usr.byAdmin === false && (usr.first_name !== '' || usr.last_name !== '')){
      let file = admin.storage().bucket().file(usr.docID);
      //usr.imgUrl = file;
      let year = (new Date(Date.now())).getFullYear();
      year += 5;
      let exp = '01-01-' + year;
      urlPromises.push(file.getSignedUrl({ action: 'read', expires: exp}));
      ret.push(usr);
    }

  }

  var result = await Promise.all(urlPromises);
  for(i = 0; i < result.length; i++){
    ret[i].imgUrl = result[i][0];
  }

  return ret;

});

exports.getTraining = functions.https.onCall(async (data, context) => {
  var training = await getTraining(data.trID);

  return training;

});

exports.getTrainers = functions.https.onCall(async (data, context) => {
  var trainersDocs = await db.collection('users').where('trainer', '==', true).get();

  var ret = [];

  for (var i = 0; i < trainersDocs.docs.length; i++) {

    var usr = getUserObject(trainersDocs.docs[i]);
    //выкидываем числовые ID
    if(isNaN(parseInt(usr.uid, 10)) || parseInt(usr.uid, 10).toString().length < usr.uid.length){
      ret.push(usr);
    }

  }

  return ret;

});

exports.getSupervisors = functions.https.onCall(async (data, context) => {
  var trainersDocs = await db.collection('users').where('supervisor', '==', true).get();

  var ret = [];

  for (var i = 0; i < trainersDocs.docs.length; i++) {

    var usr = getUserObject(trainersDocs.docs[i]);
    //выкидываем числовые ID
    if(isNaN(parseInt(usr.uid, 10)) || parseInt(usr.uid, 10).toString().length < usr.uid.length){
      ret.push(usr);
    }

  }

  return ret;

});

exports.getAllValues = functions.https.onCall(async (data, context) => {

  var promises = [];
  var aq = db.collection('addreses').orderBy('timestamp', 'desc');
  var tyq = db.collection('types').orderBy('timestamp', 'desc');
  var tiq = db.collection('titles').orderBy('timestamp', 'desc');
  var lq = db.collection('levels').orderBy('timestamp', 'desc');


  promises.push(aq.get());
  promises.push(tyq.get());
  promises.push(tiq.get());
  promises.push(lq.get());

  var responses = await Promise.all(promises);

  var ret = [];

  for (var i = 0; i < responses.length; i++) {

    var curArray = [];
    var retType = '';
    if (responses[i].query.isEqual(aq))
      retType = 'addresses';
    if (responses[i].query.isEqual(tyq))
      retType = 'types';
    if (responses[i].query.isEqual(tiq))
      retType = 'titles';
    if (responses[i].query.isEqual(lq))
      retType = 'levels';
    for (var j = 0; j < responses[i].docs.length; j++) {
      curArray.push({
        val: responses[i].docs[j].get('val'),
        timestamp: responses[i].docs[j].get('timestamp'),
        id: responses[i].docs[j].id
      });
    }

    ret.push({
      type: retType,
      values: curArray
    });

  }

  return ret;


});

exports.uploadVkPhoto = functions.https.onCall(async (data, context) => {
  var theURL = data.url;

  const https = require('https');
  const fs = require('fs');
  const { Storage } = require('@google-cloud/storage');
  const os = require('os');
  const path = require('path');

  const bucket = admin.storage().bucket()

  const destination = os.tmpdir() + "/" + context.auth.uid;
  const destinationStorage = path.join(os.tmpdir(), "/" + context.auth.uid);

  //var theURL = 'https://sun1-15.userapi.com/c847016/v847016698/9dbe7/stMK_LzQdzM.jpg?ava=1';
  var defUrl = 'https://firebasestorage.googleapis.com/v0/b/ufavolleyreleased.appspot.com/o/default_profile_image_woman.png?alt=media&token=1c2da431-0f2e-4902-a453-db926159d7c6';

  var resp = await new Promise((resolve, reject) => {

    try {
      https.get(theURL, function (response) {
        console.log(response.rawHeaders);
        if (response.statusCode === 200) {
          var file = fs.createWriteStream(destination);
          response.pipe(file);
          file.on('finish', function () {

            console.log('Pipe OK');

            bucket.upload(destinationStorage, {
              destination: context.auth.uid
            }, (file) => {

              console.log('File OK on Storage ');
              resolve({
                code: 0,
                message: "Successfully uploaded photo from VK"
              });
            });

            file.close();

          });
        }
      });

    } catch (error) {
      https.get(defUrl, function (response) {
        console.log(response.rawHeaders);
        if (response.statusCode === 200) {
          var file = fs.createWriteStream(destination);
          response.pipe(file);
          file.on('finish', function () {

            console.log('Pipe OK');

            bucket.upload(destinationStorage, {
              destination: context.auth.uid
            }, (file) => {

              console.log('File OK on Storage ');
              resolve({
                code: 0,
                message: "Successfully uploaded photo from VK"
              });
            });

            file.close();

          });
        }
      });
    }

  })

  return resp;


});


exports.addValue = functions.https.onCall(async (data, context) => {
  var valType = data.type;
  var docVal = data.val;

  return db.collection(valType).add({
    val: docVal,
    timestamp: new Date(Date.now())
  }).then((docRef) => {
    console.log("New value was added to database. VALTYPE: " + valType + " DOCVAL: " + docVal);
    return {
      id: docRef.id,
      code: 0,
      message: "Successfully added value to database"
    };
  }).catch((e) => {
    return {
      code: -1,
      message: "Error " + e
    };
  });
});



exports.updateValue = functions.https.onCall(async (data, context) => {
  var valType = data.type;
  var docID = data.id;
  var docRef = db.collection(valType).doc(docID);

  var docVal = data.val;
  var msg;

  return docRef.update({
    val: docVal
  })
    .then(function (docRef) {
      msg = "Document successfully updated! " + "TYPE: " + valType + " ID: " + docID;
      console.log(msg);

      return {
        id: docRef.id,
        code: 0,
        message: msg
      }
    })
    .catch(function (error) {
      msg = "Error updating document: " + "TYPE: " + valType + " ID: " + docID;
      console.error("Error updating document: ", error);

      return {
        code: -1,
        message: msg
      }
    });


});


exports.deleteDoc = functions.https.onCall(async (data, context) => {
  var uid = data.uid;


});


//аргументы:
//type - имя списка документов из которого хотим удалить
//id - id документа на удаление
exports.deleteDoc = functions.https.onCall(async (data, context) => {
  var valType = data.type;//'types'; //data.type;
  var docID = data.id;//'r4HQVLNXfeYSTUYds54B'; //data.id;
  var msg;

  var searchFlag = 'default';

  if (valType === 'addreses')
    searchFlag = 'address'
  if (valType === 'titles')
    searchFlag = 'title'
  if (valType === 'types')
    searchFlag = 'type'
  if(valType === 'trainer'){
    valType = 'users';
    searchFlag = 'trainer';
  }


var resp;
if(searchFlag !== 'trainer'){
 resp = await db.collection('trainings').where(searchFlag, '==', docID).get();
} else {
  resp = await db.collection('trainings').where('trainer_id', 'array-contains', docID).get();
}


  if (!resp.empty) {
    msg = "Error. This value is already in use. ";
    console.error(msg);
    return {
      code: -2,
      message: msg
    };
  }

  return db.collection(valType).doc(docID).delete().then(function () {
    msg = "Document successfully deleted! " + "TYPE: " + valType + " ID: " + docID;
    console.log(msg);
    return {
      code: 0,
      message: msg
    };
  }).catch(function (error) {
    msg = "Error removing document: ", error + "TYPE: " + valType + " ID: " + docID;
    console.error(msg);
    return {
      code: -1,
      message: msg
    };
  });

});

//аргументы: type - имя документа, из которого хотим получить список значений
exports.getValues = functions.https.onCall(async (data, context) => {
  var valType = data.type;
  console.log("Getting value type is " + valType);
  var valuesDocs = await db.collection(valType).orderBy('timestamp', 'desc').get();

  var response = [];

  for (var i = 0; i < valuesDocs.docs.length; i++) {
    var curValue = valuesDocs.docs[i];
    response.push({
      val: curValue.get('val'),
      timestamp: curValue.get('timestamp'),
      id: curValue.id
    });
  }

  return { response: response };

});


async function getNotificationsByUid(curID) {
  var currentUserDoc = (await db.collection('users').where("uid", "==", curID).get()).docs[0];
  var notDocs = await currentUserDoc.ref.collection('notifications').orderBy('timestamp', 'desc').limit(20).get();
  var notifications = [];

  for (var i = 0; i < notDocs.docs.length; i++) {
    var curNot = notDocs.docs[i];

    notifications.push({
      type: curNot.get('type'),
      read: curNot.get('read'),
      training_id: curNot.get('training_id'),
      timestamp: curNot.get('timestamp'),
      notification_id: curNot.id,
      body: curNot.get('body'),
      title: curNot.get('title')
    });
  }

  return notifications;
}

exports.getNotifications = functions.https.onCall(async (data, context) => {

  var curID = context.auth.uid; //'preSZiQiDMVsZYDiSHqINsvoUvW2'; //context.auth.uid

  return getNotificationsByUid(curID);
});


async function readNotificationByUid(notID, curID) {
  db.collection('users').doc(curID).collection('notifications').doc(notID).update({
    read: true
  })


  var docRef = db.collection("notifications").doc(notID);

  return docRef.get().then(function(doc) {
      if (doc.exists) {
          console.log("Notification " + notID + " is read for user with id " + curID, doc.data());
          db.collection('notifications').doc(notID).update({
            readUids: admin.firestore.FieldValue.arrayUnion(curID)
          })
      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }
      return 0;
  }).catch(function(error) {
      console.log("Error getting document:", error);
  });




}



exports.readNotification = functions.https.onCall(async (data, context) => {
  var curID = context.auth.uid; //"diS3jKXMnbZp5KcriSNbnitK0S83";
  var notID = data.id; //"1569911801137";

  return readNotificationByUid(notID, curID);

});

exports.readAllNotifications = functions.https.onCall(async (data, context) => {
  var curID = context.auth.uid; //"BgeXfaF0MuTT4iNYWZsuwBJMvef2"; //"diS3jKXMnbZp5KcriSNbnitK0S83";
  //var notifications = await getNotificationsByUid(curID);


  var currentUserDoc = (await db.collection('users').where("uid", "==", curID).get()).docs[0];
  var notDocs = await currentUserDoc.ref.collection('notifications').where("read", "==", false).get();
  var notifications = [];

  for (var i = 0; i < notDocs.docs.length; i++) {
    var curNot = notDocs.docs[i];

    notifications.push({
      type: curNot.get('type'),
      read: curNot.get('read'),
      training_id: curNot.get('training_id'),
      timestamp: curNot.get('timestamp'),
      notification_id: curNot.id,
      body: curNot.get('body'),
      title: curNot.get('title')
    });
  }

  var promises = [];

  for(let i = 0; i < notifications.length; i++){
    promises.push(readNotificationByUid(notifications[i].notification_id, curID));
  }

  return new Promise(function(resolve, reject) {
    return Promise.all(promises).then(function () {
      return resolve({
        code: 0,
        msg: 'OK'
      })
    });

  });



});


exports.broadcastPushNotification = functions.runWith({ timeoutSeconds: 300 }).https.onCall(async (data, context) => {
  var body = data.body; //'HELLO';
  var title = data.title;//'TEST_TITLE';
  var uids = data.uids;//['pKuXlXQKlCQHugygxlwU3p7MzQJ3', 'mqz92OkXHrQyL2eh9wRroo38yW92'];//['pKuXlXQKlCQHugygxlwU3p7MzQJ3'];

  await notifyByUids(uids, title, body, 'undef', 4, false);
  return {
    code: 0,
    msg: 'Success'
  }
});

exports.broadcastPushNotificationToAll = functions.runWith({ timeoutSeconds: 300 }).https.onCall(async (data, context) => {
  var body = data.body; //'HELLO';
  var title = data.title;//'TEST_TITLE';

  var uids = [];
  var urlPromises = [];

  for (var i = 0; i < users.docs.length; i++) {
    var usr = getUserObject(users.docs[i]);
    //здесь проверка на пустых юзеров!
    if (usr.byAdmin === false && (usr.first_name !== '' || usr.last_name !== '')){
      uids.push(usr.uid);
    }
  }


  await notifyByUids(uids, title, body, 'undef', 4, true);
  return {
    code: 0,
    msg: 'Success'
  }
});


async function notifyByUidsTemp(uids, title, body, trID, not_type) {

  //вытаскиваем тренеров параллельно
  var promises = [];
  for (let i = 0; i < uids.length; i++) {
    promises.push(db.collection('users').where('uid', '==', uids[i]).get());
  }

  var responses = await Promise.all(promises);

  promises = [];

  var tokens_map = new Map();

  var data = new Map();

  var db_promises = [];
  var fcm_promises = [];

  let stubStamp = (new Date(Date.now()));
  let notification_id = stubStamp.valueOf().toString();

  for (i = 0; i < responses.length; i++) {
    var userDoc = responses[i].docs[0];

    var fcm_tokens = userDoc.get('fcm');




    tokens_map.set(userDoc.id, fcm_tokens);

    var notObj;

    if (not_type === 4) {
      notObj = {
        type: not_type,
        training_id: trID,
        title: title,
        body: body,
        read: false,
        timestamp: new Date(Date.now())
      };

	  var notObjWithReceivers = {
      type: not_type,
        training_id: trID,
        title: title,
        body: body,
        read: false,
        timestamp: new Date(Date.now()),
        receiverUids: uids
    };

	  db.collection('notifications').doc(notification_id).set(notObjWithReceivers);

    } else if (not_type === 1) {
      notObj = {
        type: not_type,
        training_id: trID,
        read: false,
        timestamp: new Date(Date.now())
      };
    }

    data.set(userDoc.id, {
      type: not_type,
      training_id: trID
    });


    var users_map = new Map();

    users_map.set(notification_id, userDoc.id);


    db_promises.push(
      db.collection('users').doc(userDoc.id).collection('notifications').doc(notification_id).set(notObj));

        const payload = {
          notification: {
            title: title,
            body: body,
            sound: "default"
          },
          data: {
            "click_action": "FLUTTER_NOTIFICATION_CLICK",
            notification_id: notification_id,
            training_id: data.get(userDoc.id).training_id.toString(),
            type: data.get(userDoc.id).type.toString(),
            title: title,
            body: body
          }
        };

        console.log("PARENT DATA " + userDoc.id);
        console.log("TOKENS ARE " + tokens_map.get(userDoc.id).toString());

        var t = tokens_map.get(userDoc.id);
        var flag = true;
        if (t === undefined || t === null || t.length === 0)
          flag = false;

        if (flag) {
          if(!(tokens_map.get(userDoc.id) === undefined || tokens_map.get(userDoc.id) === null || tokens_map.get(userDoc.id) === '')){
            let tokens = tokens_map.get(userDoc.id);
            let doNotPush = false;
            for(let k = 0; k < tokens.length; k++){
              if(tokens[k] === null || tokens[k] === undefined || tokens[k] === ""){
                doNotPush = true;
              }
            }
            if(!doNotPush){
              console.log("Pushed");
              fcm_promises.push(admin.messaging().sendToDevice(tokens_map.get(userDoc.id), payload));
            } else {
              console.log("Null FCM was found! ");
            }

          }

        }



  }

  await Promise.all(db_promises);

  await Promise.all(fcm_promises);

  console.log("broadcast finished");

  return {
    code: 0,
    msg: 'OK'
  }

}

function chunk (arr, len) {

  var chunks = [],
      i = 0,
      n = arr.length;

  while (i < n) {
    chunks.push(arr.slice(i, i += len));
  }

  return chunks;
}

// Optionally, you can do the following to avoid cluttering the global namespace:
Array.chunk = chunk;



async function notifyByUids(uids, title, body, trID, not_type, all) {

    //вытаскиваем юзеров параллельно
    var promises = [];
    var responses;
    if(!all) {
      for (let i = 0; i < uids.length; i++) {
        promises.push(db.collection('users').where('uid', '==', uids[i]).get());
      }
      responses = await Promise.all(promises);
    } else {
      responses = await db.collection('users').get();
    }


    promises = [];

    var tokens_map = new Map();

    var db_promises = [];

    let stubStamp = (new Date(Date.now()));
    let notification_id = stubStamp.valueOf().toString();


    let allFcms = [];

    const payload = {
      notification: {
        title: title,
        body: body,
        sound: "default"
      },
      data: {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        notification_id: notification_id,
        training_id: trID.toString(),
        type: not_type.toString(),
        title: title,
        body: body
      }
    };

    console.log("Payload is " + JSON.stringify(payload));


    var len;
    if(!all){
      len = responses.length;
    } else {
      len = responses.docs.length;
    }
    var userDoc;
    for (i = 0; i < len; i++) {
      if(!all){
        userDoc = responses[i].docs[0];
      } else {
        userDoc = responses.docs[i];
      }


      var fcm_tokens = userDoc.get('fcm');
      tokens_map.set(userDoc.id, fcm_tokens);

      var notObj;

      if (not_type === 4) {
        notObj = {
          type: not_type,
          training_id: trID,
          title: title,
          body: body,
          read: false,
          timestamp: new Date(Date.now())
        };

      var notObjWithReceivers = {
        type: not_type,
          training_id: trID,
          title: title,
          body: body,
          read: false,
          timestamp: new Date(Date.now()),
          receiverUids: uids
      };

      db.collection('notifications').doc(notification_id).set(notObjWithReceivers);

      } else if (not_type === 1) {
        notObj = {
          type: not_type,
          training_id: trID,
          read: false,
          timestamp: new Date(Date.now())
        };
      }

      var users_map = new Map();
      users_map.set(notification_id, userDoc.id);
      db_promises.push(db.collection('users').doc(userDoc.id).collection('notifications').doc(notification_id).set(notObj));

          var t = tokens_map.get(userDoc.id);
          var flag = true;
          if (t === undefined || t === null || t.length === 0)
            flag = false;
          if (flag) {
            if(!(tokens_map.get(userDoc.id) === undefined || tokens_map.get(userDoc.id) === null || tokens_map.get(userDoc.id) === '')){
              let tokens = tokens_map.get(userDoc.id);
              let filteredTokens = [];
              for(let k = 0; k < tokens.length; k++){
                if(!(tokens[k] === null || tokens[k] === undefined || tokens[k] === "")){
                  filteredTokens.push(tokens[k]);
                } else {
                  console.log("Null FCM was found with user_id: " + userDoc.id);
                }
              }
              allFcms = allFcms.concat(filteredTokens);
            }
          }
    }


    console.log("ALL FCMs: " + allFcms.toString());
    let fcm_promises = [];

    //поменял местами
    await Promise.all(db_promises);

    if(allFcms.length > 0){
      let forPush = chunk(allFcms, 1000);
      for(let m = 0; m < forPush.length; m++){
        fcm_promises.push(admin.messaging().sendToDevice(forPush[m], payload));
      }
      await Promise.all(fcm_promises);
    }




    console.log("broadcast finished");

    return {
      code: 0,
      msg: 'OK'
    }


}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e30; i++) {
    if ((new Date().getTime() - start) > milliseconds) {
      break;
    }
  }
}


exports.broadcastPush = functions.runWith({ timeoutSeconds: 300 }).https.onCall(async (data, context) => {
  var body = data.body; //'HELLO';
  var title = data.title;//'TEST_TITLE';
  var uids = data.uids;//['8oOdXlTZoGcap0fy69r4jNM6bbI3', 'rwXOYVERnWbJ4SFDodcQqx59IW52'];

  await notifyByUidsTemp(uids, title, body, 'undef', 4);

  return {
    code: 0,
    msg: 'Success'
  }
  //res.status(200).send("ok_sent");




});



exports.checkForDebtors = functions.https.onRequest(async (req, res) => {
  var list = await db.collection('users').where('isDebtor', '==', true).get();



  dP = [];

  for(let n = 0; n < list.docs.length; n++){
    dP.push(db.collection('users').doc(list.docs[n].id).update(
      {
        isDebtor: false
      }
    ));
  }

  await Promise.all(dP);

  return 0;
});


exports.checkForPush = functions.https.onRequest(async (req, res) => {

  console.log("CHECK FOR PUSH TRIGGERED");

  var trainingsRef = db.collection('trainings');
  let start = new Date(Date.now());
  var end = new Date();
  end.setDate(start.getDate());
  end.addHours(5);

  var endForAuto = new Date();
  endForAuto.setDate(start.getDate())
  endForAuto.addMiuntes(10);

  var endForDebt = new Date();
  endForDebt.setDate(start.getDate());
  endForDebt.removeMinutes(420);

  var startForDebt = new Date();
  startForDebt.setDate(start.getDate());
  startForDebt.removeMinutes(360);


  var hours = start.getHours();
  var minutes = start.getMinutes();
  var TRIGGER_HOUR = 13; //время по UTC
  var TRIGGER_MINUTE_START = 0;
  var TRIGGER_MINUTE_END = 15;
  console.log("CHECK FOR PUSH HOURS IS " + hours);

  if (hours === TRIGGER_HOUR && minutes >= TRIGGER_MINUTE_START && minutes <= TRIGGER_MINUTE_END) {
    var nextDayStart = new Date(Date.now());
    nextDayStart.setDate(nextDayStart.getDate() + 1);
    nextDayStart.setHours(4); //время по UTC
    nextDayStart.setMinutes(0);
    nextDayStart.setSeconds(0);
    nextDayStart.setMilliseconds(0);

    console.log("NEXT DAY START HOUR IS " + nextDayStart.getHours());

    var nextDayEnd = new Date(Date.now());
    nextDayEnd.setDate(nextDayEnd.getDate() + 1);
    nextDayEnd.setHours(9); //время по UTC
    nextDayEnd.setMinutes(0);
    nextDayEnd.setSeconds(0);
    nextDayEnd.setMilliseconds(0);

    console.log("NEXT DAY END HOUR IS " + nextDayEnd.getHours());

    var nextDayTrDocs = await trainingsRef
      .where("date", ">=", nextDayStart)
      .where("date", "<=", nextDayEnd)
      .get();

    console.log("FOUND DAYS LENGTH: " + nextDayTrDocs.docs.length);

    for (var i = 0; i < nextDayTrDocs.docs.length; i++) {
      var curDoc = nextDayTrDocs.docs[i];
      var curUids = curDoc.get('uids');

      //var temp = curDoc.get('notified');

      if (curDoc.get('notified') === false || curDoc.get('notified') === undefined) {
        if (curUids.length > curDoc.get('limit')) {
          var limit = curDoc.get('limit');
          //здесь type - 5
          notifyByUids(curUids.slice(0, limit), "Напоминание", "Подтвердите свое посещение!", nextDayTrDocs.docs[i].id, 1, false);
          db.collection('trainings').doc(curDoc.id).update({
            notified: true
          });

        } else {
          notifyByUids(curUids.slice(0, curUids.length), "Напоминание", "Подтвердите свое посещение!", nextDayTrDocs.docs[i].id, 1, false);
          db.collection('trainings').doc(curDoc.id).update({
            notified: true
          });
        }
      }
    }
  }


  var trDocs = await trainingsRef
    .where("date", ">=", start)
    .where("date", "<=", end)
    .get();

  for (i = 0; i < trDocs.docs.length; i++) {

    curDoc = trDocs.docs[i];
    curUids = curDoc.get('uids');

    if (curDoc.get('notified') === false || curDoc.get('notified') === undefined) {
      if (curUids.length > curDoc.get('limit')) {
        limit = curDoc.get('limit');
        notifyByUids(curUids.slice(0, limit), "Напоминание", "Подтвердите свое посещение!", trDocs.docs[i].id, 1, false);
        db.collection('trainings').doc(curDoc.id).update({
          notified: true
        });



      } else {
        notifyByUids(curUids.slice(0, curUids.length), "Напоминание", "Подтвердите свое посещение!", trDocs.docs[i].id, 1, false);
        db.collection('trainings').doc(curDoc.id).update({
          notified: true
        });

      }
    }
  }



  var trDocsAuto = await trainingsRef
    .where("date", ">=", start)
    .where("date", "<=", endForAuto)
    .get();

  for (i = 0; i < trDocsAuto.docs.length; i++) {
    curDoc = trDocsAuto.docs[i];

    if (curDoc.get('auto') !== undefined
      && curDoc.get('auto') !== null
      && curDoc.get('auto') === true
      && curDoc.get('prolonged') !== true) {


      var pTimestamp = curDoc.get('date');
      var pDate = new Date(pTimestamp['seconds'] * 1000);
      pDate.setDate(pDate.getDate() + 7);

      var duration = curDoc.get('duration');
      if (duration === undefined || duration === null)
        duration = 0;

      var participantsMinimum = curDoc.get('participantsMinimum');
      if(participantsMinimum === undefined || participantsMinimum === null)
        participantsMinimum = 0;

      var training = {
        address: curDoc.get('address'),
        date: pDate,
        levels: curDoc.get('levels'),
        limit: curDoc.get('limit'),
        price: curDoc.get('price'),
        reserve: curDoc.get('reserve'),
        title: curDoc.get('title'),
        trainer_id: curDoc.get('trainer_id'),
        type: curDoc.get('type'),
        uids: [],
        auto: curDoc.get('auto'),
        duration: duration,
        comment: curDoc.get('comment'),
        participantsMinimum: participantsMinimum
      };

      db.collection('trainings').add(training);
      db.collection('trainings').doc(curDoc.id).update({
        prolonged: true
      });
    }
  }

  var debtorPromises = [];
  var trDocsDebt = await trainingsRef
  .where("date", "<=", startForDebt)
  .where("date", ">=", endForDebt)
  .get();

  for(i = 0; i < trDocsDebt.docs.length; i++){
    curDoc = trDocsDebt.docs[i];
    curUids = curDoc.get('uids');

    let lim = curDoc.get('limit');
    curUids = curUids.slice(0, lim);

    var checkedUids = curDoc.get('checked');
    if(checkedUids === undefined)
      checkedUids = [];

    if (curDoc.get('debt') === false || curDoc.get('debt') === undefined) {
      console.log("Training for debt check " + curDoc.id);
      //var trUids = curDoc.get('uids');
      //находим неплательщиков
      for(let k = 0; k < checkedUids.length; k++){
        var value = checkedUids[k];
        curUids = curUids.filter(function(item) {
          return item !== value
        })
      }

      for(let k = 0; k < curUids.length; k++){
        console.log("User is debtor " + curUids[k]);
        debtorPromises.push(db.collection('users').doc(curUids[k]).update(
          {
            isDebtor: true
          }
        ));
      }



      db.collection('trainings').doc(curDoc.id).update({
        debt: true
      });
    }

  }

  await Promise.all(debtorPromises);



  res.status(200).send("ok");

});

Date.prototype.mmdd = function () {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [(mm > 9 ? '' : '0') + mm,
  (dd > 9 ? '' : '0') + dd
  ].join('');
};

exports.cancelTraining = functions.https.onCall(async (data, context) => {

  var trID = data.training_id;
  var title = data.title;
  if (title === undefined || title === null)
    title = "";

  var training = await getTraining(trID);

  var timestamp = training.date;
  var trDate = new Date(timestamp['seconds'] * 1000);

  var dateString = ("0" + trDate.getDate()).slice(-2) + "." + ("0" + (trDate.getMonth() + 1)).slice(-2);
  var dateTimeString = ("0" + trDate.getHours()).slice(-2) + ":" + ("0" + trDate.getMinutes()).slice(-2);

  var msgTitle = "ВНИМАНИЕ! ОТМЕНА ЗАНЯТИЯ" + title;
  var msgBody = training.title.val + " " + dateString + " в " + dateTimeString
    + " в зале по адресу " + training.address.val + " ОТМЕНЯЕТСЯ! Приносим свои извинения!"

  var updatedFields = {
    cancelled: true,
    uids: []
  };

  await db.collection('trainings').doc(trID).update(updatedFields).then((docRef) => {
    return ({
      id: docRef.id,
      code: 0,
      message: 'successfully cancelled training'
    });
  }).catch(() => {
    return ({
      code: -1,
      message: 'error'
    });
  });

  await notifyByUids(training.uids, msgTitle, msgBody, 'undef', 4, false);

  return {
    code: 0,
    msg: "Training was cancelled",
	trDate: training.date
  }

});

exports.signOffFromTraining = functions.https.onCall(async (data, context) => {
  var trID = data.training_id;//"KsfIsKxbqUxzCcihwEcm"; //'0ik6ij7ViANGv2V2ZAne';
  var trDocRef = db.collection('trainings').doc(trID);
  var currentID = context.auth.uid;//"xAHKHsQHARZ6sZy1DzGyWkIlwRu1";//'rwXOYVERnWbJ4SFDodcQqx59IW52';

  let start = new Date(Date.now());



  var trDoc = (await trDocRef.get());

  var timestamp = trDoc.get('date');
  var trDate = new Date(timestamp['seconds'] * 1000);

  var allUids = trDoc.get('uids');

  var userPositionInTraining = allUids.indexOf(currentID);
  var trainingLimit = trDoc.get('limit');
  var isUserInLimit = userPositionInTraining < trainingLimit ? true : false;

  if (trDate < start) {
    var tr_exmp = await getTraining(trID);
    return {
      code: -2,
      training: tr_exmp
    }
  } else if ((trDate.getTime() - start.getTime()) / 60000 < 120 && isUserInLimit) {
    tr_exmp = await getTraining(trID);
    return {
      code: -3,
      training: tr_exmp
    }
  }

  var code;

  if (trDoc.get('uids').includes(currentID)) {



    await trDocRef.update({
      uids: admin.firestore.FieldValue.arrayRemove(currentID)
    });

    var uids = (await db.collection('trainings').doc(trID).get()).get('uids');
    var toNotify = [];
    //var trToUpdate = (await trDocRef.get());
    var oldUids = trDoc.get('uids');

    for (var i = 0; i < uids.length; i++) {
      if (oldUids.indexOf(uids[i], 0) > (trDoc.get('limit') - 1) && i <= (trDoc.get('limit') - 1)) {
        toNotify.push(uids[i]);
      }
    }

    notifyByUids(toNotify, "Приходите!", "Вы в лимите!", trID, 1, false);


    var currentUserDocID = (await db.collection('users').where("uid", "==", currentID).get()).docs[0].id;

    db.collection('users').doc(currentUserDocID).collection('notifications').add({
      type: 3,
      training_id: trID,
      read: false,
      timestamp: new Date(Date.now())
    });
    code = 0;
  } else {
    code = -1;
  }

  tr_exmp = await getTraining(trID);

  return {
    code: code,
    training: tr_exmp
	
  }





});


exports.signForTraining = functions.https.onCall(async (data, context) => {

  var trID = data.training_id; //'KcCCAbIgHBFJW9YoedA2';
  
  var usersRef = db.collection('users');
  var snapshot = await usersRef
  .where("uid", "==", context.auth.uid)
  .get();


  var usr = getUserObject(snapshot.docs[0]);

  


  //console.log(tr_exmp.toString);

  //db.collection('trainings')

  var trRef = db.collection('trainings').doc(trID);

  var tr = await getTraining(trID); //await trRef.get();
  
  

  let levelsArr = [];
  for(let k = 0; k < tr.training_levels.length; k++){
    if(tr.training_levels[k].id === 'U6zaxhj6IzaExZqcMkQ3')
      levelsArr.push('undef');
    levelsArr.push(tr.training_levels[k].id);
  }

  console.log("TRAINING LEVELS: " + levelsArr);
  console.log("USER LEVEL: " + usr.level);
  console.log("USER ADMIN: " + usr.admin);
  console.log("USER MAIN_ADMIN: " + usr.main_admin);
  console.log("LEVELS ARR INCLUDE  " + levelsArr.includes(usr.level));


  if(!levelsArr.includes(usr.level) && usr.admin === false && (usr.main_admin === false || usr.main_admin === undefined)){
    console.log("USERS LEVEL IS NOT ENOUGH ");
    return { 
      training: tr,
      message: 'Your level is not enough for this training',
      code : -7
    };
  }
    

  console.log('TRAINING ID IS: ' + trID);
  var currentID = context.auth.uid;//context.auth.uid; //'Cre0UTGyBwWovCoHQ0rreGHskr12';//'ID_FOR_TESTING_PURPOSE';//'ID_TEST';//context.auth.id;//'ID_FOR_TESTING_PURPOSE';
  

  var pTimestamp = tr.date; //tr.get('date');

  
  var pDate = new Date(pTimestamp['seconds'] * 1000);
  //pDate.setDate(pDate.getDate() + 7);
  //TODO ИЗМЕНИТЬ ЗАПРОС

  //let curentTime = pDate;
  let start = new Date();
  start.setDate(pDate.getDate());
  start.setHours(pDate.getHours() - 1);
  start.setMinutes(pDate.getMinutes());
  start.setSeconds(pDate.getSeconds());
  start.setMonth(pDate.getMonth());
  start.setFullYear(pDate.getFullYear());
  start.setMilliseconds(pDate.getMilliseconds());

  var end = new Date();
  end.setDate(pDate.getDate());
  end.setHours(pDate.getHours() + 1);
  end.setMinutes(pDate.getMinutes());
  end.setSeconds(pDate.getSeconds());
  end.setMonth(pDate.getMonth());
  end.setFullYear(pDate.getFullYear());
  end.setMilliseconds(pDate.getMilliseconds());

  console.log(pDate.toISOString());
  console.log(start.toISOString());
  console.log(end.toISOString());

  var trForSearch = await db.collection('trainings')
    .where("date", ">=", start)
    .where("date", "<=", end)
    .get();


  //var trForSearch = await db.collection('trainings').where("date", "==", pDate).get();
  var isAlreadySigned = false;
  var cause;
  
  
  for (var k = 0; k < trForSearch.docs.length; k++) {
    if (trForSearch.docs[k].get('uids').includes(currentID)) {
      isAlreadySigned = true;
	    cause = trForSearch.docs[k].id;
      break;
    }
  }

  
  if(isAlreadySigned){
    console.log("ALREADY SIGNED " + currentID + " TRAINING " + trID);
    
    for(k = 0; k < trForSearch.docs.length; k++){
      console.log("training " + k + " " + trForSearch.docs[k].id);
    }

    var tr_exmp1 = await getTraining(trID);
    var conflictingTraining = await getTraining(cause)

    console.log("TRFORSEARCH " + conflictingTraining.training_id);
    console.log("START TIME " + start.getMilliseconds());
    console.log("END TIME " + end.getMilliseconds());
    //ret.training = tr_exmp;
    return {
      training: tr_exmp1,
      message: 'Already signed for training with the same time.',
      code : -4,
	    cause: conflictingTraining
    };
  }


  var ret = await db.runTransaction(function (transaction) {
    return transaction.get(trRef).then(function (trDoc) {


      if (!trDoc.exists) {
        return {
          message: 'There is no training with ID: ' + trID,
          code: -1
        };
      }

      var parts = trDoc.get('uids');

      if (parts !== undefined) {
        if (parts.includes(currentID)) {
          return {
            message: 'Already signed for training.',
            code: -2
          }
        }

        if (parts.length >= (trDoc.get('limit') + trDoc.get('reserve'))) {
          return {
            message: 'No more place',
            code: -3
          }
        }

        var cancelled = trDoc.get('cancelled');
        if (cancelled !== undefined && cancelled !== null) {
          if (cancelled === true) {
            return {
              message: 'Тренировка отменена',
              code: -6
            }
          }
        }
      }

      //TODO HERE
      var timestamp = trDoc.get('date');
      var trDate = new Date(timestamp['seconds'] * 1000);
      let start = new Date(Date.now());
      trDate.addMiuntes(30);
      
      console.log("START TIME IS " + start.getTime());
      console.log("TR TIME IS " + trDate.getTime());
      
      if (start.getTime() > trDate.getTime()) {
        console.log("TOO LATE");
        return {
          message: 'Too late to sign for training',
          code: -5
        }
      }

      //var currentUserDocID = (await db.collection('users').where("uid", "==", currentID).get()).docs[0].id;

      if (trDoc.get('uids').length >= trDoc.get('limit')) {
        db.collection('users').doc(currentID).collection('notifications').add({
          type: 2,
          training_id: trID,
          read: false,
          timestamp: new Date(Date.now())
        });
      } else {
        db.collection('users').doc(currentID).collection('notifications').add({
          type: 0,
          training_id: trID,
          read: false,
          timestamp: new Date(Date.now())
        });
      }

      return transaction.update(trRef, { uids: admin.firestore.FieldValue.arrayUnion(currentID) });


    }).then(async function (msg) {
      if (msg.message !== undefined) {
        return msg;
      }
      console.log("Transaction successfully committed!");
      
      return {
        message: 'Transaction successfully committed!',
        code: 0
      }

    }).catch(function (error) {
      console.log("Transaction failed: ", error);
    })
  });


  var tr_exmp = await getTraining(trID);

  ret.training = tr_exmp;

  return ret;

});


exports.getUserData = functions.https.onCall(async (data, context) => {


  var usersRef = db.collection('users');
  var snapshot = await usersRef
    .where("uid", "==", context.auth.uid)
    .get();
  if (snapshot.docs.length > 0) {

    var usr = getUserObject(snapshot.docs[0]);
    return usr;
  } else {
    return { error: "No such user" }
  }

});

function parseField(document, fieldName, defaultValue){
  return (document.get(fieldName) === undefined) ? defaultValue : document.get(fieldName);
}

function getRawTrainingObject(doc) {

  let address_id = parseField(doc, 'address', "");
  let auto = parseField(doc, 'auto', false);
  let checked = parseField(doc, 'checked', []);
  let comment = parseField(doc, 'comment', "");
  let date = parseField(doc, 'date', new Date(Date.now()));
  let duration = parseField(doc, 'duration', 0);
  let levels_ids = parseField(doc, 'levels', []);
  let limit = parseField(doc, 'limit', 0);
  let notified = parseField(doc, 'notified', false);
  let price = parseField(doc, 'price', 0);
  let prolonged = parseField(doc, 'prolonged', false);
  let reserve = parseField(doc, 'reserve', 99999);
  let title_id = parseField(doc, 'title', '');
  let trainers_ids = parseField(doc, 'trainer_id', []);
  let type_id = parseField(doc, 'type', '');
  let uids = parseField(doc, 'uids', []);
  let cancelled = parseField(doc, 'cancelled', false);
  let participantsMinimum = parseField(doc, 'participantsMinimum', 0);
  let debt = parseField(doc, 'debt', false);

  return {
    address_id: address_id,
    levels_ids: levels_ids,
    date: date,
    limit: limit,
    reserve: reserve,
    trainers_ids: trainers_ids,
    type_id: type_id,
    title_id: title_id,
    uids: uids,
    price: price,
    training_id: doc.id,
    auto: auto,
    checked: checked,
    duration: duration,
    comment: comment,
    notified: notified,
    prolonged: prolonged,
    cancelled: cancelled,
    participantsMinimum: participantsMinimum,
    debt: debt

  };
}

async function getTraining(id) {
  //exports.test = functions.https.onRequest(async (req, res) => {
  //id = 'hcSbUtn7w4Om9D7gcEM0';

  console.log("getTraining id is " + id);
  var trainingDoc = await db.collection('trainings').doc(id).get();

  //вытаскиваем тренеров параллельно
  var promises = [];
  let supervisorsPromises = [];
  
  
  if(trainingDoc === undefined || trainingDoc === null)
    return {
      code: -1,
      msg: "NO SUCH TRAINING"
    }

    if(trainingDoc.get('supervisors') !== undefined){
      for (var i = 0; i < trainingDoc.get('supervisors').length; i++) {
        supervisorsPromises.push(db.collection('users').where('uid', '==', trainingDoc.get('supervisors')[i]).get())
      }
    }
    

  for (i = 0; i < trainingDoc.get('trainer_id').length; i++) {
    promises.push(db.collection('users').where('uid', '==', trainingDoc.get('trainer_id')[i]).get());
  }

  var trainersResp = await Promise.all(promises);
  var supervisorsResp = await Promise.all(supervisorsPromises);

  var adrDoc = (await db.collection('values').doc('values').get());

  var adrQuery = db.collection('addreses').orderBy('timestamp', 'desc');
  var levelsQuery = db.collection('levels').orderBy('timestamp', 'desc');
  var titleQuery = db.collection('titles').orderBy('timestamp', 'desc');
  var typeQuery = db.collection('types').orderBy('timestamp', 'desc');

  promises = [];
 
  promises.push(adrQuery.get());
  promises.push(levelsQuery.get());
  promises.push(titleQuery.get());
  promises.push(typeQuery.get());

  var adrDocs;
  var levelsDoc;
  var titleDoc;
  var typeDoc;


  var documents = await Promise.all(promises);

  for (i = 0; i < documents.length; i++) {
    if (documents[i].query.isEqual(adrQuery))
      adrDocs = documents[i];
    if (documents[i].query.isEqual(levelsQuery))
      levelsDoc = documents[i];
    if (documents[i].query.isEqual(titleQuery))
      titleDoc = documents[i];
    if (documents[i].query.isEqual(typeQuery))
      typeDoc = documents[i];
  }

  var reductionalFunction = function (map, obj) {
    map[obj.id] = obj.get('val');
    return map;
  };

  var adrMap = adrDocs.docs.reduce(reductionalFunction, {});
  var levelMap = levelsDoc.docs.reduce(reductionalFunction, {});
  var titleMap = titleDoc.docs.reduce(reductionalFunction, {});
  var typeMap = typeDoc.docs.reduce(reductionalFunction, {});



  var actualAddress = adrMap[trainingDoc.get('address')];
  var actualLevel = "";

  var levelsArr = [];

  for (j = 0; j < trainingDoc.get('levels').length; j++) {

    levelsArr.push({
      id: trainingDoc.get('levels')[j],
      val: levelMap[trainingDoc.get('levels')[j]]
    });

    actualLevel += levelMap[trainingDoc.get('levels')[j]];
    if (j !== trainingDoc.get('levels').length - 1) {
      actualLevel += "/";
    }
  }


  //данные тренера
  var trMap = new Map();
  for (i = 0; i < trainersResp.length; i++) {
    if (trainersResp[i].docs[0] !== undefined) {
      var tra;
      tra = getUserObject(trainersResp[i].docs[0]);
      trMap.set(trainersResp[i].docs[0].get('uid'), tra);
    }

  }

  var train = [];

  for (i = 0; i < trainingDoc.get('trainer_id').length; i++) {
    if (trMap.get(trainingDoc.get('trainer_id')[i]) !== undefined)
      train.push(trMap.get(trainingDoc.get('trainer_id')[i]));
  }

  var supervisorsReturn = [];

  for(i = 0; i < supervisorsResp.length; i++){
    supervisorsReturn.push(getUserObject(supervisorsResp[i].docs[0]));
  }

  //вытаскиваем записавшихся параллельно
  promises = [];

  if (trainingDoc.get('uids') !== undefined) {
    for (let j = 0; j < trainingDoc.get('uids').length; j++) {
      promises.push(db.collection('users').where('uid', '==', trainingDoc.get('uids')[j]).get());
    }
  }


  responses = await Promise.all(promises);
  var users = new Map();


  for (let i = 0; i < responses.length; i++) {
    var dbUser = responses[i].docs[0];
    if (dbUser !== undefined) {
      var usr = getUserObject(dbUser);
      users.set(dbUser.get('uid'), usr);
    }
  }

  //в итоге имеем массив с записавшимися - users
  var usersArray = [];

  if (trainingDoc.get('uids') !== undefined) {
    for (var j = 0; j < trainingDoc.get('uids').length; j++) {
      usersArray.push(users.get(trainingDoc.get('uids')[j]));
    }
  }

  var urlPromises = [];

  for(let i = 0; i < usersArray.length; i++){
    let usr = usersArray[i];
    let file = admin.storage().bucket().file(usr.docID);
      //usr.imgUrl = file;
      let year = (new Date(Date.now())).getFullYear();
      year += 5;
      let exp = '01-01-' + year;
      urlPromises.push(file.getSignedUrl({ action: 'read', expires: exp}));
  }

  for(let i = 0; i < supervisorsReturn.length; i++){
    let usr = supervisorsReturn[i];
    let file = admin.storage().bucket().file(usr.docID);
      //usr.imgUrl = file;
      let year = (new Date(Date.now())).getFullYear();
      year += 5;
      let exp = '01-01-' + year;
      urlPromises.push(file.getSignedUrl({ action: 'read', expires: exp}));
  }

  var urlResults = await Promise.all(urlPromises);
  for(i = 0; i < usersArray.length; i++){
    usersArray[i].imgUrl = urlResults[i][0];
  }

  for(i = 0; i < supervisorsReturn.length; i++){
    supervisorsReturn[i].imgUrl = urlResults[i + usersArray.length][0];
  }



  var titleObj = {
    id: trainingDoc.get('title'),
    val: titleMap[trainingDoc.get('title')]
  }

  var typeObj = {
    id: trainingDoc.get('type'),
    val: typeMap[trainingDoc.get('type')]
  }

  var addressObj = {
    id: trainingDoc.get('address'),
    val: actualAddress
  }

  var checked;
  if (trainingDoc.get('checked') === undefined || trainingDoc.get('checked') === null) {
    checked = [];
  } else {
    checked = trainingDoc.get('checked');
  }

  //новый код
  var duration = trainingDoc.get('duration');
  if (duration === undefined || duration === null) {
    duration = 0;
  }

  var comment = trainingDoc.get('comment');
  if (comment === undefined || comment === null) {
    comment = "";
  }

  var cancelled = trainingDoc.get('cancelled');
  if (cancelled === undefined || cancelled === null)
    cancelled = false;


  var participantsMinimum = trainingDoc.get('participantsMinimum');
  if (participantsMinimum === undefined || participantsMinimum === null)
    participantsMinimum = 0;

  return Promise.resolve({
    address: addressObj,
    levels: actualLevel,
    date: trainingDoc.get('date'),
    limit: trainingDoc.get('limit'),
    reserve: trainingDoc.get('reserve'),
    trainer_id: trainingDoc.get('trainer_id'),
    trainer: train,
    type: typeObj,
    title: titleObj,
    users: usersArray,
    uids: trainingDoc.get('uids'),
    price: trainingDoc.get('price'),
    training_id: trainingDoc.id,
    training_levels: levelsArr,
    auto: trainingDoc.get('auto'),
    checked: checked,
    duration: duration,
    comment: comment,
    cancelled: cancelled,
    participantsMinimum: participantsMinimum,
    supervisors: supervisorsReturn

  });


}


exports.getTrainingUsersV2 = functions.https.onCall(async (data, context) => {
  if(data.id === undefined){
    return {
      code: -1,
      msg: "No doc id"
    }
  }
  let tr = getRawTrainingObject(await db.collection('trainings').doc(data.id).get());
  return await getUsersV2(tr.uids);

});


exports.getTrainingUsers = functions.https.onCall(async (data, context) => {
  if(data.id === undefined){
    return {
      code: -1,
      msg: "No doc id"
    }
  }
  let tr = getRawTrainingObject(await db.collection('trainings').doc(data.id).get());
  return await getUsers(tr.uids);

});

async function getUsersV2(uids) {
  //let uids = data.uids;
  //let uids = ['uZnUcbNmmyTJ7CEUpC81Uxg355F3', 'oWqVeCdXPmece7G8FtSjmDA0Rc73', 'TB25mzGrYobzWJNyRk65U4P34gM2', '03Ebi5i2LiXqfAP3ff4C7Eo6uqk1'];
  
  if(uids === undefined)
    uids = [];

  let docReferences = [];
  
  Array.from(uids).forEach(val => {
    docReferences.push(db.collection('users').doc(val));
  });

  if(docReferences.length === 0)
    return [];

  var result = await db.getAll(...docReferences);
  var ret = [];

  var urlPromises = [];

    for(var i = 0; i < result.length; i++){
      var usr = getUserObject(result[i]);
      let file = admin.storage().bucket().file(usr.docID);

      
      let year = (new Date(Date.now())).getFullYear();
      year += 5;
      let exp = '01-01-' + year;
      //console.log("expires at: " + exp);

      urlPromises.push(file.getSignedUrl({ action: 'read', expires: exp}));
      ret.push(usr);
    }

    var urlResult = await Promise.all(urlPromises);
    for(i = 0; i < urlResult.length; i++){
      ret[i].imgUrl = urlResult[i][0];
    }

    return ret;
  
  
}


async function getUsers(uids) {
  if(uids === undefined)
    uids = [];

  let docReferences = [];
  
  Array.from(uids).forEach(val => {
    docReferences.push(db.collection('users').doc(val));
  });

  if(docReferences.length === 0)
    return [];

  return await db.getAll(...docReferences).then(docs => {
    let result = [];
    Array.from(docs).forEach(val => {
      console.log("Next user is " + JSON.stringify(val.data()));
      result.push(val.data());
    })
    return result;
  });
}

exports.getUsers = functions.https.onCall(async (data, context) => {
  let uids = [];
  if(data.uids !== undefined)
    uids = data.uids;
  return await getUsers(uids);

});

exports.getTrainings = functions.https.onCall(async (data, context) => {

  
  var trainingsRef = db.collection('trainings');
  let start = new Date(Date.now());

  if (start.getHours() < 19)
    start.setHours(0);

  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);

  var end = new Date();
  end.setDate(start.getDate() + 7);

  end.getMilliseconds
  var snapshot = await trainingsRef
    .where("date", ">=", start)
    .get();

  var filteredDocs = [];

  for (var l = 0; l < snapshot.docs.length; l++) {
    if (!(snapshot.docs[l].get('date').getMilliseconds > end.getMilliseconds))
      filteredDocs.push(snapshot.docs[l]);
  }

  snapshot.docs = filteredDocs;

  var response = {
    trainings: []
  };

  //параллельная загрузка списков данных (адреса, типы, виды и тд...)
  var adrQuery = db.collection('addreses').orderBy('timestamp', 'desc');
  var levelsQuery = db.collection('levels').orderBy('timestamp', 'desc');
  var titleQuery = db.collection('titles').orderBy('timestamp', 'desc');
  var typeQuery = db.collection('types').orderBy('timestamp', 'desc');

  var promises = [];

  promises.push(adrQuery.get());
  promises.push(levelsQuery.get());
  promises.push(titleQuery.get());
  promises.push(typeQuery.get());

  var adrDocs;
  var levelsDoc;
  var titleDoc;
  var typeDoc;


  var documents = await Promise.all(promises);

  for (var i = 0; i < documents.length; i++) {
    if (documents[i].query.isEqual(adrQuery))
      adrDocs = documents[i];
    if (documents[i].query.isEqual(levelsQuery))
      levelsDoc = documents[i];
    if (documents[i].query.isEqual(titleQuery))
      titleDoc = documents[i];
    if (documents[i].query.isEqual(typeQuery))
      typeDoc = documents[i];
  }

  titleDoc.query.isEqual;
  var reductionalFunction = function (map, obj) {
    map[obj.id] = obj.get('val');
    return map;
  };

  var adrMap = adrDocs.docs.reduce(reductionalFunction, {});
  var levelMap = levelsDoc.docs.reduce(reductionalFunction, {});
  var titleMap = titleDoc.docs.reduce(reductionalFunction, {});
  var typeMap = typeDoc.docs.reduce(reductionalFunction, {});


  //вытаскиваем тренеров параллельно
  promises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('trainer_id') !== undefined) {
      for (var k = 0; k < snapshot.docs[i].get('trainer_id').length; k++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('trainer_id')[k]).get());
      }

    }
  }

  var responses = await Promise.all(promises);
  var trainers = new Map();

  for (let i = 0; i < responses.length; i++) {
    var dbTrainer = responses[i].docs[0];
    if (dbTrainer !== undefined) {
      var tra = getUserObject(dbTrainer);
      trainers.set(dbTrainer.get('uid'), tra);
    }

  }

  //в итоге имеем массив с тренерами trainers

  //вытаскиваем записавшихся параллельно
  promises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('uids') !== undefined) {
      for (let j = 0; j < snapshot.docs[i].get('uids').length; j++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('uids')[j]).get());
      }
    }
  }

  responses = await Promise.all(promises);
  var users = new Map();


  for (let i = 0; i < responses.length; i++) {
    var dbUser = responses[i].docs[0];
    if (dbUser !== undefined) {
      var usr = getUserObject(dbUser);
      users.set(dbUser.get('uid'), usr);
    }
  }

  //в итоге имеем массив с записавшимися - users


  for (i = 0; i < snapshot.docs.length; i++) {

    var currentTraining = snapshot.docs[i];

    var uids = currentTraining.get('uids');
    var usersArray = [];

    if (uids !== undefined) {
      for (var j = 0; j < uids.length; j++) {
        usersArray.push(users.get(uids[j]));
      }
    }

    var tr = [];

    if (currentTraining.get('trainer_id') !== undefined) {



      for (k = 0; k < currentTraining.get('trainer_id').length; k++) {
        if (trainers.get(currentTraining.get('trainer_id')[k]) !== undefined)
          tr.push(trainers.get(currentTraining.get('trainer_id')[k]));
      }
      //tr = trainers.get(currentTraining.get('trainer_id'));




    }


    var actualAddress = adrMap[currentTraining.get('address')];
    var actualLevel = "";

    var levelsArr = [];

    for (j = 0; j < currentTraining.get('levels').length; j++) {

      levelsArr.push({
        id: currentTraining.get('levels')[j],
        val: levelMap[currentTraining.get('levels')[j]]
      });

      actualLevel += levelMap[currentTraining.get('levels')[j]];
      if (j !== currentTraining.get('levels').length - 1) {
        actualLevel += "/";
      }
    }


    var titleObj = {
      id: currentTraining.get('title'),
      val: titleMap[currentTraining.get('title')]
    }

    var typeObj = {
      id: currentTraining.get('type'),
      val: typeMap[currentTraining.get('type')]
    }

    var addressObj = {
      id: currentTraining.get('address'),
      val: actualAddress
    }

    var checked;
    if (currentTraining.get('checked') === undefined || currentTraining.get('checked') === null) {
      checked = [];
    } else {
      checked = currentTraining.get('checked');
    }

    //новый код
    var duration = currentTraining.get('duration');
    if (duration === undefined || duration === null) {
      duration = 0;
    }

    var comment = currentTraining.get('comment');
    if (comment === undefined || comment === null) {
      comment = "";
    }

    var cancelled = currentTraining.get('cancelled');
    if (cancelled === undefined || cancelled === null)
      cancelled = false;

      var participantsMinimum = currentTraining.get('participantsMinimum');
      if (participantsMinimum === undefined || participantsMinimum === null)
        participantsMinimum = 0;

    response.trainings.push(
      {
        //address : actualAddress,
        address: addressObj,
        levels: actualLevel,
        date: currentTraining.get('date'),
        limit: currentTraining.get('limit'),
        reserve: currentTraining.get('reserve'),
        trainer_id: currentTraining.get('trainer_id'),
        trainer: tr,
        //type : typeMap[currentTraining.get('type')],
        type: typeObj,
        //title : titleMap[currentTraining.get('title')],
        title: titleObj,
        users: usersArray,
        price: currentTraining.get('price'),
        training_id: currentTraining.id,
        //training_levels : currentTraining.get('levels')
        training_levels: levelsArr,
        auto: currentTraining.get('auto'),
        checked: checked,
        duration: duration,
        comment: comment,
        cancelled: cancelled,
        participantsMinimum: participantsMinimum
      }
    );


  }



  return response;



});


exports.getTrainingsV2 = functions.https.onCall(async (data, context) => {


  var trainingsRef = db.collection('trainings');
  let start = new Date(Date.now());

  if (start.getHours() < 19)
    start.setHours(0);

  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);

  var end = new Date();
  end.setDate(start.getDate() + 7);

  end.getMilliseconds
  var snapshot = await trainingsRef
    .where("date", ">=", start)
    .get();

  var filteredDocs = [];

  for (var l = 0; l < snapshot.docs.length; l++) {
    if (!(snapshot.docs[l].get('date').getMilliseconds > end.getMilliseconds))
      filteredDocs.push(snapshot.docs[l]);
  }

  snapshot.docs = filteredDocs;


  return await getTrainingsBySnapshotV2(snapshot);



});


exports.getTrainingsTemp = functions.https.onCall(async (data, context) => {


  var trainingsRef = db.collection('trainings');
  let start = new Date(Date.now());

  

  if (start.getHours() < 19)
    start.setHours(0);

  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);

  var end = new Date();
  end.setDate(start.getDate() + 7);

  end.getMilliseconds
  var snapshot = await trainingsRef
    .where("date", ">=", start)
    .get();

  var filteredDocs = [];

  for (var l = 0; l < snapshot.docs.length; l++) {
    if (!(snapshot.docs[l].get('date').getMilliseconds > end.getMilliseconds))
      filteredDocs.push(snapshot.docs[l]);
  }

  snapshot.docs = filteredDocs;

  var response = {
    trainings: []
  };

  //параллельная загрузка списков данных (адреса, типы, виды и тд...)
  var adrQuery = db.collection('addreses').orderBy('timestamp', 'desc');
  var levelsQuery = db.collection('levels').orderBy('timestamp', 'desc');
  var titleQuery = db.collection('titles').orderBy('timestamp', 'desc');
  var typeQuery = db.collection('types').orderBy('timestamp', 'desc');

  var promises = [];

  promises.push(adrQuery.get());
  promises.push(levelsQuery.get());
  promises.push(titleQuery.get());
  promises.push(typeQuery.get());

  var adrDocs;
  var levelsDoc;
  var titleDoc;
  var typeDoc;


  var documents = await Promise.all(promises);

  for (var i = 0; i < documents.length; i++) {
    if (documents[i].query.isEqual(adrQuery))
      adrDocs = documents[i];
    if (documents[i].query.isEqual(levelsQuery))
      levelsDoc = documents[i];
    if (documents[i].query.isEqual(titleQuery))
      titleDoc = documents[i];
    if (documents[i].query.isEqual(typeQuery))
      typeDoc = documents[i];
  }

  titleDoc.query.isEqual;
  var reductionalFunction = function (map, obj) {
    map[obj.id] = obj.get('val');
    return map;
  };

  var adrMap = adrDocs.docs.reduce(reductionalFunction, {});
  var levelMap = levelsDoc.docs.reduce(reductionalFunction, {});
  var titleMap = titleDoc.docs.reduce(reductionalFunction, {});
  var typeMap = typeDoc.docs.reduce(reductionalFunction, {});


  //вытаскиваем тренеров параллельно
  promises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('trainer_id') !== undefined) {
      for (var k = 0; k < snapshot.docs[i].get('trainer_id').length; k++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('trainer_id')[k]).get());
      }

    }
  }

  var responses = await Promise.all(promises);
  var trainers = new Map();

  for (let i = 0; i < responses.length; i++) {
    var dbTrainer = responses[i].docs[0];
    if (dbTrainer !== undefined) {
      var tra = getUserObject(dbTrainer);
      trainers.set(dbTrainer.get('uid'), tra);
    }

  }

  //в итоге имеем массив с тренерами trainers

  //вытаскиваем записавшихся параллельно
  promises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('uids') !== undefined) {
      for (let j = 0; j < snapshot.docs[i].get('uids').length; j++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('uids')[j]).get());
      }
    }
  }

  responses = await Promise.all(promises);
  var users = new Map();


  for (let i = 0; i < responses.length; i++) {
    var dbUser = responses[i].docs[0];
    if (dbUser !== undefined) {
      var usr = getUserObject(dbUser);
      users.set(dbUser.get('uid'), usr);
    }
  }

  //в итоге имеем массив с записавшимися - users


  for (i = 0; i < snapshot.docs.length; i++) {

    var currentTraining = snapshot.docs[i];

    var uids = currentTraining.get('uids');
    var usersArray = [];

    if (uids !== undefined) {
      for (var j = 0; j < uids.length; j++) {
        usersArray.push(users.get(uids[j]));
      }
    }

    var tr = [];

    if (currentTraining.get('trainer_id') !== undefined) {



      for (k = 0; k < currentTraining.get('trainer_id').length; k++) {
        if (trainers.get(currentTraining.get('trainer_id')[k]) !== undefined)
          tr.push(trainers.get(currentTraining.get('trainer_id')[k]));
      }
      //tr = trainers.get(currentTraining.get('trainer_id'));




    }




    var actualAddress = adrMap[currentTraining.get('address')];
    var actualLevel = "";

    var levelsArr = [];

    for (j = 0; j < currentTraining.get('levels').length; j++) {

      levelsArr.push({
        id: currentTraining.get('levels')[j],
        val: levelMap[currentTraining.get('levels')[j]]
      });

      actualLevel += levelMap[currentTraining.get('levels')[j]];
      if (j !== currentTraining.get('levels').length - 1) {
        actualLevel += "/";
      }
    }


    var titleObj = {
      id: currentTraining.get('title'),
      val: titleMap[currentTraining.get('title')]
    }

    var typeObj = {
      id: currentTraining.get('type'),
      val: typeMap[currentTraining.get('type')]
    }

    var addressObj = {
      id: currentTraining.get('address'),
      val: actualAddress
    }

    var checked;
    if (currentTraining.get('checked') === undefined || currentTraining.get('checked') === null) {
      checked = [];
    } else {
      checked = currentTraining.get('checked');
    }

    //новый код
    var duration = currentTraining.get('duration');
    if (duration === undefined || duration === null) {
      duration = 0;
    }

    var comment = currentTraining.get('comment');
    if (comment === undefined || comment === null) {
      comment = "";
    }

    var cancelled = currentTraining.get('cancelled');
    if (cancelled === undefined || cancelled === null)
      cancelled = false;

      var participantsMinimum = currentTraining.get('participantsMinimum');
      if (participantsMinimum === undefined || participantsMinimum === null)
        participantsMinimum = 0;

    response.trainings.push(
      {
        //address : actualAddress,
        address: addressObj,
        levels: actualLevel,
        date: currentTraining.get('date'),
        limit: currentTraining.get('limit'),
        reserve: currentTraining.get('reserve'),
        trainer_id: currentTraining.get('trainer_id'),
        trainer: tr,
        //type : typeMap[currentTraining.get('type')],
        type: typeObj,
        //title : titleMap[currentTraining.get('title')],
        title: titleObj,
        users: usersArray,
        price: currentTraining.get('price'),
        training_id: currentTraining.id,
        //training_levels : currentTraining.get('levels')
        training_levels: levelsArr,
        auto: currentTraining.get('auto'),
        checked: checked,
        duration: duration,
        comment: comment,
        cancelled: cancelled,
        participantsMinimum: participantsMinimum
      }
    );


  }



  return response;



});

exports.getUpcomingTrainingsForAdmin = functions.https.onCall(async (data, context) => {

  var trainingsRef = db.collection('trainings');

  let start = new Date(Date.now());
  start.setHours(0);
  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);

  var end = new Date();
  end.setDate(start.getDate() + 7);

  var snapshot = await trainingsRef
    .where("date", ">=", start)
    .where("date", "<=", end)
    .get();

  return await getTrainingsBySnapshot(snapshot);



});


exports.getUpcomingTrainingsForAdminV2 = functions.https.onCall(async (data, context) => {

  var trainingsRef = db.collection('trainings');

  let start = new Date(Date.now());
  start.setHours(0);
  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);

  var end = new Date();
  end.setDate(start.getDate() + 7);

  var snapshot = await trainingsRef
    .where("date", ">=", start)
    .where("date", "<=", end)
    .get();

  return await getTrainingsBySnapshotV2(snapshot);


});

async function getTrainingsBySnapshot(snapshot) {
  var response = {
    trainings: []
  };

  //параллельная загрузка списков данных (адреса, типы, виды и тд...)
  var adrQuery = db.collection('addreses').orderBy('timestamp', 'desc');
  var levelsQuery = db.collection('levels').orderBy('timestamp', 'desc');
  var titleQuery = db.collection('titles').orderBy('timestamp', 'desc');
  var typeQuery = db.collection('types').orderBy('timestamp', 'desc');

  var promises = [];

  promises.push(adrQuery.get());
  promises.push(levelsQuery.get());
  promises.push(titleQuery.get());
  promises.push(typeQuery.get());

  var adrDocs;
  var levelsDoc;
  var titleDoc;
  var typeDoc;


  var documents = await Promise.all(promises);

  for (var i = 0; i < documents.length; i++) {
    if (documents[i].query.isEqual(adrQuery))
      adrDocs = documents[i];
    if (documents[i].query.isEqual(levelsQuery))
      levelsDoc = documents[i];
    if (documents[i].query.isEqual(titleQuery))
      titleDoc = documents[i];
    if (documents[i].query.isEqual(typeQuery))
      typeDoc = documents[i];
  }

  titleDoc.query.isEqual;
  var reductionalFunction = function (map, obj) {
    map[obj.id] = obj.get('val');
    return map;
  };

  var adrMap = adrDocs.docs.reduce(reductionalFunction, {});
  var levelMap = levelsDoc.docs.reduce(reductionalFunction, {});
  var titleMap = titleDoc.docs.reduce(reductionalFunction, {});
  var typeMap = typeDoc.docs.reduce(reductionalFunction, {});


  //вытаскиваем тренеров параллельно
  promises = [];
  supervisorPromises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('trainer_id') !== undefined) {
      for (let k = 0; k < snapshot.docs[i].get('trainer_id').length; k++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('trainer_id')[k]).get());
      }
    }
    if (snapshot.docs[i].get('supervisors') !== undefined) {
      for (let k = 0; k < snapshot.docs[i].get('supervisors').length; k++) {
        supervisorPromises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('supervisors')[k]).get());
      }
    }
  }

  var supervisorResponses = await Promise.all(supervisorPromises);
  var responses = await Promise.all(promises);
  var trainers = new Map();

  for (let i = 0; i < responses.length; i++) {
    var dbTrainer = responses[i].docs[0];
    if (dbTrainer !== undefined) {
      let tra = getUserObject(dbTrainer);
      trainers.set(dbTrainer.get('uid'), tra);
    }
  }
  var supervisors = new Map();
  for (let i = 0; i < supervisorResponses.length; i++) {
    var dbSupervisor = supervisorResponses[i].docs[0];
    if (dbSupervisor !== undefined) {
      let tra = getUserObject(dbSupervisor);
      supervisors.set(dbSupervisor.get('uid'), tra);
    }
  }

  //в итоге имеем массив с тренерами trainers

  //вытаскиваем записавшихся параллельно
  promises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('uids') !== undefined) {
      for (let j = 0; j < snapshot.docs[i].get('uids').length; j++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('uids')[j]).get());
      }
    }
  }

  responses = await Promise.all(promises);
  var users = new Map();


  for (let i = 0; i < responses.length; i++) {
    var dbUser = responses[i].docs[0];
    if (dbUser !== undefined) {
      var usr = getUserObject(dbUser);
      users.set(dbUser.get('uid'), usr);
    }
  }

  //в итоге имеем массив с записавшимися - users


  for (i = 0; i < snapshot.docs.length; i++) {

    var currentTraining = snapshot.docs[i];

    var uids = currentTraining.get('uids');
    var usersArray = [];

    if (uids !== undefined) {
      for (var j = 0; j < uids.length; j++) {
        usersArray.push(users.get(uids[j]));
      }
    }

    var tr = [];
    var supervisorsReturn = [];
    if (currentTraining.get('trainer_id') !== undefined) {
      for (k = 0; k < currentTraining.get('trainer_id').length; k++) {
        if (trainers.get(currentTraining.get('trainer_id')[k]) !== undefined)
          tr.push(trainers.get(currentTraining.get('trainer_id')[k]));
      }
    }
    if (currentTraining.get('supervisors') !== undefined) {
      for (k = 0; k < currentTraining.get('supervisors').length; k++) {
        if (supervisors.get(currentTraining.get('supervisors')[k]) !== undefined)
        supervisorsReturn.push(supervisors.get(currentTraining.get('supervisors')[k]));
      }
    }



    var actualAddress = adrMap[currentTraining.get('address')];
    var actualLevel = "";

    var levelsArr = [];

    for (j = 0; j < currentTraining.get('levels').length; j++) {

      levelsArr.push({
        id: currentTraining.get('levels')[j],
        val: levelMap[currentTraining.get('levels')[j]]
      });

      actualLevel += levelMap[currentTraining.get('levels')[j]];
      if (j !== currentTraining.get('levels').length - 1) {
        actualLevel += "/";
      }
    }


    var titleObj = {
      id: currentTraining.get('title'),
      val: titleMap[currentTraining.get('title')]
    }

    var typeObj = {
      id: currentTraining.get('type'),
      val: typeMap[currentTraining.get('type')]
    }

    var addressObj = {
      id: currentTraining.get('address'),
      val: actualAddress
    }

    var checked;
    if (currentTraining.get('checked') === undefined || currentTraining.get('checked') === null) {
      checked = [];
    } else {
      checked = currentTraining.get('checked');
    }


    //новый код
    var duration = currentTraining.get('duration');
    if (duration === undefined || duration === null) {
      duration = 0;
    }

    var comment = currentTraining.get('comment');
    if (comment === undefined || comment === null) {
      comment = "";
    }

    var cancelled = currentTraining.get('cancelled');
    if (cancelled === undefined || cancelled === null)
      cancelled = false;

    var participantsMinimum = currentTraining.get('participantsMinimum');
      if (participantsMinimum === undefined || participantsMinimum === null)
        participantsMinimum = 0;

    response.trainings.push(
      {
        address: addressObj,
        levels: actualLevel,
        date: currentTraining.get('date'),
        limit: currentTraining.get('limit'),
        reserve: currentTraining.get('reserve'),
        trainer_id: currentTraining.get('trainer_id'),
        trainer: tr,
        type: typeObj,
        title: titleObj,
        users: usersArray,
        price: currentTraining.get('price'),
        training_id: currentTraining.id,
        training_levels: levelsArr,
        auto: currentTraining.get('auto'),
        checked: checked,
        duration: duration,
        comment: comment,
        cancelled: cancelled,
        participantsMinimum: participantsMinimum,
        supervisors: supervisorsReturn
      }
    );


  }



  return response;

}

exports.getPreviousTrainingsForAdmin = functions.https.onCall(async (data, context) => {


  var trainingsRef = db.collection('trainings');

  let currentTime = new Date(Date.now());
  var begin = new Date();
  begin.setDate(currentTime.getDate() - 7);
  begin.setHours(0);
  begin.setMinutes(0);
  begin.setSeconds(0);
  begin.setMilliseconds(0);


  var snapshot = await trainingsRef
    .where("date", ">=", begin)
    .where("date", "<=", currentTime)
    .get();


  var ret = await getTrainingsBySnapshot(snapshot);
  return ret;
});

async function getTrainingsBySnapshotV2(snapshot){
  var response = {
    trainings: []
  };

  //параллельная загрузка списков данных (адреса, типы, виды и тд...)
  var adrQuery = db.collection('addreses').orderBy('timestamp', 'desc');
  var levelsQuery = db.collection('levels').orderBy('timestamp', 'desc');
  var titleQuery = db.collection('titles').orderBy('timestamp', 'desc');
  var typeQuery = db.collection('types').orderBy('timestamp', 'desc');

  var promises = [];

  promises.push(adrQuery.get());
  promises.push(levelsQuery.get());
  promises.push(titleQuery.get());
  promises.push(typeQuery.get());

  var adrDocs;
  var levelsDoc;
  var titleDoc;
  var typeDoc;


  var documents = await Promise.all(promises);

  for (var i = 0; i < documents.length; i++) {
    if (documents[i].query.isEqual(adrQuery))
      adrDocs = documents[i];
    if (documents[i].query.isEqual(levelsQuery))
      levelsDoc = documents[i];
    if (documents[i].query.isEqual(titleQuery))
      titleDoc = documents[i];
    if (documents[i].query.isEqual(typeQuery))
      typeDoc = documents[i];
  }

  titleDoc.query.isEqual;
  var reductionalFunction = function (map, obj) {
    map[obj.id] = obj.get('val');
    return map;
  };

  var adrMap = adrDocs.docs.reduce(reductionalFunction, {});
  var levelMap = levelsDoc.docs.reduce(reductionalFunction, {});
  var titleMap = titleDoc.docs.reduce(reductionalFunction, {});
  var typeMap = typeDoc.docs.reduce(reductionalFunction, {});


  //вытаскиваем тренеров параллельно
  promises = [];
  supervisorPromises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('trainer_id') !== undefined) {
      for (let k = 0; k < snapshot.docs[i].get('trainer_id').length; k++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('trainer_id')[k]).get());
      }
    }
    if (snapshot.docs[i].get('supervisors') !== undefined) {
      for (let k = 0; k < snapshot.docs[i].get('supervisors').length; k++) {
        supervisorPromises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('supervisors')[k]).get());
      }
    }
  }

  var responses = await Promise.all(promises);
  var supervisorResponses = await Promise.all(supervisorPromises);
  var trainers = new Map();

  for (let i = 0; i < responses.length; i++) {
    var dbTrainer = responses[i].docs[0];
    if (dbTrainer !== undefined) {
      let tra = getUserObject(dbTrainer);
      trainers.set(dbTrainer.get('uid'), tra);
    }
  }
  var supervisors = new Map();
  let supervisorsList = [];
  for (let i = 0; i < supervisorResponses.length; i++) {
    var dbSupervisor = supervisorResponses[i].docs[0];
    if (dbSupervisor !== undefined) {
      let tra = getUserObject(dbSupervisor);
      supervisors.set(dbSupervisor.get('uid'), tra);
      supervisorsList.push(tra);
    }
  }


  var urlPromises = [];
  for(i = 0; i < supervisorsList.length; i++){
    var usr = supervisorsList[i];
    let file = admin.storage().bucket().file(usr.docID);
    let year = (new Date(Date.now())).getFullYear();
    year += 5;
    let exp = '01-01-' + year;
    urlPromises.push(file.getSignedUrl({ action: 'read', expires: exp}));
  }
  var urlResult = await Promise.all(urlPromises);
  for(i = 0; i < urlResult.length; i++){
    supervisorsList[i].imgUrl = urlResult[i][0];
  }

  //в итоге имеем массив с тренерами trainers

  //вытаскиваем записавшихся параллельно
  
  //в итоге имеем массив с записавшимися - users


  for (i = 0; i < snapshot.docs.length; i++) {

    var currentTraining = snapshot.docs[i];

    var uids = currentTraining.get('uids');

    var tr = [];
    var supervisorsReturn = [];

    if (currentTraining.get('trainer_id') !== undefined) {
      for (k = 0; k < currentTraining.get('trainer_id').length; k++) {
        if (trainers.get(currentTraining.get('trainer_id')[k]) !== undefined)
          tr.push(trainers.get(currentTraining.get('trainer_id')[k]));
      }
    }
    if (currentTraining.get('supervisors') !== undefined) {
      for (k = 0; k < currentTraining.get('supervisors').length; k++) {
        if (supervisors.get(currentTraining.get('supervisors')[k]) !== undefined)
        supervisorsReturn.push(supervisors.get(currentTraining.get('supervisors')[k]));
      }
     
    }




    var actualAddress = adrMap[currentTraining.get('address')];
    var actualLevel = "";

    var levelsArr = [];

    for (j = 0; j < currentTraining.get('levels').length; j++) {

      levelsArr.push({
        id: currentTraining.get('levels')[j],
        val: levelMap[currentTraining.get('levels')[j]]
      });

      actualLevel += levelMap[currentTraining.get('levels')[j]];
      if (j !== currentTraining.get('levels').length - 1) {
        actualLevel += "/";
      }
    }


    var titleObj = {
      id: currentTraining.get('title'),
      val: titleMap[currentTraining.get('title')]
    }

    var typeObj = {
      id: currentTraining.get('type'),
      val: typeMap[currentTraining.get('type')]
    }

    var addressObj = {
      id: currentTraining.get('address'),
      val: actualAddress
    }

    var checked;
    if (currentTraining.get('checked') === undefined || currentTraining.get('checked') === null) {
      checked = [];
    } else {
      checked = currentTraining.get('checked');
    }


    //новый код
    var duration = currentTraining.get('duration');
    if (duration === undefined || duration === null) {
      duration = 0;
    }

    var comment = currentTraining.get('comment');
    if (comment === undefined || comment === null) {
      comment = "";
    }

    var cancelled = currentTraining.get('cancelled');
    if (cancelled === undefined || cancelled === null)
      cancelled = false;

    var participantsMinimum = currentTraining.get('participantsMinimum');
      if (participantsMinimum === undefined || participantsMinimum === null)
        participantsMinimum = 0;

    response.trainings.push(
      {
        address: addressObj,
        levels: actualLevel,
        date: currentTraining.get('date'),
        limit: currentTraining.get('limit'),
        reserve: currentTraining.get('reserve'),
        trainer_id: currentTraining.get('trainer_id'),
        trainer: tr,
        type: typeObj,
        title: titleObj,
        uids: uids,
        price: currentTraining.get('price'),
        training_id: currentTraining.id,
        training_levels: levelsArr,
        auto: currentTraining.get('auto'),
        checked: checked,
        duration: duration,
        comment: comment,
        cancelled: cancelled,
        participantsMinimum: participantsMinimum,
        supervisors: supervisorsReturn
      }
    );


  }


  return response;

}

exports.getPreviousTrainingsForAdminV2 = functions.https.onCall(async (data, context) => {


  var trainingsRef = db.collection('trainings');

  let currentTime = new Date(Date.now());
  var begin = new Date();
  begin.setDate(currentTime.getDate() - 7);
  begin.setHours(0);
  begin.setMinutes(0);
  begin.setSeconds(0);
  begin.setMilliseconds(0);

  //var end = new Date();
  //end.setDate(currentTime.getDate() + 7);

  var snapshot = await trainingsRef
    .where("date", ">=", begin)
    .where("date", "<=", currentTime)
    .get();



  
  var ret = await getTrainingsBySnapshotV2(snapshot);

  return ret;

});


exports.getOldTrainings = functions.https.onCall(async (data, context) => {


  var trainingsRef = db.collection('trainings');
  let start = new Date(Date.now());
  start.setDate(start.getDate() - 7);
  start.setHours(0);
  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);


  var end = new Date();
  end.setDate(start.getDate() + 7);

  end.getMilliseconds
  var snapshot = await trainingsRef
    .where("date", ">=", start)
    .get();

  var filteredDocs = [];

  for (var l = 0; l < snapshot.docs.length; l++) {
    if (!(snapshot.docs[l].get('date').getMilliseconds > end.getMilliseconds))
      filteredDocs.push(snapshot.docs[l]);
  }

  snapshot.docs = filteredDocs;

  var response = {
    trainings: []
  };

  //параллельная загрузка списков данных (адреса, типы, виды и тд...)
  var adrQuery = db.collection('addreses').orderBy('timestamp', 'desc');
  var levelsQuery = db.collection('levels').orderBy('timestamp', 'desc');
  var titleQuery = db.collection('titles').orderBy('timestamp', 'desc');
  var typeQuery = db.collection('types').orderBy('timestamp', 'desc');

  var promises = [];

  promises.push(adrQuery.get());
  promises.push(levelsQuery.get());
  promises.push(titleQuery.get());
  promises.push(typeQuery.get());

  var adrDocs;
  var levelsDoc;
  var titleDoc;
  var typeDoc;


  var documents = await Promise.all(promises);

  for (var i = 0; i < documents.length; i++) {
    if (documents[i].query.isEqual(adrQuery))
      adrDocs = documents[i];
    if (documents[i].query.isEqual(levelsQuery))
      levelsDoc = documents[i];
    if (documents[i].query.isEqual(titleQuery))
      titleDoc = documents[i];
    if (documents[i].query.isEqual(typeQuery))
      typeDoc = documents[i];
  }

  titleDoc.query.isEqual;
  var reductionalFunction = function (map, obj) {
    map[obj.id] = obj.get('val');
    return map;
  };

  var adrMap = adrDocs.docs.reduce(reductionalFunction, {});
  var levelMap = levelsDoc.docs.reduce(reductionalFunction, {});
  var titleMap = titleDoc.docs.reduce(reductionalFunction, {});
  var typeMap = typeDoc.docs.reduce(reductionalFunction, {});


  //вытаскиваем тренеров параллельно
  promises = [];
  supervisorPromises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('trainer_id') !== undefined) {
      for (let k = 0; k < snapshot.docs[i].get('trainer_id').length; k++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('trainer_id')[k]).get());
      }
    }
    if (snapshot.docs[i].get('supervisors') !== undefined) {
      for (let k = 0; k < snapshot.docs[i].get('supervisors').length; k++) {
        supervisorPromises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('supervisors')[k]).get());
      }
    }
  }

  var responses = await Promise.all(promises);
  var supervisorResponses = await Promise.all(supervisorPromises);

  var trainers = new Map();

  for (let i = 0; i < responses.length; i++) {
    var dbTrainer = responses[i].docs[0];
    if (dbTrainer !== undefined) {
      let tra = getUserObject(dbTrainer);
      trainers.set(dbTrainer.get('uid'), tra);
    }

  }

  var supervisors = new Map();
  for (let i = 0; i < supervisorResponses.length; i++) {
    var dbSupervisor = supervisorResponses[i].docs[0];
    if (dbSupervisor !== undefined) {
      let tra = getUserObject(dbSupervisor);
      supervisors.set(dbSupervisor.get('uid'), tra);
    }
  }

  //в итоге имеем массив с тренерами trainers

  //вытаскиваем записавшихся параллельно
  promises = [];
  for (let i = 0; i < snapshot.docs.length; i++) {
    if (snapshot.docs[i].get('uids') !== undefined) {
      for (let j = 0; j < snapshot.docs[i].get('uids').length; j++) {
        promises.push(db.collection('users').where('uid', '==', snapshot.docs[i].get('uids')[j]).get());
      }
    }
  }

  responses = await Promise.all(promises);
  var users = new Map();


  for (let i = 0; i < responses.length; i++) {
    var dbUser = responses[i].docs[0];
    if (dbUser !== undefined) {
      var usr = getUserObject(dbUser);
      users.set(dbUser.get('uid'), usr);
    }
  }

  //в итоге имеем массив с записавшимися - users


  for (i = 0; i < snapshot.docs.length; i++) {

    var currentTraining = snapshot.docs[i];

    var uids = currentTraining.get('uids');
    var usersArray = [];

    if (uids !== undefined) {
      for (var j = 0; j < uids.length; j++) {
        usersArray.push(users.get(uids[j]));
      }
    }

    var tr = [];
    var supervisorsReturn = [];

    if (currentTraining.get('trainer_id') !== undefined) {



      for (k = 0; k < currentTraining.get('trainer_id').length; k++) {
        if (trainers.get(currentTraining.get('trainer_id')[k]) !== undefined)
          tr.push(trainers.get(currentTraining.get('trainer_id')[k]));
      }
      //tr = trainers.get(currentTraining.get('trainer_id'));
    }

    if (currentTraining.get('supervisors') !== undefined) {
      for (k = 0; k < currentTraining.get('supervisors').length; k++) {
        if (supervisors.get(currentTraining.get('supervisors')[k]) !== undefined)
        supervisorsReturn.push(supervisors.get(currentTraining.get('supervisors')[k]));
      }
     
    }


    var actualAddress = adrMap[currentTraining.get('address')];
    var actualLevel = "";

    var levelsArr = [];

    for (j = 0; j < currentTraining.get('levels').length; j++) {

      levelsArr.push({
        id: currentTraining.get('levels')[j],
        val: levelMap[currentTraining.get('levels')[j]]
      });

      actualLevel += levelMap[currentTraining.get('levels')[j]];
      if (j !== currentTraining.get('levels').length - 1) {
        actualLevel += "/";
      }
    }


    var titleObj = {
      id: currentTraining.get('title'),
      val: titleMap[currentTraining.get('title')]
    }

    var typeObj = {
      id: currentTraining.get('type'),
      val: typeMap[currentTraining.get('type')]
    }

    var addressObj = {
      id: currentTraining.get('address'),
      val: actualAddress
    }

    var checked;
    if (currentTraining.get('checked') === undefined || currentTraining.get('checked') === null) {
      checked = [];
    } else {
      checked = currentTraining.get('checked');
    }


    //новый код
    var duration = currentTraining.get('duration');
    if (duration === undefined || duration === null) {
      duration = 0;
    }

    var comment = currentTraining.get('comment');
    if (comment === undefined || comment === null) {
      comment = "";
    }

    var cancelled = currentTraining.get('cancelled');
    if (cancelled === undefined || cancelled === null)
      cancelled = false;

    var participantsMinimum = currentTraining.get('participantsMinimum');
    if (participantsMinimum === undefined || participantsMinimum === null)
      participantsMinimum = 0;

    response.trainings.push(
      {
        //address : actualAddress,
        address: addressObj,
        levels: actualLevel,
        date: currentTraining.get('date'),
        limit: currentTraining.get('limit'),
        reserve: currentTraining.get('reserve'),
        trainer_id: currentTraining.get('trainer_id'),
        trainer: tr,
        //type : typeMap[currentTraining.get('type')],
        type: typeObj,
        //title : titleMap[currentTraining.get('title')],
        title: titleObj,
        users: usersArray,
        price: currentTraining.get('price'),
        training_id: currentTraining.id,
        //training_levels : currentTraining.get('levels')
        training_levels: levelsArr,
        auto: currentTraining.get('auto'),
        checked: checked,
        duration: duration,
        comment: comment,
        cancelled: cancelled,
        participantsMinimum: participantsMinimum,
        supervisors: supervisorsReturn
      }
    );


  }



  return response;



});



exports.addNewUser = functions.https.onCall(async (data, context) => {

  let stubStamp = (new Date(Date.now()));
  let trainer_id = "trainer".concat(stubStamp.valueOf().toString());
  

  var manualID = data.isTrainer;
  if (manualID === undefined || manualID === null || manualID === false)
    return addUser(context.auth.uid, data);
  else
    return addUser(trainer_id, data);
  
});


async function addUser(id, data) {

  console.log("addNewUser UID: " + id);
  if (id === null) {
    return { error: "UNAUTHORIZED ACCESS" };
  }

  var trainer = data.isTrainer;
  if (trainer === undefined || trainer === null)
    trainer = false;

  var adm = data.isAdmin
  if (adm === undefined || adm === null)
    adm = false;

  var byAdm = data.byAdmin
  if (byAdm === undefined || byAdm === null)
    byAdm = false;

  var supervisor = data.supervisor;
  if (supervisor === undefined || supervisor === null)
    supervisor = false;

  var banned = data.banned;
  if (banned === undefined || banned === null)
    banned = false;

  console.log("addNewUser byAdmin " + byAdm);

  if (byAdm === false) {
    if ((await db.collection('users').where("uid", "==", id).get()).docs.length !== 0) {
      console.log("USER ALREADY IN DB " + id);
      return {
        code: -1,
        error: "ALREADY IN DB"
      };
    }
  }

  const token = data.token;
  var name = data.name;
  var lastname = data.lastname;
  if (name === undefined || name === null) {
    name = "";
  }

  if (lastname === undefined || lastname === null) {
    lastname = "";
  }


  const sex = data.sex;
  //TODO cast to integer
  //const age = Number.parseInt(data.age);
  var age = new Date();
  age.setTime(data.age);

  var phone = data.phone;
  if (phone === undefined || phone === null)
    phone = "";
  const fcm_token = data.fcm;




  if (byAdm === true) {
    let stubStamp = (new Date(Date.now()));
    let stubStampUID = stubStamp.valueOf();
    console.log("addNewUser adding byAdmin wit UID " + stubStampUID.toString());
    var infoStub = createUserObject(false, stubStamp, name, lastname, "undef",
      "", sex, trainer, stubStampUID.toString(), "", true, byAdm, supervisor, banned);
    return db.collection('users').doc(stubStampUID.toString())
      .set(infoStub)
      .then(() => {
        console.log("New user was added to database");
        return {
          uid: stubStampUID.toString(),
          message: "SUCCESS"
        };
      });
  }


  var info = createUserObject(adm, age, name, lastname, 'undef',
    phone, sex, trainer, id, fcm_token, true, byAdm, supervisor, banned);


  return db.collection('users').doc(id)
    .set(info)
    .then(() => {
      console.log("New user was added to database");
      return {
        uid: id,
        message: "SUCCESS"
      };
    });

}


exports.signOut = functions.https.onCall(async (data, context) => {
  var fcm = data.fcm;

  if (fcm !== undefined) {
    var userDocRef = await db.collection('users').doc(context.auth.uid);
    await userDocRef.update({
      fcm: admin.firestore.FieldValue.arrayRemove(fcm)
    });
    return {
      code: 0,
      message: "fcm deleted"
    }
  } else {
    return {
      code: -1,
      message: "undefined fcm"
    }
  }
});

const createKeywords = name => {
  const arrName = [];
  let curName = '';
  name.split('').forEach(letter => {
    curName += letter;
    arrName.push(curName);
  });
  return arrName;
}


const generateKeywords = info => {

  [first, last, ph] = info;
  first = first.toLowerCase();
  last = last.toLowerCase();
  const phone = ph.length > 0 ? `${ph}` : '';
  const keywordFirstLast = createKeywords(`${first} ${last}`);
  const keywordLastFirst = createKeywords(`${last} ${first}`);
  const keywordPhone = createKeywords(phone);
  return [
    ...new Set([
      '',
      ...keywordFirstLast,
      ...keywordLastFirst,
      ...keywordPhone
    ])
  ];
}

exports.updateUser = functions.https.onCall(async (data, context) => {

  var isDebtor = data.isDebtor;
  if(isDebtor === undefined || isDebtor === null)
    isDebtor = false;

  var isTrainer = data.isTrainer;
  if (isTrainer === undefined || isTrainer === null)
    isTrainer = false;


  var byAdm = data.byAdmin;
  if (byAdm === undefined || byAdm === null)
    byAdm = false;

  var editable = data.editable;
  if (editable === undefined || editable === null)
    editable = true;

  console.log("Editable: " + editable);
  console.log("byHimself: " + data.byHimself);


  var curID;

  if (data.id !== null && data.id !== undefined) {
    curID = data.id;
  } else {
    curID = context.auth.uid; //'E7lCl5OTUzMExVCW7xN6Oje3KQB3';
  }

  var curUser = (await db.collection('users').where("uid", "==", curID).get());

  if (data.byHimself !== undefined) {
    if (curUser.docs[0].get('editable') === false && data.byHimself === true) {
      console.log("USER WITH NO RIGHTS TRIED TO UPDATE HIS PROFILE");
      return {
        code: -1,
        message: 'You have no rights to update profile'
      }
    }
  }


  if (curUser.docs.length === 0) {
    return {
      code: -2,
      message: 'No such user'
    }
  }

  var currentUserDocID = curUser.docs[0].id;

  var tokens = curUser.docs[0].get('fcm');

  var currentLevel = curUser.docs[0].get('level');
  var level = data.level;
  if (level === undefined || level === null) {
    if (currentLevel !== undefined || currentLevel !== null) {
      level = currentLevel
    } else {
      level = 'undef';
    }
  }

  var currentBanned = curUser.docs[0].get('banned');
  var banned = data.banned;
  if (banned === undefined || banned === null) {
    if (currentBanned !== undefined || currentBanned !== null) {
      banned = currentBanned
    } else {
      banned = false;
    }
  }

  var currentAdm = curUser.docs[0].get('admin');
  var adm = data.isAdmin;

  if (adm === undefined || adm === null) {
    if (currentAdm !== undefined || currentAdm !== null) {
      adm = currentAdm
    } else {
      adm = false;
    }
  }

  var currentSupervisor = curUser.docs[0].get('supervisor');
  var supervisor = data.supervisor;
  if (supervisor === undefined || supervisor === null) {
    if (currentSupervisor !== undefined || currentSupervisor !== null) {
      supervisor = currentSupervisor
    } else {
      supervisor = false;
    }
  }

  //const token = data.token;
  const name = data.name;
  const lastname = data.lastname;
  var sex = data.sex;
  //TODO cast to integer

  if (data.age !== undefined) {
    var age = new Date();
    age.setTime(data.age);
    console.log("Update user age is " + data.age.toString());
  } else {
    age = null;
    console.log("Age is null ");
  }

  //const age = Number.parseInt(data.age);
  const phone = data.phone;
  const fcm_token = data.fcm;

  if (!tokens.includes(fcm_token) && fcm_token !== null && fcm_token !== undefined)
    tokens.push(fcm_token);


  if (data.fcm_update !== undefined) {
    return db.collection('users').doc(currentUserDocID).update({
      last_updated: new Date(Date.now()),
      fcm: tokens
    })
      .then(() => {
        console.log("User was updated in database");
        return { message: "SUCCESS" };
      });
  }

  console.log("updateUser UID: " + curID);
  if (curID === null) {
    return { error: "UNAUTHORIZED ACCESS" };
  }


  if (sex === undefined || sex === null) {
    sex = true;
  }




  console.log("Updating user " + curID + " isDebtor:" + isDebtor);
  var info = createUserObject(
    adm, 
    age, 
    name, 
    lastname, 
    level, 
    phone, 
    sex, 
    isTrainer, 
    curID, 
    tokens, 
    editable, 
    byAdm, 
    supervisor, 
    banned, 
    isDebtor,
  );

  info.last_updated = new Date(Date.now());

  return db.collection('users').doc(currentUserDocID).update(info)
    .then(() => {
      console.log("User was updated in database");
      return { message: "SUCCESS" };
    });


});


exports.addMessage = functions.https.onRequest((req, res) => {
  const original = req.query.text;
  return admin.database().ref('/messages').push({ original: original }).then((snapshot) => {
    return res.redirect(303, snapshot.ref.toString());
  });
});


exports.makeUppercase = functions.database.ref('/messages/{pushId}/original')
  .onCreate((snapshot, context) => {
    const original = snapshot.val();
    console.log('Uppercasing', context.params.pushId, original);
    const uppercase = original.toUpperCase();
    return snapshot.ref.parent.child('uppercase').set(uppercase);
  });


exports.getAllNotifications = functions.https.onCall(async (data, context) => {

  var notificationsRef = db.collection('notifications');

  var snapshot = await notificationsRef.get();

  var response = {
    notifications: snapshot.docs
  };

  return response;

});

exports.getNotificationArchive = functions.https.onCall(async (data, context) => {
  var snapshot = await db.collection('notifications').orderBy('timestamp', 'desc').get();
  var response = [];

  for (var i = 0; i < snapshot.docs.length; i++) {
    var currentNotification = snapshot.docs[i];
    response.push({
      title: currentNotification.get('title'),
      body: currentNotification.get('body'),
      notification_id: currentNotification.id,
      timestamp: currentNotification.get('timestamp'),
      type: currentNotification.get('type'),
      read: currentNotification.get('read'),
      receiverUids: currentNotification.get('receiverUids')
    });
  }

  return { response: response };

});

exports.getUsersNotifications = functions.https.onCall(async (data, context) => {

  var notificationId = data.notificationId;//"1569745426225";
  var snapshot = await db.collection('notifications').doc(notificationId).get();
  var pushes = [];

  var receiverUids = snapshot.get('receiverUids');
  
  var promises = [];
  for (let i = 0; i < receiverUids.length; i++) {
    promises.push(db.collection('users').where('uid', '==', receiverUids[i]).get());
  }

  var promisesUsers = await Promise.all(promises);


  var users = [];
  /* eslint-disable no-await-in-loop */
  for (var i = 0; i < promisesUsers.length; i++)
    users.push(getUserObject(promisesUsers[i].docs[0]));

  
  var readUids = snapshot.get('readUids');
  if(readUids === undefined || readUids === null)
    readUids = [];

  var notification = {
    title: snapshot.get('title'),
    body: snapshot.get('body'),
    notification_id: notificationId,
    timestamp: snapshot.get('timestamp'),
    type: snapshot.get('type'),
    readUids: readUids,
  }


  return {
      notification: notification,
      users: users
  }
  

});



exports.saveLastLoginTimestamps = functions.https.onRequest(async (req, res) => {
  const listAllUsers = (nextPageToken) => {
    // List batch of users, 1000 at a time.
    admin
      .auth()
      .listUsers(1000, nextPageToken)
      .then(async (listUsersResult) => {
        let usqw = listUsersResult.users;
        
        for(let i = 0; i < usqw.length; i++) {
          let testDate = new Date(usqw[i].metadata.lastSignInTime);
          try{
            await db.collection('users').doc(usqw[i].uid).update({
              last_updated: testDate,
            });
          } catch (e) {
            console.log('error ' + e.toString());
          }
        }
        
        if (listUsersResult.pageToken) {
          // List next batch of users.
          listAllUsers(listUsersResult.pageToken);
        }
      return usqw;
      })
      .catch((error) => {
        console.log('Error listing users:', error);
      });
  };
  // Start listing users from the beginning, 1000 at a time.
  listAllUsers();
});
