# UfaVolley

Ufavolley Flutter application.

### Emulator and Cloud Functions debugging

Install firebase tools:
```sh
npm install -g firebase-tools
```
Then select current project to be "ufavolleyreleased":
```sh
firebase use ufavolleyreleased
```
Run emulator with following command:
```sh
firebase emulators:start --inspect-functions
```

Add following debug configuration in VS Code:
```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "attach",
      "name": "Attach",
      "port": 9229,
      "restart": true,
      "skipFiles": ["<node_internals>/**"]
    }
  ]
}
```
Use functions emulator in dart code:
```dart
CloudFunctions.instance.useFunctionsEmulator(origin: "http://10.0.2.2:5001");
```

Don't forget to allow HTTP connections on Android API_LEVEL > 28 (9.0 Pie)

### Model classes generation (JSON Serializable)

```sh
flutter pub run build_runner build 
```

### EXTRA
Use following version in native android plugin for cloud_functions to get rid of DEADLINE_EXCEEDED error (TIMEOUT) on Android
```gradle
api 'com.google.firebase:firebase-functions:19.0.1'
```

Полезные штуки

Для генерации кода использовать flutter pub run build_runner build
Для фикса библиотеки cloud_functions нужно поменять ее версию на 19.0.1