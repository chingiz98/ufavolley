import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  @JsonKey(name: 'phone')
  String phoneNumber;

  @JsonKey(name: 'first_name')
  String name;

  @JsonKey(name: 'last_name')
  String lastName;

  @JsonKey(name: 'imgUrl')
  String imageUrl;

  @JsonKey(name: 'sex')
  bool gender;

  static DateTime _dateParse(val) {
    if (val == null) {
      return null;
    }
    return DateTime.fromMillisecondsSinceEpoch(val['_seconds'] * 1000);
  }

  static int _dataToJson(val) {
    return val.toUtc().millisecondsSinceEpoch;
  }

  @JsonKey(name: 'age', fromJson: _dateParse, toJson: _dataToJson)
  DateTime dateOfBirth;

  @JsonKey(name: 'main_admin', defaultValue: false)
  bool isMainAdmin;

  @JsonKey(name: 'admin', defaultValue: false)
  bool isAdmin;

  @JsonKey(name: 'trainer', defaultValue: false)
  bool isTrainer;

  @JsonKey(name: 'editable', defaultValue: true)
  bool isEditable;

  @JsonKey(name: 'byAdmin', defaultValue: false)
  bool byAdmin;

  @JsonKey(name: 'uid')
  String uid;

  @JsonKey(name: 'level')
  String level;

  @JsonKey(ignore: true)
  String vkLink;

  @JsonKey(name: 'supervisor', defaultValue: false)
  bool isSupervisor;

  @JsonKey(name: 'banned', defaultValue: false)
  bool isBanned;

  @JsonKey(name: 'isDebtor', defaultValue: false)
  bool isDebtor;

  @JsonKey(name: 'rating', defaultValue: 0)
  int rating;

  User(
      {this.name,
      this.dateOfBirth,
      this.gender,
      this.lastName,
      this.phoneNumber,
      this.isAdmin,
      this.isTrainer,
      this.uid,
      this.level,
      this.isEditable = true,
      this.byAdmin = false,
      this.isMainAdmin = false,
      this.isBanned = false,
      this.isSupervisor = false,
      this.isDebtor = false,
      this.imageUrl,
      this.vkLink,
      this.rating});

  bool operator ==(o) =>
      o is User &&
      phoneNumber == o.phoneNumber &&
      name == o.name &&
      lastName == o.lastName &&
      gender == o.gender &&
      dateOfBirth == o.dateOfBirth &&
      isAdmin == o.isAdmin &&
      isTrainer == o.isTrainer &&
      isEditable == o.isEditable &&
      byAdmin == o.byAdmin &&
      uid == o.uid &&
      level == o.level;

  int get hashCode => Object.hash(
        phoneNumber,
        name,
        lastName,
        gender,
        dateOfBirth,
        isAdmin,
        isTrainer,
        isEditable,
        byAdmin,
        uid,
        level,
      );

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  bool get canSeeAnyTraining => isMainAdmin || isAdmin || isSupervisor;
}

class UserStub extends User {
  UserStub(String uid) : super(name: "", lastName: "", uid: uid);
}
