import 'package:json_annotation/json_annotation.dart';
import 'package:ufavolley/models/user.dart';
import 'package:flutter/foundation.dart';
import 'package:ufavolley/models/cloud_value.dart';


//part 'training.g.dart';
@JsonSerializable()

class Training {

  /*
   dynamic usersWrap = result.data;
    List<User> _users = new List();
    for (var userWrap in usersWrap){
      var wrap = new Map<String, dynamic>.from(userWrap);
      _users.add(User.fromJson(wrap));
    }
   */

  //CloudValue.fromMap(trainingWrap['type'])
  //static _valueParse
  //@JsonKey(name: 'type', )
  CloudValue type;
  CloudValue title;
  List<CloudValue> levels;
  CloudValue address;
  List<String> trainerIDs;

  @JsonKey(name: 'limit')
  int capacity;

  @JsonKey(name: 'reserve')
  int reserve;

  @JsonKey(name: 'price')
  int price;

  DateTime dateTime;
  List<User> trainers;
  List<User> participants;
  List<String> incomingUsersUids;

  @JsonKey(name: 'training_id')
  String trainingID;

  @JsonKey(name: 'auto')
  bool isAutoContinue;

  int durationImMinutes;

  @JsonKey(name: 'comment', defaultValue: '')
  String comment;

  @JsonKey(name: 'cancelled', defaultValue: false)
  bool isCancelled;

  @JsonKey(name: 'participantsMinimum', defaultValue: 0)
  int participantsMinimum;

  @JsonKey(name: 'supervisors', defaultValue: [])
  List<User> supervisors;



  Training({
    @required this.type,
    @required this.title,
    @required this.levels,
    @required this.address,
    @required this.trainerIDs,
    @required this.trainers,
    @required this.capacity,
    @required this.reserve,
    @required this.price,
    @required this.dateTime,
    this.participants,
    this.trainingID,
    this.isAutoContinue = false,
    this.incomingUsersUids = const [],
    this.durationImMinutes = 0,
    this.comment = "",
    this.isCancelled = false,
    this.participantsMinimum,
    this.supervisors
  });

  int getNumberOfEmptyPlace() {
    return capacity - (participants.length + supervisors.length) < 0 ? 0 : capacity - (participants.length + supervisors.length);
  }

  bool isLevelCanSignIn(String levelId) {
    final trainingLevelsUids = levels.map((level) => level.id).toList(growable: false);

    final userLevelInTraining = trainingLevelsUids.contains(levelId);

    if (userLevelInTraining) {
      return true;
    }

    final isUserLevelUndefined = levelId == 'undef';
    final isTrainingContainsLiteLevel = trainingLevelsUids.contains('U6zaxhj6IzaExZqcMkQ3');

    return isUserLevelUndefined && isTrainingContainsLiteLevel;
  }

}

class TrainingStub extends Training {

  double height = 0.0;

  TrainingStub({height}) : super(
    type: CloudValue(val: 'STUB', id: 'STUB'),
    title: CloudValue(val: 'STUB', id: 'STUB'),
    levels: [CloudValue(val: 'STUB', id: 'STUB')],
    address: CloudValue(val: 'STUB', id: 'STUB'),
    trainerIDs: [''],
    capacity: 0,
    reserve: 0,
    price: 0,
    dateTime: DateTime.now(),
    trainers: [User(name: 'ban', lastName: 'ban')],
    participants: [],
    incomingUsersUids: []
  ) {
    this.height = height;
  }
}