import 'package:flutter/widgets.dart';
import 'training.dart';
import 'package:ufavolley/utils/utils.dart';

class FilterObject {

  bool isEmpty = true;
  Map<String, List<FilterItem>> paramMaps = Map();

  List<Training> applyTo(List<Training> inputTrainings) {
    if (isEmpty)
      return inputTrainings;
    List<Training> outputTrainings = [];
    for (Training training in inputTrainings) {
      if (_isSatisfied(training) || training is TrainingStub)
        outputTrainings.add(training);
    }
    print(outputTrainings.length.toString() + ' items satisfied');
    return outputTrainings;
  }

  bool _isSatisfied(Training training) {
    if (isEmpty)
      return true;
    bool isTypeSatisfied = _isSatisfiedFiled(paramMaps['Вид тренировки'], training.type.getValue);
    if (!isTypeSatisfied) return false;
    bool isTitleSatisfied = _isSatisfiedFiled(paramMaps['Тип тренировки'], training.title.getValue);
    if (!isTitleSatisfied) return false;
    bool isAddressSatisfied = _isSatisfiedFiled(paramMaps['Адрес тренировки'], training.address.getValue);
    if (!isAddressSatisfied) return false;
    bool isTimeSatisfied = _isSatisfiedFiled(paramMaps['Время тренировки'], _convertTimeToFilterParam(training.dateTime));
    if (!isTimeSatisfied) return false;
    return true;
  }

  String _convertTimeToFilterParam(DateTime dateTime) {
    return getHours(dateTime) + '-' + getMinutes(dateTime);
  }

  bool _isSatisfiedFiled(List<FilterItem> filterItems, String fieldValue) {
    bool isSatisfied = false;
    int selectedCount = 0;
    filterItems.forEach((FilterItem filterItem) {
      if (filterItem.isCheck) {
        if (filterItem.label == fieldValue)
          isSatisfied = true;
        selectedCount++;
      }
    });
    if (selectedCount == 0)
      return true;
    return isSatisfied;
  }

  void addFilterParams(String key, List<FilterItem> filterItems) {
    isEmpty = false;
    paramMaps[key] = filterItems;
  }

  void removeFilterParams(String key) {
    paramMaps.remove(key);
    if (paramMaps.length == 0)
      isEmpty = true;
  }

}

class FilterItem {
  String label;
  bool isCheck;

  FilterItem({@required this.label, this.isCheck = false});
}