import 'package:ufavolley/models/user.dart';

class UserParamFilter {
  bool isAdminCheck = false;
  bool isSupervisorCheck = false;
  bool isMainAdminCheck = false;
  bool isManSexCheck = false;
  bool isWomanSexCheck = false;

  bool isUnknownLevelCheck = false;
  bool isLiteLevelCheck = false;
  bool isLitePlusLevelCheck = false;
  bool isMediumLevelCheck = false;
  bool isMediumPlusLevelCheck = false;
  bool isHardLevelCheck = false;

  List<User> applyTo(List<User> users) {
    List<User> filteredUsers = [];
    for (User user in users) {
      if (_isSatisfied(user)) filteredUsers.add(user);
    }
    return filteredUsers;
  }

  bool _isSatisfied(User user) {
    if (!_isPermissionSatisfied(user)) return false;
    if (!_isLevelSatisfied(user)) return false;
    if (!_isSexSatisfied(user)) return false;
    return true;
  }

  bool _isPermissionSatisfied(User user) {
    if (!isAdminCheck && !isSupervisorCheck && !isMainAdminCheck)
      return true;
    if (isAdminCheck && user.isAdmin) return true;
    if (isSupervisorCheck && user.isSupervisor) return true;
    if (isMainAdminCheck && user.isMainAdmin) return true;
    return false;
  }

  bool _isLevelSatisfied(User user) {
    if (!isUnknownLevelCheck &&
        !isLiteLevelCheck &&
        !isLitePlusLevelCheck &&
        !isMediumLevelCheck &&
        !isMediumPlusLevelCheck &&
        !isHardLevelCheck)
      return true;
    if (isUnknownLevelCheck && user.level == 'undef')
      return true;
    if (isLiteLevelCheck && user.level == 'U6zaxhj6IzaExZqcMkQ3')
      return true;
    if (isLitePlusLevelCheck && user.level == 'Y5gzffkscPylzcP8rNkz')
      return true;
    if (isMediumLevelCheck && user.level == 'ec9tH4CEAVLxfT398nV4')
      return true;
    if (isMediumPlusLevelCheck && user.level == '10wUQYexD94OP9PeeoUy')
      return true;
    if (isHardLevelCheck && user.level == 'e3uKZoSndEv4jNsQ9GHW')
      return true;
    return false;
  }

  bool _isSexSatisfied(User user) {
    if (user.gender == null) return false;
    if (!isManSexCheck && !isWomanSexCheck) return true;
    if (isManSexCheck && user.gender) return true;
    if (isWomanSexCheck && !user.gender) return true;
    return false;
  }
}

class UserDesiredFilter {

  String _desiredParameter;

  void setDesiredParameter(String desiredParam) {
    this._desiredParameter = desiredParam;
  }

  String getDesiredParameter() {
    return _desiredParameter;
  }

  List<User> applyTo(List<User> users) {
    List<User> filteredUsers = [];
    for (User user in users) {
      if (_isSatisfied(user)) filteredUsers.add(user);
    }
    return filteredUsers;
  }

  bool _isSatisfied(User user) {
    if (_desiredParameter == null || _desiredParameter == '') return true;
    if (_containsIgnoreCase(user.phoneNumber, _desiredParameter) ||
        _containsIgnoreCase(user.name, _desiredParameter) ||
        _containsIgnoreCase(user.lastName, _desiredParameter)) return true;
    if (_isNameAndLastNameSatisfiedTogether(user)) return true;
    return false;
  }

  bool _containsIgnoreCase(String string1, String string2) {
    return string1.toLowerCase().contains(string2.toLowerCase());
  }

  bool _isNameAndLastNameSatisfiedTogether(User user) {

    String nameAndLastName = user.name + ' ' + user.lastName;
    String lastNameAndName = user.lastName + ' ' + user.name;

    if (_containsIgnoreCase(nameAndLastName, _desiredParameter) ||
      _containsIgnoreCase(lastNameAndName, _desiredParameter)
    ) return true;

    return false;
  }


}
