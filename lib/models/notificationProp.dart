import 'package:flutter/widgets.dart';

enum NotificationType{userAction, custom, reminder}

class NotificationProp {

  NotificationType type;
  String title;
  String subTitle;
  DateTime dateOfReceiving;
  bool read;
  String trainingID;
  String id;

  NotificationProp({@required this.title, @required this.subTitle, @required this.id, this.read = false, this.type = NotificationType.userAction, this.trainingID});

}

class ArchivedNotificationProp extends NotificationProp {
  List<String> receiveUids;
  ArchivedNotificationProp(NotificationProp notificationProp, List<String> receiveUids) {
    this.receiveUids = receiveUids;
    this.title = notificationProp.title;
    this.subTitle = notificationProp.subTitle;
    this.id = notificationProp.id;
    this.dateOfReceiving = notificationProp.dateOfReceiving;
    if (notificationProp.read != null)
      this.read = notificationProp.read;
    else
      this.read = false;
    if (notificationProp.type != null)
      this.type = notificationProp.type;
    else
      this.type = NotificationType.userAction;
    this.trainingID = notificationProp.trainingID;
  }
}