class UserSort {
  String sortBy;
  int direction;

  UserSort(String sortBy, int direction) {
    this.sortBy = sortBy;
    this.direction = direction;
  }
}