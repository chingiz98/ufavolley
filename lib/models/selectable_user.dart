import 'package:meta/meta.dart';
import 'package:ufavolley/models/user.dart';

class SelectableUser {
  bool isSelected;
  User user;
  SelectableUser({@required this.user, this.isSelected = false});
}