import 'package:flutter/widgets.dart';
import 'package:ufavolley/utils/constants.dart';

class CloudValue {
  String val;

  String get getValue {
    if (id == C.legacyMediumPlusLevelId)
      return C.litePlusProLevelName;
    else
      return val;
  }

  String id;

  CloudValue({@required this.val, @required this.id});

  CloudValue.fromMap(Map map) {
    this.val = map['val'];
    this.id = map['id'];
  }

  bool operator ==(o) => o is CloudValue && val == o.val && id == o.id;

  int get hashCode => Object.hash(val.hashCode, id.hashCode);
}
