import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ufavolley/widgets/date_picker/date_picker_controller.dart';

class DatePicker extends StatefulWidget {
  final DatePickerController controller;

  const DatePicker({
    Key key,
    @required this.controller,
  }) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime _selectedDate;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener((dateTime) {
      _selectedDate = dateTime;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        final date = await _selectDate(context);
        if (date != null) {
          _selectedDate = date;
          widget.controller.setValue(_selectedDate);
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30.0),
        ),
        padding: const EdgeInsets.only(
          top: 4.0,
          left: 16.0,
          right: 4.0,
        ),
        margin: EdgeInsets.only(
          bottom: 5,
          left: 10,
          right: 10,
        ),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 7),
                child: Text(
                  _text(),
                  style: _textStyle(),
                ),
              ),
              if (_selectedDate != null) _clearButton(),
            ],
          ),
        ),
      ),
    );
  }

  String _text() {
    return _selectedDate != null ? DateFormat("dd.MM.y").format(_selectedDate) : 'Дата рождения';
  }

  TextStyle _textStyle() {
    return TextStyle(
      fontFamily: 'PFDin',
      fontWeight: FontWeight.w500,
      fontSize: 26.0,
      color: _selectedDate != null ? Colors.black : Colors.grey.shade600,
    );
  }

  Widget _clearButton() {
    return IconButton(
      icon: Icon(Icons.clear),
      color: Colors.grey.shade800,
      onPressed: () {
        widget.controller.setValue(null);
      },
    );
  }

  Future<DateTime> _selectDate(BuildContext context) async {
    return Platform.isIOS ? _showIOSPicker() : _showAndroidPicker();
  }

  Future<DateTime> _showAndroidPicker() async {
    return showDatePicker(
      context: context,
      initialDate: _selectedDate != null ? _selectedDate : DateTime(2000),
      firstDate: DateTime(1940),
      lastDate: DateTime.now(),
    );
  }

  Future<DateTime> _showIOSPicker() {
    DateTime selectedDate = _selectedDate;
    return showCupertinoModalPopup(
      context: context,
      builder: (context) => CupertinoActionSheet(
        actions: [
          SizedBox(
            height: 180,
            child: CupertinoDatePicker(
              minimumYear: 1940,
              maximumYear: DateTime.now().year,
              initialDateTime: _selectedDate,
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (dateTime) {
                selectedDate = dateTime;
              },
            ),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('Готово'),
          onPressed: () {
            Navigator.of(context).pop(selectedDate);
          },
        ),
      ),
    );
  }
}
