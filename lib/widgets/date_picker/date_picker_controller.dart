class DatePickerController {
  final List<Function(DateTime)> _listeners = [];

  DateTime value;

  void addListener(Function(DateTime) listener) {
    _listeners.add(listener);
  }

  void setValue(DateTime dateTime) {
    this.value = dateTime;
    _listeners.forEach((element) {
      element.call(value);
    });
  }
}
