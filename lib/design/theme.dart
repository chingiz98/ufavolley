import 'package:flutter/material.dart';
import 'package:ufavolley/constants.dart';

import 'colors.dart';

class AppTheme {
  static final light = ThemeData(
    primaryColor: AppColors.primary,
    primarySwatch: Constants.primaryColorShades,
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        backgroundColor: Colors.transparent,
      ),
    ),
  );
}
