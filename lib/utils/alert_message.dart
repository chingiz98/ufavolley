import 'package:flutter/widgets.dart';

class AlertMessage {
  final String title;
  final String message;
  const AlertMessage({@required this.title, @required this.message});
}