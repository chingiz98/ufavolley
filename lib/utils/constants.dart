class C {
  /// Уровень больше не используется. Сохранили для мапинга в Лайт+ pro
  static const legacyMediumPlusLevelId = "10wUQYexD94OP9PeeoUy";

  static const litePlusProLevelName = "Лайт Pro";
}