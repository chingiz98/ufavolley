import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:ufavolley/models/filter_object.dart';
import 'package:flutter/widgets.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/base/base_view.dart';

class ResourceManager {
  static final ResourceManager _instance = ResourceManager._internal();
  final Map<CloudResource, CloudResourceContainer> containers = Map();
  List<Training> trainings = [];
  List<NotificationProp> notifications = [];
  User currentUser = User();
  FilterObject filterObject;
  TrainingStub bottomTrainingStub;
  static bool isInitialized = false;

  List<BaseView> _subscribers = <BaseView>[];

  factory ResourceManager() {
    return _instance;
  }

  ResourceManager._internal();

  get() => currentUser;

  CloudResourceContainer getCloudResourceContainerByName(CloudResource resourceType) {
    return containers[resourceType];
  }

  Future<void> init() async {
    if (!isInitialized) {
      var observable = await Future.wait([
        APISupport.getUserData(),
        APISupport.getTrainingsV2(),
        APISupport.getNotifications(),
        _loadValues('types'),
        _loadValues('titles'),
        _loadValues('addreses'),
        _loadValues('levels')
      ]).then((List responses) => _initApplicationData(responses)).catchError((e) {
        print("!!!!!!!INIT_ERROR " + e.toString());
      });
      isInitialized = true;
      filterObject = FilterObject();
      return observable;
    }
    return Future.value();
  }

  Future<Map<String, List<CloudValue>>> _loadValues(String collectionName) async {
    Map<String, List<CloudValue>> result = await APISupport.getValues(collectionName: collectionName);
    return result;
  }

  void _initApplicationData(List responses) {
    print("initAppData called with list length = " + responses.length.toString());

    for (var response in responses) {
      if (response is User) {
        currentUser = response;
        continue;
      }

      if (response is List<Training>) {
        trainings = response;
        print('trainings length is ' + trainings.length.toString());
        continue;
      }

      if (response is List<NotificationProp>) {
        notifications = response;
        continue;
      }

      for (var key in response.keys) {
        CloudResourceContainer container = CloudResourceContainer(collectionName: key);
        container.values.addAll(response[key]);
        containers[_getCloudResourceType(key)] = container;
      }
    }
  }

  void _updateViews() {
    for (var s in _subscribers) {
      s.updateState();
    }
  }

  CloudResource _getCloudResourceType(String name) {
    switch (name) {
      case 'types':
        return CloudResource.TRAINING_TYPE;
      case 'titles':
        return CloudResource.TRAINING_TITLE;
      case 'addreses':
        return CloudResource.ADDRESS;
      case 'levels':
        return CloudResource.LEVEL;
    }
    throw Exception('_getCloudResourceType method exception: CloudResource with \'' + name + '\' name not found');
  }

  Future<int> refreshNotifications() async {
    notifications = await APISupport.getNotifications();
    _updateViews();

    return 0;
  }

  void readAllNotifications() {
    notifications.forEach((notification) => notification.read = true);
    _updateViews();
  }

  void unsubscribeFromUpdates(BaseView view) {
    _subscribers.remove(view);
  }

  void signForUpdates(BaseView view) {
    _subscribers.add(view);
  }

  void processTraining(Training tr) {
    for (int i = 0; i < trainings.length; i++) {
      if (trainings[i].trainingID == tr.trainingID) {
        trainings[i] = tr;
      }
    }
    _updateViews();
  }
}

class CloudResourceContainer {
  final String collectionName;
  final List<CloudValue> values = [];

  CloudResourceContainer({@required this.collectionName});

  Map<String, CloudValue> getValuesMap() {
    Map<String, CloudValue> m = new Map();

    for (var v in values) {
      m[v.id] = v;
    }

    return m;
  }
}

/// {value} хранит название коллекции в базе данных, соответствующую данному типу

class CloudResource {
  final value;

  const CloudResource._internal(this.value);

  toString() => 'Enum.$value';

  static const TRAINING_TYPE = const CloudResource._internal('types');
  static const TRAINING_TITLE = const CloudResource._internal('titles');
  static const ADDRESS = const CloudResource._internal('addreses');
  static const LEVEL = const CloudResource._internal('levels');
  static const TRAINERS = const CloudResource._internal('trainers');
  static const UNKNOWN = const CloudResource._internal('unknown');
}
