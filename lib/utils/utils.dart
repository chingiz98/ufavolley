import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ufavolley/constants.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/constants.dart';

class CustomButton extends StatefulWidget {
  final String text;
  final double size;
  final EdgeInsetsGeometry margin;
  final LinearGradient gradient;
  final Color strokeColor;
  final Color textColor;
  final Alignment textAlignment;
  final BorderRadius borderRadius;
  final double relativeWidth;
  final EdgeInsetsGeometry textMargin;
  final Border border;
  final Function onTap;
  final bool textSizedByContainer;
  final bool enabled;

  const CustomButton(
      {@required this.text,
      this.size,
      this.margin,
      this.gradient,
      this.strokeColor,
      this.textColor,
      this.textAlignment,
      this.borderRadius,
      this.relativeWidth,
      this.textMargin,
      this.border,
      this.onTap,
      this.textSizedByContainer,
      this.enabled = true});

  @override
  CustomButtonState createState() => new CustomButtonState();
}

class CustomButtonState extends State<CustomButton> {
  bool enabled;

  @override
  Widget build(BuildContext context) {
    enabled = widget.enabled;

    double percentWidth = 0.8;

    if (widget.relativeWidth != null) percentWidth = widget.relativeWidth;

    double containerWidth = MediaQuery.of(context).size.width * percentWidth;

    double textSize = 32.0;

    if (widget.size != null) textSize = widget.size;

    EdgeInsetsGeometry margin = EdgeInsets.all(6.0);

    if (widget.margin != null) margin = widget.margin;

    LinearGradient gradient = new LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, stops: [
      0.1,
      0.3,
      0.5,
      0.7,
      0.9
    ], colors: [
      Colors.blue,
      Color.fromARGB(255, 58, 111, 196),
      Color.fromARGB(255, 5, 71, 178),
      Color.fromARGB(255, 58, 111, 196),
      Colors.blue
    ]);

    if (widget.gradient != null) gradient = widget.gradient;

    Color strokeColor = Colors.indigo;

    if (widget.strokeColor != null) strokeColor = widget.strokeColor;

    Color textColor = Colors.white;

    if (widget.textColor != null) textColor = widget.textColor;

    Alignment alignment = Alignment.center;

    if (widget.textAlignment != null) alignment = widget.textAlignment;

    BorderRadius borderRadius = new BorderRadius.all(Radius.circular(30));

    if (widget.borderRadius != null) borderRadius = widget.borderRadius;

    EdgeInsetsGeometry textMargin = EdgeInsets.all(0.0);

    if (widget.textMargin != null) textMargin = widget.textMargin;

    Border border = Border.all(color: Colors.indigo, width: 2.0);

    if (widget.border != null) {
      border = widget.border;
    }

    EdgeInsets padding = const EdgeInsets.only(top: 8.0, bottom: 4.0);

    TextStyle textStyle =
        TextStyle(color: textColor, fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: textSize);

    Color highlightColor = Colors.blue;
    Color splashColor = Colors.indigo;

    Function onTap = () {};

    if (widget.onTap != null && widget.enabled) {
      onTap = widget.onTap;
    } else {
      gradient = new LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, stops: [
        0.1,
        0.3,
        0.5,
        0.7,
        0.9
      ], colors: [
        Color.fromARGB(255, 191, 191, 191),
        Color.fromARGB(255, 166, 166, 166),
        Color.fromARGB(255, 153, 153, 153),
        Color.fromARGB(255, 166, 166, 166),
        Color.fromARGB(255, 191, 191, 191),
      ]);
      border = Border.all(color: Colors.black54, width: 2.0);

      highlightColor = Colors.white12;
      splashColor = Colors.white10;
    }

    return Container(
        width: containerWidth,
        height: 48,
        decoration: BoxDecoration(gradient: gradient, borderRadius: borderRadius, border: border),
        margin: margin,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: borderRadius,
            highlightColor: highlightColor,
            splashColor: splashColor,
            child: Container(
              padding: padding,
              alignment: alignment,
              margin: textMargin,
              child: FittedBox(
                child: Text(
                  widget.text,
                  style: textStyle,
                ),
              ),
            ),
            onTap: () {
              print('tap on button with enabled = ' + enabled.toString());
              if (enabled) {
                onTap();
              }
            },
          ),
        ));
  }
}

class CustomTextField extends StatefulWidget {
  final EdgeInsetsGeometry margin;
  final String hint;
  final TextEditingController controller;
  final bool isImportant;
  final TextInputType textInputType;
  final List<TextInputFormatter> formatters;

  const CustomTextField(
      {this.hint = "",
      @required this.controller,
      this.margin = const EdgeInsets.all(0),
      this.isImportant = false,
      this.textInputType = TextInputType.text,
      this.formatters = const []});

  @override
  CustomTextFieldState createState() => new CustomTextFieldState();
}

class CustomTextFieldState extends State<CustomTextField> {
  TextStyle textFieldStyle = TextStyle(
    fontFamily: 'PFDin',
    fontWeight: FontWeight.w500,
    fontSize: 26.0,
    color: Colors.black,
  );

  @override
  Widget build(BuildContext context) {
    TextField textField = new TextField(
      controller: widget.controller,
      style: textFieldStyle,
      decoration: InputDecoration(border: InputBorder.none, hintText: widget.hint),
      inputFormatters: widget.formatters,
      keyboardType: widget.textInputType,
    );

    Container textBox = new Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30.0),
        ),
        padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
        margin: widget.margin,
        child: new Center(child: textField));
    return widget.isImportant ? _createImportantField(textBox) : _createCustomField(textBox);
  }
}

class CustomDropDownButton extends StatefulWidget {
  final EdgeInsetsGeometry margin;

  final Function onChanged;

  final String initialValue;

  const CustomDropDownButton({this.margin = const EdgeInsets.all(0), this.onChanged, this.initialValue});

  @override
  CustomDropDownButtonState createState() => new CustomDropDownButtonState();
}

class CustomDropDownButtonState extends State<CustomDropDownButton> {
  String value;
  List<String> values = [];

  @override
  void initState() {
    values.addAll(['Мужской', 'Женский']);

    value = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var cont = Container(
      padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
      margin: widget.margin,
      width: MediaQuery.of(context).size.width * 0.8,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Center(
        child: DropdownButtonHideUnderline(
            child: DropdownButton(
          isExpanded: true,
          style: new TextStyle(
            fontFamily: 'PFDin',
            fontWeight: FontWeight.w500,
            fontSize: 26.0,
            color: Colors.black,
          ),
          value: value,
          hint: new Text('Пол'),
          items: values.map((String value) {
            return new DropdownMenuItem(
              value: value,
              child: new Text(value),
            );
          }).toList(),
          onChanged: (String value) {
            onChanged(value);
          },
        )),
      ),
    );

    return _createImportantField(cont);
  }

  void onChanged(String value) {
    setState(() {
      this.value = value;
      widget.onChanged(value);
    });
  }
}

Container _createCustomField(Widget textField) {
  return Container(
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      //alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: 10,
        ),
        Expanded(child: textField),
        Container(
          width: 10,
        )
      ],
    ),
  );
}

Container _createImportantField(Widget textField) {
  return Container(
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      //alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: 10,
          child: Image(image: AssetImage('assets/important_field.png')),
        ),
        Expanded(child: textField),
        Container(
          width: 10,
        )
      ],
    ),
  );
}

class DashLineDivider extends StatelessWidget {
  final double height;
  final Color color;
  final EdgeInsetsGeometry margin;
  final double relativeWidth;

  const DashLineDivider(
      {this.height = 1,
      this.color = Colors.black,
      this.margin = const EdgeInsets.only(top: 8.0, bottom: 8.0),
      this.relativeWidth = 1});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 4.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return new Container(
          margin: margin,
          width: MediaQuery.of(context).size.width,
          child: Flex(
            children: List.generate(dashCount, (_) {
              return new Container(
                margin: EdgeInsets.only(left: 2.0),
                child: SizedBox(
                  width: dashWidth,
                  height: dashHeight,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: color),
                  ),
                ),
              );
            }),
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            direction: Axis.horizontal,
          ),
        );
      },
    );
  }
}

class DottedDivider extends StatelessWidget {
  final double height;
  final Color color;

  const DottedDivider({this.height = 1, this.color = const Color.fromARGB(255, 102, 102, 102)});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 2.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return new Container(
          margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
          child: Flex(
            children: List.generate(dashCount, (_) {
              return new Container(
                margin: EdgeInsets.only(left: 2.0),
                child: SizedBox(
                  width: dashWidth - 1.0,
                  height: dashHeight,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: color),
                  ),
                ),
              );
            }),
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            direction: Axis.horizontal,
          ),
        );
      },
    );
  }
}

var months = [
  'января',
  'февраля',
  'марта',
  'апреля',
  'мая',
  'июня',
  'июля',
  'августа',
  'сентября',
  'октября',
  'ноября',
  'декабря'
];

String convertToLevelsFormat(List<CloudValue> levels) {
  final List<String> levelsNames = levels.map((e) => e.getValue + '/').toList();

  /// Меняем местами Лайт Про и Медиум, т.к. у медиума приоритет выше, но с бэка приходит в неверном порядке
  if (levelsNames.contains("${C.litePlusProLevelName}/") && levelsNames.contains('Медиум/')) {
    final litePlusLevelPosition = levelsNames.indexOf("${C.litePlusProLevelName}/");
    final mediumLevelPosition = levelsNames.indexOf('Медиум/');
    levelsNames[litePlusLevelPosition] = 'Медиум/';
    levelsNames[mediumLevelPosition] = "${C.litePlusProLevelName}/";
  }

  final result = levelsNames.join();

  return result.substring(0, result.length - 1);
}

String convertToDateFormat(DateTime oldDateTime) {
  DateTime newDateTime = getTimeWithUfaTimezone(oldDateTime);
  return newDateTime.day.toString() + ' ' + months[newDateTime.month - 1] + ' ' + newDateTime.year.toString();
}

String convertToTimeFormat(DateTime oldDateTime) {
  DateTime newDateTime = getTimeWithUfaTimezone(oldDateTime);
  StringBuffer buffer = new StringBuffer();
  buffer.write(newDateTime.hour.toString());
  buffer.write(':');
  if (newDateTime.minute < 10)
    buffer.write('0' + newDateTime.minute.toString());
  else
    buffer.write(newDateTime.minute.toString());
  return buffer.toString();
}

Future<bool> showLoadingAlert(BuildContext context) {
  Widget content = WillPopScope(
    child: Container(
      color: Colors.transparent,
      child: Center(
        child: Container(
          width: 100,
          height: 100,
          child: CircularProgressIndicator(),
        ),
      ),
    ),
    onWillPop: () async => false,
  );

  return showDialog(
        context: context,
        builder: (context) => content,
      ) ??
      false;
}

Future<bool> showErrorAlert(BuildContext context, String message) {
  return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: Text(message),
          title: Text('Ошибка', style: TextStyle(color: Colors.redAccent)),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Закрыть'),
            ),
          ],
        ),
      ) ??
      false;
}

String getHours(DateTime dateTime) {
  return dateTime.hour > 9 ? dateTime.hour.toString() : '0' + dateTime.hour.toString();
}

String getMinutes(DateTime dateTime) {
  return dateTime.minute > 9 ? dateTime.minute.toString() : '0' + dateTime.minute.toString();
}

String getTime(int timeValue) {
  int minutes = (timeValue ~/ 60).toInt();
  int seconds = timeValue % 60;
  String times = minutes.toString() + ':';
  if (seconds < 10) {
    times = times + '0' + seconds.toString();
  } else {
    times = times + seconds.toString();
  }
  return times;
}

DateTime getTimeWithUfaTimezone(DateTime oldDateTime) {
  Duration ufaTimezoneOffset = Duration(hours: 5);
  Duration diffTimezone = ufaTimezoneOffset - oldDateTime.timeZoneOffset;
  DateTime newDateTime = oldDateTime.add(diffTimezone);
  return newDateTime;
}

String getLevelNameById(String id) {
  ResourceManager resourceManager = ResourceManager();
  List<CloudValue> levels = resourceManager.containers[CloudResource.LEVEL].values;
  for (int i = 0; i < levels.length; i++) {
    if (levels[i].id == id) return levels[i].getValue;
  }
  return 'error';
}
