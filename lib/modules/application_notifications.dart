import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart' as fa;
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/utils/resource_manager.dart';

/*
Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  print('onBackgroundMsg');
  if (message.containsKey('data')) {
    if (message['data']['training'] != null) {
      var obj = JsonDecoder().convert(message['data']['training']);
      print('parsed');
      Parser parser = Parser();
      Training tr = parser.parseTraining(obj);
      ResourceManager().processTraining(tr);
    }
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}
*/

class ApplicationNotifications {
  FirebaseMessaging msg = FirebaseMessaging.instance;
  NotificationsListener listener;

//TODO РАССКОММЕНТИТЬ ПОД ANDROID
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  ApplicationNotifications(NotificationsListener listener) {
    this.listener = listener;

    //TODO РАССКОММЕНТИТЬ ПОД ANDROID

    if (Platform.isAndroid) {
      flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
      var android = AndroidInitializationSettings('@mipmap/launcher_icon');
      var iOS = IOSInitializationSettings();
      var initSettings = InitializationSettings(android: android, iOS: iOS);
      flutterLocalNotificationsPlugin.initialize(initSettings, onSelectNotification: onSelectNotification);
    }

    if (Platform.isIOS) IOS_Permission();

    /*msg.configure(
      onMessage: (Map<String, dynamic> message) {
        print('OnMessage');
        if (Platform.isAndroid) {
          if (message['data']['training'] != null) {
            var obj = JsonDecoder().convert(message['data']['training']);
            print('parsed');
            Parser parser = Parser();
            Training tr = parser.parseTraining(obj);
            ResourceManager().processTraining(tr);
          } else {
            print('Cloud messaging onMessage ' + message.toString());
            var prop = NotificationProp(
                title: message['data']['title'],
                subTitle: message['data']['body'],
                id: message['data']['notification_id'],
                trainingID: message['data']['training_id']);
            int type = int.parse(message['data']['type']);
            showNotification(message['notification']['title'], message['notification']['body'], prop, type);
          }
        } else {
          print('OnMessage IOS');

          if (message['training'] != null) {
            var obj = JsonDecoder().convert(message['training']);
            print('parsed');
            Parser parser = Parser();
            Training tr = parser.parseTraining(obj);
            ResourceManager().processTraining(tr);
          } else {
            print('Cloud messaging onMessage ' + message.toString());
            var prop = NotificationProp(
                title: message['title'],
                subTitle: message['body'],
                id: message['notification_id'],
                trainingID: message['training_id']);
            int type = int.parse(message['type']);
            listener.onNewNotification(prop, type);
            // ignore: missing_return
          }
        }
        return null;
      },
      onResume: (Map<String, dynamic> message) {
        if (Platform.isAndroid) {
          print("OnResume push android");
          if (message['data']['training'] != null) {
            var obj = JsonDecoder().convert(message['data']['training']);
            print('parsed');
            Parser parser = Parser();
            Training tr = parser.parseTraining(obj);
            ResourceManager().processTraining(tr);
          } else {
            print('Cloud messaging onResume ' + message.toString());
            int type = int.parse(message['data']['type']);
            var prop = NotificationProp(
                title: message['data']['title'],
                subTitle: message['data']['body'],
                id: message['data']['notification_id'],
                trainingID: message['data']['training_id']);
            listener.onNewNotification(prop, type);
          }
        } else {
          print('Cloud messaging onResume ' + message.toString());
          int type = int.parse(message['type']);
          var prop = NotificationProp(
              title: message['title'],
              subTitle: message['body'],
              id: message['notification_id'],
              trainingID: message['training_id']);
          listener.onNewNotification(prop, type);
        }
        return null;
      },
      onLaunch: (Map<String, dynamic> message) {
        if (Platform.isAndroid) {
          print("OnLaunch push android");
          if (message['data']['training'] != null) {
            var obj = JsonDecoder().convert(message['data']['training']);
            print('parsed');
            Parser parser = Parser();
            Training tr = parser.parseTraining(obj);
            ResourceManager().processTraining(tr);
          } else {
            print('Cloud messaging onLaunch ' + message.toString());
            int type = int.parse(message['data']['type']);
            var prop = NotificationProp(
                title: message['data']['title'],
                subTitle: message['data']['body'],
                id: message['data']['notification_id'],
                trainingID: message['data']['training_id']);
            listener.onNewNotification(prop, type);
          }
        } else {
          print('Cloud messaging onLaunch ' + message.toString());
          int type = int.parse(message['type']);
          var prop = NotificationProp(
              title: message['title'],
              subTitle: message['body'],
              id: message['notification_id'],
              trainingID: message['training_id']);
          listener.onNewNotification(prop, type);
        }
        return null;
      },
    );*/

    //_connectToRealTimeDatabase();
  }

  void _connectToRealTimeDatabase() async {
    //TODO FOR ANDROID ONLY
    fa.FirebaseAuth _auth = fa.FirebaseAuth.instance;
    fa.User user = _auth.currentUser;
    String fcm = await FirebaseMessaging.instance.getToken();

    FirebaseDatabase db = FirebaseDatabase.instance;
    var itemRef = db.reference().child('test/' + fcm);
    itemRef.update({
      'uid': user.uid,
    });

    var isOfflineForDatabase = {'state': 'offline'};

    var isOnlineForDatabase = {'state': 'online'};

    var userStatusDatabaseRef = db.reference().child('/status/' + fcm);
    db.reference().child('.info').child('connected').onValue.listen((data) {
      if (data.snapshot.value == false) {
        return;
      }
      userStatusDatabaseRef.onDisconnect().set(isOfflineForDatabase).then((_) {
        userStatusDatabaseRef.set(isOnlineForDatabase);
      });
    });
  }

  void IOS_Permission() {
    msg.requestPermission(sound: true, badge: true, alert: true);
    /*msg.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });*/
  }

  Future<void> onSelectNotification(String payload) {
    print('on click');

    Map payloadMap = json.decode(payload);

    String title = payloadMap['title'];
    String body = payloadMap['body'];
    String notificationID = payloadMap['notification_id'];
    String trainingID = payloadMap['training_id'];
    int type = int.parse(payloadMap['type']);

    NotificationProp prop = NotificationProp(title: title, subTitle: body, id: notificationID, trainingID: trainingID);

    listener.onNewNotification(prop, type);
  }

  //TODO РАССКОММЕНТИТЬ ПОД ANDROID

  Future<int> showNotification(String title, String body, NotificationProp prop, int type) async {
    var android = AndroidNotificationDetails('channel id', 'channel NAME', channelDescription: 'CHANNEL DESCRIPTION');
    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(
      android: android,
      iOS: iOS,
    );
    Map<String, String> payloadMap = {
      'title': title,
      'body': body,
      'type': type.toString(),
      'notification_id': prop.id,
      'training_id': prop.trainingID,
    };
    await flutterLocalNotificationsPlugin.show(0, title, body, platform, payload: json.encode(payloadMap));
    return 0;
  }
}

abstract class NotificationsListener {
  void onNewNotification(NotificationProp notificationProp, int type);
}
