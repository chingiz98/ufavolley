import 'dart:convert';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/modules/application_notifications.dart';

class Notifications {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  final AndroidNotificationChannel androidNotificationChannel = AndroidNotificationChannel(
    'channel id',
    'channel NAME',
    description: 'CHANNEL DESCRIPTION',
    importance: Importance.max,
  );

  final AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
    'channel id',
    'channel NAME',
    channelDescription: 'CHANNEL DESCRIPTION',
    importance: Importance.max,
    priority: Priority.high,
  );

  final IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

  NotificationsListener listener;

  void initialize(NotificationsListener listener) async {
    this.listener = listener;

    if (Platform.isIOS) {
      FirebaseMessaging.instance.requestPermission(sound: true, badge: true, alert: true);
    }

    _createNotificationsChannel();
    await _initializeLocalNotifications();
    _startListenForegroundNotifications();
  }

  Future<void> _createNotificationsChannel() async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(androidNotificationChannel);
  }

  Future<void> _initializeLocalNotifications() async {
    const initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/launcher_icon');
    const initializationSettingsIOS = IOSInitializationSettings();
    const initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: onSelectNotification,
    );
  }

  void _startListenForegroundNotifications() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      if (message.notification != null) {
        if (Platform.isAndroid) {
          showNotification(message);
        } else {
          var prop = _fromMessage(message);
          int type = int.parse(message.data['type']);
          listener.onNewNotification(prop, type);
        }
        print('Message also contained a notification: ${message.notification}');
      }
    });
  }

  void startListenBackgroundNotifications() {
    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  }

  Future<int> showNotification(RemoteMessage remoteMessage) async {
    final platform = NotificationDetails(
      android: androidNotificationDetails,
      iOS: iosNotificationDetails,
    );

    final title = remoteMessage.notification.title;
    final body = remoteMessage.notification.body;

    var prop = _fromMessage(remoteMessage);
    int type = int.parse(remoteMessage.data['type']);

    Map<String, String> payloadMap = {
      'title': title,
      'body': body,
      'type': type.toString(),
      'notification_id': prop.id,
      'training_id': prop.trainingID,
    };
    await flutterLocalNotificationsPlugin.show(0, title, body, platform, payload: json.encode(payloadMap));
    return 0;
  }

  void onSelectNotification(String payload) {
    print('on click');

    Map payloadMap = json.decode(payload);

    String title = payloadMap['title'];
    String body = payloadMap['body'];
    String notificationID = payloadMap['notification_id'];
    String trainingID = payloadMap['training_id'];
    int type = int.parse(payloadMap['type']);

    NotificationProp prop = NotificationProp(title: title, subTitle: body, id: notificationID, trainingID: trainingID);

    listener.onNewNotification(prop, type);
  }

  NotificationProp _fromMessage(RemoteMessage message) {
    return NotificationProp(
      title: message.data['title'],
      subTitle: message.data['body'],
      id: message.data['notification_id'],
      trainingID: message.data['training_id'],
    );
  }
}

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("Handling a background message: ${message.messageId}");
}
