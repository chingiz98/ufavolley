import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';

abstract class ApplicationViewContract extends BaseView {
  void onInit(bool isLoggedIn);
  void showOutdatedDialog();
}

abstract class ApplicationPresenterContract extends BasePresenter {
  //void tryShowOutdatedDialog();
}