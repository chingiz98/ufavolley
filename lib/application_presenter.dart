import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufavolley/application_contract.dart';

class ApplicationPresenter extends ApplicationPresenterContract {

  ApplicationViewContract _view;
  SharedPreferences _sharedPreferences;

  ApplicationPresenter(ApplicationViewContract view) {
    this._view = view;
  }

  void init() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = _sharedPreferences.getBool('logedin') == null ? false : _sharedPreferences.getBool('logedin');
    _view.onInit(isLoggedIn);
  }

}