import 'dart:math';

import 'package:flutter/material.dart';
import 'package:ufavolley/constants.dart';
import 'dart:math' as math;

class SplashBackground extends CustomPainter {

  static const _bias = 0.9;

  SplashBackground({
    @required this.progress,
    @required this.sizeBias,
    @required this.curtainOpacity,
  });

  final double progress;

  final double curtainOpacity;

  final double sizeBias;

  final Paint _linePaint = Paint()
    ..color = Constants.primaryColorShades.shade700
    ..strokeWidth = 100
    ..strokeCap = StrokeCap.round;

  final Paint _line2Paint = Paint()
    ..color = Constants.primaryColorShades.shade700
    ..strokeWidth = 50
    ..strokeCap = StrokeCap.round;

  final _circlePaint = Paint()
    ..style = PaintingStyle.stroke
    ..strokeCap = StrokeCap.round
    ..strokeWidth = 25;

  static const _pointSize = 30.0;

  final _pointPaint = Paint()
    ..strokeCap = StrokeCap.round
    ..color = Colors.white.withAlpha(23)
    ..strokeWidth = _pointSize;

  @override
  void paint(Canvas canvas, Size size) {

    _linePaint..strokeWidth = 100 / _bias * sizeBias;
    _line2Paint..strokeWidth = 50 / _bias * sizeBias;
    _circlePaint..strokeWidth = 25 / _bias * sizeBias;
    _pointPaint..strokeWidth = _pointSize / _bias * sizeBias;

    //_line2Paint..color = Constants.primaryColorShades.shade700.withOpacity(progress);
    //_linePaint..color = Constants.primaryColorShades.shade700.withOpacity(progress);

    _drawDots(
      canvas: canvas,
      size: size,
      progress: progress,
    );

    _drawCircle(
      canvas: canvas,
      center: Offset(size.width - 40, size.height / 3),
      radius: progress * 100,
      startAngle: (-math.pi / 2 * 3 * progress),
    );

    _drawLine(
      canvas: canvas,
      start: Offset(size.width / 2 - 100, 0),
      end: Offset(size.width / 2 + progress * 200, 100 + progress * 50),
      paint: _line2Paint,
    );

    _drawLine(
      canvas: canvas,
      start: Offset(size.width / 2 + progress * 200, 100 + progress * 50),
      end: Offset(size.width / 2 + progress * 200 + 100, 0),
      paint: _line2Paint,
    );

    //drawLeftTriangle(canvas, size);


    _drawLine(
      canvas: canvas,
      start: Offset(-100, 400),
      end: Offset(0 + progress * 100, 300 - progress * 100),
      paint: _linePaint,
    );

    _drawLine(
      canvas: canvas,
      start: Offset(0 + progress * 100, 300 - progress * 100),
      end: Offset(size.width - progress * 100, size.height * 3 / 4),
      paint: _linePaint,
    );

    _drawLine(
      canvas: canvas,
      start: Offset(size.width - progress * 100, size.height * 3 / 4),
      end: Offset(0, size.height * 3 / 4 + progress * 100),
      paint: _linePaint,
    );

    //final curtainOpacity = if (1 - progress )

    final curtainPaint = Paint()
      ..color = Constants.primaryColorShades.shade700
          .withOpacity(curtainOpacity);

    canvas.drawRect(Rect.fromLTWH(0, 0, size.width, size.height), curtainPaint);
  }

  void drawLeftTriangle(Canvas canvas, Size size) {
    /*_drawLine(
      canvas: canvas,
      start: Offset(-100, 400),
      end: Offset(0 + progress * 100, 300 - progress * 100),
      paint: _linePaint,
    );

    _drawLine(
      canvas: canvas,
      start: Offset(0 + progress * 100, 300 - progress * 100),
      end: Offset(size.width - progress * 100, size.height * 3 / 4),
      paint: _linePaint,
    );

    _drawLine(
      canvas: canvas,
      start: Offset(size.width - progress * 100, size.height * 3 / 4),
      end: Offset(0, size.height * 3 / 4 + progress * 100),
      paint: _linePaint,
    );*/

    Paint paint = Paint()
      ..color = Constants.primaryColorShades.shade700
      ..strokeWidth = 50
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    final path = new Path();

/*
    path.addPolygon(
      [
        Offset(-100, 400),
        Offset(0 + progress * 100, 300 - progress * 100),
        Offset(size.width - progress * 100, size.height * 3 / 4),
        Offset(0, size.height * 3 / 4 + progress * 100),
      ],
      true,
    );
    */
    //path.moveTo(-100, 400);

    path.moveTo(-100, 400);
    path.lineTo(0 + progress * 100, 300 - progress * 100);
    path.lineTo(size.width - progress * 100, size.height * 3 / 4);
    path.lineTo(0, size.height * 3 / 4 + progress * 100);
    //path.lineTo(size.width - progress * 100, size.height * 3 / 4);

    canvas.drawPath(path, paint);
  }

  void _drawLine({
    Canvas canvas,
    Offset start,
    Offset end,
    Paint paint,
  }) {
    canvas.drawLine(
      start,
      end,
      paint,
    );
  }

  void _drawCircle({
    Canvas canvas,
    Offset center,
    double radius,
    double startAngle,
  }) {
    final rect = Rect.fromCenter(center: center, width: 200, height: 150);
    final sweepAngle = math.pi * 3 / 2;
    final useCenter = false;

    final gradient = SweepGradient(
      startAngle: startAngle,
      endAngle: startAngle + math.pi * 1.9,
      tileMode: TileMode.repeated,
      colors: [Colors.white.withOpacity(progress / 50), Colors.white.withAlpha(0)],
    );

    _circlePaint..shader = gradient.createShader(rect);

    canvas.drawArc(rect, startAngle, sweepAngle, useCenter, _circlePaint);
  }

  void _drawDots({
    Canvas canvas,
    Size size,
    double progress,
  }) {

    final pointsInRow = 10;
    final pointsInColumn = 10;

    final offsetBetween = 10.0 + (_pointSize / _bias * sizeBias);

    final xStart = size.width - (pointsInRow * offsetBetween) + offsetBetween;
    final yStart = size.height - (pointsInColumn * offsetBetween) + offsetBetween;

    final List<Offset> dots = [];

    for (int i = 0; i < pointsInColumn; i++) {
      for (int j = 0; j < pointsInRow; j++) {
        dots.add(Offset(xStart + j * offsetBetween, yStart + i * offsetBetween));
      }
    }

    for (int i = 0; i < pointsInRow * pointsInColumn; i++) {
      canvas.drawCircle(dots[i], _radiusForPoint(i, progress), _pointPaint);
    }
  }

  double _radiusForPoint(int index, double progress) {
    final nonZeroProgress = progress > 0 ? progress : 0.01;
    final normalizedProgress = nonZeroProgress * 100 * 1.5;
    final radius = 2 * index / normalizedProgress;
    if (radius > _pointSize) {
      return _pointSize;
    } else {
      return radius;
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
