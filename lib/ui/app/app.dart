import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:ufavolley/design/theme.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/views/pages/registration/welcome/view/welcome_page.dart';
import 'package:firebase_auth/firebase_auth.dart' as fa;

import '../../api/api_support.dart';
import '../../application_contract.dart';
import '../../application_presenter.dart';
import '../../main/main_page.dart';
import '../../utils/alert_message.dart';
import '../../views/pages/splash_screen/splash_screen.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<App> implements ApplicationViewContract {
  ApplicationPresenter _presenter;
  bool _isLoggedIn;
  bool _isAppPrepared;
  bool _showWelcome = false;

  @override
  void initState() {
    super.initState();
    _isLoggedIn = false;
    _isAppPrepared = false;
    _presenter = ApplicationPresenter(this);
    _presenter.init();
  }

  @override
  Widget build(BuildContext context) {
    var routes = <String, WidgetBuilder>{
      '/main': (BuildContext context) => MainPage(),
      '/login': (BuildContext context) => WelcomePage()
    };

    var home;

    if (_isAppPrepared) {
      home = MainPage();
    } else {
      if (_isLoggedIn) {
        home = SplashScreen(
            futures: [ResourceManager().init(), checkVersion(), _updateToken()],
            dataListener: (List responses) {
              _manageResponse(responses);
            });
      } else {
        if (_showWelcome)
          home = WelcomePage();
        else
          home = Scaffold(
            body: Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('assets/background.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          );
      }
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      title: 'UfaVolley',
      routes: routes,
      theme: AppTheme.light,
      home: home,
    );
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }

  @override
  void onInit(bool isLoggedIn) {
    setState(() {
      this._isLoggedIn = isLoggedIn;
      this._showWelcome = true;
    });
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    // TODO: implement showError
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  void _manageResponse(List responses) {
    /*
    for (var response in responses) {
      if (response is bool) {
        bool isOutdated = response;
        if (isOutdated) {
          _presenter.tryShowOutdatedDialog();
        }
      }
    }

     */
    setState(() {
      _isAppPrepared = true;
    });
  }

  Future<bool> checkVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;
    return await APISupport.checkVersion(version: version);
  }

  Future<void> _updateToken() async {
    String fcmToken = await FirebaseMessaging.instance.getToken();
    fa.User us = fa.FirebaseAuth.instance.currentUser;
    HttpsCallable callable = FirebaseFunctions.instance.httpsCallable('updateUser');
    await callable.call(<String, dynamic>{'fcm_update': true, 'fcm': fcmToken, 'id': us.uid});
  }

  @override
  void showOutdatedDialog() {
    // TODO: implement showOutdatedDialog
  }

/*
  @override
  void showOutdatedDialog() {
    showDialog(
      context: context,
      builder: (context) =>
      new AlertDialog(
        title: new Text('Внимание!'),
        content: new Text(
            'Версия вашего приложения устарела! Обновите приложение во избежание возникновения ошибок.'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('OK'),
          ),
        ],
      ),
    );
  }

   */
}
