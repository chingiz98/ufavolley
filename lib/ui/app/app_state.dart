import 'package:flutter/cupertino.dart';

abstract class AppState {}

class Progress extends AppState {}

class AppLoadedSuccess extends AppState {
  final isAppVersionOutdated;

  AppLoadedSuccess({
    @required this.isAppVersionOutdated,
  });
}

class AppLoadedFailed extends AppState {}
