import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufavolley/api/etc/etc_api.dart';
import 'package:ufavolley/repository/app_config/app_config_repository.dart';
import 'package:ufavolley/ui/app/app_state.dart';
import 'package:firebase_auth/firebase_auth.dart' as fa;
import '../../utils/resource_manager.dart';

class AppCubit extends Cubit<AppState> {
  final AppConfigRepository appConfigRepository;
  final EtcApi etcApi;

  AppCubit({
    @required this.appConfigRepository,
    @required this.etcApi,
  }) : super(Progress());

  void initialize() async {
    try {
      await Firebase.initializeApp();
      final appVersion = await appConfigRepository.getVersion();
      await Future.wait(
        [
          ResourceManager().init(),
          etcApi.isAppVersionOutdated(appVersion),
          _updateToken(),
        ],
      );
    } catch (e) {
      emit(AppLoadedFailed());
    }
  }

  Future<void> _updateToken() async {
    String fcmToken = await FirebaseMessaging.instance.getToken();
    fa.User us = fa.FirebaseAuth.instance.currentUser;
    HttpsCallable callable = FirebaseFunctions.instance.httpsCallable('updateUser');
    await callable.call(<String, dynamic>{'fcm_update': true, 'fcm': fcmToken, 'id': us.uid});
  }
}
