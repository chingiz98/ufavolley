import 'package:ufavolley/utils/alert_message.dart';

abstract class BaseView {
  void showLoading();
  void hideLoading();
  void showAlert(AlertMessage message);
  void showError(AlertMessage message);
  void updateState();
  void hideView();
}
