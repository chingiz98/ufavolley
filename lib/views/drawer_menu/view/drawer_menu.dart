import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/main/main_contract.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';

class DrawerMenu extends StatelessWidget {
  final LinearGradient gradient;
  final Color textColor;
  final Color strokeColor;
  final Function listener;

  const DrawerMenu(
      {this.listener,
      this.textColor = const Color.fromARGB(255, 5, 71, 178),
      this.strokeColor = const Color.fromARGB(255, 0, 184, 230),
      this.gradient = const LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, stops: [
        0.1,
        0.3,
        0.5,
        0.7,
        0.9
      ], colors: [
        Color.fromARGB(255, 102, 224, 255),
        Colors.lightBlueAccent,
        Color.fromARGB(255, 0, 184, 230),
        Color.fromARGB(255, 0, 184, 230),
        Color.fromARGB(255, 0, 184, 230),
      ])});

  @override
  Widget build(BuildContext context) {
    int notReadCount = 0;
    ResourceManager().notifications.forEach((notification) => {
          if (!notification.read) {notReadCount++}
        });

    var notBtn = new CustomButton(
      text: 'УВЕДОМЛЕНИЯ',
      margin: EdgeInsets.only(bottom: 16, right: 16),
      gradient: gradient,
      strokeColor: strokeColor,
      textColor: textColor,
      size: 28.0,
      textAlignment: Alignment.centerLeft,
      borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
      relativeWidth: 1.0,
      textMargin: EdgeInsets.only(left: 30),
      border: Border.all(
        color: strokeColor,
        width: 2.0,
      ),
      onTap: () {
        listener(AppPages.Notifications);
        Navigator.pop(context);
      },
    );

    var notBtnExtended;
    if (notReadCount == 0)
      notBtnExtended = notBtn;
    else
      notBtnExtended = Stack(
        clipBehavior: Clip.none,
        children: <Widget>[
          notBtn,
          Positioned(
            top: -6,
            right: 18,
            child: Material(
              borderRadius: BorderRadius.circular(4.0),
              color: Colors.transparent,
              elevation: 6.0,
              child: Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(40),
                ),
                constraints: BoxConstraints(
                  minWidth: 28,
                  minHeight: 20,
                ),
                child: new Text(
                  notReadCount > 9 ? '9+' : notReadCount.toString(),
                  style: new TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          )
        ],
      );

    List<Widget> contents = [
      new Container(
        child: new Image(
          image: new AssetImage('assets/new-drawer-logo.png'),
        ),
        width: 220,
        margin: EdgeInsets.only(top: 50, bottom: 50),
      ),
      new CustomButton(
        text: 'РАСПИСАНИЕ',
        margin: EdgeInsets.only(bottom: 16, right: 16),
        size: 28.0,
        textAlignment: Alignment.centerLeft,
        borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        relativeWidth: 1.0,
        textMargin: EdgeInsets.only(left: 30),
        onTap: () {
          listener(AppPages.Schedule);
          Navigator.pop(context);
        },
      ),
      new CustomButton(
        text: 'ПРАВИЛА',
        margin: EdgeInsets.only(bottom: 16, right: 16),
        gradient: gradient,
        strokeColor: strokeColor,
        textColor: textColor,
        size: 28.0,
        textAlignment: Alignment.centerLeft,
        borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        relativeWidth: 1.0,
        textMargin: EdgeInsets.only(left: 30),
        border: Border.all(
          color: strokeColor,
          width: 2.0,
        ),
        onTap: () {
          listener(AppPages.Rules);
          Navigator.pop(context);
        },
      ),
      new CustomButton(
        text: 'МОЙ ПРОФИЛЬ',
        margin: EdgeInsets.only(bottom: 16, right: 16),
        size: 28.0,
        textAlignment: Alignment.centerLeft,
        borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        relativeWidth: 1.0,
        textMargin: EdgeInsets.only(left: 30),
        onTap: () {
          listener(AppPages.Profile);
          Navigator.pop(context);
        },
      ),
      notBtnExtended,
      new CustomButton(
        text: 'РЕЙТИНГ',
        margin: EdgeInsets.only(bottom: 16, right: 16),
        size: 28.0,
        textAlignment: Alignment.centerLeft,
        borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        relativeWidth: 1.0,
        textMargin: EdgeInsets.only(left: 30),
        onTap: () {
          listener(AppPages.Rating);
          Navigator.pop(context);
        },
      ),
      new CustomButton(
        text: 'КОНТАКТЫ',
        margin: EdgeInsets.only(bottom: 16, right: 16),
        size: 28.0,
        gradient: gradient,
        strokeColor: strokeColor,
        textColor: textColor,
        border: Border.all(
          color: strokeColor,
          width: 2.0,
        ),
        textAlignment: Alignment.centerLeft,
        borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        relativeWidth: 1.0,
        textMargin: EdgeInsets.only(left: 30),
        onTap: () {
          listener(AppPages.Contacts);
          Navigator.pop(context);
        },
      ),
    ];

    if (ResourceManager().currentUser.isAdmin ||
        ResourceManager().currentUser.isMainAdmin ||
        ResourceManager().currentUser.isSupervisor) {
      contents.add(new CustomButton(
        text: 'ПАНЕЛЬ АДМИНА',
        margin: EdgeInsets.only(bottom: 16, right: 16),
        size: 28.0,
        textAlignment: Alignment.centerLeft,
        borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        relativeWidth: 1.0,
        textMargin: EdgeInsets.only(left: 30),
        onTap: () {
          listener(AppPages.ControlPanel);
          Navigator.pop(context);
        },
      ));
    }

    contents.add(
      GestureDetector(
        onTap: () async {
          return showDialog(
                context: context,
                builder: (context1) => AlertDialog(
                  title: Text("Внимание"),
                  content: Text("Вы действительно хотите выйти из аккаунта?"),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.of(context1).pop(false),
                      child: new Text('Нет'),
                    ),
                    TextButton(
                      onPressed: () async {
                        try {
                          await APISupport.signOut(context: context, showLoadingWidget: true);
                          await FirebaseAuth.instance.signOut();
                          SharedPreferences prefs = await SharedPreferences.getInstance();
                          await prefs.setBool("logedin", false);
                          ResourceManager.isInitialized = false;
                          Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                        } catch (e) {
                          print(e.toString());
                        }
                      },
                      child: Text('Да'),
                    ),
                  ],
                ),
              ) ??
              false;
        },
        child: Container(
          margin: EdgeInsets.only(right: 180, bottom: 24),
          alignment: Alignment.centerLeft,
          height: 50,
          width: 90,
          child: Image(
            image: AssetImage('assets/exit.png'),
          ),
        ),
      ),
    );

    return new Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: contents,
        ),
      ),
    );
  }
}
