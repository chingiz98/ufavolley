import 'package:flutter/material.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/dialogs/select_user/presenter/select_supervisor_presenter.dart';
import 'package:ufavolley/views/dialogs/select_user/presenter/select_trainer_presenter.dart';
import 'package:ufavolley/views/dialogs/select_user/select_user_contract.dart';

class SelectUserView extends StatefulWidget {
  final Function listener;
  final String mode;
  static final String modeTrainers = "trainers";
  static final String modeSupervisors = "supervisors";

  SelectUserView(this.mode, {@required this.listener});

  @override
  State<StatefulWidget> createState() {
    return SelectUserViewState();
  }
}

class SelectUserViewState extends State<SelectUserView>
    implements SelectUserViewContract {
  SelectUserPresenterContract _presenter;
  List<User> users;

  @override
  void initState() {
    if(widget.mode == SelectUserView.modeTrainers){
      _presenter = SelectTrainerPresenter(this);
    }

    if(widget.mode == SelectUserView.modeSupervisors){
      _presenter = SelectSupervisorPresenter(this);
    }

    _presenter.loadUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var content;

    if (users != null) if (users.length > 0) {
      content = Container(
        height: 300,
        child: ListView.builder(itemBuilder: (BuildContext context, int index) {
          return UserItemWidget(
              user: users[index], listener: widget.listener);
        }, itemCount: users.length),
      );
    } else {
      content = Container(
        height: 300,
          child: Center(
            child: Text("Список пуст", style: TextStyle(fontSize: 16)),
          ));
    }
    else
      content = Container(
        height: 300,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );

    return Dialog(child: content);
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    // TODO: implement showError
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void showUsers(List<User> users) {
    this.users = users;
    setState(() {});
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }
}

class UserItemWidget extends StatelessWidget {
  final User user;
  final Function listener;

  UserItemWidget({@required this.user, @required this.listener});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: Row(
          children: <Widget>[Text(user.lastName + " ", style: TextStyle(fontSize: 18)), Text(user.name, style: TextStyle(fontSize: 18))],
        ),
        margin: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
      ),
      onTap: () {
        listener(user);
      },
    );
  }
}
