import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/views/dialogs/select_user/select_user_contract.dart';

class SelectSupervisorPresenter extends SelectUserPresenterContract {

  SelectUserViewContract _view;

  SelectSupervisorPresenter(SelectUserViewContract view) {
    this._view = view;
  }

  @override
  void loadUsers() async {
    List<User> users = await APISupport.getSupervisors();
    _view.showUsers(users);
  }

}