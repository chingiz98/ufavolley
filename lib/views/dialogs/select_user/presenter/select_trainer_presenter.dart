import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/views/dialogs/select_user/select_user_contract.dart';

class SelectTrainerPresenter extends SelectUserPresenterContract {

  SelectUserViewContract _view;

  SelectTrainerPresenter(SelectUserViewContract view) {
    this._view = view;
  }

  @override
  void loadUsers() async {
    List<User> users = await APISupport.getTrainers();
    _view.showUsers(users);
  }

}