import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/user.dart';

abstract class SelectUserViewContract extends BaseView {
  void showUsers(List<User> users);
}

abstract class SelectUserPresenterContract extends BasePresenter {
  void loadUsers();
}