import 'package:flutter/material.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/dialogs/send_notification/presenter/send_notification_presenter.dart';
import '../send_notification_contract.dart';

class SendNotificationDialog extends StatefulWidget {
  final List<String> userUids;

  SendNotificationDialog({@required this.userUids});

  @override
  State createState() => _SendNotificationDialogState();
}

class _SendNotificationDialogState extends State<SendNotificationDialog> implements SendNotificationViewContract {
  SendNotificationPresenterContract _presenter;
  final TextEditingController _titleEditingController = TextEditingController();
  final TextEditingController _messageEditingController = TextEditingController();

  @override
  void initState() {
    _presenter = SendNotificationPresenter(this, widget.userUids);
    _titleEditingController.addListener(() {
      _presenter.onNotificationTitleChanged(_titleEditingController.text);
    });
    _messageEditingController.addListener(() {
      _presenter.onNotificationMessageChanged(_messageEditingController.text);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget title = Text('Создание уведомления');

    Widget body = WillPopScope(
      child: Container(
        height: 250,
        child: Column(
          children: <Widget>[
            TextField(
              controller: _titleEditingController,
              decoration: InputDecoration(border: InputBorder.none, hintText: 'Заголовок'),
            ),
            Expanded(
              child: TextField(
                controller: _messageEditingController,
                decoration: InputDecoration(border: InputBorder.none, hintText: 'Сообщение'),
                keyboardType: TextInputType.multiline,
                maxLines: 22,
              ),
            )
          ],
        ),
      ),
      onWillPop: () async => true,
    );

    return AlertDialog(
      title: title,
      content: body,
      actions: <Widget>[
        TextButton(
          onPressed: () => _presenter.onClickBackwardButton(),
          child: Text('Отмена'),
        ),
        TextButton(
          onPressed: () {
            _presenter.onClickSendButton();
          },
          child: Text('Разослать'),
        )
      ],
    );
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showErrorAlert(context, message.message);
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void showNotificationSendFailed(AlertMessage message) {
    showError(message);
  }

  @override
  void showSuccessNotificationSend() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Уведомления успешно отправлены'),
        title: Text('Внимание'),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Закрыть'),
          ),
        ],
      ),
    );
  }
}
