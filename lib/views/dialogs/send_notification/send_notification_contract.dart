import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/utils/alert_message.dart';

abstract class SendNotificationViewContract implements BaseView {
  void showNotificationSendFailed(AlertMessage message);
  void showSuccessNotificationSend();
}

abstract class SendNotificationPresenterContract implements BasePresenter {
  void onClickBackwardButton();
  void onClickSendButton();
  void onNotificationTitleChanged(String newTitle);
  void onNotificationMessageChanged(String newMessage);
}