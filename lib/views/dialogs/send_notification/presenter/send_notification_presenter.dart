import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/utils/alert_message.dart';

import '../send_notification_contract.dart';

class SendNotificationPresenter implements SendNotificationPresenterContract {

  SendNotificationViewContract _view;
  List<String> _userUids;

  String _title = '';
  String _message = '';

  SendNotificationPresenter(SendNotificationViewContract view, List<String> userUids) {
    this._view = view;
    this._userUids = userUids;
  }

  void _sendNotifications(String title, String message, List<String> userUIDs) async {

    _view.showLoading();

    print('Отправляю всем уведомление...');
    print('Заголовок: ' + title);
    print('Текст: ' + message);

    var data = {
      'title':title,
      'body':message,
      'uids':userUIDs,
    };

    int responseCode = await APISupport.broadcastPushNotification(data: data);

    _view.hideLoading();
    _view.hideView();

    if (responseCode != 0) {
      _view.showNotificationSendFailed(AlertMessage(title: 'Ошбика', message: 'Не удалось разослать уведомление'));
    } else {
      _view.showSuccessNotificationSend();
    }
  }

  @override
  void onClickBackwardButton() {
    _view.hideView();
  }

  @override
  void onClickSendButton() {

    if (_userUids.length == 0) {
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Не выбран ни один пользователь'));
      return;
    }

    if (_title == '') {
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Заголовок не может быть пуст'));
      return;
    }

    if (_message == '') {
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Сообщение не может быть пустым'));
      return;
    }

    _sendNotifications(_title, _message, _userUids);

  }

  @override
  void onNotificationMessageChanged(String newMessage) {
    _message = newMessage;
  }

  @override
  void onNotificationTitleChanged(String newTitle) {
    _title = newTitle;
  }

}