import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/base/base_presenter.dart';

import 'package:ufavolley/views/pages/tournament_rating/tournament_rating_contract.dart';

class TournamentRatingPresenter extends BasePresenter {
  TournamentRatingViewContract _view;

  TournamentRatingPresenter(TournamentRatingViewContract view) {
    this._view = view;
    _loadRatingData();
  }

  void _loadRatingData() async {
    var futures = [
      APISupport.getRatingList("U6zaxhj6IzaExZqcMkQ3"),
      APISupport.getRatingList("Y5gzffkscPylzcP8rNkz"),
      APISupport.getRatingList("ec9tH4CEAVLxfT398nV4")
    ];

    var list = await Future.wait(futures);

    _view.onDataLoaded(list);
  }
}
