import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/user.dart';

abstract class TournamentRatingViewContract extends BaseView {
  void onDataLoaded(List<List<User>> users);
}