import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/views/pages/tournament_rating/presenter/tournament_rating_presenter.dart';
import 'package:ufavolley/views/pages/tournament_rating/tournament_rating_contract.dart';

class TournamentRatingPage extends StatefulWidget {
  final TabController tabController;

  TournamentRatingPage({this.tabController});

  @override
  State<StatefulWidget> createState() {
    return TournamentRatingPageState(tabController);
  }
}

class TournamentRatingPageState extends State<TournamentRatingPage> implements TournamentRatingViewContract {
  TournamentRatingPresenter _presenter;
  List<List<User>> _users;

  TabController tabController;

  AssetImage placeholder = AssetImage('assets/boy.jpg');

  TournamentRatingPageState(this.tabController);

  @override
  void initState() {
    super.initState();
    _presenter = TournamentRatingPresenter(this);
    //tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return _users != null
        ? TabBarView(controller: tabController, children: _buildPages(_users))
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  List<Widget> _buildPages(List<List<User>> users) {
    List<Widget> pages = [];

    pages.add(_buildRating(users[0]));
    pages.add(_buildRating(users[1]));
    pages.add(_buildRating(users[2]));
    return pages;
  }

  Widget _buildRating(List<User> users) {
    if (users.length < 4) {
      return Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
        ),
        child: Center(
          child: Container(
            margin: EdgeInsets.all(16),
            child: Text("Данных для отображения рейтинга недостаточно. Попробуйте зайти позже.",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 24), textAlign: TextAlign.center),
          ),
        ),
      );
    }
    return Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
      ),
      child: ListView(children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(vertical: 20),
          child: _buildTop3(users.sublist(0, 3)),
        ),
        Container(
          margin: EdgeInsets.only(left: 16, right: 24),
          child: _buildOtherRating(users.sublist(3)),
        )
      ]),
    );
  }

  Widget _buildTop3(List<User> users) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[_top2Item(users[1]), _top1Item(users[0]), _top3Item(users[2])],
    );
  }

  Widget _buildOtherRating(List<User> users) {
    List<Widget> _items = [];
    for (int i = 0; i < users.length; i++) {
      _items.add(_commonItem(users[i], i + 4));
    }

    //users.forEach((user) => {_items.add(_commonItem(user))});
    return Column(
      children: _items,
    );
  }

  Widget _top1Item(User user) {
    return Container(
      margin: EdgeInsets.all(4),
      child: Column(
        children: <Widget>[
          Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: FadeInImage(
                  placeholder: placeholder,
                  image: CachedNetworkImageProvider(user.imageUrl),
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                )),
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 240, 200, 0),
              borderRadius: BorderRadius.all(Radius.circular(29.0)),
              border: Border.all(
                color: Color.fromARGB(255, 255, 223, 0),
                width: 4.0,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: Color.fromARGB(255, 255, 223, 0)),
            margin: EdgeInsets.all(8),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: Text(user.rating.toString(), style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700)),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("${user.lastName}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
                Text("${user.name}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _top2Item(User user) {
    return Container(
      margin: EdgeInsets.all(4),
      child: Column(
        children: <Widget>[
          Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: FadeInImage(
                  placeholder: placeholder,
                  image: CachedNetworkImageProvider(user.imageUrl),
                  height: 90,
                  width: 90,
                  fit: BoxFit.cover,
                )),
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 170, 170, 170),
              borderRadius: BorderRadius.all(Radius.circular(29.0)),
              border: Border.all(
                color: Color.fromARGB(255, 192, 192, 192),
                width: 4.0,
              ),
            ),
          ),
          Container(
            decoration:
                BoxDecoration(borderRadius: BorderRadius.circular(8), color: Color.fromARGB(255, 192, 192, 192)),
            margin: EdgeInsets.all(8),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: Text(user.rating.toString(), style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700)),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("${user.lastName}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
                Text("${user.name}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _top3Item(User user) {
    return Container(
      margin: EdgeInsets.all(4),
      child: Column(
        children: <Widget>[
          Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: FadeInImage(
                  placeholder: placeholder,
                  image: CachedNetworkImageProvider(user.imageUrl),
                  height: 80,
                  width: 80,
                  fit: BoxFit.cover,
                )),
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 180, 127, 50),
              borderRadius: BorderRadius.all(Radius.circular(29.0)),
              border: Border.all(
                color: Color.fromARGB(255, 205, 127, 50),
                width: 4.0,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: Color.fromARGB(255, 205, 127, 50)),
            margin: EdgeInsets.all(8),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: Text(user.rating.toString(), style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700)),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("${user.lastName}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
                Text("${user.name}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _commonItem(User user, int pos) {
    TextStyle textStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.w500);

    //ResourceManager().currentUser

    return Container(
      margin: EdgeInsets.only(bottom: 8),
      child: Row(
        children: <Widget>[
          Container(
            width: 30,
            child: Center(
              child: Text(
                pos.toString(),
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 12.0),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: FadeInImage(
                  placeholder: placeholder,
                  image: CachedNetworkImageProvider(user.imageUrl),
                  height: 50,
                  width: 50,
                  fit: BoxFit.cover,
                )),
            decoration: BoxDecoration(
              color: Colors.deepPurple,
              borderRadius: BorderRadius.all(Radius.circular(50.0)),
              border: Border.all(
                color: Colors.blueAccent,
                width: 4.0,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[Text("${user.lastName} ${user.name}", style: textStyle)],
              ),
            ),
          ),
          Text(
            user.rating.toString(),
            style: textStyle,
          ),
        ],
      ),
    );
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    // TODO: implement showError
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void onDataLoaded(List<List<User>> users) {
    setState(() {
      this._users = users;
    });
  }
}
