import 'package:flutter/material.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/notification_archive/view/archived_notification_view.dart';
import 'package:ufavolley/views/pages/control_panel/notifications_archive/mail_archive_contract.dart';
import 'package:ufavolley/views/pages/control_panel/notifications_archive/presenter/mail_archive_presenter.dart';

class MailArchivePage extends StatefulWidget {
  @override
  State createState() => MailArchivePageState();
}

class MailArchivePageState extends State<MailArchivePage> implements MailArchiveViewContract {
  List<ArchivedNotificationProp> _notifications;
  MailArchivePresenterContract _presenter;

  @override
  void initState() {
    super.initState();
    _presenter = MailArchivePresenter(this);
    _presenter.loadArchivedNotifications();
  }

  Widget _buildNotification(ArchivedNotificationProp notification) {
    final TextStyle textStyle = TextStyle(
        fontFamily: 'PFDin', fontWeight: FontWeight.w300, fontSize: 24.0, color: Color.fromARGB(255, 102, 102, 102));

    final TextStyle textStyle2 = TextStyle(
        fontFamily: 'PFDin', fontWeight: FontWeight.w300, fontSize: 18.0, color: Color.fromARGB(255, 102, 102, 102));

    return Container(
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(notification.title, style: textStyle, overflow: TextOverflow.ellipsis),
                    Text(notification.subTitle, style: textStyle, overflow: TextOverflow.ellipsis)
                  ],
                ),
              ),
              Expanded(
                child: _buildDateTimeWidget(notification.dateOfReceiving, textStyle2),
              )
            ],
          ),
          onTap: () async {
            _presenter.clickOnNotification(notification);
          },
        ),
      ),
    );
  }

  Widget _buildDateTimeWidget(DateTime dateTime, TextStyle style) {
    String _getHours(int hour) {
      return hour > 9 ? hour.toString() : '0' + hour.toString();
    }

    String _getMinutes(int minutes) {
      return minutes > 9 ? minutes.toString() : '0' + minutes.toString();
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            _getHours(dateTime.hour) + ':' + _getMinutes(dateTime.minute),
            style: style,
          ),
          Text(
            dateTime.day.toString() + '.' + dateTime.month.toString() + '.' + dateTime.year.toString(),
            style: style,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      title: Text('Отправленные уведомления'),
    );

    var body;

    if (_notifications == null)
      body = Center(
        child: CircularProgressIndicator(),
      );
    else
      body = ListView.separated(
        shrinkWrap: true,
        itemCount: _notifications.length,
        itemBuilder: (context, index) {
          return _buildNotification(_notifications[index]);
        },
        separatorBuilder: (context, index) {
          return DottedDivider();
        },
      );

    return Scaffold(
      appBar: appBar,
      body: body,
    );
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void showNotifications(List<ArchivedNotificationProp> notifications) {
    _notifications = notifications;
    setState(() {});
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void showNotification(ArchivedNotificationProp notification) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ArchivedNotificationPage(archivedNotificationProp: notification)));
  }
}
