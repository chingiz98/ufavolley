import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/views/pages/control_panel/notifications_archive/mail_archive_contract.dart';

class MailArchivePresenter implements MailArchivePresenterContract {
  MailArchiveViewContract _view;

  MailArchivePresenter(MailArchiveViewContract view) {
    this._view = view;
  }

  @override
  void loadArchivedNotifications() {
    APISupport.getNotificationArchive()
        .then((value) => _view.showNotifications(value))
        .catchError((e) => {
              _view.showError(
                  AlertMessage(title: 'Ошибка', message: e.toString()))
            });
  }

  @override
  void clickOnNotification(ArchivedNotificationProp notification) {
    _view.showNotification(notification);
  }
}
