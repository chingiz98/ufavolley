import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/models/notificationProp.dart';

abstract class MailArchiveViewContract implements BaseView {
  void showNotifications(List<ArchivedNotificationProp> notification);
  void showNotification(ArchivedNotificationProp notification);
}

abstract class MailArchivePresenterContract implements BasePresenter {
  void loadArchivedNotifications();
  void clickOnNotification(ArchivedNotificationProp notification);
}