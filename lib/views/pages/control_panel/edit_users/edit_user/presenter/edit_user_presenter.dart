import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:url_launcher/url_launcher.dart';
import '../edit_user_contract.dart';

class EditUserPresenter implements EditUserPresenterContract {

  EditUserDialogContract _view;
  User _user;

  String _name;
  String _lastName;
  bool _isEditable = false;
  bool _isSupervisor = false;
  bool _isBanned = false;
  bool _isRedactor = false;
  List<CloudValue> _levels;
  CloudValue _level;
  bool _isDebtor = false;

  EditUserPresenter(EditUserDialogContract view, User user) {
    this._view = view;
    this._user = user;

    _name = user.name;
    _lastName = user.lastName;

    _levels = ResourceManager().containers[CloudResource.LEVEL].values;
    if (user.level != null || user.level != '')
      _level = _getLevelByID(user.level);
    _isEditable = user.isEditable;
    _isRedactor = user.isAdmin;
    _isSupervisor = user.isSupervisor;
    _isBanned = user.isBanned;
    _isDebtor = user.isDebtor;

    if (user.phoneNumber == null || user.phoneNumber == '' && !user.byAdmin)
      _loadVKID();

    _loadImageUrl();
  }

  _loadImageUrl() async {
    var url = await FirebaseStorage.instance
        .ref()
        .child(_user.uid)
        .getDownloadURL()
        .catchError((e) {});
    if (url is String)
      _view.updateProfileImageURL(url);
  }

  Future<void> _loadVKID() async {
    Map<String, String> emailsMap = await APISupport.getEmailsByUids(uids: [_user.uid]);
    _user.vkLink = _parseVkLinkFromEmail(emailsMap[_user.uid]);
    _view.updateState();
  }

  String _parseVkLinkFromEmail(String email) {
    return 'vk.com/id' + email.split('@')[0];
  }

  CloudValue _getLevelByID(String id) {
    for (int i = 0; i < _levels.length; i++)
      if (_levels[i].id == id) return _levels[i];
    return CloudValue(val: 'error', id: 'error');
  }

  _launchURL(String url) async {
    if (await canLaunch(url))
      await launch(url);
    else
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Невозможно отобразить страницу: $url'));
  }

  @override
  void onBannedStatusChanged(bool isBanned) {
    this._isBanned = isBanned;
  }

  @override
  void onClickDenyButton() {
    _view.hideView();
  }

  @override
  void onClickOnContactData() {
    if (_user.vkLink != null)
      _launchURL('https://' + _user.vkLink);
    else
      launch('tel:' + _user.phoneNumber);
  }

  @override
  void copyContactData() {
    if (_user.vkLink != null) {
      Clipboard.setData(ClipboardData(text: 'https://' + _user.vkLink));
    } else {
      Clipboard.setData(ClipboardData(text: _user.phoneNumber));
    }
    _view.showSuccessCopyContactDataMessage();
  }

  @override
  void onClickSaveChangesButton() async {
    _view.showLoading();
    User user = User(
      name: _name,
      lastName: _lastName,
      level: _level.id,
      dateOfBirth: _user.dateOfBirth,
      isAdmin: _isRedactor,
      isTrainer: _user.isTrainer,
      uid: _user.uid,
      phoneNumber: _user.phoneNumber,
      gender: _user.gender,
      isEditable: _isEditable,
      byAdmin: _user.byAdmin,
      isSupervisor: _isSupervisor,
      // TODO (Переделать)
      isBanned: _isBanned,
      isMainAdmin: _user.isMainAdmin,
      isDebtor: _isDebtor
    );
    await APISupport.updateUser(user: user, token: null);
    _view.hideLoading();
    _view.hideView();
    _view.onUserUpdated(user);
  }

  @override
  void onEditLastName(String newLastName) {
    this._lastName = newLastName;
  }

  @override
  void onEditName(String newName) {
    this._name = newName;
  }

  @override
  void onEditableChanged(bool isEditable) {
    this._isEditable = isEditable;
  }

  @override
  void onEditorPermissionChanged(bool isEditor) {
    this._isRedactor = isEditor;
  }

  @override
  void onLevelChanged(CloudValue newLevel) {
    this._level = newLevel;
  }

  @override
  void onSupervisorPermissionChanged(bool isSupervisor) {
    this._isSupervisor = isSupervisor;
  }

  @override
  void onDebtorStatusChanged(bool value) {
    this._isDebtor = value;
  }


}