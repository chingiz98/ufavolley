import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/edit_users/edit_user/presenter/edit_user_presenter.dart';

import '../edit_user_contract.dart';

class EditUserPage extends StatefulWidget {
  final Function listener;
  final User user;

  EditUserPage({@required this.listener, @required this.user});

  @override
  _EditUserPageState createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> implements EditUserDialogContract {
  //TODO(Добавить редактирование нового юзера)

  EditUserPresenterContract _presenter;

  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  bool isEditable = false;
  bool isSupervisor = false;
  bool isBanned = false;
  bool isRedactor = false;
  bool isDebtor = false;
  List<CloudValue> levels;
  CloudValue level;
  ResourceManager resourceManager = ResourceManager();
  Widget onUserDataTapChild;

  String url = 'assets/boy.jpg';

  @override
  void initState() {
    _presenter = EditUserPresenter(this, widget.user);

    nameController.text = widget.user.name;
    nameController.addListener(() {
      _presenter.onEditName(nameController.text);
    });
    lastNameController.text = widget.user.lastName;
    lastNameController.addListener(() {
      _presenter.onEditLastName(lastNameController.text);
    });

    levels = resourceManager.containers[CloudResource.LEVEL].values;
    if (widget.user.level != null || widget.user.level != '')
      level = _getLevelByID(widget.user.level);
    isEditable = widget.user.isEditable;
    isRedactor = widget.user.isAdmin;
    isSupervisor = widget.user.isSupervisor;
    isBanned = widget.user.isBanned;
    isDebtor = widget.user.isDebtor;
    if (widget.user.vkLink != null)
      onUserDataTapChild = Text(widget.user.vkLink);
    else
      onUserDataTapChild = Text(widget.user.phoneNumber);
    super.initState();
  }

  CloudValue _getLevelByID(String id) {
    for (int i = 0; i < levels.length; i++) {
      if (levels[i].id == id) return levels[i];
    }
    return CloudValue(val: 'error', id: 'error');
  }

  @override
  Widget build(BuildContext context) {
    if (widget.user.vkLink != null)
      onUserDataTapChild = Text(widget.user.vkLink);
    else
      onUserDataTapChild = Text(widget.user.phoneNumber);

    Widget titleContent = Text("Редактирование пользователя");

    AssetImage placeholder = AssetImage('assets/boy.jpg');

    return Scaffold(
      appBar: AppBar(title: titleContent),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: 300,
              height: 300,
              child: FadeInImage(
                placeholder: placeholder,
                image: CachedNetworkImageProvider(url),
                height: 50,
                width: 50,
                fit: BoxFit.cover,
              ),
            ),
            GestureDetector(
              onTap: () => _presenter.onClickOnContactData(),
              onLongPress: () => _presenter.copyContactData(),
              child: onUserDataTapChild,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(hintText: 'Имя'),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: TextField(
                controller: lastNameController,
                decoration: InputDecoration(hintText: 'Фамилия'),
              ),
            ),
            CheckboxListTile(
              value: isEditable,
              onChanged: (bool value) {
                _presenter.onEditableChanged(value);
                setState(() {
                  isEditable = value;
                });
              },
              title: Text('Разрешить редактирование'),
            ),
            CheckboxListTile(
              value: isSupervisor,
              onChanged: (bool value) {
                _presenter.onSupervisorPermissionChanged(value);
                setState(() {
                  isSupervisor = value;
                });
              },
              title: Text('Разрешить отмечать посещаемость'),
            ),
            CheckboxListTile(
              value: isBanned,
              onChanged: (bool value) {
                _presenter.onBannedStatusChanged(value);
                setState(() {
                  isBanned = value;
                });
              },
              title: Text('Заблокировать пользователя'),
            ),
            CheckboxListTile(
              value: isDebtor,
              onChanged: (bool value) {
                _presenter.onDebtorStatusChanged(value);
                setState(() {
                  isDebtor = value;
                });
              },
              title: Text('Выставить статус неплательщика'),
            ),
            resourceManager.currentUser.isMainAdmin && !widget.user.isMainAdmin
                ? CheckboxListTile(
                    value: isRedactor,
                    onChanged: (bool value) {
                      _presenter.onEditorPermissionChanged(value);
                      setState(() {
                        isRedactor = value;
                      });
                    },
                    title: Text('Выдать права редактора'),
                  )
                : Container(),
            widget.user.isMainAdmin
                ? Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'Главный администратор',
                      style: TextStyle(fontSize: 16),
                    ),
                  )
                : Container(),
            Container(
              child: DropdownButton(
                isExpanded: true,
                value: level,
                items: levels.map((CloudValue cloudValue) {
                  return DropdownMenuItem(
                    value: cloudValue,
                    child: Text(cloudValue.getValue),
                  );
                }).toList(),
                hint: Text('Уровень'),
                onChanged: (CloudValue cloudValue) {
                  onLevelChanged(cloudValue);
                },
              ),
              padding: EdgeInsets.symmetric(horizontal: 16),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextButton(
                      onPressed: () {
                        _presenter.onClickDenyButton();
                      },
                      child: Text(
                        'Отмена',
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ),
                  Expanded(
                    child: TextButton(
                      onPressed: () async {
                        _presenter.onClickSaveChangesButton();
                      },
                      child: Text(
                        'Сохранить',
                        style: TextStyle(color: Colors.indigo),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void onLevelChanged(CloudValue cloudValue) {
    _presenter.onLevelChanged(cloudValue);
    setState(() {
      this.level = cloudValue;
    });
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void onUserUpdated(User user) {
    widget.listener(user);
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {
    setState(() {});
  }

  @override
  void updateProfileImageURL(String url) {
    setState(() {
      if (url is String) this.url = url;
    });
  }

  @override
  void showSuccessCopyContactDataMessage() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          "Контактные данные скопированы",
        ),
      ),
    );
  }
}
