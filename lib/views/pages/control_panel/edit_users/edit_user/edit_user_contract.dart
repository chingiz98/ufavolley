import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/user.dart';

abstract class EditUserDialogContract implements BaseView {
  void onUserUpdated(User user);
  void updateProfileImageURL(String url);
  void showSuccessCopyContactDataMessage();
}

abstract class EditUserPresenterContract implements BasePresenter {
  void onClickOnContactData();
  void copyContactData();
  void onEditName(String newName);
  void onEditLastName(String newLastName);
  void onEditableChanged(bool isEditable);
  void onSupervisorPermissionChanged(bool isSupervisor);
  void onBannedStatusChanged(bool isBanned);
  void onEditorPermissionChanged(bool isEditor);
  void onLevelChanged(CloudValue newLevel);
  void onClickDenyButton();
  void onClickSaveChangesButton();
  void onDebtorStatusChanged(bool value);
}