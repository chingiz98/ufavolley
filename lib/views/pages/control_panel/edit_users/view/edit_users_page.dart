import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ufavolley/constants.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/models/user_filter.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/constants.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/edit_users/edit_user/view/edit_user_page.dart';
import 'package:ufavolley/views/pages/control_panel/edit_users/presenter/edit_users_presenter.dart';
import 'package:ufavolley/views/pages/control_panel/filter_users/view/filter_users_dialog.dart';

import '../edit_users_contract.dart';

class EditUsersPage extends StatefulWidget {
  @override
  State createState() => EditUsersPageState();
}

class EditUsersPageState extends State<EditUsersPage> implements EditUsersViewContract {
  EditUsersPresenterContract _presenter;
  List<User> users;
  bool loadingStarted = false;
  bool stopPagination = false;
  bool isLoading = false;

  final TextEditingController _searchEditingController = TextEditingController();

  String sortBy = 'name';
  int direction = 1;

  @override
  void initState() {
    _presenter = EditUsersPresenter(this);
    _searchEditingController.addListener(() {
      setState(() {
        _presenter.onSearchValueChanged(_searchEditingController.text);
      });
    });
    //_presenter.loadUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var content;

    Widget inputControls = Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(right: 8, left: 8, bottom: 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: TextField(
                  controller: _searchEditingController,
                  decoration: InputDecoration(hintText: 'Искать'),
                  onChanged: (changed) {
                    _presenter.clearUsersList();
                    users.clear();
                    setState(() {
                      loadingStarted = false;
                      stopPagination = false;
                      isLoading = false;
                    });
                  },
                  onSubmitted: (value) {
                    if (_searchEditingController.text.isNotEmpty) {
                      _presenter.clearUsersList();
                      _presenter.loadUsers(searchString: value.toString());
                      setState(() {
                        loadingStarted = true;
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        /*
        Container(
          child: Row(
            children: <Widget>[
              ButtonBar(
                children: <Widget>[
                  Radio(
                      value: 'lastName',
                      groupValue: sortBy,
                      onChanged: (String newValue) {
                        sortBy = newValue;
                        direction = 1;
                        _presenter.onSortParamChanged(newValue);
                      }),
                  Text('Ф'),
                  Radio(
                      value: 'name',
                      groupValue: sortBy,
                      onChanged: (String newValue) {
                        sortBy = newValue;
                        direction = 1;
                        _presenter.onSortParamChanged(newValue);
                      }),
                  Text('И'),
                ],
              ),
              ButtonBar(
                children: <Widget>[
                  Radio(
                      value: 1,
                      groupValue: direction,
                      onChanged: (int newDirection) {
                        direction = newDirection;
                        _presenter.onSortDirectionChanged(newDirection);
                      }),
                  Text('↓'),
                  Radio(
                      value: -1,
                      groupValue: direction,
                      onChanged: (int newDirection) {
                        direction = newDirection;
                        _presenter.onSortDirectionChanged(newDirection);
                      }),
                  Text('↑')
                ],
              ),
            ],
          ),
        )
              */
      ],
    );

    //if (users != null) {
    if (users == null) {
      users = [];
    }
    content = Column(
      children: <Widget>[
        inputControls,
        !loadingStarted
            ? Expanded(child: Center(child: Text('Введите имя, фамилию или номер телефона для поиска')))
            : (!isLoading && users.length == 0 && stopPagination)
                ? Expanded(
                    child: Center(child: Text('Пользователи не найдены.')),
                  )
                : Expanded(
                    child: NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (!isLoading && scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
                        //start loading data
                        setState(() {
                          isLoading = true;
                        });
                        if (!stopPagination)
                          _presenter.loadNextUsers(
                              searchString: _searchEditingController.text,
                              startAtUid: users.last.uid,
                              startAtName: users.last.name);
                        //_loadData();
                      }
                      return true;
                    },
                    child: ListView.builder(
                        shrinkWrap: false,
                        itemCount: !stopPagination && loadingStarted ? users.length + 1 : users.length,
                        itemBuilder: (context, index) {
                          if (index == users.length)
                            return Container(
                              height: 50.0,
                              color: Colors.transparent,
                              child: Center(
                                child: new CircularProgressIndicator(),
                              ),
                            );
                          return UserItem(user: users[index], listener: onUserUpdated);
                        }),
                  )),
      ],
    );
    /*
    } else {
      content = Center(
        child: CircularProgressIndicator(),
      );
    }

       */

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('Управление пользователями'),
          actions: <Widget>[
            IconButton(
                tooltip: "Загрузить всех",
                icon: Icon(Icons.cloud_download),
                onPressed: () async {
                  setState(() {
                    _presenter.clearUsersList();
                    users.clear();
                    isLoading = true;
                    stopPagination = false;
                    loadingStarted = true;
                    _presenter.loadAllUsers();
                  });
                }),
            IconButton(
                tooltip: "Фильтры",
                icon: Icon(Icons.clear_all),
                onPressed: () async {
                  _presenter.clickOnFilterButton();
                })
          ],
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (_) {
            bool isKeyboardShowing = MediaQuery.of(context).viewInsets.vertical > 0;
            if (isKeyboardShowing && _searchEditingController.text.isNotEmpty) {
              _presenter.clearUsersList();
              _presenter.loadUsers(searchString: _searchEditingController.text);
              setState(() {
                loadingStarted = true;
              });
            }
            print('KEYBOARD SHOWING $isKeyboardShowing');
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: content,
        ));
  }

  @override
  void dispose() {
    users = null;
    _searchEditingController.dispose();
    super.dispose();
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void onUserUpdated(User user) {
    for (int i = 0; i < users.length; i++) {
      if (users[i].uid == user.uid) {
        users[i] = user;
        break;
      }
    }
    setState(() {});
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void showUsersPart(List<User> users, bool stopPagination) {
    setState(() {
      this.users = users;
      this.stopPagination = stopPagination;
      this.isLoading = false;
    });
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showEditUserPage(User user) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => EditUserPage(listener: onUserUpdated, user: user)));
  }

  @override
  void showUsersFilterDialog(UserParamFilter filter) {
    showDialog(
      context: context,
      builder: (context) => FilterUsersDialog(filterableUsersPresenter: _presenter, filter: filter),
    );
  }

  @override
  void allUsersLoaded() {}
}

class UserItem extends StatefulWidget {
  final User user;
  final Function listener;

  const UserItem({@required this.user, @required this.listener});

  @override
  State createState() => UserItemState();
}

class UserItemState extends State<UserItem> {
  String url = 'assets/boy.jpg';

  AssetImage placeholder = AssetImage('assets/boy.jpg');

  TextStyle _textStyle = TextStyle(fontSize: 16);

  Color imageBorderColor;

  @override
  Widget build(BuildContext context) {
    var levels = ResourceManager().containers[CloudResource.LEVEL].getValuesMap();
    imageBorderColor = Colors.grey;
    if (widget.user.level != null && widget.user.level != '0') {
      if (levels[widget.user.level].getValue == 'Лайт') imageBorderColor = Colors.yellow;
      if (levels[widget.user.level].getValue == 'Лайт+') imageBorderColor = Colors.green;
      if (levels[widget.user.level].getValue == 'Медиум') imageBorderColor = Colors.red;
      if (levels[widget.user.level].getValue == C.litePlusProLevelName) imageBorderColor = Constants.litePlusProColor;
      if (levels[widget.user.level].getValue == 'Хард') imageBorderColor = Colors.black;
    }
    return _buildItemContent();
  }

  Widget _buildItemContent() {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 4),
          child: Row(
            children: <Widget>[
              Container(
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(25.0),
                    child: FadeInImage(
                      placeholder: placeholder,
                      image: CachedNetworkImageProvider(widget.user.imageUrl),
                      height: 50,
                      width: 50,
                      fit: BoxFit.cover,
                    )),
                decoration: BoxDecoration(
                  color: Colors.deepPurple,
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  border: Border.all(
                    color: imageBorderColor,
                    width: 4.0,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("${widget.user.lastName} ${widget.user.name}", style: _textStyle),
                    Text(_getContactData(widget.user), style: _textStyle),
                  ],
                ),
              )
            ],
          ),
        ),
        onTap: () {
          showEditUserAlert(widget.listener, widget.user);
        },
      ),
    );
  }

  String _getContactData(User user) {
    if (user.phoneNumber != null && user.phoneNumber != "") return user.phoneNumber;
    if (user.vkLink != null && user.vkLink != "") return user.vkLink;
    return "Контактная информация отсутствует";
  }

  void _onUserUpdated(User user) async {
    widget.listener(user);
    setState(() {});
    print('_onUserUpdated called');
  }

  Future<bool> showEditUserAlert(Function listener, User user) {
    return showDialog(
          context: context,
          builder: (context) => EditUserPage(user: user, listener: _onUserUpdated),
        ) ??
        false;
  }

  @override
  void dispose() {
    url = null;
    //placeholder = null;
    _textStyle = null;
    imageBorderColor = null;

    super.dispose();
  }
}
