import 'package:ufavolley/filter_users_contract.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/user.dart';

abstract class EditUsersViewContract implements BaseView, FilterableUsersViewContract {
  void showUsersPart(List<User> users, bool stopPagination);
  void allUsersLoaded();
  void onUserUpdated(User user);
  void showEditUserPage(User user);
}

abstract class EditUsersPresenterContract implements BasePresenter, FilterableUsersPresenterContract {
  void loadUsers({String searchString});
  void loadNextUsers({String searchString, String startAtUid, String startAtName});
  void loadAllUsers();
  void onClickOnUser(User user);
  void onSearchValueChanged(String newSearchValue);
  void onSortParamChanged(String paramName);
  void onSortDirectionChanged(int direction);
  void clearUsersList();
}