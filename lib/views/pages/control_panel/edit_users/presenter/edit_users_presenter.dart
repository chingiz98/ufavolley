import 'package:ufavolley/models/user_filter.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/utils/alert_message.dart';

import '../edit_users_contract.dart';

class EditUsersPresenter implements EditUsersPresenterContract {

  EditUsersViewContract _view;
  List<User> _allUsers = [];
  List<User> _visibleUsers = [];
  UserParamFilter _paramFilter;
  UserDesiredFilter _desiredFilter;
  String _sortedBy;
  int _sortDirection;

  EditUsersPresenter(EditUsersViewContract view) {
    this._view = view;
    _paramFilter = UserParamFilter();
    _desiredFilter = UserDesiredFilter();
    _sortedBy = 'name';
    _sortDirection = 1;
  }

  @override
  void loadUsers({String searchString}) async {
    await Future.delayed(Duration(seconds: 1));
    List<User> users = await APISupport.searchUsers(searchString: searchString);

    _allUsers.addAll(users);

    /*List<User> filteredUsers = _applyFilters(users);
    _visibleUsers.addAll(filteredUsers);

    _sort(_visibleUsers, _sortedBy, _sortDirection);*/
    //_visibleUsers.clear();
    //_visibleUsers = _applyFilterAndSort(_allUsers);
    bool stopPagination = false;
    if (users.length < 200)
      stopPagination = true;
    _view.showUsersPart(_allUsers, stopPagination);

    /*
    if (users.length > 0) {
      print('loaded part of all users');
      loadUsers(lastID: _allUsers.last.uid);
    } else {
      _view.allUsersLoaded();
    }

     */

  }

  List<User> _applySearchValue(List<User> users) {
    return _desiredFilter.applyTo(users);
  }

  List<User> _applyFilter(List<User> users) {
    return _paramFilter.applyTo(users);
  }

  List<User> _applyFilters(List<User> users) {
    List<User> filteredUsers = _applyFilter(users);
    return _applySearchValue(filteredUsers);
  }

  @override
  void onClickOnUser(User user) {
     _view.showEditUserPage(user);
  }

  @override
  void onSearchValueChanged(String newSearchValue) {
    _desiredFilter.setDesiredParameter(newSearchValue);
    List<User> users = _applyFilters(_allUsers);
    _visibleUsers = users;
    //_view.showUsersPart(_visibleUsers);
  }

  @override
  void clickOnConfirmFilterButton(UserParamFilter filter) {
    this._paramFilter = filter;
    List<User> users = _applyFilters(_allUsers);
    _visibleUsers = users;
    _view.hideView();
    _view.showUsersPart(_visibleUsers, true);
  }

  @override
  void clickOnFilterButton() {
    _view.showUsersFilterDialog(_paramFilter);
  }

  @override
  void onSortDirectionChanged(int direction) {

    print('new direction = ' + direction.toString());
    if (direction.abs() == 1) {
      //_view.showUsersPart(_sort(_visibleUsers, _sortedBy, direction));
    } else
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Невозможно отсортировать список'));
  }

  @override
  void onSortParamChanged(String paramName) {
    print('new paramName = ' + paramName);
    if (paramName == 'name' || paramName == 'lastName') {
      //_view.showUsersPart(_sort(_visibleUsers, paramName, _sortDirection));
    } else
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Невозможно отсортировать список'));
  }

  List<User> _sort(List<User> users, String sortBy, int direction) {
    print("sort params: sortBy $sortBy direction $direction");
    switch (sortBy) {
      case "name":
        users.sort((user1, user2) => user1.name.compareTo(user2.name));
        break;
      case "lastName":
        users.sort((user1, user2) => user1.lastName.compareTo(user2.lastName));
        break;
    }
    _sortedBy = sortBy;
    if (direction != _sortDirection && direction != 1) {
      users = users.reversed.toList();
    }
    _sortDirection = direction;
    return users;
  }

  @override
  void updateUserFilter(UserParamFilter filter) {
    _paramFilter = filter;
    _visibleUsers = _applyFilters(_allUsers);
    //_view.showUsersPart(_visibleUsers);
  }

  List<User> _applyFilterAndSort(List<User> users) {
    List<User> filterResult = [];
    filterResult = _applyFilters(users);
    List<User> sortResult = _sort(filterResult, _sortedBy, _sortDirection);
    return sortResult;
  }

  @override
  void loadNextUsers({String searchString, String startAtUid, String startAtName}) async {
    await Future.delayed(Duration(seconds: 1));
    List<User> users = await APISupport.searchUsers(searchString: searchString, startAtUid: startAtUid, startAtName: startAtName);
    _allUsers.addAll(users);
    bool stopPagination = false;
    if (users.length < 200)
      stopPagination = true;
    _view.showUsersPart(_allUsers, stopPagination);
  }

  @override
  void clearUsersList() {
    _allUsers.clear();
  }

  @override
  void loadAllUsers() async {
    List<User> users = await APISupport.getAllUsersV2(lastID: '');
    _allUsers.addAll(users);

    List<User> filteredUsers = _applyFilters(users);
    _visibleUsers.addAll(filteredUsers);

    _sort(_visibleUsers, _sortedBy, _sortDirection);
    _visibleUsers.clear();
    _visibleUsers = _applyFilterAndSort(_allUsers);



    if (users.length > 0) {
      print('loaded part of all users');
      //loadUsers(lastID: _allUsers.last.uid);
    } else {
      _view.allUsersLoaded();
    }

    _view.showUsersPart(_allUsers, true);

  }

}