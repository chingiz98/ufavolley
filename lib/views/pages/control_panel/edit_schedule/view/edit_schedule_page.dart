import 'package:flutter/material.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/views/pages/control_panel/edit_schedule/training_redactor/view/training_redactor.dart';

class EditSchedulePage extends StatefulWidget {
  @override
  State createState() => new EditSchedulePageState();
}

class EditSchedulePageState extends State<EditSchedulePage> with SingleTickerProviderStateMixin {
  List<Training> _actualTrainings;
  List<Training> _finishedTrainings;
  TabController tabController;
  bool isDataLoaded;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    _actualTrainings = [];
    _finishedTrainings = [];
    isDataLoaded = false;
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('edit_schedule_page build called');

    return Scaffold(
      appBar: AppBar(
        title: Text('Управление расписанием'),
        bottom: TabBar(
          controller: tabController,
          tabs: [Tab(text: 'Актуальные'), Tab(text: 'Прошедшие')],
        ),
      ),
      body: FutureBuilder(
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              _actualTrainings = snapshot.data['actual'];
              _finishedTrainings = snapshot.data['finished'];
              _finishedTrainings = _finishedTrainings.reversed.toList();

              return TabBarView(controller: tabController, children: _buildPages());
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
          future: _loadData()),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.push(context, MaterialPageRoute(builder: (context) => TrainingEditorPage()));
          print('returned from training redactor');
          setState(() {});
        },
        child: Icon(Icons.add),
      ),
    );
  }

  List<Widget> _buildPages() {
    List<Widget> pages = [];
    pages.add(RefreshIndicator(
        child: ListView.separated(
          itemBuilder: (context, pos) {
            //_buildRows(_finishedTrainings)
            return _buildListViewItem(_actualTrainings[pos]);
          },
          separatorBuilder: (context, index) {
            return Container(
              height: 1,
              width: MediaQuery.of(context).size.width,
              color: Colors.black,
            );
          },
          itemCount: _actualTrainings.length,
        ),
        onRefresh: () {
          return refresh();
        }));
    pages.add(RefreshIndicator(
        child: ListView.separated(
          itemBuilder: (context, pos) {
            //_buildRows(_finishedTrainings)
            return _buildListViewItem(_finishedTrainings[pos]);
          },
          separatorBuilder: (context, index) {
            return Container(
              height: 1,
              width: MediaQuery.of(context).size.width,
              color: Colors.black,
            );
          },
          itemCount: _finishedTrainings.length,
        ),
        onRefresh: () {
          return refresh();
        }));
    return pages;
  }

  Future<Null> refresh() async {
    List result =
        await Future.wait([APISupport.getUpcomingTrainingsForAdminV2(), APISupport.getPreviousTrainingsForAdminV2()]);

    _actualTrainings = result[0]; //await APISupport.getUpcomingTrainingsForAdminV2();
    _finishedTrainings = result[1]; //await APISupport.getPreviousTrainingsForAdminV2();
    return null;
  }

  Future<Map<String, List<Training>>> _loadData() async {
    if (isDataLoaded) {
      return {"actual": _actualTrainings, "finished": _finishedTrainings};
    }

    List result =
        await Future.wait([APISupport.getUpcomingTrainingsForAdminV2(), APISupport.getPreviousTrainingsForAdminV2()]);

    List<Training> actualTrainings = result[0]; //await APISupport.getUpcomingTrainingsForAdminV2();
    List<Training> finishedTrainings = result[1]; //await APISupport.getPreviousTrainingsForAdminV2();
    return {"actual": actualTrainings, "finished": finishedTrainings};
  }

  Widget _buildListViewItem(Training training) {
    TextStyle _textStyle = TextStyle(fontSize: 16);

    return Material(
      color: training.isCancelled ? Colors.redAccent.shade200 : Colors.transparent,
      child: InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(training.type.getValue + ' ' + training.title.getValue, style: _textStyle),
              Text(convertToLevelsFormat(training.levels), style: _textStyle),
              Text(convertToTimeFormat(training.dateTime) + ' ' + convertToDateFormat(training.dateTime),
                  style: _textStyle),
              Text(training.address.getValue, style: _textStyle),
              Text(_trainersName(training), style: _textStyle),
            ],
          ),
        ),
        onTap: () async {
          await Navigator.push(
              context, MaterialPageRoute(builder: (context) => TrainingEditorPage(training: training)));
          print('returned from training redactor');
          setState(() {});
        },
        onLongPress: () async {
          await _showRemoveTrainingAlert(training);
          setState(() {});
        },
      ),
    );
  }

  Future<bool> _showRemoveTrainingAlert(Training training) {
    Widget content = SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(training.type.getValue + ' ' + training.title.getValue),
          Text(convertToLevelsFormat(training.levels)),
          Text(convertToTimeFormat(training.dateTime) + ' ' + convertToDateFormat(training.dateTime)),
          Text(training.address.getValue),
          Text(_trainersName(training)),
        ],
      ),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Удаление тренировки'),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Отмена'),
              ),
              TextButton(
                onPressed: () async {
                  Navigator.of(context).pop(false);
                  await _removeTraining(training.trainingID);
                  //await ResourceManager().removeTraining(training.trainingID);
                  setState(() {});
                },
                child: Text('Удалить'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<int> _removeTraining(String trainingID) async {
    int response = await APISupport.removeTraining(
      trainingID: trainingID,
      context: context,
      showLoadingWidget: true,
    );
    return response;
  }

  String _trainersName(Training training) {
    if (training.trainers.length == 0) return 'Без тренера';
    String trainersStr = '';
    for (User trainer in training.trainers) {
      trainersStr += trainer.name + ' ' + trainer.lastName;
      trainersStr += ', ';
    }
    return trainersStr.substring(0, trainersStr.length - 2);
  }
}
