import 'dart:async';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/constants.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/constants.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/edit_schedule/training_redactor/presenter/training_redactor_presenter.dart';
import 'package:ufavolley/views/pages/control_panel/edit_schedule/training_redactor/training_redactor_contract.dart';
import 'package:ufavolley/views/pages/control_panel/edit_schedule/training_redactor/widgets/participants_widget.dart';
import 'package:ufavolley/views/pages/control_panel/edit_schedule/training_redactor/widgets/trainer_editor_widget.dart';
import 'package:ufavolley/views/pages/control_panel/edit_schedule/training_redactor/widgets/trainer_supervisor_widget.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/training_leader_board_data.dart';

class TrainingEditorPage extends StatefulWidget {
  final Training training;

  const TrainingEditorPage({this.training});

  @override
  State createState() => new TrainingEditorPageState();
}

class TrainingEditorPageState extends State<TrainingEditorPage> implements TrainingRedactorViewContract {
  bool isDataLoaded = false;

  Training training;

  String trainingID;

  CloudValue title;
  CloudValue type;
  DateTime date;
  CloudValue address;

  List<CloudValue> levels;
  List<CloudValue> titles;
  List<CloudValue> types;
  List<CloudValue> addresses;

  Function action;
  String appBarTitle;

  TextEditingController limitCountTextController;
  TextEditingController participantsMinimumTextController;
  TextEditingController priceTextController;
  TextEditingController durationTextController;
  TextEditingController commentTextController;

  List<User> participants;
  List<String> incomingUsersUids;

  List<bool> isLevelSelected;
  bool autoContinue;
  bool isCancelled;

  TrainerEditorWidget trainerEditorWidget;
  SupervisorEditorWidget supervisorEditorWidget;
  TrainingParticipantsEditorWidget trainingParticipantsEditorWidget;

  TrainingRedactorPresenterContract _presenter;

  bool isUsersListLoaded = false;
  bool isLeaderBoardDataLoaded = false;

  TrainingLeaderBoardData leaderBoardData;

  @override
  void initState() {
    training = widget.training;

    ResourceManager resourceManager = ResourceManager();
    titles = resourceManager.containers[CloudResource.TRAINING_TITLE].values;
    types = resourceManager.containers[CloudResource.TRAINING_TYPE].values;
    addresses = resourceManager.containers[CloudResource.ADDRESS].values;
    levels = resourceManager.containers[CloudResource.LEVEL].values;
    isLevelSelected = [false, false, false, false, false];
    autoContinue = false;
    isCancelled = false;
    limitCountTextController = TextEditingController();
    priceTextController = TextEditingController();
    durationTextController = TextEditingController();
    commentTextController = TextEditingController();
    participantsMinimumTextController = TextEditingController();
    trainerEditorWidget = TrainerEditorWidget(trainingTrainers: []);
    supervisorEditorWidget = SupervisorEditorWidget(trainingSupervisors: []);

    if (training != null) {
      action = _editTraining;
      appBarTitle = 'Редактирование';
      participants = training.participants;
      incomingUsersUids = training.incomingUsersUids;
    } else {
      action = _createTraining;
      appBarTitle = 'Создание';
      participants = [];
      incomingUsersUids = [];
    }
    if (training != null) _initFieldsValues(training: training);
    _presenter = TrainingRedactorPresenter(this, widget.training);
    super.initState();
  }

  void _initFieldsValues({Training training}) {
    trainingID = '';
    if (training != null) {
      trainingID = training.trainingID;
      type = training.type;
      title = training.title;
      address = training.address;
      _selectLevel(training.levels);
      trainerEditorWidget.addTrainers(training.trainers);
      supervisorEditorWidget.addSupervisors(training.supervisors);
      //if(training.capacity != null){
      limitCountTextController.text = training.capacity.toString();
      //}

      //if(training.price != null){
      priceTextController.text = training.price.toString();
      //}

      date = training.dateTime;
      autoContinue = training.isAutoContinue;
      isCancelled = training.isCancelled;
      if (training.comment != null) commentTextController.text = training.comment;

      if (training.durationImMinutes != null) durationTextController.text = training.durationImMinutes.toString();

      if (training.participantsMinimum != null)
        participantsMinimumTextController.text = training.participantsMinimum.toString();
    }
  }

  void _selectLevel(List<CloudValue> levels) {
    for (CloudValue level in levels) {
      if (level.getValue == 'Лайт') isLevelSelected[0] = true;
      if (level.getValue == 'Лайт+') isLevelSelected[1] = true;
      if (level.getValue == 'Медиум') isLevelSelected[2] = true;
      if (level.getValue == C.litePlusProLevelName) isLevelSelected[3] = true;
      if (level.getValue == 'Хард') isLevelSelected[4] = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.cancel),
              onPressed: () async {
                bool result = await _showCancelTrainingAlert();

                if (result == null) result = false;

                if (result) {
                  print('cancel training ${training.trainingID}');

                  await APISupport.cancelTraining(trainingID: training.trainingID);

                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      title: Text('Вы отменили тренировку'),
                    ),
                  );
                }
              }),
          IconButton(
              icon: Icon(Icons.mail),
              onPressed: () async {
                bool result = await _showMailingAlert();

                if (result == null) result = false;

                if (result) {
                  print(
                      'notifications succesfully created and prepare to sending... for training ${training.trainingID}');

                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      title: Text('Уведомления успешно отправлены'),
                    ),
                  );
                }
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              DropdownButton(
                isExpanded: true,
                value: type,
                items: types.map((CloudValue cloudValue) {
                  return DropdownMenuItem(
                    value: cloudValue,
                    child: new Text(cloudValue.getValue),
                  );
                }).toList(),
                hint: Text('Тип'),
                onChanged: (CloudValue cloudValue) {
                  onTypeChanged(cloudValue);
                },
              ),
              DropdownButton(
                isExpanded: true,
                value: title,
                items: titles.map((CloudValue cloudValue) {
                  return DropdownMenuItem(
                    value: cloudValue,
                    child: new Text(cloudValue.getValue),
                  );
                }).toList(),
                hint: Text('Вид'),
                onChanged: (CloudValue cloudValue) {
                  onTitleChanged(cloudValue);
                },
              ),
              DropdownButton(
                isExpanded: true,
                value: address,
                items: addresses.map((CloudValue cloudValue) {
                  return DropdownMenuItem(
                    value: cloudValue,
                    child: new Text(cloudValue.getValue),
                  );
                }).toList(),
                hint: Text('Место'),
                onChanged: (CloudValue cloudValue) {
                  onPlaceChanged(cloudValue);
                },
              ),
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: DateTimeField(
                  initialValue: date,
                  //inputType: InputType.both,
                  format: DateFormat("HH:mm dd.MM.y"),
                  style: TextStyle(fontSize: 18),
                  decoration: InputDecoration(labelText: 'Дата/Время'),
                  onChanged: (dt) {
                    //DateTime utc = dt.toUtc();
                    //utc.toUtc();
                    //final yek = getLocation('Asia/Yekaterinburg');
                    //final yekTime = new TZDateTime.from(dt, yek);

                    setState(() => date = dt);
                  },
                  onShowPicker: (context, currentValue) async {
                    final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                    if (date != null) {
                      final time = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                      );
                      return DateTimeField.combine(date, time);
                    } else {
                      return currentValue;
                    }
                  },
                ),
              ),
              Text('Уровень: ', style: TextStyle(fontSize: 18)),
              _buildLevelsCheckboxList(),
              trainerEditorWidget,
              supervisorEditorWidget,
              TextField(
                controller: limitCountTextController,
                decoration: InputDecoration(
                  labelText: 'Количество мест в лимите',
                ),
                inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[0-9]"))],
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 18),
              ),
              TextField(
                controller: participantsMinimumTextController,
                decoration: InputDecoration(
                  labelText: 'Минимальное количество участников',
                ),
                inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[0-9]"))],
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 18),
              ),
              TextField(
                controller: priceTextController,
                decoration: InputDecoration(
                  labelText: 'Стоимость занятия',
                ),
                inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[0-9]"))],
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 18),
              ),
              TextField(
                controller: durationTextController,
                decoration: InputDecoration(
                  labelText: 'Продолжительность в минутах',
                ),
                inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[0-9]"))],
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 18),
              ),
              Container(
                child: new ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: 300.0,
                  ),
                  child: new Scrollbar(
                    child: new SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      reverse: true,
                      child: new TextField(
                        controller: commentTextController,
                        decoration: InputDecoration(
                          labelText: 'Комментарий (Не обязательно)',
                        ),
                        style: TextStyle(fontSize: 18),
                        maxLines: null,
                      ),
                    ),
                  ),
                ),
              ),
              CheckboxListTile(
                  value: autoContinue,
                  onChanged: _onAutoContinueSelected,
                  title: Text('Автопродление'),
                  activeColor: Colors.indigo),
              _getTrainingParticipantsEditorWidget()
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.done),
          onPressed: () {
            _onConfirm();
          }),
    );
  }

  Widget _getTrainingParticipantsEditorWidget() {
    if (training == null) {
      return Container();
    }
    if (isLeaderBoardDataLoaded && isUsersListLoaded) {
      if (trainingParticipantsEditorWidget == null) {
        trainingParticipantsEditorWidget = TrainingParticipantsEditorWidget(
          participants: participants,
          incomingUsersUids: incomingUsersUids,
          trainingLeaderBoardData: leaderBoardData,
        );
      }
      return trainingParticipantsEditorWidget;
    } else {
      return Center(child: CircularProgressIndicator());
    }
  }

  Widget _buildLevelsCheckboxList() {
    List<Widget> checkboxes = [];

    checkboxes.add(CheckboxListTile(
        value: isLevelSelected[0], onChanged: _onLiteLevelSelected, title: Text('Лайт'), activeColor: Colors.indigo));

    checkboxes.add(CheckboxListTile(
        value: isLevelSelected[1],
        onChanged: _onLitePlusLevelSelected,
        title: Text('Лайт+'),
        activeColor: Colors.indigo));

    checkboxes.add(CheckboxListTile(
        value: isLevelSelected[3],
        onChanged: _onMediumPlusLevelSelected,
        title: Text(C.litePlusProLevelName),
        activeColor: Constants.litePlusProColor));

    checkboxes.add(CheckboxListTile(
        value: isLevelSelected[2],
        onChanged: _onMediumLevelSelected,
        title: Text('Медиум'),
        activeColor: Colors.indigo));

    checkboxes.add(CheckboxListTile(
        value: isLevelSelected[4], onChanged: _onHardLevelSelected, title: Text('Хард'), activeColor: Colors.black));

    return Column(children: checkboxes);
  }

  Future<bool> _showCancelTrainingAlert() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Отмена тренировки', style: TextStyle(color: Colors.red)),
            content: Text('Вы уверены, что хотите отменить тренировку?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Назад'),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text('Да, отменить'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<bool> _showMailingAlert() {
    TextEditingController _titleEditingController = TextEditingController();
    TextEditingController _messageEditingController = TextEditingController();

    Container content = Container(
      height: 250,
      child: Column(
        children: <Widget>[
          TextField(
            controller: _titleEditingController,
            decoration: InputDecoration(border: InputBorder.none, hintText: 'Заголовок'),
          ),
          Expanded(
            child: TextField(
              controller: _messageEditingController,
              decoration: InputDecoration(border: InputBorder.none, hintText: 'Сообщение'),
              keyboardType: TextInputType.multiline,
              maxLines: 22,
            ),
          )
        ],
      ),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Рассылка уведомлений', style: TextStyle(color: Colors.red)),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Назад'),
              ),
              TextButton(
                onPressed: () async {
                  var data = {
                    'title': _titleEditingController.text,
                    'body': _messageEditingController.text,
                    'uids': training.participants.map((user) => user.uid).toList(),
                  };

                  showLoadingAlert(context);
                  await APISupport.broadcastPushNotification(data: data);
                  Navigator.of(context).pop();

                  Navigator.of(context).pop(true);
                },
                child: Text('Отправить'),
              )
            ],
          ),
        ) ??
        false;
  }

  bool isLevelSelect() {
    return !(isLevelSelected[0] ||
        isLevelSelected[1] ||
        isLevelSelected[2] ||
        isLevelSelected[3] ||
        isLevelSelected[4]);
  }

  Future<bool> _onConfirm() {
    if (type == null ||
        title == null ||
        address == null ||
        isLevelSelect() ||
        limitCountTextController.text == '' ||
        priceTextController.text == '' ||
        date == null)
      return showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text('Ошибка', style: TextStyle(color: Colors.red)),
              content: Text('Нельзя оставлять пустые поля'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text('Ок'),
                )
              ],
            ),
          ) ??
          false;

    Container content = Container(
      height: 250,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(type.getValue),
            Text(title.getValue),
            Text('Время: ' + convertToTimeFormat(date)),
            Text('Дата: ' + convertToDateFormat(date)),
            Text('Место: ' + address.getValue),
            Text('Уровни: ' + displaySelectedLevelsList()),
            Text('Тренер: ' + displayTrainer()),
            Text('Администратор: ' + displaySupervisors()),
            Text('Мест в лимите: ' + limitCountTextController.text),
            Text('Цена: ' + priceTextController.text),
            Text('Минимум: ' + participantsMinimumTextController.text)
          ],
        ),
      ),
    );

    return showDialog(
          context: context,
          builder: (inContext) => AlertDialog(
            title: Text(appBarTitle + ' занятия'),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(inContext).pop(false),
                child: Text('Отмена'),
              ),
              TextButton(
                onPressed: () async {
                  Training training = Training(
                      trainingID: trainingID,
                      type: type,
                      title: title,
                      levels: _getSelectedLevels(),
                      address: address,
                      trainerIDs: trainerEditorWidget.trainingTrainers.map((trainer) => trainer.uid).toList(),
                      trainers: [],
                      capacity: int.parse(limitCountTextController.text),
                      reserve: 1000,
                      price: int.parse(priceTextController.text),
                      dateTime: date,
                      participants: participants,
                      isAutoContinue: autoContinue,
                      incomingUsersUids: incomingUsersUids,
                      //widget.training == null ? [] : widget.training.incomingUsersUids,
                      durationImMinutes: durationTextController.text != null && durationTextController.text != ''
                          ? int.parse(durationTextController.text)
                          : 0,
                      comment: commentTextController.text != null && commentTextController.text != ''
                          ? commentTextController.text
                          : '',
                      isCancelled: isCancelled,
                      participantsMinimum:
                          participantsMinimumTextController.text != null && participantsMinimumTextController.text != ''
                              ? int.parse(participantsMinimumTextController.text)
                              : 0,
                      supervisors: supervisorEditorWidget.trainingSupervisors);
                  await action(training);
                  Navigator.of(inContext).pop(false);
                  Navigator.of(inContext).pop();
                },
                child: Text('Подтвердить'),
              )
            ],
          ),
        ) ??
        false;
  }

  void onTitleChanged(CloudValue cloudValue) {
    setState(() {
      this.title = cloudValue;
    });
  }

  void onTypeChanged(CloudValue cloudValue) {
    setState(() {
      this.type = cloudValue;
    });
  }

  void onPlaceChanged(CloudValue cloudValue) {
    setState(() {
      this.address = cloudValue;
    });
  }

  void _onLiteLevelSelected(bool value) => setState(() => isLevelSelected[0] = value);

  void _onLitePlusLevelSelected(bool value) => setState(() => isLevelSelected[1] = value);

  void _onMediumLevelSelected(bool value) => setState(() => isLevelSelected[2] = value);

  void _onMediumPlusLevelSelected(bool value) => setState(() => isLevelSelected[3] = value);

  void _onHardLevelSelected(bool value) => setState(() => isLevelSelected[4] = value);

  void _onAutoContinueSelected(bool value) => setState(() => autoContinue = value);

  Future<int> _createTraining(Training training) async {
    int responseCode = await APISupport.addTraining(training: training, context: context, showLoadingWidget: true);
    //await ResourceManager().createTraining(training, context);
    return responseCode;
  }

  Future<int> _editTraining(Training training) async {
    showLoadingAlert(context);
    await Future.wait([
      APISupport.updateTraining(training: training, context: context, showLoadingWidget: false),
      trainingParticipantsEditorWidget.setLeaderBoardData()
    ]);
    Navigator.of(context).pop();
    return 0;
  }

  List<CloudValue> _getSelectedLevels() {
    List<CloudValue> levels = [];
    if (isLevelSelected[0]) {
      levels.add(this.levels.where((l) => l.getValue == 'Лайт').toList()[0]);
    }
    if (isLevelSelected[1]) {
      levels.add(this.levels.where((l) => l.getValue == 'Лайт+').toList()[0]);
    }
    if (isLevelSelected[2]) {
      levels.add(this.levels.where((l) => l.getValue == 'Медиум').toList()[0]);
    }
    if (isLevelSelected[3]) {
      levels.add(this.levels.where((l) => l.getValue == C.litePlusProLevelName).toList()[0]);
    }
    if (isLevelSelected[4]) {
      levels.add(this.levels.where((l) => l.getValue == 'Хард').toList()[0]);
    }
    return levels;
  }

  String displaySelectedLevelsList() {
    String levels = '';
    if (isLevelSelected[0]) levels += 'Лайт/';
    if (isLevelSelected[1]) levels += 'Лайт+/';
    if (isLevelSelected[3]) levels += '${C.litePlusProLevelName}/';
    if (isLevelSelected[2]) levels += 'Медиум/';
    if (isLevelSelected[4]) levels += 'Хард/';
    return levels.substring(0, levels.length - 1);
  }

  String displayTrainer() {
    if (trainerEditorWidget.trainingTrainers.length == 0) return 'Без тренера';
    String trainers = '';
    for (int i = 0; i < trainerEditorWidget.trainingTrainers.length; i++) {
      trainers += trainerEditorWidget.trainingTrainers[i].lastName + ' ' + trainerEditorWidget.trainingTrainers[i].name;
      trainers += ', ';
    }
    return trainers.substring(0, trainers.length - 2);
  }

  String displaySupervisors() {
    if (supervisorEditorWidget.trainingSupervisors.length == 0) return 'Без админа';
    String trainers = '';
    for (int i = 0; i < supervisorEditorWidget.trainingSupervisors.length; i++) {
      trainers += supervisorEditorWidget.trainingSupervisors[i].lastName +
          ' ' +
          supervisorEditorWidget.trainingSupervisors[i].name;
      trainers += ', ';
    }
    return trainers.substring(0, trainers.length - 2);
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }

  @override
  void onTrainersLoaded(List<User> trainers) {
    // TODO: implement onTrainersLoaded
  }

  @override
  void onUsersListLoaded(List<User> users) {
    training.participants = users;
    participants = users;
    isUsersListLoaded = true;
    setState(() {});
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    // TODO: implement showError
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void onTrainingLeaderBoardDataLoaded(TrainingLeaderBoardData trainingLeaderBoardData) {
    this.leaderBoardData = trainingLeaderBoardData;
    setState(() {
      isLeaderBoardDataLoaded = true;
    });
  }
}
