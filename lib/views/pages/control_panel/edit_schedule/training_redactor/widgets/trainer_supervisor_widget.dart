import 'package:flutter/material.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/views/dialogs/select_user/view/select_user_view.dart';

class SupervisorEditorWidget extends StatefulWidget {
  final List<User> trainingSupervisors;

  const SupervisorEditorWidget({@required this.trainingSupervisors});

  @override
  State createState() => new SupervisorEditorWidgetState();

  void addSupervisors(List<User> trains) {
    for (User tr in trains) trainingSupervisors.add(tr);
  }
}

class SupervisorEditorWidgetState extends State<SupervisorEditorWidget> {
  List<User> trainingSupervisors;

  @override
  void initState() {
    trainingSupervisors = widget.trainingSupervisors;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Список администраторов: ', style: TextStyle(fontSize: 20)),
        Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Column(
              children: _buildSupervisorEditorRows(),
            ))
      ],
    );
  }

  List<Widget> _buildSupervisorEditorRows() {
    List<Widget> rows = [];
    for (int i = 0; i < trainingSupervisors.length; i++) {
      Widget row = Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(vertical: 4),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Text(trainingSupervisors[i].lastName + ' ' + trainingSupervisors[i].name,
                    style: TextStyle(fontSize: 18, color: Colors.indigo)),
              ),
              onTap: () {
                _showRemoveSupervisorAlert(trainingSupervisors[i], _onSupervisorRemoved);
              }),
        ),
      );
      rows.add(row);
    }
    TextButton addTrainerButton = TextButton(
      onPressed: () {
        _showAddSupervisorAlert(_onSupervisorAdded);
      },
      child: Text('+'),
    );
    Row controlButton = Row(
      children: <Widget>[addTrainerButton],
    );
    rows.add(controlButton);
    return rows;
  }

  void _onSupervisorAdded(User trainer) async {
    widget.trainingSupervisors.add(trainer);
    setState(() {});
  }

  void _onSupervisorRemoved(User trainer) {
    widget.trainingSupervisors.remove(trainer);
    setState(() {});
  }

  Future<bool> _showAddSupervisorAlert(Function listener) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => SelectUserView(SelectUserView.modeSupervisors, listener: listener));
  }

  Future<bool> _showRemoveSupervisorAlert(User trainer, Function listener) {
    Container content = Container(
      child: Text('Удалить администратора: ' + trainer.lastName + ' ' + trainer.name + ' из тренировки?'),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Удаление администратора'),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Отмена'),
              ),
              TextButton(
                onPressed: () {
                  listener(trainer);
                  Navigator.of(context).pop(false);
                },
                child: Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }
}
