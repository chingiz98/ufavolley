import 'package:flutter/material.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/edit_users/edit_user/view/edit_user_page.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_details.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_group.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/training_leader_board_data.dart';

class TrainingParticipantsEditorWidget extends StatefulWidget {
  final TrainingLeaderBoardData trainingLeaderBoardData;
  final List<User> participants;
  final List<String> incomingUsersUids;

  const TrainingParticipantsEditorWidget(
      {@required this.participants, @required this.incomingUsersUids, @required this.trainingLeaderBoardData});

  @override
  State createState() => TrainingParticipantsEditorWidgetState();

  Future<int> setLeaderBoardData() async {
    return await APISupport.setLeaderBoardData(trainingLeaderBoardData);
  }
}

class TrainingParticipantsEditorWidgetState extends State<TrainingParticipantsEditorWidget> {
  List<String> places = ["-", "1", "2", "3", "4"];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Список участников: ', style: TextStyle(fontSize: 20)),
        Container(
            padding: EdgeInsets.only(top: 8, bottom: 16),
            child: Column(
              children: _buildParticipantsWidgetContent(),
            ))
      ],
    );
  }

  List<Widget> _buildParticipantsWidgetContent() {
    List<Widget> rows = _buildUserRowItems();
    rows.add(_buildControls());
    return rows;
  }

  Widget _buildControls() {
    TextButton addTrainerButton = TextButton(
      onPressed: () {
        _showAddParticipantAlert(_onParticipantAdded);
      },
      child: Text('+'),
    );
    return Row(
      children: <Widget>[
        addTrainerButton,
      ],
    );
  }

  void showEditUserAlert(User user, Function listener) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => EditUserPage(listener: listener, user: user)));
  }

  String _getUserRatingPlace(User user) {
    for (int i = 0; i < widget.trainingLeaderBoardData.groups.length; i++) {
      for (int j = 0; j < widget.trainingLeaderBoardData.groups[i].users.length; j++) {
        if (user.uid == widget.trainingLeaderBoardData.groups[i].users[j].uid) {
          int place = widget.trainingLeaderBoardData.groups[i].ratingDetails.place;
          if (place == 0) {
            return "-";
          } else {
            return place.toString();
          }
        }
      }
    }
    return "-";
  }

  void _updateLeaderBoardData(User user, String oldPlace, String newPlace) {
    int oldPlaceInt = int.tryParse(oldPlace) ?? 0;
    bool searchWhileTrue = true;
    for (int i = 0; i < widget.trainingLeaderBoardData.groups.length && searchWhileTrue; i++) {
      if (widget.trainingLeaderBoardData.groups[i].ratingDetails.place == oldPlaceInt) {
        for (int j = 0; j < widget.trainingLeaderBoardData.groups[i].users.length && searchWhileTrue; j++) {
          if (user.uid == widget.trainingLeaderBoardData.groups[i].users[j].uid) {
            widget.trainingLeaderBoardData.groups[i].users.removeAt(j);
            searchWhileTrue = false;
          }
        }
      }
    }
    searchWhileTrue = true;
    bool groupFound = false;
    int newPlaceInt = int.tryParse(newPlace) ?? 0;
    for (int i = 0; i < widget.trainingLeaderBoardData.groups.length && searchWhileTrue; i++) {
      if (widget.trainingLeaderBoardData.groups[i].ratingDetails.place == newPlaceInt) {
        widget.trainingLeaderBoardData.groups[i].users.add(user);
        searchWhileTrue = false;
        groupFound = true;
      }
    }
    if (!groupFound) {
      List<User> users = [user];
      RatingDetails ratingDetails = RatingDetails(newPlaceInt, getRatingByPlace(newPlaceInt));
      RatingGroup ratingGroup = RatingGroup(users, ratingDetails);
      widget.trainingLeaderBoardData.groups.add(ratingGroup);
    }
  }

  int getRatingByPlace(int place) {
    if (place == 1) {
      return 40;
    }
    if (place == 2) {
      return 30;
    }
    if (place == 3) {
      return 20;
    }
    if (place == 4) {
      return 10;
    }
    return 0;
  }

  List<Widget> _buildUserRowItems() {
    List<Widget> rows = [];
    for (int i = 0; i < widget.participants.length; i++) {
      bool hasCome = _isUserHasCome(widget.participants[i], widget.incomingUsersUids);

      String place = _getUserRatingPlace(widget.participants[i]);
      DropdownButton<String> placeSelector = DropdownButton(
        value: place,
        items: places.map((String place) {
          return DropdownMenuItem(
            value: place,
            child: Text(place, style: TextStyle(fontSize: 18)),
          );
        }).toList(),
        hint: Text('Тип', style: TextStyle(fontSize: 18)),
        onChanged: (String newPlace) {
          setState(() {
            _updateLeaderBoardData(widget.participants[i], place, newPlace);
          });
        },
      );

      Widget row = Row(
        children: [
          Checkbox(
              value: hasCome,
              onChanged: (newValue) {
                setState(() {
                  hasCome = newValue;
                });
                if (hasCome) {
                  widget.incomingUsersUids.add(widget.participants[i].uid);
                } else {
                  widget.incomingUsersUids.remove(widget.participants[i].uid);
                }
              }),
          Expanded(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.symmetric(vertical: 14),
                  child: Text(
                      (i + 1).toString() + '. ' + widget.participants[i].lastName + ' ' + widget.participants[i].name,
                      style: TextStyle(fontSize: 18, color: Colors.indigo)),
                ),
                onTap: () {
                  showEditUserAlert(widget.participants[i], _onParticipantChanged);
                },
                onLongPress: () {
                  _showRemoveParticipantAlert(widget.participants[i], _onParticipantRemoved);
                },
              ),
            ),
          ),
          placeSelector
        ],
      );
      rows.add(row);
    }
    return rows;
  }

  bool _isUserHasCome(User user, List incoming) {
    for (String uid in incoming) {
      if (uid == user.uid) {
        return true;
      }
    }
    return false;
  }

  void _onParticipantChanged(User participant) {
    for (int i = 0; i < widget.participants.length; i++) {
      if (widget.participants[i].uid == participant.uid) {
        widget.participants[i] = participant;
        break;
      }
    }
    setState(() {});
  }

  void _onParticipantAdded(User participant) async {
    showLoadingAlert(context);
    String uid = await APISupport.addNewUser(user: participant, token: '');
    Navigator.of(context).pop();
    participant.uid = uid;

    widget.participants.add(participant);
    setState(() {});
  }

  void _onParticipantRemoved(User participant) {
    widget.participants.remove(participant);
    setState(() {});
  }

  Future<bool> _showAddParticipantAlert(Function listener) {
    TextEditingController nameController = TextEditingController();
    TextEditingController lastNameController = TextEditingController();

    Container content = Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'Имя',
              ),
              style: TextStyle(fontSize: 18),
            ),
            TextField(
              controller: lastNameController,
              decoration: InputDecoration(
                labelText: 'Фамилия',
              ),
              style: TextStyle(fontSize: 18),
            ),
          ],
        ),
      ),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Добавление незарегистрированного участника'),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Отмена'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  listener(
                    User(
                      name: nameController.text,
                      lastName: lastNameController.text,
                      byAdmin: true,
                      gender: true,
                    ),
                  );
                  nameController.clear();
                  lastNameController.clear();
                },
                child: Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<bool> _showRemoveParticipantAlert(User participant, Function listener) {
    Container content = Container(
      child: Text(
        'Удалить пользователя: ' + participant.lastName + ' ' + participant.name + ' из тренировки?',
      ),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Удаление пользователя'),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Отмена'),
              ),
              TextButton(
                onPressed: () {
                  listener(participant);
                  Navigator.of(context).pop(false);
                },
                child: Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }
}
