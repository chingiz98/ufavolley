import 'package:flutter/material.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/views/dialogs/select_user/view/select_user_view.dart';

class TrainerEditorWidget extends StatefulWidget {
  final List<User> trainingTrainers;

  const TrainerEditorWidget({@required this.trainingTrainers});

  @override
  State createState() => new TrainerEditorWidgetState();

  void addTrainers(List<User> trains) {
    for (User tr in trains) trainingTrainers.add(tr);
  }
}

class TrainerEditorWidgetState extends State<TrainerEditorWidget> {
  List<User> trainingTrainers;

  @override
  void initState() {
    trainingTrainers = widget.trainingTrainers;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Список тренеров: ', style: TextStyle(fontSize: 20)),
        Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Column(
              children: _buildTrainerEditorRows(),
            ))
      ],
    );
  }

  List<Widget> _buildTrainerEditorRows() {
    List<Widget> rows = [];
    for (int i = 0; i < trainingTrainers.length; i++) {
      Widget row = Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(vertical: 4),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Text(trainingTrainers[i].lastName + ' ' + trainingTrainers[i].name,
                    style: TextStyle(fontSize: 18, color: Colors.indigo)),
              ),
              onTap: () {
                _showRemoveTrainerAlert(trainingTrainers[i], _onTrainerRemoved);
              }),
        ),
      );
      rows.add(row);
    }
    TextButton addTrainerButton = TextButton(
      onPressed: () {
        _showAddTrainerAlert(_onTrainerAdded);
      },
      child: Text('+'),
    );
    Row controlButton = Row(
      children: <Widget>[addTrainerButton],
    );
    rows.add(controlButton);
    return rows;
  }

  void _onTrainerAdded(User trainer) async {
    widget.trainingTrainers.add(trainer);
    setState(() {});
  }

  void _onTrainerRemoved(User trainer) {
    widget.trainingTrainers.remove(trainer);
    setState(() {});
  }

  Future<bool> _showAddTrainerAlert(Function listener) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => SelectUserView(SelectUserView.modeTrainers, listener: listener));
  }

  Future<bool> _showRemoveTrainerAlert(User trainer, Function listener) {
    Container content = Container(
      child: Text('Удалить тренера: ' + trainer.lastName + ' ' + trainer.name + ' из тренировки?'),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Удаление тренера'),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Отмена'),
              ),
              TextButton(
                onPressed: () {
                  listener(trainer);
                  Navigator.of(context).pop(false);
                },
                child: Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }
}
