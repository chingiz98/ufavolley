import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/training_leader_board_data.dart';

import '../training_redactor_contract.dart';

class TrainingRedactorPresenter implements TrainingRedactorPresenterContract {

  TrainingRedactorViewContract _view;
  Training _training;

  List<User> _trainers;

  TrainingRedactorPresenter(TrainingRedactorViewContract view, Training training) {
    this._view = view;
    this._training = training;
    if (_training != null) {
      _loadTrainingUsers();
      _loadTrainingLeaderBoardData();
    }
  }


  void _loadTrainingUsers() async {
    List<User> usersList = await APISupport.getTrainingUsers(trainingID: _training.trainingID);
    _training.participants = usersList;
    _view.onUsersListLoaded(usersList);
  }

  void _loadTrainingLeaderBoardData() async {
    if (_training != null) {
      TrainingLeaderBoardData trainingLeaderBoardData = await APISupport.getLeaderBoardData(_training.trainingID);
      _view.onTrainingLeaderBoardDataLoaded(trainingLeaderBoardData);
    } else {
      _view.onTrainingLeaderBoardDataLoaded(null);
    }
  }

  @override
  void onAutoContinueTrainingSelected(bool isSelected) {
    // TODO: implement onAutoContinueTrainingSelected
  }

  @override
  void onClickAddTrainerButton() {
    // TODO: implement onClickAddTrainerButton
  }

  @override
  void onClickAddUserButton() {
    // TODO: implement onClickAddUserButton
  }

  @override
  void onClickCloseTrainingButton() {
    // TODO: implement onClickCloseTrainingButton
  }

  @override
  void onClickMailingButton() {
    // TODO: implement onClickMailingButton
  }

  @override
  void onClickOnUser(User user) {
    // TODO: implement onClickOnUser
  }

  @override
  void onClickOnUserAttendance(User user) {
    // TODO: implement onClickOnUserAttendance
  }

  @override
  void onClickSaveChangesButton() {
    // TODO: implement onClickSaveChangesButton
  }

  @override
  void onLiteLevelSelected(bool isSelected) {
    // TODO: implement onLiteLevelSelected
  }

  @override
  void onLitePlusLevelSelected(bool isSelected) {
    // TODO: implement onLitePlusLevelSelected
  }

  @override
  void onMediumLevelSelected(bool isSelected) {
    // TODO: implement onMediumLevelSelected
  }

  @override
  void onTrainingAddressChanged(CloudValue newAddress) {
    // TODO: implement onTrainingAddressChanged
  }

  @override
  void onTrainingCommentChanged(String newComment) {
    // TODO: implement onTrainingCommentChanged
  }

  @override
  void onTrainingDateChanged(DateTime newDate) {
    // TODO: implement onTrainingDateChanged
  }

  @override
  void onTrainingLimitChanged(int newLimit) {
    // TODO: implement onTrainingLimitChanged
  }

  @override
  void onTrainingPriceChanged(int newPrice) {
    // TODO: implement onTrainingPriceChanged
  }

  @override
  void onTrainingTitleChanged(CloudValue newTitle) {
    // TODO: implement onTrainingTitleChanged
  }

  @override
  void onTrainingTypeChanged(CloudValue newType) {
    // TODO: implement onTrainingTypeChanged
  }

  @override
  void loadTrainers() async {
    _view.showLoading();
    _trainers = await APISupport.getTrainers();
    _view.hideLoading();
    _view.onTrainersLoaded(_trainers);
  }

}