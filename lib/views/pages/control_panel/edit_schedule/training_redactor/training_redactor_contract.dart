import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/training_leader_board_data.dart';

abstract class TrainingRedactorViewContract implements BaseView {
  void onTrainersLoaded(List<User> trainers);
  void onUsersListLoaded(List<User> users);
  void onTrainingLeaderBoardDataLoaded(TrainingLeaderBoardData trainingLeaderBoardData);
}

abstract class TrainingRedactorPresenterContract implements BasePresenter {
  void loadTrainers();
  void onTrainingTitleChanged(CloudValue newTitle);
  void onTrainingTypeChanged(CloudValue newType);
  void onTrainingAddressChanged(CloudValue newAddress);
  void onTrainingDateChanged(DateTime newDate);
  void onLiteLevelSelected(bool isSelected);
  void onLitePlusLevelSelected(bool isSelected);
  void onMediumLevelSelected(bool isSelected);
  void onClickAddTrainerButton();
  void onTrainingLimitChanged(int newLimit);
  void onTrainingPriceChanged(int newPrice);
  void onTrainingCommentChanged(String newComment);
  void onAutoContinueTrainingSelected(bool isSelected);
  void onClickOnUser(User user);
  void onClickOnUserAttendance(User user);
  void onClickAddUserButton();
  void onClickCloseTrainingButton();
  void onClickMailingButton();
  void onClickSaveChangesButton();

}