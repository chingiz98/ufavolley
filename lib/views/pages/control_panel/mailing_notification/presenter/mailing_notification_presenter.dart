import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/selectable_user.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/models/user_filter.dart';
import 'package:ufavolley/utils/alert_message.dart';

import '../mailing_notification_contract.dart';

class MailingNotificationPresenter implements MailingNotificationPresenterContract {

  MailingNotificationViewContract _view;

  List<SelectableUser> _users;
  List<SelectableUser> _visibleUsers;

  UserParamFilter _paramFilter;
  UserDesiredFilter _desiredFilter;
  String _sortedBy;
  int _sortDirection;

  MailingNotificationPresenter(MailingNotificationViewContract view) {
    this._view = view;
    _paramFilter = UserParamFilter();
    _desiredFilter = UserDesiredFilter();
    _sortedBy = 'name';
    _sortDirection = 1;
  }

  List<String> _getSelectedUserUIDs() {
    List<String> uIDs = [];
    for (SelectableUser user in _users) {
      if (user.isSelected)
        uIDs.add(user.user.uid);
    }
    return uIDs;
  }

  @override
  void onClickOnCreateNotificationButton() {
    _view.showCreateNotificationDialog(_getSelectedUserUIDs());
  }

  @override
  void onClickOnSelectAllVisibleUsers(bool isSelected) {
    for (int i = 0; i < _visibleUsers.length; i++) {
      _visibleUsers[i].isSelected = isSelected;
    }
    _view.showUsers(_visibleUsers);
    _checkIsNeedToUpdateCheckbox();
  }

  @override
  void onClickOnUser(SelectableUser user) {
    for (int i = 0; i < _users.length; i++) {
      if (_users[i].user.uid == user.user.uid) {
        _users[i].isSelected = !_users[i].isSelected;
        break;
      }
    }
    _view.onUserUpdated(user);
    _checkIsNeedToUpdateCheckbox();
  }

  @override
  void loadUsers() async {
    List<User> users = await APISupport.getAllUsers();
    List<SelectableUser> userProps = [];
    for (int i = 0; i < users.length; i++)
      userProps.add(SelectableUser(user: users[i]));
    _users = userProps;
    _visibleUsers = _users;
    _view.showUsers(_visibleUsers);
  }

  @override
  void onSearchValueChanged(String newSearchValue) {
    _desiredFilter.setDesiredParameter(newSearchValue);
    List<SelectableUser> users = _applyFilters();
    _visibleUsers = users;
    _view.showUsers(_visibleUsers);
    _checkIsNeedToUpdateCheckbox();
  }

  @override
  void onSortDirectionChanged(int direction) {
    print('new direction = ' + direction.toString());
    if (direction.abs() == 1) {
      _view.showUsers(_sort(_visibleUsers, _sortedBy, direction));
    } else
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Невозможно отсортировать список'));
  }

  @override
  void onSortParamChanged(String paramName) {
    print('new paramName = ' + paramName);
    if (paramName == 'name' || paramName == 'lastName') {
      _view.showUsers(_sort(_visibleUsers, paramName, _sortDirection));
    } else
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Невозможно отсортировать список'));
  }

  List<User> _applySearchValue(List<User> users) {
    return _desiredFilter.applyTo(users);
  }

  List<User> _applyFilter(List<User> users) {
    return _paramFilter.applyTo(users);
  }

  List<SelectableUser> _applyFilters() {
    List<User> users = _users.map((SelectableUser s) => s.user).toList();
    users = _applyFilter(users);
    users = _applySearchValue(users);

    return _associateSelectableUsers(users);
  }

  List<SelectableUser> _associateSelectableUsers(List<User> users) {

    List<SelectableUser> selectableUsers = [];

    for (User user in users) {
      for (SelectableUser selectableUser in _users) {
        if (user.uid == selectableUser.user.uid) {
          selectableUsers.add(selectableUser);
          continue;
        }
      }
    }

    return selectableUsers;
  }

  List<SelectableUser> _sort(List<SelectableUser> users, String sortBy, int direction) {
    switch (sortBy) {
      case "name":
        users.sort((user1, user2) => user1.user.name.compareTo(user2.user.name));
        break;
      case "lastName":
        users.sort((user1, user2) => user1.user.lastName.compareTo(user2.user.lastName));
        break;
    }
    _sortedBy = sortBy;
    if (direction != _sortDirection && direction != 1) {
      users = users.reversed.toList();
    }
    _sortDirection = direction;
    return users;
  }

  @override
  int getSelectedUsersCount() {
    return _users.where((SelectableUser s) => s.isSelected).length;
  }

  void _checkIsNeedToUpdateCheckbox() {
    int selectedAndVisibleUsers = 0;
    for (int i = 0; i < _visibleUsers.length; i++) {
      if (_visibleUsers[i].isSelected)
        selectedAndVisibleUsers++;
    }
    if (selectedAndVisibleUsers == _visibleUsers.length)
      _view.onCheckBoxSelectedChanged(true);
    else
      _view.onCheckBoxSelectedChanged(false);
  }

  @override
  void clickOnConfirmFilterButton(UserParamFilter filter) {
    this._paramFilter = filter;
    List<SelectableUser> users = _applyFilters();
    _visibleUsers = users;
    _view.hideView();
    _view.showUsers(_visibleUsers);
    _checkIsNeedToUpdateCheckbox();
  }

  @override
  void clickOnFilterButton() {
    _view.showUsersFilterDialog(_paramFilter);
  }

  @override
  void updateUserFilter(UserParamFilter filter) {
    _paramFilter = filter;
    _visibleUsers = _applyFilters();
    _view.showUsers(_visibleUsers);
  }

}