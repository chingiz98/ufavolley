import 'package:flutter/material.dart';
import 'package:ufavolley/models/selectable_user.dart';
import 'package:ufavolley/models/user_filter.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/dialogs/send_notification/view/send_notification_dialog.dart';
import 'package:ufavolley/views/pages/control_panel/filter_users/view/filter_users_dialog.dart';
import 'package:ufavolley/views/pages/control_panel/mailing_notification/presenter/mailing_notification_presenter.dart';

import '../mailing_notification_contract.dart';

class MailingNotificationPage extends StatefulWidget {
  @override
  State createState() => new MailingNotificationPageState();
}

class MailingNotificationPageState extends State<MailingNotificationPage> implements MailingNotificationViewContract {
  MailingNotificationPresenter _presenter;

  bool isAllVisibleSelected = false;

  List<SelectableUser> userProps;

  final TextEditingController _searchEditingController = TextEditingController();

  String sortBy = 'name';
  int direction = 1;

  @override
  void initState() {
    _presenter = MailingNotificationPresenter(this);
    _searchEditingController.addListener(() {
      setState(() {
        _presenter.onSearchValueChanged(_searchEditingController.text);
      });
    });
    _presenter.loadUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var content;

    if (userProps != null) {
      content = Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              children: <Widget>[
                Text('Получателей: ' + _presenter.getSelectedUsersCount().toString(), style: TextStyle(fontSize: 16)),
                Expanded(
                  child: CheckboxListTile(
                      value: isAllVisibleSelected,
                      onChanged: _selectAllVisibleChanged,
                      title: Text('Выбрать всех'),
                      activeColor: Colors.indigo),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 8, left: 8, bottom: 4),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: TextField(
                    controller: _searchEditingController,
                    decoration: InputDecoration(hintText: 'Искать'),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                ButtonBar(
                  children: <Widget>[
                    Radio(
                        value: 'lastName',
                        groupValue: sortBy,
                        onChanged: (String newValue) {
                          sortBy = newValue;
                          direction = 1;
                          _presenter.onSortParamChanged(newValue);
                        }),
                    Text('Ф'),
                    Radio(
                        value: 'name',
                        groupValue: sortBy,
                        onChanged: (String newValue) {
                          sortBy = newValue;
                          direction = 1;
                          _presenter.onSortParamChanged(newValue);
                        }),
                    Text('И'),
                  ],
                ),
                ButtonBar(
                  children: <Widget>[
                    Radio(
                        value: 1,
                        groupValue: direction,
                        onChanged: (int newDirection) {
                          direction = newDirection;
                          _presenter.onSortDirectionChanged(newDirection);
                        }),
                    Text('↓'),
                    Radio(
                        value: -1,
                        groupValue: direction,
                        onChanged: (int newDirection) {
                          direction = newDirection;
                          _presenter.onSortDirectionChanged(newDirection);
                        }),
                    Text('↑')
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
                shrinkWrap: false,
                itemCount: userProps.length,
                itemBuilder: (context, index) =>
                    _buildListViewItem(userProps[index]) // UserItem(selectableUser: userProps[index], listener: null)
                ),
          )
        ],
      );
    } else {
      content = Center(
        child: CircularProgressIndicator(),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Рассылка уведомлений'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.clear_all),
              onPressed: () async {
                _presenter.clickOnFilterButton();
              })
        ],
      ),
      body: content,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.mail),
          onPressed: () {
            _presenter.onClickOnCreateNotificationButton();
          }),
    );
  }

  Material _buildListViewItem(SelectableUser selectableUser) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
          color: selectableUser.isSelected ? Colors.blue : Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(selectableUser.user.lastName + ' ' + selectableUser.user.name),
              Text('Уровень: ' + getLevelNameById(selectableUser.user.level)),
              Text('Номер телефона: ' + selectableUser.user.phoneNumber)
            ],
          ),
        ),
        onTap: () {
          _presenter.onClickOnUser(selectableUser);
        },
      ),
    );
  }

  void _selectAllVisibleChanged(bool value) => _presenter.onClickOnSelectAllVisibleUsers(value);

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showUsers(List<SelectableUser> users) {
    userProps = users;
    setState(() {});
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void onCheckBoxSelectedChanged(bool isSelected) {
    setState(() {
      isAllVisibleSelected = isSelected;
    });
  }

  @override
  void onUserUpdated(SelectableUser user) {
    for (int i = 0; i < userProps.length; i++) {
      if (userProps[i].user.uid == user.user.uid) {
        userProps[i] = user;
        break;
      }
    }
    setState(() {});
  }

  @override
  void showCreateNotificationDialog(List<String> userUids) {
    showDialog(context: context, builder: (context) => SendNotificationDialog(userUids: userUids));
  }

  @override
  void showUsersFilterDialog(UserParamFilter filter) {
    showDialog(
      context: context,
      builder: (context) => FilterUsersDialog(filterableUsersPresenter: _presenter, filter: filter),
    );
  }
}
