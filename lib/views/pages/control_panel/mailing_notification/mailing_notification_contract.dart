import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/filter_users_contract.dart';
import 'package:ufavolley/models/selectable_user.dart';
import 'package:ufavolley/models/user_filter.dart';

abstract class MailingNotificationViewContract implements BaseView, FilterableUsersViewContract {
  void onCheckBoxSelectedChanged(bool isSelected);
  void showCreateNotificationDialog(List<String> userUids);
  void showUsersFilterDialog(UserParamFilter filter);
  void showUsers(List<SelectableUser> users);
  void onUserUpdated(SelectableUser user);
}

abstract class MailingNotificationPresenterContract implements BasePresenter, FilterableUsersPresenterContract {
  void loadUsers();
  void onClickOnUser(SelectableUser user);
  void onClickOnSelectAllVisibleUsers(bool isSelected);
  void onClickOnCreateNotificationButton();
  void onSearchValueChanged(String newSearchValue);
  void onSortParamChanged(String paramName);
  void onSortDirectionChanged(int direction);

  int getSelectedUsersCount();
}