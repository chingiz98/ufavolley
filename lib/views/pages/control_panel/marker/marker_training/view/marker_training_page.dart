import 'package:flutter/material.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/view/training_leaderboard_settings_view.dart';

class MarkerTrainingPage extends StatefulWidget {
  final Training training;
  final Function listener;

  const MarkerTrainingPage({@required this.training, @required this.listener});

  @override
  State createState() => MarkerTrainingPageState();
}

class MarkerTrainingPageState extends State<MarkerTrainingPage> {
  List<String> incomingUsersUids;
  List<User> participants;

  @override
  void initState() {
    incomingUsersUids = widget.training.incomingUsersUids;
    participants = widget.training.participants;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('marker_training_page build called');

    return Scaffold(
      appBar: _buildAppBar(),
      body: FutureBuilder(
        future: APISupport.getTrainingUsers(trainingID: widget.training.trainingID),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            participants = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Список участников: ', style: TextStyle(fontSize: 20)),
                Container(padding: EdgeInsets.symmetric(vertical: 8)),
                Expanded(
                    child: ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return _buildUserRowItem(index);
                  },
                  itemCount: participants.length,
                ))
              ],
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Training training = widget.training;
          training.incomingUsersUids = incomingUsersUids;
          await APISupport.updateTraining(training: training, context: context, showLoadingWidget: true);
          widget.listener(training);
          Navigator.of(context).pop(true);
        },
        child: Icon(Icons.done),
      ),
    );
  }

  AppBar _buildAppBar() {
    List<Widget> actions = [];
    if (ResourceManager().currentUser.isAdmin || ResourceManager().currentUser.isMainAdmin)
      actions.add(IconButton(
          icon: Icon(Icons.assignment),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => TrainingLeaderBoardSettingsView(widget.training)));
          }));

    return AppBar(
      actions: actions,
    );
  }

  Widget _buildUserRowItem(int i) {
    bool hasCome = _isUserHasCome(participants[i], incomingUsersUids);
    Widget row = Row(
      children: <Widget>[
        Checkbox(
            value: hasCome,
            onChanged: (newValue) {
              setState(() {
                hasCome = newValue;
              });
              if (hasCome) {
                incomingUsersUids.add(participants[i].uid);
              } else {
                incomingUsersUids.remove(participants[i].uid);
              }
            }),
        Expanded(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              child: GestureDetector(
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.symmetric(vertical: 14),
                  child: Text(participants[i].lastName + ' ' + participants[i].name,
                      style: TextStyle(fontSize: 18, color: Colors.indigo)),
                ),
                onTap: () async {
                  User currentUser = participants[i];

                  if (currentUser.isDebtor == null || currentUser.isDebtor == false) {
                    bool isDebtor = await showDialog(
                      context: context,
                      builder: (context) => new AlertDialog(
                        title: Text("Статус неплательщика"),
                        content: Text("Добавить статус неплательщика?"),
                        actions: <Widget>[
                          new TextButton(
                            onPressed: () => Navigator.of(context).pop(false),
                            child: new Text('Нет'),
                          ),
                          new TextButton(
                            onPressed: () => Navigator.of(context).pop(true),
                            child: new Text('Да'),
                          ),
                        ],
                      ),
                    );
                    if (isDebtor != null && isDebtor) {
                      currentUser.isDebtor = true;
                      showLoadingAlert(context);
                      await APISupport.updateUser(user: currentUser, token: null);
                      Navigator.of(context).pop();
                    }
                  } else {
                    bool isUserNoMoreDebtor = await showDialog(
                      context: context,
                      builder: (context) => new AlertDialog(
                        title: Text("Статус неплательщика"),
                        content: Text("Удалить статус неплательщика?"),
                        actions: <Widget>[
                          new TextButton(
                            onPressed: () => Navigator.of(context).pop(false),
                            child: new Text('Нет'),
                          ),
                          new TextButton(
                            onPressed: () => Navigator.of(context).pop(true),
                            child: new Text('Да'),
                          ),
                        ],
                      ),
                    );
                    if (isUserNoMoreDebtor != null && isUserNoMoreDebtor) {
                      currentUser.isDebtor = false;
                      showLoadingAlert(context);
                      await APISupport.updateUser(user: currentUser, token: null);
                      Navigator.of(context).pop();
                    }
                  }
                },
              ),
            ),
          ),
        )
      ],
    );
    return row;
  }

  bool _isUserHasCome(User user, List incoming) {
    for (String uid in incoming) {
      if (uid == user.uid) {
        return true;
      }
    }
    return false;
  }
}
