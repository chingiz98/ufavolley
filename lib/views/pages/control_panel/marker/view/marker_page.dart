import 'package:flutter/material.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/marker/marker_training/view/marker_training_page.dart';

class MarkerPage extends StatefulWidget {
  @override
  State createState() => MarkerPageState();
}

class MarkerPageState extends State<MarkerPage> {
  List<Training> _trainings;
  bool isDataLoaded;

  TextStyle _textStyle = TextStyle(fontSize: 16);

  @override
  void initState() {
    _trainings = [];
    isDataLoaded = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('marker_page build called');

    return Scaffold(
      appBar: AppBar(title: Text("Актуальные занятия")),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            _trainings = snapshot.data;

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text('Все занятия', style: _textStyle),
                  alignment: Alignment.center,
                  height: 20,
                ),
                Expanded(
                  child: ListView.separated(
                    itemCount: _trainings.length,
                    itemBuilder: (context, index) {
                      return _buildListViewItem(index);
                    },
                    separatorBuilder: (context, index) {
                      return Container(
                        height: 1,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.black,
                      );
                    },
                  ),
                )
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
        future: loadData(),
      ),
    );
  }

  Future<List<Training>> loadData() async {
    if (isDataLoaded) {
      return _trainings;
    }

    isDataLoaded = true;

    return await APISupport.getTrainingsV2();
  }

  Widget _buildListViewItem(int index) {
    Training training = _trainings[index];
    return Material(
      color: Colors.transparent,
      child: InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(training.type.getValue + ' ' + training.title.getValue, style: _textStyle),
              Text(convertToLevelsFormat(training.levels), style: _textStyle),
              Text(convertToTimeFormat(training.dateTime) + ' ' + convertToDateFormat(training.dateTime),
                  style: _textStyle),
              Text(training.address.getValue, style: _textStyle),
              Text(_trainersName(training), style: _textStyle),
            ],
          ),
        ),
        onTap: () async {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MarkerTrainingPage(training: training, listener: _onTrainingUpdated)));
          print('returned from training redactor');
          setState(() {});
        },
      ),
    );
  }

  void _onTrainingUpdated(Training updatedTraining) {
    for (int i = 0; i < _trainings.length; i++) {
      if (_trainings[i].trainingID == updatedTraining.trainingID) {
        _trainings[i] = updatedTraining;
        break;
      }
    }
    setState(() {});
  }

  String _trainersName(Training training) {
    if (training.trainers.length == 0) return 'Без тренера';
    String trainersStr = '';
    for (User trainer in training.trainers) {
      trainersStr += trainer.name + ' ' + trainer.lastName;
      trainersStr += ', ';
    }
    return trainersStr.substring(0, trainersStr.length - 2);
  }
}
