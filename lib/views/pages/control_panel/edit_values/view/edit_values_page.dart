import 'package:flutter/material.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/views/pages/control_panel/edit_values/edit_values_contract.dart';
import 'package:ufavolley/views/pages/control_panel/edit_values/presenter/edit_values_presenter.dart';

class EditValuesPage extends StatefulWidget {
  @override
  State createState() => new EditValuesPageState();
}

class EditValuesPageState extends State<EditValuesPage>
    with SingleTickerProviderStateMixin
    implements EditValuesViewContract {
  TabController tabController;
  EditValuesPresenterContract _presenter;

  List<List<CloudValue>> cloudValues;
  List<User> trainers;

  @override
  void initState() {
    _presenter = EditValuesPresenter(this);
    _presenter.loadData();
    tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget body;
    Widget fab;

    if (_isDataLoaded()) {
      body = TabBarView(controller: tabController, children: _buildPages(cloudValues, trainers));
      fab = FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            _presenter.onClickOnAddNewValue();
          });
    } else {
      body = Center(
        child: CircularProgressIndicator(),
      );
      fab = Container();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Редактирование списков'),
        bottom: TabBar(
          controller: tabController,
          tabs: [
            Tab(text: 'Типы'),
            Tab(text: 'Виды'),
            Tab(text: 'Адреса'),
            Tab(
              text: 'Тренеры',
            )
          ],
        ),
      ),
      body: body,
      floatingActionButton: fab,
    );
  }

  bool _isDataLoaded() {
    return trainers != null;
  }

  List<Widget> _buildPages(List<List<CloudValue>> cloudValues, List<User> trainers) {
    List<Widget> pages = [];
    cloudValues.forEach((values) => pages.add(ListView(children: _buildRows(values))));
    pages.add(ListView(children: _buildTrainersRows(trainers)));
    return pages;
  }

  List<Widget> _buildRows(List<CloudValue> data) {
    List<Widget> rows = [];
    for (CloudValue cValue in data) rows.add(_buildValueItem(cValue));
    return rows;
  }

  List<Widget> _buildTrainersRows(List<User> data) {
    List<Widget> rows = [];
    for (User user in data) rows.add(_buildTrainerValueItem(user));
    return rows;
  }

  Widget _buildValueItem(CloudValue cValue) {
    return Container(
        child: Material(
      child: InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 4),
          alignment: Alignment.centerLeft,
          child: Text(
            cValue.getValue,
            style: TextStyle(fontSize: 20),
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
        ),
        onTap: () {
          _showEditValueAlert(cValue);
        },
        onLongPress: () {
          _showRemoveValueAlert(cValue);
        },
      ),
    ));
  }

  Widget _buildTrainerValueItem(User trainer) {
    return Container(
        child: Material(
      child: InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 4),
          alignment: Alignment.centerLeft,
          child: Text(
            trainer.name + ' ' + trainer.lastName,
            style: TextStyle(fontSize: 20),
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
        ),
        onTap: () {
          _showEditTrainerValueAlert(trainer);
        },
        onLongPress: () {
          _showRemoveTrainerValueAlert(trainer);
        },
      ),
    ));
  }

  Future<bool> _showAddValueAlert() {
    TextEditingController controller = TextEditingController();

    Container content = Container(
      child: TextField(
        controller: controller,
        decoration: InputDecoration(hintText: 'Название'),
      ),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Добавление в список ' + _getCurrentListName(tabController.index)),
            content: content,
            actions: <Widget>[
              new TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Отмена'),
              ),
              new TextButton(
                onPressed: () {
                  _presenter.addValue(controller.text, _indexToCloudResource(tabController.index));
                  controller.clear();
                },
                child: new Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<bool> _showEditValueAlert(CloudValue oldCloudValue) {
    TextEditingController controller = TextEditingController();

    Container content = Container(
      child: TextField(
        controller: controller,
      ),
    );

    controller.text = oldCloudValue.getValue;

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Редактирование'),
            content: content,
            actions: <Widget>[
              new TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Отмена'),
              ),
              new TextButton(
                onPressed: () {
                  CloudValue newCloudValue = CloudValue(val: controller.text, id: oldCloudValue.id);
                  controller.clear();
                  _presenter.updateValue(newCloudValue, _indexToCloudResource(tabController.index));
                },
                child: new Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<bool> _showRemoveValueAlert(CloudValue cValue) {
    Container content = Container(
      child: Text('Вы уверены, что хотите удалить: \n' + cValue.getValue),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Удаление из списка ' + _getCurrentListName(tabController.index)),
            content: content,
            actions: <Widget>[
              new TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Отмена'),
              ),
              new TextButton(
                onPressed: () {
                  _presenter.removeValue(cValue, _indexToCloudResource(tabController.index));
                },
                child: new Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<bool> _showAddTrainerValueAlert() {
    TextEditingController nameController = TextEditingController();
    TextEditingController lastNameController = TextEditingController();

    Container content = Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'Имя',
              ),
              style: TextStyle(fontSize: 18),
            ),
            TextField(
              controller: lastNameController,
              decoration: InputDecoration(
                labelText: 'Фамилия',
              ),
              style: TextStyle(fontSize: 18),
            )
          ],
        ),
      ),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Создание тренера'),
            content: content,
            actions: <Widget>[
              new TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Отмена'),
              ),
              new TextButton(
                onPressed: () {
                  _presenter.addTrainerValue(nameController.text, lastNameController.text);
                  nameController.clear();
                  lastNameController.clear();
                },
                child: new Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<bool> _showEditTrainerValueAlert(User oldTrainer) {
    TextEditingController nameController = TextEditingController();
    TextEditingController lastNameController = TextEditingController();

    Container content = Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'Имя',
              ),
              style: TextStyle(fontSize: 18),
            ),
            TextField(
              controller: lastNameController,
              decoration: InputDecoration(
                labelText: 'Фамилия',
              ),
              style: TextStyle(fontSize: 18),
            )
          ],
        ),
      ),
    );

    nameController.text = oldTrainer.name;
    lastNameController.text = oldTrainer.lastName;

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Изменение тренера'),
            content: content,
            actions: <Widget>[
              new TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Отмена'),
              ),
              new TextButton(
                onPressed: () {
                  User newTrainer = oldTrainer;
                  newTrainer.name = nameController.text;
                  newTrainer.lastName = lastNameController.text;
                  newTrainer.isTrainer = true;

                  _presenter.updateTrainerValue(newTrainer);
                  nameController.clear();
                  lastNameController.clear();
                },
                child: new Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }

  Future<bool> _showRemoveTrainerValueAlert(User trainer) {
    Container content = Container(
      child: Text('Вы уверены, что хотите удалить тренера: \n' + trainer.name + ' ' + trainer.lastName),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Удаление из списка ' + _getCurrentListName(tabController.index)),
            content: content,
            actions: <Widget>[
              new TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Отмена'),
              ),
              new TextButton(
                onPressed: () {
                  _presenter.removeTrainerValue(trainer);
                },
                child: new Text('Ок'),
              )
            ],
          ),
        ) ??
        false;
  }

  String _getCurrentListName(int index) {
    if (index == 0) return 'типов';
    if (index == 1) return 'видов';
    if (index == 2) return 'адресов';
    if (index == 3) return 'тренеров';
    return 'unknown';
  }

  CloudResource _indexToCloudResource(int index) {
    if (index == 0) return CloudResource.TRAINING_TYPE;
    if (index == 1) return CloudResource.TRAINING_TITLE;
    if (index == 2) return CloudResource.ADDRESS;
    if (index == 3) return CloudResource.TRAINERS;
    return CloudResource.UNKNOWN;
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {
    setState(() {});
  }

  @override
  void onDataUpdated(Map<CloudResource, List<CloudValue>> data, List<User> trainers) {
    this.cloudValues = data.values.toList();
    this.trainers = trainers;
    updateState();
  }

  @override
  void showAddValueDialog() {
    if (tabController.offset == 0.0) {
      if (tabController.index == 3)
        _showAddTrainerValueAlert();
      else
        _showAddValueAlert();
    }
  }
}
