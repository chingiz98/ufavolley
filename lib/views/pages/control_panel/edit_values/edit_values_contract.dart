import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/resource_manager.dart';

abstract class EditValuesViewContract implements BaseView {
  void onDataUpdated(Map<CloudResource, List<CloudValue>> data, List<User> trainers);

  void showAddValueDialog();
}

abstract class EditValuesPresenterContract implements BasePresenter {
  void loadData();

  void updateValue(CloudValue value, CloudResource resource);

  void removeValue(CloudValue value, CloudResource resource);

  void addValue(String value, CloudResource resource);

  void updateTrainerValue(User trainer);

  void removeTrainerValue(User trainer);

  void addTrainerValue(String firstName, String middleName);

  void onClickOnAddNewValue();
}
