import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/views/pages/control_panel/edit_values/edit_values_contract.dart';

class EditValuesPresenter implements EditValuesPresenterContract {

  EditValuesViewContract _view;
  ResourceManager resourceManager;

  List<CloudValue> types;
  List<CloudValue> titles;
  List<CloudValue> addresses;
  List<User> trainers;

  EditValuesPresenter(EditValuesViewContract view) {
    this._view = view;
    this.resourceManager = ResourceManager();
  }

  @override
  void onClickOnAddNewValue() {
    _view.showAddValueDialog();
  }

  @override
  void addValue(String value, CloudResource resource) async {

    _view.hideView();
    _view.showLoading();

    CloudValue cValue = CloudValue(val: value, id: '');
    dynamic newCloudValue = await APISupport.addValue(cValue: cValue, collectionName: resource.value);

    if (newCloudValue != null) {
      _addToCloudValues(resource, newCloudValue);
      _view.hideLoading();
      _view.onDataUpdated(_buildData(), trainers);
    }
    else {
      _view.hideLoading();
      _view.showError(
          AlertMessage(title: 'Ошибка', message: 'Не удалось добавить новое значение'));
    }
  }

  @override
  void removeValue(CloudValue cloudValue, CloudResource resource) async {

    _view.hideView();
    _view.showLoading();

    dynamic response = await APISupport.removeValue(cValue: cloudValue, collectionName: resource.value);
    if (response == 0) {
      _removeFromCloudValues(resource, cloudValue);
      _view.hideLoading();
      _view.onDataUpdated(_buildData(), trainers);
    }
    else {
      _view.hideLoading();
      _view.showError(
          AlertMessage(title: 'Ошибка', message: 'Не удалось удалить значение'));
    }
  }

  @override
  void updateValue(CloudValue cloudValue, CloudResource resource) async {

    _view.hideView();
    _view.showLoading();

    dynamic response = await APISupport.updateValue(cValue: cloudValue, collectionName: resource.value);
    if (response == 0) {
      _updateFromCloudValues(resource, cloudValue);
      _view.hideLoading();
      _view.onDataUpdated(_buildData(), trainers);
    }
    else {
      _view.hideLoading();
      _view.showError(
          AlertMessage(title: 'Ошибка', message: 'Не удалось изменить значение'));
    }
  }

  @override
  void loadData() async {
    Map<CloudResource, List<CloudValue>> data = _buildData();
    data.forEach((cloudResource, values) => {
      if (cloudResource == CloudResource.ADDRESS) {
        addresses = data[cloudResource]
      } else if (cloudResource == CloudResource.TRAINING_TITLE) {
        titles = data[cloudResource]
      } else if (cloudResource == CloudResource.TRAINING_TYPE) {
        types = data[cloudResource]
      }
    });
    trainers = await APISupport.getTrainers();
    _view.onDataUpdated(data, trainers);
  }

  Map<CloudResource, List<CloudValue>> _buildData() {
    List<CloudResource> cloudResources = [CloudResource.TRAINING_TYPE, CloudResource.TRAINING_TITLE, CloudResource.ADDRESS];
    Map<CloudResource, List<CloudValue>> data = Map();
    cloudResources.forEach((cloudResource) => data[cloudResource] = resourceManager.containers[cloudResource].values);
    return data;
  }

  void _addToCloudValues(CloudResource cloudResource, CloudValue newCloudValue) {
    switch (cloudResource) {
      case CloudResource.TRAINING_TYPE:
        {
          List<CloudValue> temp = List.from(types);
          types.clear();
          types.add(newCloudValue);
          types.addAll(temp);
          break;
        }
      case CloudResource.TRAINING_TITLE:
        {
          List<CloudValue> temp = List.from(titles);
          titles.clear();
          titles.add(newCloudValue);
          titles.addAll(temp);
          break;
        }
      case CloudResource.ADDRESS:
        {
          List<CloudValue> temp = List.from(addresses);
          addresses.clear();
          addresses.add(newCloudValue);
          addresses.addAll(temp);
          break;
        }
    }
  }

  void _updateFromCloudValues(CloudResource cloudResource, CloudValue newCloudValue) {
    switch (cloudResource) {
      case CloudResource.TRAINING_TYPE: {
        for (int i = 0; i < types.length; i++) {
          if (types[i].id == newCloudValue.id) {
            types[i] = newCloudValue;
            break;
          }
        }
        break;
      }
      case CloudResource.TRAINING_TITLE: {
        for (int i = 0; i < titles.length; i++) {
          if (titles[i].id == newCloudValue.id) {
            titles[i] = newCloudValue;
            break;
          }
        }
        break;
      }
      case CloudResource.ADDRESS: {
        for (int i = 0; i < addresses.length; i++) {
          if (addresses[i].id == newCloudValue.id) {
            addresses[i] = newCloudValue;
            break;
          }
        }
        break;
      }
    }
  }

  void _removeFromCloudValues(CloudResource cloudResource, CloudValue cloudValue) {
    switch (cloudResource) {
      case CloudResource.TRAINING_TYPE: {
        types.remove(cloudValue);
        break;
      }
      case CloudResource.TRAINING_TITLE: {
        titles.remove(cloudValue);
        break;
      }
      case CloudResource.ADDRESS: {
        addresses.remove(cloudValue);
        break;
      }
    }
  }

  @override
  void addTrainerValue(String firstName, String middleName) async {
    _view.hideView();
    _view.showLoading();
    User newTrainer = User(
      name: firstName,
      lastName: middleName,
      isTrainer: true,
      dateOfBirth: DateTime(2000),
      phoneNumber: "тренер без номера",
      level: "undef",
      isAdmin: false
    );
    String uid = await APISupport.addNewUser(user: newTrainer, token: null);
    newTrainer.uid = uid;
    List<User> temp = List.from(trainers);
    trainers.clear();
    trainers.add(newTrainer);
    trainers.addAll(temp);
    _view.hideLoading();
    _view.onDataUpdated(_buildData(), trainers);
  }

  @override
  void removeTrainerValue(User trainer) async {
    _view.hideView();
    _view.showLoading();
    int code = await APISupport.removeTrainer(uid: trainer.uid);
    if (code == 0) {
      trainers.remove(trainer);
      _view.hideLoading();
      _view.onDataUpdated(_buildData(), trainers);
    } else {
      _view.hideLoading();
      _view.showError(
          AlertMessage(title: 'Ошибка', message: 'Этот тренер вписан в тренировку'));
    }
  }

  @override
  void updateTrainerValue(User trainer) async {
    _view.hideView();
    _view.showLoading();

    await APISupport.updateUser(user: trainer, token: null);
    for (int i = 0; i < trainers.length; i++) {
      if (trainers[i].uid == trainer.uid) {
        trainers[i] = trainer;
        break;
      }
    }

    _view.hideLoading();
    _view.onDataUpdated(_buildData(), trainers);
  }

}