import 'package:ufavolley/models/user_filter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';

abstract class FilterUsersDialogViewContract implements BaseView {
  void updateUserFilter(UserParamFilter filter);
}

abstract class FilterUsersDialogPresenterContract implements BasePresenter {
  void onClickConfirmButton();
  void onMainAdminParamChanged(bool newValue);
  void onAdminParamChanged(bool newValue);
  void onSupervisorParamChanged(bool newValue);
  void onManParamChanged(bool newValue);
  void onWomanParamChanged(bool newValue);
  void onUnknownLevelChanged(bool newValue);
  void onLiteLevelChanged(bool newValue);
  void onLitePlusLevelChanged(bool newValue);
  void onMediumLevelChanged(bool newValue);
  void onMediumPlusLevelChanged(bool newValue);
  void onHardLevelChanged(bool newValue);
}