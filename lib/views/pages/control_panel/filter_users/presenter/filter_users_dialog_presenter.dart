import 'package:ufavolley/filter_users_contract.dart';
import 'package:ufavolley/models/user_filter.dart';

import '../filter_users_dialog_contract.dart';

class FilterUsersPresenter implements FilterUsersDialogPresenterContract {

  FilterUsersDialogViewContract _view;
  FilterableUsersPresenterContract _filterableUsersPresenter;
  UserParamFilter _filter;

  bool isAdminCheck = false;
  bool isSupervisorCheck = false;
  bool isMainAdminCheck = false;

  bool isManSexCheck = false;
  bool isWomanSexCheck = false;

  bool isUnknownLevelCheck = false;
  bool isLiteLevelCheck = false;
  bool isLitePlusLevelCheck = false;
  bool isMediumLevelCheck = false;
  bool isMediumPlusLevelCheck = false;
  bool isHardLevelCheck = false;

  FilterUsersPresenter(FilterUsersDialogViewContract view, FilterableUsersPresenterContract filterableUsersPresenter, UserParamFilter filter) {
    this._view = view;
    this._filterableUsersPresenter = filterableUsersPresenter;
    this._filter = filter;

    isAdminCheck = _filter.isAdminCheck;
    isSupervisorCheck = _filter.isSupervisorCheck;
    isMainAdminCheck = _filter.isMainAdminCheck;

    isManSexCheck = _filter.isManSexCheck;
    isWomanSexCheck = _filter.isWomanSexCheck;

    isUnknownLevelCheck = _filter.isUnknownLevelCheck;
    isLiteLevelCheck = _filter.isLiteLevelCheck;
    isLitePlusLevelCheck = _filter.isLitePlusLevelCheck;
    isMediumLevelCheck = _filter.isMediumLevelCheck;
    isMediumPlusLevelCheck = _filter.isMediumPlusLevelCheck;
    isHardLevelCheck = _filter.isHardLevelCheck;
  }

  @override
  void onClickConfirmButton() {
    UserParamFilter filter = UserParamFilter();

    filter.isMainAdminCheck = isMainAdminCheck;
    filter.isAdminCheck = isAdminCheck;
    filter.isSupervisorCheck = isSupervisorCheck;

    filter.isManSexCheck = isManSexCheck;
    filter.isWomanSexCheck = isWomanSexCheck;

    filter.isUnknownLevelCheck = isUnknownLevelCheck;
    filter.isLiteLevelCheck = isLiteLevelCheck;
    filter.isLitePlusLevelCheck = isLitePlusLevelCheck;
    filter.isMediumLevelCheck = isMediumLevelCheck;
    filter.isMediumPlusLevelCheck = isMediumPlusLevelCheck;
    filter.isHardLevelCheck = isHardLevelCheck;

    _filterableUsersPresenter.clickOnConfirmFilterButton(filter);
  }

  @override
  void onMainAdminParamChanged(bool newValue) {
    isMainAdminCheck = newValue;
  }

  @override
  void onAdminParamChanged(bool newValue) {
    isAdminCheck = newValue;
  }

  @override
  void onSupervisorParamChanged(bool newValue) {
    isSupervisorCheck = newValue;
  }

  @override
  void onManParamChanged(bool newValue) {
    isManSexCheck = newValue;
  }

  @override
  void onWomanParamChanged(bool newValue) {
    isWomanSexCheck = newValue;
  }

  @override
  void onUnknownLevelChanged(bool newValue) {
    isUnknownLevelCheck = newValue;
  }

  @override
  void onLiteLevelChanged(bool newValue) {
    isLiteLevelCheck = newValue;
  }

  @override
  void onLitePlusLevelChanged(bool newValue) {
    isLitePlusLevelCheck = newValue;
  }

  @override
  void onMediumLevelChanged(bool newValue) {
    isMediumLevelCheck = newValue;
  }

  @override
  void onHardLevelChanged(bool newValue) {
    isHardLevelCheck = newValue;
  }

  @override
  void onMediumPlusLevelChanged(bool newValue) {
    isMediumPlusLevelCheck = newValue;
  }

}