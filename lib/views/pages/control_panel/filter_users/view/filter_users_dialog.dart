import 'package:flutter/material.dart';
import 'package:ufavolley/filter_users_contract.dart';
import 'package:ufavolley/models/user_filter.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/constants.dart';
import 'package:ufavolley/views/pages/control_panel/filter_users/presenter/filter_users_dialog_presenter.dart';

import '../filter_users_dialog_contract.dart';

class FilterUsersDialog extends StatefulWidget {
  final FilterableUsersPresenterContract filterableUsersPresenter;
  final UserParamFilter filter;

  const FilterUsersDialog({@required this.filterableUsersPresenter, @required this.filter});

  @override
  State createState() => new FilterUsersDialogState();
}

class FilterUsersDialogState extends State<FilterUsersDialog> implements FilterUsersDialogViewContract {
  FilterUsersDialogPresenterContract _presenter;

  bool isAdminCheck = false;
  bool isSupervisorCheck = false;
  bool isMainAdminCheck = false;
  bool isManSexCheck = false;
  bool isWomanSexCheck = false;

  bool isUnknownLevelCheck = false;
  bool isLiteLevelCheck = false;
  bool isLitePlusLevelCheck = false;
  bool isMediumLevelCheck = false;
  bool isMediumPlusLevelCheck = false;
  bool isHardLevelCheck = false;

  void initCheckboxes() {
    isAdminCheck = widget.filter.isAdminCheck;
    isSupervisorCheck = widget.filter.isSupervisorCheck;
    isMainAdminCheck = widget.filter.isMainAdminCheck;
    isManSexCheck = widget.filter.isManSexCheck;
    isWomanSexCheck = widget.filter.isWomanSexCheck;

    isUnknownLevelCheck = widget.filter.isUnknownLevelCheck;
    isLiteLevelCheck = widget.filter.isLiteLevelCheck;
    isLitePlusLevelCheck = widget.filter.isLitePlusLevelCheck;
    isMediumLevelCheck = widget.filter.isMediumLevelCheck;
    isMediumPlusLevelCheck = widget.filter.isMediumPlusLevelCheck;
    isHardLevelCheck = widget.filter.isHardLevelCheck;
  }

  @override
  void initState() {
    _presenter = FilterUsersPresenter(this, widget.filterableUsersPresenter, widget.filter);
    initCheckboxes();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text('Выбор фильтров'),
      children: <Widget>[
        Container(alignment: Alignment.center, child: Text('Привилегии', style: TextStyle(fontSize: 18))),
        CheckboxListTile(
          value: isMainAdminCheck,
          onChanged: (bool value) {
            _presenter.onMainAdminParamChanged(value);
            setState(() {
              isMainAdminCheck = value;
            });
          },
          title: Text('Главный администратор'),
        ),
        CheckboxListTile(
          value: isAdminCheck,
          onChanged: (bool value) {
            _presenter.onAdminParamChanged(value);
            setState(() {
              isAdminCheck = value;
            });
          },
          title: Text('Редактор'),
        ),
        CheckboxListTile(
          value: isSupervisorCheck,
          onChanged: (bool value) {
            _presenter.onSupervisorParamChanged(value);
            setState(() {
              isSupervisorCheck = value;
            });
          },
          title: Text('Проверяющий'),
        ),
        Container(alignment: Alignment.center, child: Text('Пол', style: TextStyle(fontSize: 18))),
        CheckboxListTile(
          value: isManSexCheck,
          onChanged: (bool value) {
            _presenter.onManParamChanged(value);
            setState(() {
              isManSexCheck = value;
            });
          },
          title: Text('Мужской'),
        ),
        CheckboxListTile(
          value: isWomanSexCheck,
          onChanged: (bool value) {
            _presenter.onWomanParamChanged(value);
            setState(() {
              isWomanSexCheck = value;
            });
          },
          title: Text('Женский'),
        ),
        Container(alignment: Alignment.center, child: Text('Уровень', style: TextStyle(fontSize: 18))),
        CheckboxListTile(
          value: isUnknownLevelCheck,
          onChanged: (bool value) {
            _presenter.onUnknownLevelChanged(value);
            setState(() {
              isUnknownLevelCheck = value;
            });
          },
          title: Text('Неизвестно'),
        ),
        CheckboxListTile(
          value: isLiteLevelCheck,
          onChanged: (bool value) {
            _presenter.onLiteLevelChanged(value);
            setState(() {
              isLiteLevelCheck = value;
            });
          },
          title: Text('Лайт'),
        ),
        CheckboxListTile(
          value: isLitePlusLevelCheck,
          onChanged: (bool value) {
            _presenter.onLitePlusLevelChanged(value);
            setState(() {
              isLitePlusLevelCheck = value;
            });
          },
          title: Text('Лайт+'),
        ),
        CheckboxListTile(
          value: isMediumPlusLevelCheck,
          onChanged: (bool value) {
            _presenter.onMediumPlusLevelChanged(value);
            setState(() {
              isMediumPlusLevelCheck = value;
            });
          },
          title: Text(C.litePlusProLevelName),
        ),
        CheckboxListTile(
          value: isMediumLevelCheck,
          onChanged: (bool value) {
            _presenter.onMediumLevelChanged(value);
            setState(() {
              isMediumLevelCheck = value;
            });
          },
          title: Text('Медиум'),
        ),
        CheckboxListTile(
          value: isHardLevelCheck,
          onChanged: (bool value) {
            _presenter.onHardLevelChanged(value);
            setState(() {
              isHardLevelCheck = value;
            });
          },
          title: Text('Хард'),
        ),
        TextButton(
          onPressed: () {
            _presenter.onClickConfirmButton();
          },
          child: Text('Применить'),
        )
      ],
    );
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void updateUserFilter(UserParamFilter filter) {
    widget.filterableUsersPresenter.updateUserFilter(filter);
  }
}
