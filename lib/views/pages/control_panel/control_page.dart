import 'package:flutter/material.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/constants.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';

import 'edit_schedule/view/edit_schedule_page.dart';
import 'edit_users/view/edit_users_page.dart';
import 'edit_values/view/edit_values_page.dart';
import 'mailing_notification/view/mailing_notification_page.dart';
import 'marker/view/marker_page.dart';
import 'notifications_archive/view/mail_archive_page.dart';

class ControlPage extends StatefulWidget {
  @override
  State createState() => new ControlPageState();
}

class ControlPageState extends State<ControlPage> {
  @override
  Widget build(BuildContext context) {
    return Column(children: _buildControlButtonsWithPermissionsLevel(ResourceManager().currentUser));
  }

  Future<bool> _showMailingAlert() {
    TextEditingController _titleEditingController = TextEditingController();
    TextEditingController _messageEditingController = TextEditingController();

    Container content = Container(
      height: 250,
      child: Column(
        children: <Widget>[
          TextField(
            controller: _titleEditingController,
            decoration: InputDecoration(border: InputBorder.none, hintText: 'Заголовок'),
          ),
          Expanded(
            child: TextField(
              controller: _messageEditingController,
              decoration: InputDecoration(border: InputBorder.none, hintText: 'Сообщение'),
              keyboardType: TextInputType.multiline,
              maxLines: 22,
            ),
          )
        ],
      ),
    );

    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Рассылка уведомлений', style: TextStyle(color: Colors.red)),
            content: content,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Назад'),
              ),
              TextButton(
                onPressed: () async {
                  if (_titleEditingController.text == '') {
                    showError(AlertMessage(title: 'Ошибка', message: 'Заголовок не может быть пуст'));
                    return;
                  }

                  if (_messageEditingController.text == '') {
                    showError(AlertMessage(title: 'Ошибка', message: 'Сообщение не может быть пустым'));
                    return;
                  }

                  var data = {
                    'title': _titleEditingController.text,
                    'body': _messageEditingController.text,
                  };

                  //showLoadingAlert(context);

                  Navigator.of(context).pop();
                  APISupport.broadcastPushNotificationToAll(data: data);
                  print("Broadcast to everyone");
                },
                child: Text('Отправить'),
              )
            ],
          ),
        ) ??
        false;
  }

  List<Widget> _buildControlButtonsWithPermissionsLevel(User user) {
    List<Widget> buttons = [];

    Widget scheduleEditorButton = TextButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditSchedulePage(),
          ),
        );
      },
      child: Text('Управление расписанием'),
    );
    Widget notificationMailingButton = Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: Container(
            child: TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MailingNotificationPage(),
                  ),
                );
              },
              child: Text('Рассылка уведомлений'),
            ),
            decoration: BoxDecoration(
              border: Border(
                right: BorderSide(
                  color: Constants.primaryColorShades,
                  width: 2.0,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: TextButton(
            onPressed: () {
              _showMailingAlert();
            },
            child: Text('Рассылка всем'),
          ),
        ),
      ],
    );
    Widget userControlButton = TextButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditUsersPage(),
          ),
        );
      },
      child: Text('Управление пользователями'),
    );
    Widget valueEditorButton = TextButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditValuesPage(),
          ),
        );
      },
      child: Text('Управление списками данных'),
    );
    Widget userCheckerButton = TextButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MarkerPage(),
          ),
        );
      },
      child: Text('Отмечалка'),
    );
    Widget mailArchiveButton = TextButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MailArchivePage(),
          ),
        );
      },
      child: Text('Архив пушей'),
    );

    if (user.isMainAdmin || user.isAdmin) {
      buttons.add(scheduleEditorButton);
      buttons.add(notificationMailingButton);
      buttons.add(userControlButton);
      buttons.add(valueEditorButton);
      buttons.add(userCheckerButton);
      buttons.add(mailArchiveButton);

      return buttons
          .map(
            (button) => Flexible(
              child: SizedBox.expand(
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Constants.primaryColorShades,
                      width: 2.0,
                    ),
                  ),
                  child: button,
                ),
              ),
              flex: 1,
            ),
          )
          .toList();
    }

    if (user.isSupervisor) {
      buttons.add(userCheckerButton);
      return buttons;
    }

    buttons.add(
      TextButton(
        onPressed: null,
        child: Text('И как ты сюда попал??'),
      ),
    );

    return buttons;
  }

  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }
}
