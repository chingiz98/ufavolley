import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/models/user.dart';
import '../archived_notification_contract.dart';

class ArchivedNotificationPresenter implements ArchivedNotificationPresenterContract {

  ArchivedNotificationViewContract _view;
  Map<User, NotificationProp> _notifications;

  ArchivedNotificationPresenter(ArchivedNotificationViewContract view) {
    this._view = view;
  }

  @override
  void loadUsersNotifications(String notificationId) async {
    _notifications = await APISupport.getUsersNotifications(notificationId);
    _view.showUsersNotifications(_notifications);
  }

}