import 'package:flutter/material.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/notification_archive/presenter/archived_notification_presenter.dart';
import 'package:ufavolley/views/pages/notifications/user_action/view/user_action_notification_page.dart';

import '../archived_notification_contract.dart';

class ArchivedNotificationPage extends StatefulWidget {
  final ArchivedNotificationProp archivedNotificationProp;

  ArchivedNotificationPage({@required this.archivedNotificationProp});

  @override
  State createState() => ArchivedNotificationPageState();
}

class ArchivedNotificationPageState extends State<ArchivedNotificationPage>
    implements ArchivedNotificationViewContract {
  ArchivedNotificationPresenterContract _presenter;
  Map<User, NotificationProp> _notifications;

  @override
  void initState() {
    super.initState();
    _presenter = ArchivedNotificationPresenter(this);
    _presenter.loadUsersNotifications(widget.archivedNotificationProp.id);
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(title: Text("Детализация"), actions: <Widget>[
      IconButton(
          icon: Icon(Icons.textsms),
          onPressed: () async {
            await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => UserActionNotificationPage(
                        notification: widget.archivedNotificationProp)));
          })
    ]);

    Widget body;

    if (_notifications == null)
      body = Center(
        child: CircularProgressIndicator(),
      );
    else {
      List<User> keys = _notifications.keys.toList();
      body = ListView.separated(
        shrinkWrap: true,
        itemCount: keys.length,
        itemBuilder: (context, index) {
          return _buildNotification(keys[index], _notifications[keys[index]]);
        },
        separatorBuilder: (context, index) {
          return DottedDivider();
        },
      );
    }

    return Scaffold(
      appBar: appBar,
      body: body,
    );
  }

  Widget _buildNotification(User user, NotificationProp notification) {
    final TextStyle textStyle = TextStyle(
        fontFamily: 'PFDin',
        fontWeight: FontWeight.w300,
        fontSize: 24.0,
        color: Color.fromARGB(255, 102, 102, 102));

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(user.name,
                    style: textStyle, overflow: TextOverflow.ellipsis),
                Text(user.lastName,
                    style: textStyle, overflow: TextOverflow.ellipsis),
                Text(notification.read ? "Прочитано" : "Не прочитано")
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    // TODO: implement showError
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void showUsersNotifications(Map<User, NotificationProp> notifications) {
    this._notifications = notifications;
    setState(() {});
  }
}
