import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/models/user.dart';

abstract class ArchivedNotificationViewContract implements BaseView {
  void showUsersNotifications(Map<User, NotificationProp> notifications);
}

abstract class ArchivedNotificationPresenterContract implements BasePresenter {
  void loadUsersNotifications(String notificationId);
}