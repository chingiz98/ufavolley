import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:url_launcher/url_launcher.dart';

import '../profile_viewer_contract.dart';

class ProfileViewerPresenter implements ProfileViewerPresenterContract {
  ProfileViewerViewContract _view;
  User _user;

  ProfileViewerPresenter(ProfileViewerViewContract view, User user) {
    this._view = view;
    this._user = user;
    _loadVKID();
  }

  Future<void> _loadVKID() async {
    Map<String, String> emailsMap = await APISupport.getEmailsByUids(uids: [_user.uid]);
    _user.vkLink = _parseVkLinkFromEmail(emailsMap[_user.uid]);
    _view.updateState();
  }

  String _parseVkLinkFromEmail(String email) {
    return 'vk.com/id' + email.split('@')[0];
  }

  @override
  void onClickOnContactData() {
    if (_user.vkLink != null)
      _launchURL('https://' + _user.vkLink);
    else
      launch('tel:' + _user.phoneNumber);
  }

  _launchURL(String url) async {
    if (await canLaunch(url))
      await launch(url);
    else
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Невозможно отобразить страницу: $url'));
  }
}
