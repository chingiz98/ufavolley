import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';

abstract class ProfileViewerViewContract implements BaseView {
  void onVKLinkLoaded(String link);
}

abstract class ProfileViewerPresenterContract implements BasePresenter {
  void onClickOnContactData();
}
