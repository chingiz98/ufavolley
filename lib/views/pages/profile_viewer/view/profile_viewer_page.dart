import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ufavolley/constants.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/profile_viewer/presenter/profile_viewer_presenter.dart';
import 'package:ufavolley/views/pages/profile_viewer/profile_viewer_contract.dart';

class ProfileViewerPage extends StatefulWidget {
  final User user;

  ProfileViewerPage(this.user);

  @override
  State createState() => ProfileViewerPageState();
}

class ProfileViewerPageState extends State<ProfileViewerPage> implements ProfileViewerViewContract {
  int _rating;
  Widget onUserDataTapChild;

  ProfileViewerPresenterContract _presenter;
  Image profileImage;

  @override
  void initState() {
    _presenter = ProfileViewerPresenter(this, widget.user);
    _rating = widget.user.rating;
    super.initState();
  }

  Widget _getProfileImage() {
    return Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
        child: FadeInImage(
          placeholder: AssetImage('assets/default_profile_image_woman.png'),
          image: CachedNetworkImageProvider(widget.user.imageUrl),
          height: 50,
          width: 50,
          fit: BoxFit.cover,
        ));
  }

  @override
  Widget build(BuildContext context) {
    print("PROFILE BUILD WAS CALLED");

    var content = _buildContent();

    return Scaffold(body: content, appBar: AppBar(title: Text("Профиль")));
  }

  Widget _buildContent() {
    var imageWidget = LayoutBuilder(
      builder: (context, constraints) {
        return AspectRatio(
            aspectRatio: 1, child: Container(height: constraints.biggest.width, child: _getProfileImage()));
      },
    );

    var ratingWidget = Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Constants.primaryColorShades,
        customBorder: StadiumBorder(),
        child: Card(
          shape: StadiumBorder(),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            child: Row(children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 2),
                padding: EdgeInsets.only(top: 1),
                child: new Image(
                  image: new AssetImage('assets/trophy.png'),
                  height: 18,
                  width: 18,
                  fit: BoxFit.cover,
                ),
              ),
              Text(_rating.toString(), style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600))
            ]),
          ),
        ),
        onTap: () {},
      ),
    );

    var imageWithRating = Stack(
      children: <Widget>[
        imageWidget,
        Positioned(bottom: 8, right: 8, child: (_rating != null) && (_rating != 0) ? ratingWidget : Container())
      ],
    );

    Container profileImageEditorWidget = Container(
      margin: EdgeInsets.all(12.0),
      child: new Container(
          margin: EdgeInsets.only(right: 35, left: 35),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Expanded(child: imageWithRating),
            ],
          )),
    );

    CustomButton finishButton = CustomButton(
        text: 'ГОТОВО',
        size: 24.0,
        margin: EdgeInsets.only(top: 30, bottom: 6, left: 35, right: 35),
        onTap: () async {
          Navigator.pop(context);
        });

    TextStyle textStyle = TextStyle(
      fontFamily: 'PFDin',
      fontWeight: FontWeight.w500,
      fontSize: 26.0,
      color: Colors.black,
    );

    if (widget.user.vkLink != null)
      onUserDataTapChild = Text(widget.user.vkLink, style: textStyle);
    else
      onUserDataTapChild = Text(widget.user.phoneNumber, style: textStyle);

    Text name = Text(widget.user.name, style: textStyle, textAlign: TextAlign.center);
    Text lastName = Text(widget.user.lastName, style: textStyle, textAlign: TextAlign.center);
    var levelMap = ResourceManager().getCloudResourceContainerByName(CloudResource.LEVEL).getValuesMap();

    Text level = Text('Уровень: ' + levelMap[widget.user.level].getValue, style: textStyle, textAlign: TextAlign.center);

    var contacts = (widget.user.phoneNumber != "" || widget.user.vkLink != null)
        ? Row(children: <Widget>[
            Text("Контакты:", style: textStyle),
            TextButton(
              onPressed: () {
                _presenter.onClickOnContactData();
              },
              child: onUserDataTapChild,
            )
          ], mainAxisAlignment: MainAxisAlignment.center)
        : Container();

    return Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: AssetImage('assets/background.png'), fit: BoxFit.cover, alignment: Alignment.topCenter),
      ),
      child: new Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Center(
          child: new ListView(
            children: <Widget>[profileImageEditorWidget, name, lastName, level, contacts, finishButton],
          ),
        ),
      ),
    );
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text(message.title),
              content: new Text(message.message),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Закрыть',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ],
            ));
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {
    setState(() {});
  }

  @override
  void onImageLoaded(Image image) {
    // this._isImageLoaded = true;
    this.profileImage = image;
    setState(() {});
  }

  @override
  void openSchedulePage() {
    Navigator.of(context).pushNamedAndRemoveUntil('/main', (Route<dynamic> route) => false);
  }

  @override
  void setRating(int rating) {
    setState(() {
      _rating = rating;
    });
  }

  @override
  void onVKLinkLoaded(String link) {
    // TODO: implement onVKLinkLoaded
  }
}
