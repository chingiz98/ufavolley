import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/models/training.dart';

abstract class ScheduleViewContract implements BaseView {
  void showFilterPage();
  /// weekday in range 0..6, today - 0, tomorrow - 1 and other
  void moveToWeekday(int weekday);
  void onTrainingsLoaded(List<Training> trainings);
}

abstract class SchedulePresenterContract implements BasePresenter {
  /// weekday in range 0..6, today - 0, tomorrow - 1 and other
  void onClickOnWeekday(int weekday);
  void onScrollPositionChanged(double position);
  void loadTrainings();
}