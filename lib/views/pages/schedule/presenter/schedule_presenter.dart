import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/api/api_support.dart';

import '../schedule_contract.dart';

class SchedulePresenter extends BasePresenter implements SchedulePresenterContract {

  ScheduleViewContract _view;
  int selectedWeekday;
  double scrollPosition;

  SchedulePresenter(ScheduleViewContract view) {
    _view = view;
    selectedWeekday = 0;
    scrollPosition = 0.0;
  }

  @override
  void onScrollPositionChanged(double position) {
    // TODO: implement onScrollPositionChanged
  }

  @override
  void onClickOnWeekday(int weekday) {
    // TODO: implement selectWeekday
  }

  @override
  void loadTrainings() async {
    _view.onTrainingsLoaded(await APISupport.getTrainings());
  }

}