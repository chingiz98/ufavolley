import 'package:flutter/material.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/views/pages/schedule/presenter/schedule_presenter.dart';

import '../schedule_contract.dart';

class Schedule extends StatefulWidget {
  @override
  ScheduleState createState() => ScheduleState();
}

class ScheduleState extends State<Schedule> implements ScheduleViewContract {
  SchedulePresenterContract _presenter;

  @override
  void initState() {
    _presenter = SchedulePresenter(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return null;
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void moveToWeekday(int weekday) {
    // TODO: implement moveToWeekday
  }

  @override
  void onTrainingsLoaded(List<Training> trainings) {
    // TODO: implement onTrainingsLoaded
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showFilterPage() {
    // TODO: implement showFilterPage
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }
}
