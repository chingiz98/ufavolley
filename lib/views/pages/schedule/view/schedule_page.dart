import 'package:flutter/material.dart';
import 'package:ufavolley/utils/constants.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:rect_getter/rect_getter.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:async';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/views/pages/sign_up_for_training/view/sign_up_for_training_page.dart';

class Schedule extends StatefulWidget {
  @override
  ScheduleState createState() => ScheduleState();
}

class ScheduleState extends State<Schedule> {
  @override
  void initState() {
    print("initState in schedule page called");

    //await _loadData();
    //Инициализация календаря
    //тут как раз передаем callback - onCurrentDayChanged
    //который дальше распространяется по иерархии виджетов
    //можешь внимательно провалиться и увидеть весь стек вызовов
    //можно конечно запутаться, но вроде видно, если внимательно отследить

    /*
    _week = new WeekWidget(
        currentSelected: DateTime.now().weekday,
        onDaySelected: onCurrentDayChanged);
        */

    //Инициализация списка и запоминание ключей элементов, чтобы далее можно было расчитать, какие элементы находятся в поле зрения.
    _listViewKey = RectGetter.createGlobalKey();
    _listScrollController = new ScrollController();
    FirebaseMessaging msg = FirebaseMessaging.instance;

    msg.getToken().then((token) {
      print('Cloud messaging token: ' + token);
    });

    super.initState();
  }

  static final weekdayNames = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВСК'];

  static String getWeekdayName(int weekday) {
    return weekdayNames[(weekday - 1) % 7];
  }

  bool isLoaded = false;
  String uid = 'nanEtoBan';
  List<Training> trainings = [];

  bool isBottomStubAdded = false;
  bool updateWeek = false;

  Future<List<Training>> _loadData() async {
    if (!isLoaded) {
      List<Training> trainings = ResourceManager().trainings;

      int day = 0;
      if (trainings[0] != null) day = trainings[0].dateTime.weekday;
      currentWeekDay = day;

      _week = new WeekWidget(currentSelected: day, onDaySelected: onCurrentDayChanged, activeDays: activeWeekDays);

      isLoaded = true;

      for (int i = 0; i < trainings.length - 1; i++) {
        if (!activeWeekDays.contains(getWeekdayName(trainings[i + 1].dateTime.weekday)))
          activeWeekDays.add(getWeekdayName(trainings[i + 1].dateTime.weekday));
      }
    }

    ResourceManager resourceManager = ResourceManager();

    final User currentUser = resourceManager.currentUser;

    uid = currentUser.uid;

    List<Training> trainings = resourceManager.trainings;

    List<Training> levelingTrainings = [];

    final currentUserLevel = currentUser.level;

    for (Training training in trainings) {
      if (training.isLevelCanSignIn(currentUserLevel) || currentUser.canSeeAnyTraining) {
        levelingTrainings.add(training);
        continue;
      }
    }

    List<Training> list = <Training>[];
    list.add(new TrainingStub());
    list.addAll(levelingTrainings);

    return list;
  }

  var _keys = {}; //ключи элеметов в списке
  var _divKey; //ключ дивайдера с датой
  var _smallDivKey; //ключ мелкого дивайдера
  //по сути ключи дивайдеров пока не используются, т.к. я через дебаг узнал их размеры и пока жестко прописал
  //но таким способом как-то можно будет динамически размеры доставать для адаптивности

  WeekWidget _week; //календарь
  RectGetter _list; //список
  var _listViewKey; //его ключ
  ScrollController _listScrollController;
  bool _jump = false; //флаг прыжка

  //хардкод размеры
  static const double _ITEM_HEIGHT = 87.0;
  static const double _DIV_HEIGHT = 17.0;
  static const double _DATE_DIV_HEIGHT = 23.0;

  //переменная текущего дня
  int currentWeekDay = DateTime.now().weekday;

  var activeWeekDays = [];

  Widget _buildRow(Training training) {
    //проверяем. Если тренировка - заглушка, то возвращаем пустой блок без высоте и вообще без ничего.
    if (training is TrainingStub) {
      if (training.height == 0.0)
        return new Container();
      else
        return new Container(height: training.height);
    } else {
      //иначе возвращаем item тренировки
      return InkWell(
          child: TrainingItemWidget(training, uid: uid),
          highlightColor: Colors.blue,
          splashColor: Colors.indigo,
          onTap: () {
            //вот здесь клик-лисенер нажатия на item в списке.
            //tr (выбранная тренировка) - доступен и его уже можно будет передать на след экран.
            print('old listTile onTap');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SignUpForTrainingPage(training)),
            );
          });
    }
  }

  void onTrainingUpdated(Training tr) {
    print('listener called');
    for (int i = 0; i < trainings.length; i++) {
      if (tr.trainingID == trainings[i].trainingID) {
        for (int i = 0; i < ResourceManager().trainings.length; i++) {
          if (ResourceManager().trainings[i].trainingID == tr.trainingID) {
            ResourceManager().trainings[i] = tr;
          }
        }
        setState(() {
          trainings[i] = tr;
        });
        print('training updated');
        break;
      }
    }
  }

  //Callback. Вызывается при изменении дня вручную.
  void onCurrentDayChanged(DateTime date) {
    print("ITEM WAS SELECTED");
    int daysDivCount = 0;
    int smallDivCount = 0;
    for (int i = 1; i < trainings.length; i++) {
      if (i > 1) {
        if (trainings[i - 1].dateTime.weekday != trainings[i].dateTime.weekday)
          daysDivCount++;
        else
          smallDivCount++;
      }

      if (trainings[i].dateTime.weekday == date.weekday) {
        print("JUMPING");
        _jump = true;
        //_listScrollController.jumpTo(_ITEM_HEIGHT * (i - 1) + _DIV_HEIGHT * (i - 1));
        _listScrollController
            .jumpTo(_ITEM_HEIGHT * (i - 1) + _DIV_HEIGHT * smallDivCount + _DATE_DIV_HEIGHT * (daysDivCount));
        break;
      }
    }

    currentWeekDay = date.weekday;
    try {
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    print("BUILD CALLED");

    //метод, который возвращает список индексов видимых элементов (тренировок) в ListView.
    List<int> getVisible() {
      /// First, get the rect of ListView, and then traver the _keys
      /// get rect of each item by keys in _keys, and if this rect in the range of ListView's rect,
      /// add the index into result list.
      var rect = RectGetter.getRectFromKey(_listViewKey);

      var _items = <int>[];

      for (var i in _keys.keys) {
        var itemRect = RectGetter.getRectFromKey(_keys[i]);
        //print("HEIGHT IS " + itemRect.height.toString());
        //print("DIVIDER HEIGHT IS " + RectGetter.getRectFromKey(_divKey).height.toString());
        //print("SMALL DIVIDER HEIGHT IS " + RectGetter.getRectFromKey(_smallDivKey).height.toString());
        if (itemRect != null && !(itemRect.top > rect.bottom || itemRect.bottom - 5.0 < rect.top)) {
          _items.add(i);
          break;
        }
      }
      /*
      _keys.forEach((index, key) {
        var itemRect = RectGetter.getRectFromKey(key);
        //print("HEIGHT IS " + itemRect.height.toString());
        //print("DIVIDER HEIGHT IS " + RectGetter.getRectFromKey(_divKey).height.toString());
        //print("SMALL DIVIDER HEIGHT IS " + RectGetter.getRectFromKey(_smallDivKey).height.toString());
        if (itemRect != null && !(itemRect.top > rect.bottom || itemRect.bottom < rect.top)) {
          _items.add(index);

        }

      });
      */

      /// so all visible item's index are in this _items.
      return _items;
    }

    var schedulePage = FutureBuilder(
      future: _loadData(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          WidgetsBinding.instance.addPostFrameCallback((_) => executeAfterBuild());
          trainings = ResourceManager().filterObject.applyTo(snapshot.data);

          if (ResourceManager().bottomTrainingStub != null) trainings.add(ResourceManager().bottomTrainingStub);

          print('trainings.length = ' + trainings.length.toString());
          //currentWeekDay = trainings[0].dateTime.weekday;
          //onCurrentDayChanged(trainings[0].dateTime);

          if (!updateWeek) {
            _list = RectGetter(
                key: _listViewKey,
                child: ListView.separated(
                    controller: _listScrollController,
                    physics: new ClampingScrollPhysics(),
                    itemCount: trainings.length,
                    itemBuilder: (context, i) {
                      _keys[i] = RectGetter.createGlobalKey();

                      return RectGetter(
                          key: _keys[i],
                          child: Material(type: MaterialType.transparency, child: _buildRow(trainings[i])));
                    },

                    //Билдер дивайдеров. Если соседние тренировки находятся в разные дни - то нужно вернуть дивайдер с отображением даты,
                    //иначе возвращаем стандартный дивайдер. (Можно будет заменить потом на другой)
                    separatorBuilder: (context, position) {
                      //на нулевой позиции стоит заглушка, а дивайдеры по-дефолту отрисовываются после элементов. Для этого и нужна заглушка
                      //поэтому после первого элемента-заглушки возвращаем дивайдер с датой.
                      //RectGetter - объект из библиотеки. Если обернуть им виджет, то потом можно достать о нем данные такие как: длина, ширина и тд.
                      if (position == 0) {
                        if (!activeWeekDays.contains(getWeekdayName(trainings[position + 1].dateTime.weekday)))
                          activeWeekDays.add(getWeekdayName(trainings[position + 1].dateTime.weekday));
                        _divKey = RectGetter.createGlobalKey();
                        RectGetter rect = new RectGetter(
                            key: _divKey,
                            child: new Text(
                              getWeekdayName(trainings[position + 1].dateTime.weekday) +
                                  ', ' +
                                  trainings[position + 1].dateTime.day.toString() +
                                  ' число',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                            ));
                        return rect;
                      }
                      // обычный дивайдер (тренировки в один день, разница между днями 0)
                      if (trainings[position + 1].dateTime.weekday - trainings[position].dateTime.weekday == 0) {
                        _smallDivKey = RectGetter.createGlobalKey();

                        return new RectGetter(
                            key: _smallDivKey,
                            child: Container(padding: EdgeInsets.symmetric(horizontal: 16), child: DottedDivider()));
                        //дивайдер с датой. Тренировки между днями.
                      } else {
                        if (trainings[position + 1] is TrainingStub) {
                          _smallDivKey = RectGetter.createGlobalKey();
                          return new RectGetter(
                              key: _smallDivKey,
                              child: Container(padding: EdgeInsets.symmetric(horizontal: 16), child: DottedDivider()));
                        } else {
                          if (!activeWeekDays.contains(getWeekdayName(trainings[position + 1].dateTime.weekday)))
                            activeWeekDays.add(getWeekdayName(trainings[position + 1].dateTime.weekday));

                          return new Text(
                            getWeekdayName(trainings[position + 1].dateTime.weekday) +
                                ', ' +
                                trainings[position + 1].dateTime.day.toString() +
                                ' число',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                          );
                        }
                      }
                    }));
          } else {
            updateWeek = false;
          }

          //отслеживаем скролл...

          return NotificationListener<ScrollUpdateNotification>(
              onNotification: (notification) {
                print('SCROLL DETECTED');
                //print("SCROLL NOTIFICATION DELTA IS " + (DateTime.now().millisecondsSinceEpoch - threshold.millisecondsSinceEpoch).toString());

                //print("SCROLL NOTIFICATION DELTA IS " + notification.scrollDelta.toString());
                //флаг смены дня нажатием пользователя, чтобы не срабатывали лишние callback'и
                if (_jump) {
                  print("PERFORMING JUMP");
                  _jump = false;
                  return false;
                }

                //если первая видимая тренировка не совпадает с текущей видимой, то обновляем переменную с текущей датой
                //и триггерим обновление

                new Timer(const Duration(milliseconds: 25), () {
                  var tempWeekDay = trainings[getVisible().first].dateTime.weekday;

                  if (tempWeekDay != currentWeekDay) {
                    print("NEW VISIBLE DAY");
                    currentWeekDay = tempWeekDay;
                    setState(() {
                      updateWeek = true;
                      _week = new WeekWidget(
                          currentSelected: currentWeekDay,
                          onDaySelected: onCurrentDayChanged,
                          activeDays: activeWeekDays);
                    });
                  }
                  //print(getVisible());
                });

                return true;
              },
              //ну здесь понятно - стэк со всеми виджетами...
              child: new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
                ),
                child: RefreshIndicator(
                    child: Column(children: <Widget>[
                      _week,
                      Expanded(
                        child: Container(child: _list, color: Color.fromARGB(90, 21, 204, 255)),
                      ),
                    ]),
                    onRefresh: () async {
                      isLoaded = false;
                      ResourceManager().trainings = await APISupport.getTrainingsV2();
                      setState(() {});
                      return null;
                    }),
              ));

          //return Text(snapshot.data.title);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner
        return Container();
      },
    );

    return Container(child: schedulePage);
  }

  void executeAfterBuild() {
    // this code will get executed after the build method
    // because of the way async functions are scheduled

    int lastDaysCount = 1;
    for (int i = trainings.length - 1; i > 0; i--) {
      if (!(trainings[i - 1].dateTime.weekday != trainings[i].dateTime.weekday)) {
        lastDaysCount++;
      } else {
        break;
      }
    }

    double h = _list.getRect().height;

    if (!isBottomStubAdded) {
      _week = new WeekWidget(
          currentSelected: currentWeekDay, onDaySelected: onCurrentDayChanged, activeDays: activeWeekDays);

      double size = h - (_ITEM_HEIGHT * lastDaysCount + _DATE_DIV_HEIGHT + _DIV_HEIGHT * (lastDaysCount));
      if (size < 0) {
        size = 0;
      }
      ResourceManager().bottomTrainingStub = TrainingStub(height: size);
      setState(() {
        trainings.add(TrainingStub(height: size));
        isBottomStubAdded = true;
      });
    }
  }
}

class TrainingItemWidget extends StatefulWidget {
  final Training training;
  final String uid;

  const TrainingItemWidget(this.training, {this.uid = 'ban4ik'});

  @override
  TrainingItemWidgetState createState() => new TrainingItemWidgetState();
}

class TrainingItemWidgetState extends State<TrainingItemWidget> {
  Color _getTrainingTextColor(Training training, String uid) {
    Color color = Colors.black;
    for (User user in training.participants) {
      if (user.uid == uid) {
        color = Colors.indigo;
        break;
      }
    }
    return color;
  }

  List<Shadow> _getTrainingTextShadows(Training training, String uid) {
    List<Shadow> shadows = [];
    for (User user in training.participants) {
      if (user.uid == uid) {
        shadows = [
          Shadow(color: Colors.white, offset: Offset(0, 0)),
          Shadow(color: Colors.white, offset: Offset(0, 1)),
          Shadow(color: Colors.white, offset: Offset(1, 0)),
          Shadow(color: Colors.white, offset: Offset(1, 1))
        ];
        break;
      }
    }
    return shadows;
  }

  Text _getTrainingWidgetRightRowItem(Training training, String uid) {
    Color color = Colors.black;
    String numberOfEmptyPlace = training.getNumberOfEmptyPlace().toString();
    String result = 'Осталось ' + numberOfEmptyPlace + ' мест(а)';

    for (int i = 0; i < training.participants.length; i++) {
      if (training.participants[i].uid == uid) {
        if (i + 1 <= training.capacity) {
          result = 'Вы в лимите';
          color = Colors.indigo;
        } else {
          result = 'Вы в резерве';
          color = Colors.black45;
        }
        break;
      }
    }

    if (training.isCancelled) {
      result = 'ОТМЕНЕНО';
      color = Colors.red;
    }

    return Text(result,
        style: new TextStyle(
            fontFamily: 'PFDin',
            fontWeight: FontWeight.w600,
            fontSize: 13.0,
            color: color,
            shadows: _getTrainingTextShadows(training, uid)));
  }

  @override
  Widget build(BuildContext context) {
    Color color = _getTrainingTextColor(widget.training, widget.uid);

    List<Shadow> shadows = _getTrainingTextShadows(widget.training, widget.uid);

    return new Container(
      //color: _getTrainingColor(widget.training, widget.uid),
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(
            widget.training.type.getValue + ' | ' + widget.training.title.getValue,
            style: new TextStyle(
                color: color, fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 16.0, shadows: shadows),
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      new Text('Уровень:',
                          style: new TextStyle(
                              color: color,
                              fontFamily: 'PFDin',
                              fontWeight: FontWeight.w400,
                              fontSize: 14.0,
                              shadows: shadows)),
                      new Text('Дата:',
                          style: new TextStyle(
                              color: color,
                              fontFamily: 'PFDin',
                              fontWeight: FontWeight.w400,
                              fontSize: 14.0,
                              shadows: shadows)),
                      new Text('Время:',
                          style: new TextStyle(
                              color: color,
                              fontFamily: 'PFDin',
                              fontWeight: FontWeight.w400,
                              fontSize: 14.0,
                              shadows: shadows)),
                      new Text('Место:',
                          style: new TextStyle(
                              color: color,
                              fontFamily: 'PFDin',
                              fontWeight: FontWeight.w400,
                              fontSize: 14.0,
                              shadows: shadows)),
                    ],
                  ),
                  new Container(
                    margin: EdgeInsets.only(left: 8.0),
                  ),
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        convertToLevelsFormat(widget.training.levels),
                        style: new TextStyle(
                            color: color,
                            fontFamily: 'PFDin',
                            fontWeight: FontWeight.w500,
                            fontSize: 14.0,
                            shadows: shadows),
                      ),
                      new Text(
                        convertToDateFormat(widget.training.dateTime),
                        style: new TextStyle(
                            color: color,
                            fontFamily: 'PFDin',
                            fontWeight: FontWeight.w500,
                            fontSize: 14.0,
                            shadows: shadows),
                      ),
                      new Text(
                        convertToTimeFormat(widget.training.dateTime),
                        style: new TextStyle(
                            color: color,
                            fontFamily: 'PFDin',
                            fontWeight: FontWeight.w500,
                            fontSize: 14.0,
                            shadows: shadows),
                      ),
                      new Text(
                        widget.training.address.getValue,
                        style: new TextStyle(
                            color: color,
                            fontFamily: 'PFDin',
                            fontWeight: FontWeight.w500,
                            fontSize: 14.0,
                            shadows: shadows),
                      )
                    ],
                  ),
                ],
              ),
              _getTrainingWidgetRightRowItem(widget.training, widget.uid)
            ],
          )
        ],
      ),
    );
  }
}

class WeekDayWidget extends StatefulWidget {
  final DateTime dateTime;
  final bool isActive;
  final bool isSelected;
  final ValueChanged<DateTime> onDaySelected;
  final bool isSelectable;

  const WeekDayWidget(
      {@required this.dateTime,
      @required this.isSelected,
      @required this.onDaySelected,
      @required this.isActive,
      @required this.isSelectable});

  @override
  WeekDayWidgetState createState() => new WeekDayWidgetState();
}

class WeekDayWidgetState extends State<WeekDayWidget> {
  Color bgColor = Colors.transparent;
  bool selection;

  //bool isSelectable;

  @override
  void initState() {
    selection = widget.isSelected;
    //isSelectable = widget.isSelectable;

    //print("INITIATING WEEKDAY STATE");

    super.initState();
  }

  @override
  void didUpdateWidget(WeekDayWidget oldWidget) {
    // TODO: implement didUpdateWidget
    selection = widget.isSelected;
    super.didUpdateWidget(oldWidget);
  }

  var weekdayNames = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВСК'];

  String getWeekdayName(int weekday) {
    return weekdayNames[(weekday - 1) % 7];
  }

  @override
  Widget build(BuildContext context) {
    //print("BUILDING WEEKDAY");
    return new GestureDetector(
      onTap: () {
        if (widget.isSelectable) {
          widget.onDaySelected(widget.dateTime);
          setState(() {
            print("SELECTION STATE WAS CHANGED");
            //selection = true;
          });
        }
      },
      child: new Container(
        child: new Column(
          children: <Widget>[
            new Text(
              getWeekdayName(widget.dateTime.weekday),
              style: new TextStyle(
                  fontFamily: 'PFDin',
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                  color: widget.isSelectable
                      ? Colors.indigo
                      : Colors.grey //_getWeekdayColor(getWeekdayName(widget.dateTime.weekday))
                  ),
            ),
            new Container(
              margin: EdgeInsets.only(bottom: 4.0),
            ),
            new Container(
              decoration: new BoxDecoration(
                  color: _getWeekdayButtonBackground(selection),
                  borderRadius: new BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))),
              alignment: Alignment.bottomCenter,
              height: 20,
              width: 32,
              child: new Text(
                widget.dateTime.day.toString(),
                style: new TextStyle(
                    fontFamily: 'PFDin', fontWeight: FontWeight.w700, fontSize: 16.0, color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isHoliday(String weekday) {
    bool isHoliday = false;
    if (weekday == 'СБ' || weekday == 'ВСК') isHoliday = true;
    return isHoliday;
  }

  Color _getWeekdayButtonBackground(bool isSelected) {
    if (isSelected)
      return Color.fromARGB(90, 51, 204, 255);
    else
      return Colors.transparent;
  }
}

class WeekWidget extends StatefulWidget {
  final List activeDays;

  final int currentSelected;

  final ValueChanged<DateTime> onDaySelected;

  //final ValueListenable<int> position;
  const WeekWidget({@required this.currentSelected, @required this.onDaySelected, @required this.activeDays});

  @override
  WeekWidgetState createState() => new WeekWidgetState();
}

class WeekWidgetState extends State<WeekWidget> {
  static final weekdayNames = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВСК'];

  static String getWeekdayName(int weekday) {
    return weekdayNames[(weekday - 1) % 7];
  }

  List activeDays;
  int currentSelected;
  bool recreate = false;
  DateTime selectedDate = DateTime.now();

  void onDayChanged(DateTime date) {
    setState(() {
      print("CURRENT DAY SELECTION");
      currentSelected = date.weekday;
      //currentSelected++;
      recreate = true;
    });
    widget.onDaySelected(date);
  }

  //инетерсная особенность
  //initState не вызывается после setState родителя, как я преполагал
  //этот метод же напротив, вызывается
  //и через него уже можно отследить изменения
  //зачем так сделали - хз

  @override
  void didUpdateWidget(WeekWidget oldWidget) {
    // TODO: implement didUpdateWidget
    currentSelected = widget.currentSelected;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    currentSelected = widget.currentSelected;
    activeDays = widget.activeDays;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("BUILDING WEEK WIDGET");

    List<Widget> days = [];

    //добавляем каждый отдельный день

    for (int i = 0; i < 7; i++) {
      bool sel = false;

      DateTime dt = DateTime.now().add(Duration(days: i));

      if (dt.weekday == currentSelected) sel = true;

      bool selectable = false;

      if (activeDays.contains(getWeekdayName(dt.weekday))) selectable = true;

      days.add(
        WeekDayWidget(
          dateTime: DateTime.now().add(new Duration(days: i)),
          isSelected: sel,
          onDaySelected: onDayChanged,
          isActive: true,
          isSelectable: selectable,
        ),
      );
    }

    Row daysRow = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: days,
    );

    if (true) {
      return new Container(margin: EdgeInsets.only(left: 16, right: 16, top: 12), child: daysRow);
    }
  }
}
