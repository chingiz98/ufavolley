import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';

abstract class SignForTrainingViewContract implements BaseView {
  void onUsersListLoaded(List<User> users);
  void onSuccessRegisterOnTraining(Training training, {bool isInLimit = false});
  void onSuccessUnregisterOnTraining();
  void showSignInForTrainingDialog();
  void showSignOffFromTrainingDialog();
  void showTrainingConflictDialog(Training conflictingTraining);
  void onTrainingUpdated(Training training);
  void setTrainingActionButtonTitle(String title);
  void showNotAvailableSignInForTrainingDialog();
}

abstract class SignForTrainingPresenterContract implements BasePresenter {
  void registerOnTrainingButtonClick();
  void rulesButtonClick();
  void trainingRedactorButtonClick();
  void confirmSignInForTrainingButtonClick();
  void confirmSignOffFromTrainingButtonClick();
  void trainingChanged(Training training);
  Future<void> updatePage();
}