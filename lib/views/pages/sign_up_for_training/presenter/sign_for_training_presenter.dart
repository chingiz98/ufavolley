import 'dart:async';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/views/pages/sign_up_for_training/sign_for_training_contract.dart';

class SignForTrainingPresenter implements SignForTrainingPresenterContract {

  SignForTrainingViewContract _view;
  Training _training;

  SignForTrainingPresenter(SignForTrainingViewContract view, Training training) {
    this._view = view;
    this._training = training;
    _setupActionButton();
    _loadTrainingUsers();
  }
  
  bool _checkIsUserSignedForTraining(String uid, Training training) {
    return training.participants.map((user) => user.uid).toList().contains(uid)
        || training.supervisors.map((user) => user.uid).toList().contains(uid);
  }

  void _setupActionButton() {
    bool _isUserSigned = _checkIsUserSignedForTraining(ResourceManager().currentUser.uid, _training);
    if (_isUserSigned)
      _view.setTrainingActionButtonTitle('ОТПИСАТЬСЯ');
    else
      _view.setTrainingActionButtonTitle('ЗАПИСАТЬСЯ');
  }

  void _loadTrainingUsers() async {
    List<User> usersList = await APISupport.getTrainingUsers(trainingID: _training.trainingID);
    _training.participants = usersList;
    _view.onUsersListLoaded(usersList);
  }

  @override
  void registerOnTrainingButtonClick() {
    final user = ResourceManager().currentUser;
    bool _isUserSigned = _checkIsUserSignedForTraining(user.uid, _training);
    if (_isUserSigned) {
      _view.showSignOffFromTrainingDialog();
    } else {
      if (user.isAdmin ||
          user.isMainAdmin ||
          _training.isLevelCanSignIn(user.level)) {
        _view.showSignInForTrainingDialog();
      } else {
        _view.showNotAvailableSignInForTrainingDialog();
      }
    }
  }

  @override
  void rulesButtonClick() {
    // TODO: implement rulesButtonClick
  }

  @override
  void trainingRedactorButtonClick() {
    // TODO: implement trainingRedactorButtonClick
  }

  @override
  void confirmSignInForTrainingButtonClick() async {

    _view.showLoading();

    final rm = ResourceManager();

    final emailsMap = await APISupport.getEmailsByUids(uids: [rm.currentUser.uid]);

    if (rm.currentUser.phoneNumber.isEmpty && emailsMap.isEmpty) {
      _view.hideLoading();
      _view.showError(AlertMessage(
        title: 'Ошибка',
        message: 'Чтобы записаться на занятие, необходимо привязать номер телефона к профилю. '
            'Перейдите в раздел МОЙ ПРОФИЛЬ в главном меню и нажмите кнопку '
            '"Привязать номер телефона"',
      ));
      return;
    }

    List response = await APISupport.signForTraining(trainingID: _training.trainingID);

    if (response == null) return;

    _training = response[1];

    _view.hideLoading();

    int responseCode = response[0];

    if (responseCode == 0) {
      bool isInLimit = _training.participants.length < _training.capacity;
      _view.onSuccessRegisterOnTraining(_training, isInLimit: isInLimit);
    }

    if (responseCode == -1) {
      _view.showError(AlertMessage(title: 'Внимание', message: 'Данной тренировки больше нет'));
    }

    if (responseCode == -2) {
      _view.showError(AlertMessage(title: 'Внимание', message: 'Вы уже записаны на это занятие'));
    }

    if (responseCode == 3) {
      _view.showError(AlertMessage(title: 'Внимание', message: 'Нет свободных мест'));
    }

    if (responseCode == -4) {
      Training conflictingTraining = response[2];
      _view.showTrainingConflictDialog(conflictingTraining);
    }

    if (responseCode == -5) {
      _view.showError(AlertMessage(title: 'Внимаение', message: 'Вы пытаетесь записаться на занятие, которое уже началось!'));
    }

    if (responseCode == -6) {
      _view.showError(AlertMessage(title: 'Внимание', message: 'Занятие отменено!'));
    }

    if (responseCode == -7) {
      _view.showError(AlertMessage(title: 'Внимание', message: 'Ваш уровень не соответствует требуемому!'));
    }
    // 0 - юзер успешно вписан в треню
    // -1 - нет тренировки с таким ID
    // -2 - юзер уже вписан
    // -3 - места нет

    _view.onTrainingUpdated(_training);
    _setupActionButton();

  }

  @override
  void confirmSignOffFromTrainingButtonClick() async {

    _view.showLoading();

    List response = await APISupport.signOffFromTraining(trainingID: _training.trainingID);

    if (response == null)
      return;

    _training = response[1];

    int responseCode = response[0];

    _view.hideLoading();

    if (responseCode == 0) {
      _view.onSuccessUnregisterOnTraining();
    }

    if (responseCode == -3) {
      var content =
          'Вы не можете отписаться от занятия, т.к. оно начнется менее чем через 2 часа!';
      _view.showError(AlertMessage(title: 'Внимание', message: content));
    }

    _view.onTrainingUpdated(_training);
    _setupActionButton();
  }

  @override
  Future<void> updatePage() async {
    Training training = await APISupport.getTraining(
        trainingID: _training.trainingID);
    _view.onTrainingUpdated(training);
  }

  @override
  void trainingChanged(Training training) {
    _training = training;
  }

}