import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:ufavolley/constants.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/constants.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/control_panel/edit_schedule/training_redactor/view/training_redactor.dart';
import 'package:ufavolley/views/pages/control_panel/edit_users/edit_user/view/edit_user_page.dart';
import 'package:ufavolley/views/pages/profile_viewer/view/profile_viewer_page.dart';
import 'package:ufavolley/views/pages/rules/view/rules_page.dart';
import 'package:ufavolley/views/pages/sign_up_for_training/presenter/sign_for_training_presenter.dart';
import 'package:ufavolley/views/pages/sign_up_for_training/sign_for_training_contract.dart';
import 'package:ufavolley/views/pages/success_registration_on_training/view/success_register_on_training_page.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:share/share.dart';

class SignUpForTrainingPage extends StatefulWidget {
  final Training training;

  const SignUpForTrainingPage(this.training);

  @override
  SignUpForTrainingPageState createState() => SignUpForTrainingPageState();
}

class SignUpForTrainingPageState extends State<SignUpForTrainingPage> implements SignForTrainingViewContract {
  bool isUsersListLoaded = false;
  SignForTrainingPresenterContract _presenter;
  Training _training;
  String actionButtonTitle;
  bool _trainingUpdating = false;

  @override
  void initState() {
    actionButtonTitle = 'ЗАПИСАТЬСЯ';
    _presenter = SignForTrainingPresenter(this, widget.training);
    _training = widget.training;
    ResourceManager().signForUpdates(this);
    super.initState();
  }

  @override
  void dispose() {
    ResourceManager().unsubscribeFromUpdates(this);
    super.dispose();
  }

  bool isActionButtonEnabled() {
    if (_training.isCancelled) {
      return false;
    } else if (_training.supervisors.where((user) => user.uid == ResourceManager().currentUser.uid).toList().length >
        0) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = _buildAppBar();
    var innerContainer = _buildInnerContent();

    var container = Scaffold(
      appBar: appBar,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(16),
            child: Column(
              children: [
                TrainingInfoDashboard(training: _training),
                Row(
                  children: [
                    CustomButton(
                      text: actionButtonTitle,
                      relativeWidth: 0.4,
                      textMargin: EdgeInsets.all(4.0),
                      size: 28,
                      onTap: () async {
                        _presenter.registerOnTrainingButtonClick();
                      },
                      enabled: isActionButtonEnabled(),
                    ),
                    CustomButton(
                      text: 'ПРАВИЛА',
                      textMargin: EdgeInsets.all(4.0),
                      size: 28,
                      relativeWidth: 0.4,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Scaffold(
                              appBar: AppBar(
                                title: Text('Правила'),
                              ),
                              body: RulesPage(),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                ),
                Container(
                  margin: EdgeInsets.only(top: 4),
                  child: Text(
                    'При записи БЕЗ ОТМЕНЫ и НЕ ПОСЕЩЕНИЯ ЗАНЯТИЯ по правилам нашей Школы Вы обязаны ОПЛАТИТЬ НЕУСТОЙКУ!',
                    style: TextStyle(
                      fontFamily: 'PFDin',
                      fontWeight: FontWeight.w500,
                      fontSize: 18.0,
                      color: Color.fromARGB(255, 102, 102, 102),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  child: innerContainer,
                )
              ],
            ),
          ),
        ),
      ),
    );

    return container;
  }

  AppBar _buildAppBar() {
    if (ResourceManager().currentUser.isAdmin || ResourceManager().currentUser.isMainAdmin)
      return AppBar(
        title: new Text('ЗАПИСЬ НА ЗАНЯТИЕ'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TrainingEditorPage(training: _training),
                ),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Share.share('https://ufavolley.page.link/training?id=${_training.trainingID}');
            },
          ),
          _trainingUpdating
              ? Container(
                  width: 38,
                  padding: EdgeInsets.all(9),
                  margin: EdgeInsets.all(9),
                  child: CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                  ),
                )
              : IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () {
                    setState(() {
                      _trainingUpdating = true;
                    });
                    _presenter.updatePage();
                  },
                ),
        ],
      );
    else {
      return AppBar(
        title: Text('ЗАПИСЬ НА ЗАНЯТИЕ'),
        actions: [
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Share.share('https://ufavolley.page.link/training?id=${_training.trainingID}');
            },
          ),
        ],
      );
    }
  }

  Widget _buildInnerContent() {
    if (isUsersListLoaded || _training.participants.length == 0)
      return TrainingParticipantsWidget(_training);
    else
      return CircularProgressIndicator();
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void onSuccessUnregisterOnTraining() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Внимание'),
        content: Text('Вы отписались от занятия!'),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text('Ок'),
          ),
        ],
      ),
    );
  }

  @override
  void onUsersListLoaded(List<User> users) {
    _training.participants = users;
    isUsersListLoaded = true;
    setState(() {});
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {
    ResourceManager().trainings.forEach((training) => {
          if (training.trainingID == _training.trainingID) {_training = training, _presenter.trainingChanged(_training)}
        });
    setState(() {});
  }

  @override
  void showSignInForTrainingDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Записаться на занятие'),
        content: Text('Вы уверены?'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Нет'),
          ),
          TextButton(
            onPressed: () async {
              Navigator.of(context).pop(false);
              _presenter.confirmSignInForTrainingButtonClick();
            },
            child: Text('Да'),
          ),
        ],
      ),
    );
  }

  @override
  void showSignOffFromTrainingDialog() {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: Text('Отписаться от занятия'),
        content: Text('Вы уверены?'),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Нет'),
          ),
          TextButton(
            onPressed: () async {
              Navigator.of(context).pop(false);
              _presenter.confirmSignOffFromTrainingButtonClick();
            },
            child: Text('Да'),
          ),
        ],
      ),
    );
  }

  @override
  void onSuccessRegisterOnTraining(Training training, {bool isInLimit = false}) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SuccessRegisterOnTrainingPage(
          isInLimit: isInLimit,
          training: training,
        ),
      ),
    );
  }

  @override
  void showTrainingConflictDialog(Training conflictingTraining) {
    String levels = "";

    for (CloudValue value in conflictingTraining.levels) {
      levels += value.getValue;
      levels += '/';
    }

    levels = levels.substring(0, levels.length - 1);

    String conflictHint = '${conflictingTraining.type.getValue} | ${conflictingTraining.title.getValue} \n' +
        'Уровень: $levels\n' +
        'Время: ${convertToTimeFormat(conflictingTraining.dateTime)}\n' +
        'Дата: ${convertToDateFormat(conflictingTraining.dateTime)}\n' +
        'Место: ${conflictingTraining.address.getValue}';

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Внимание'),
        content: Text('Вы уже записаны на тренировку в ближайшее время!\n\n' + conflictHint),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text('Ок'),
          ),
        ],
      ),
    );
  }

  @override
  void onTrainingUpdated(Training training) {
    this._training = training;
    setState(() {
      _trainingUpdating = false;
    });
  }

  @override
  void setTrainingActionButtonTitle(String title) {
    this.actionButtonTitle = title;
    setState(() {});
  }

  @override
  void showNotAvailableSignInForTrainingDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Ошибка'),
        content: Text('Не удалось записаться на тренировку. Уровень не соответствует требуемому'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('Ок'),
          ),
        ],
      ),
    );
  }
}

class TrainingInfoDashboard extends StatelessWidget {
  final Training training;
  final List<bool> visibleWidgets;
  final List<ImageProvider> images = const [
    AssetImage('assets/training_name_icon.png'),
    AssetImage('assets/schedule_icon.png'),
    AssetImage('assets/clock_icon.png'),
    AssetImage('assets/map_marker_icon.png'),
    AssetImage('assets/whistle_icon.png'),
    AssetImage('assets/back_card_icon.png'),
    AssetImage('assets/people_shadows_icon.png'),
    AssetImage('assets/people_shadows_icon.png'),
    AssetImage('assets/eye_icon.png')
  ];

  const TrainingInfoDashboard({
    @required this.training,
    this.visibleWidgets = const [
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
    ],
  });

  @override
  Widget build(BuildContext context) {
    List<String> texts = [
      training.type.getValue + '\n' + training.title.getValue,
      convertToDateFormat(training.dateTime),
      _buildTimeWithDuration(training.dateTime, training.durationImMinutes),
      training.address.getValue,
      'Тренер: ' + _trainersName(),
      'Стоимость ' + training.price.toString(),
      'Лимит ' + training.capacity.toString() + ' человек(а)',
      _buildParticipantsMinimumString(training.participantsMinimum),
      'Осталось ' + training.getNumberOfEmptyPlace().toString() + ' мест(а)'
    ];

    List<Widget> displayedFields = [];

    for (int i = 0; i < images.length; i++) {
      if (i == 5 && training.price == 0) continue;
      if (visibleWidgets[i]) displayedFields.add(TrainingInfoColumnItem(images[i], texts[i]));
    }

    displayedFields.add(_buildCommentWidget(training.comment));

    return Container(
      child: new Column(
        children: displayedFields,
      ),
    );
  }

  String _buildParticipantsMinimumString(int participantsMinimum) {
    if (participantsMinimum == 0) return 'Минимум -';
    return 'Минимум ' + participantsMinimum.toString() + ' человек(a)';
  }

  String _buildTimeWithDuration(DateTime startTime, int durationInMinutes) {
    if (durationInMinutes == 0) return convertToTimeFormat(startTime);
    Duration trainingDuration = Duration(minutes: durationInMinutes);
    DateTime endTime = startTime.add(trainingDuration);
    return convertToTimeFormat(startTime) + '-' + convertToTimeFormat(endTime);
  }

  Widget _buildCommentWidget(String comment) {
    return Container(
      child: Linkify(
        text: comment,
        onOpen: (link) => _launchURL(link.url),
        style: TextStyle(
            fontFamily: 'PFDin',
            fontWeight: FontWeight.w500,
            fontSize: 20.0,
            color: Color.fromARGB(255, 102, 102, 102)),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("ERROR: link error occurred");
    }
  }

  String _trainersName() {
    if (training.trainers.length == 0) return 'Без тренера';
    String trainersStr = '';
    for (User trainer in training.trainers) {
      trainersStr += trainer.name + ' ' + trainer.lastName;
      trainersStr += ',\n';
    }
    return trainersStr.substring(0, trainersStr.length - 2);
  }
}

class TrainingInfoColumnItem extends StatelessWidget {
  final ImageProvider imageProvider;
  final String text;
  final double imageHeight;

  const TrainingInfoColumnItem(this.imageProvider, this.text, {this.imageHeight});

  @override
  Widget build(BuildContext context) {
    double imageHeight = 28;
    if (this.imageHeight != null) imageHeight = this.imageHeight;

    print(imageHeight.toString());

    return Container(
      child: Row(
        children: <Widget>[
          Container(
            width: 40,
            child: Center(
              child: Image(image: imageProvider, height: imageHeight),
            ),
            margin: EdgeInsets.only(right: 12),
          ),
          Expanded(
            child: Text(
              text,
              style: TextStyle(
                  fontFamily: 'PFDin',
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0,
                  color: Color.fromARGB(255, 102, 102, 102)),
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
      margin: EdgeInsets.only(bottom: 8),
    );
  }
}

class TrainingParticipantsWidget extends StatefulWidget {
  final Training training;

  const TrainingParticipantsWidget(this.training);

  @override
  State createState() => new TrainingParticipantsWidgetState();
}

class TrainingParticipantsWidgetState extends State<TrainingParticipantsWidget> {
  Training training;

  @override
  void didUpdateWidget(TrainingParticipantsWidget oldWidget) {
    training = widget.training;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    training = widget.training;

    super.initState();
  }

  Widget _buildSingleSupervisorItem() {
    return Center(
      child: ParticipantItem(training.supervisors[0]),
    );
  }

  @override
  Widget build(BuildContext context) {
    final int limitSize = training.capacity;
    final List<User> limitParticipants = training.participants.take(limitSize).toList();
    final List<User> unLimitParticipants = training.participants.skip(limitSize).toList();

    print('unLimitParticipants.length = ' + unLimitParticipants.length.toString());

    Container supervisorsContainer = Container();
    if (training.supervisors.length > 0) {
      supervisorsContainer = Container(
        child: Column(
          children: <Widget>[
            _buildTextDivider(training.supervisors.length == 1 ? "Администратор" : "Администрация"),
            training.supervisors.length == 1
                ? _buildSingleSupervisorItem()
                : ListOfTrainingLimitParticipants(training.supervisors)
          ],
        ),
      );
    }

    Container limitParticipantsContainer = Container();

    if (training.participants.length > 0) {
      limitParticipantsContainer = Container(
        child: Column(
          children: <Widget>[
            _buildTextDivider("Участники"),
            ListOfTrainingLimitParticipants(limitParticipants),
          ],
        ),
      );
    }

    Container unLimitParticipantsContainer = Container();

    if (unLimitParticipants.length > 0) {
      unLimitParticipantsContainer = Container(
        child: Column(
          children: <Widget>[
            _buildTextDivider("РЕЗЕРВ"),
            ListOfTrainingUnLimitParticipants(unLimitParticipants, limitSize)
          ],
        ),
      );
    }

    return Container(
      child: Column(
        children: [
          supervisorsContainer,
          limitParticipantsContainer,
          unLimitParticipantsContainer,
        ],
      ),
    );
  }

  Widget _buildTextDivider(String text) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: _buildDashLine(EdgeInsets.only(right: 8)),
          ),
          Text(
            text,
            style: TextStyle(
              fontFamily: 'PFDin',
              fontWeight: FontWeight.w500,
              fontSize: 20.0,
              color: Color.fromARGB(255, 102, 102, 102),
            ),
          ),
          Expanded(
            child: _buildDashLine(
              EdgeInsets.only(left: 8),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDashLine(EdgeInsetsGeometry margin) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 4.0;
        final dashHeight = 1.0;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Container(
          margin: margin,
          width: MediaQuery.of(context).size.width,
          child: Flex(
            children: List.generate(
              dashCount,
              (_) {
                return Container(
                  margin: EdgeInsets.only(left: 2.0),
                  child: SizedBox(
                    width: dashWidth,
                    height: dashHeight,
                    child: DecoratedBox(
                      decoration: BoxDecoration(color: Colors.grey),
                    ),
                  ),
                );
              },
            ),
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            direction: Axis.horizontal,
          ),
        );
      },
    );
  }
}

class ListOfTrainingParticipants extends StatefulWidget {
  final List<User> participants;
  final Color textColor;
  final int sentStartFrom;

  const ListOfTrainingParticipants(this.participants, {this.textColor, this.sentStartFrom});

  @override
  State createState() => new ListOfTrainingParticipantsState();
}

class ListOfTrainingParticipantsState extends State<ListOfTrainingParticipants> {
  List<User> participants;
  Color textColor;
  int sentStartFrom;

  @override
  void didUpdateWidget(ListOfTrainingParticipants oldWidget) {
    participants = widget.participants;
    textColor = widget.textColor;
    sentStartFrom = widget.sentStartFrom;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    participants = widget.participants;
    textColor = widget.textColor;
    sentStartFrom = widget.sentStartFrom;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("sign_up build called");
    final List<Row> rows = generateTableRows();

    return ListView.separated(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, position) {
        return rows[position];
      },
      separatorBuilder: (context, position) {
        return Container(
          margin: EdgeInsets.symmetric(vertical: 4),
        );
      },
      itemCount: rows.length,
    );
  }

  List<Row> generateTableRows() {
    List<Row> rows = [];
    List<int> rowsCapacities = [];
    for (int i = 0; i < participants.length; i += 2)
      rowsCapacities.add(participants.length - i >= 2 ? 2 : participants.length - i);

    int sentStartFrom = 0;
    if (this.sentStartFrom != null) sentStartFrom = this.sentStartFrom;
    print('sentStartFrom = ' + this.sentStartFrom.toString());

    for (int i = 0; i < rowsCapacities.length; i++) {
      int startFrom = 2 * i;
      List<User> part = [];
      for (int j = 0; j < rowsCapacities[i]; j++) {
        print(startFrom + j);
        part.add(participants[startFrom + j]);
      }
      List<Row> widgets = getTableRows(part, startFrom + sentStartFrom);
      List<Expanded> expandedWidgets = [];
      for (int i = 0; i < widgets.length; i++) expandedWidgets.add(Expanded(child: widgets[i]));
      if (expandedWidgets.length != 2) {
        int diff = 2 - expandedWidgets.length;
        while (diff != 0) {
          expandedWidgets.add(Expanded(child: Container()));
          diff--;
        }
      }
      rows.add(Row(children: expandedWidgets));
    }
    return rows;
  }

  List<Row> getTableRows(List<User> participants, int startFrom) {
    double fontSize = 30.0;
    String fontFamily = 'PFDin';
    FontWeight fontWeight = FontWeight.w600;

    var textStyle = TextStyle(
      color: Color.fromARGB(255, 102, 102, 102),
      fontSize: fontSize,
      fontFamily: fontFamily,
      fontWeight: fontWeight,
    );

    var kostylStyle = TextStyle(
      color: Colors.transparent,
      fontSize: fontSize,
      fontFamily: fontFamily,
      fontWeight: fontWeight,
    );

    List<Row> rows = [];

    for (int i = 0; i < participants.length; i++) {
      Row row = Row(
        children: [
          Text((startFrom + i + 1).toString(), style: textStyle),
          getSraniyKostil(startFrom + i + 1, kostylStyle),
          ParticipantItem(participants[i], textColor: textColor),
        ],
      );
      rows.add(row);
    }
    return rows;
  }

  Text getSraniyKostil(int index, TextStyle textStyle) {
    // TODO(Надо будет убрать эту дичь, определенно. Когда-нибудь)
    if (index >= 10) return Text('');
    return Text('1', style: textStyle); // просто пробел между аватаркой и номером в списке
  }
}

//TODO
class ListOfTrainingLimitParticipants extends StatefulWidget {
  final List<User> participants;

  const ListOfTrainingLimitParticipants(this.participants);

  @override
  State createState() => new ListOfTrainingLimitParticipantsState();
}

class ListOfTrainingLimitParticipantsState extends State<ListOfTrainingLimitParticipants> {
  List<User> participants;

  @override
  void didUpdateWidget(ListOfTrainingLimitParticipants oldWidget) {
    participants = widget.participants;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    participants = widget.participants;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListOfTrainingParticipants(participants);
  }
}

class ListOfTrainingUnLimitParticipants extends StatefulWidget {
  final List<User> participants;
  final int startFrom;

  const ListOfTrainingUnLimitParticipants(this.participants, this.startFrom);

  @override
  State createState() => ListOfTrainingUnLimitParticipantsState();
}

class ListOfTrainingUnLimitParticipantsState extends State<ListOfTrainingUnLimitParticipants> {
  List<User> participants;
  int startFrom;

  @override
  void didUpdateWidget(ListOfTrainingUnLimitParticipants oldWidget) {
    participants = widget.participants;
    startFrom = widget.startFrom;
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    participants = widget.participants;
    startFrom = widget.startFrom;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Color textColor = Color.fromARGB(255, 102, 102, 102);

    return ListOfTrainingParticipants(participants, textColor: textColor, sentStartFrom: startFrom);
  }
}

class ParticipantItem extends StatefulWidget {
  final User user;
  final Color textColor;

  const ParticipantItem(this.user, {this.textColor});

  @override
  ParticipantItemState createState() => ParticipantItemState();
}

class ParticipantItemState extends State<ParticipantItem> {
  User user;

  //String url = 'assets/boy.jpg';

  @override
  void didUpdateWidget(ParticipantItem oldWidget) {
    user = widget.user;
    //_loadImageUrl();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    user = widget.user;
    //_loadImageUrl();
    super.initState();
  }

  /*
  _loadImageUrl() async {
    var url = await FirebaseStorage.instance
        .ref()
        .child(user.uid)
        .getDownloadURL()
        .catchError((e) => {});
    setState(() {
      if (url is String) this.url = url;
      print('setState called in _loadImageUrl');
    });
  }

   */

  @override
  Widget build(BuildContext context) {
    Color textColor = Colors.indigo;

    if (widget.textColor != null) textColor = widget.textColor;

    var textStyle = TextStyle(color: textColor, fontSize: 16.0, fontFamily: 'PFDin', fontWeight: FontWeight.w600);

    ResourceManager rm = ResourceManager();
    var levels = rm.containers[CloudResource.LEVEL].getValuesMap();

    Color borderColor = Colors.grey;

    AssetImage placeholder = AssetImage('assets/boy.jpg');

    if (user.gender == false && user.gender != null) placeholder = AssetImage('assets/girl.jpg');

    if (user.level != null && user.level != '0') {
      if (levels[user.level].getValue == 'Лайт') borderColor = Colors.yellow;
      if (levels[user.level].getValue == 'Лайт+') borderColor = Colors.green;
      if (levels[user.level].getValue == 'Медиум') borderColor = Colors.red;
      if (levels[user.level].getValue == C.litePlusProLevelName) borderColor = Constants.litePlusProColor;
      if (levels[user.level].getValue == 'Хард') borderColor = Colors.black;
    }

    if (user.byAdmin) borderColor = Colors.indigo;

    return TextButton(
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: FadeInImage(
                  placeholder: placeholder,
                  image: CachedNetworkImageProvider(user.imageUrl == null ? '' : user.imageUrl),
                  height: 50,
                  width: 50,
                  fit: BoxFit.cover,
                )),
            decoration: BoxDecoration(
              color: const Color(0xff7c94b6),
              borderRadius: BorderRadius.all(Radius.circular(50.0)),
              border: Border.all(
                color: borderColor,
                width: 4.0,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 4),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(user.name, style: textStyle),
              Text(user.lastName, style: textStyle),
            ],
          ),
          user.isDebtor
              ? Container(
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  child: Text('!', style: TextStyle(fontSize: 30)),
                )
              : Text('')
        ],
      ),
      onPressed: () {
        if (ResourceManager().currentUser.isMainAdmin || ResourceManager().currentUser.isAdmin) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => EditUserPage(listener: () {}, user: user)));
        } else if (user.isSupervisor) {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProfileViewerPage(user)));
        }
      },
    );
  }
}
