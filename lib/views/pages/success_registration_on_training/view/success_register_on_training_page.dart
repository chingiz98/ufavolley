import 'package:flutter/material.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/rules/view/rules_page.dart';
import 'package:ufavolley/views/pages/sign_up_for_training/view/sign_up_for_training_page.dart';

class SuccessRegisterOnTrainingPage extends StatelessWidget {
  final bool isInLimit;
  final Training training;

  const SuccessRegisterOnTrainingPage({@required this.isInLimit, @required this.training});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text(
                    'Вы записались на занятие.\n' + (isInLimit ? 'Лимит' : 'Резерв'),
                    style: TextStyle(
                      fontFamily: 'PFDin',
                      fontWeight: FontWeight.w500,
                      fontSize: 30.0,
                      color: Color.fromARGB(255, 102, 102, 102),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  child: TrainingInfoDashboard(training: training),
                ),
                CustomButton(
                  relativeWidth: 0.4,
                  textMargin: EdgeInsets.all(4.0),
                  text: 'ПРАВИЛА',
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Scaffold(
                          appBar: AppBar(
                            title: Text('Правила'),
                          ),
                          body: RulesPage(),
                        ),
                      ),
                    );
                  },
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 8),
                ),
                Text(
                  'При записи БЕЗ ОТМЕНЫ и НЕ ПОСЕЩЕНИЯ ЗАНЯТИЯ по правилам нашей Школы Вы обязаны ОПЛАТИТЬ НЕУСТОЙКУ!',
                  style: TextStyle(
                    fontFamily: 'PFDin',
                    fontWeight: FontWeight.w500,
                    fontSize: 18.0,
                    color: Color.fromARGB(255, 102, 102, 102),
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
