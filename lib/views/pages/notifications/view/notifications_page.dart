import 'package:flutter/material.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/notifications/confirm_visit/view/confirm_your_visit_page.dart';
import 'package:ufavolley/views/pages/notifications/informative/view/informative_notification_page.dart';
import 'package:ufavolley/views/pages/notifications/presenter/notifications_presenter.dart';
import 'package:ufavolley/views/pages/notifications/user_action/view/user_action_notification_page.dart';

import '../notifications_contract.dart';

class NotificationsPage extends StatefulWidget {
  @override
  State createState() => new NotificationsPageState();
}

class NotificationsPageState extends State<NotificationsPage>
    implements NotificationViewContract {
  NotificationPresenterContract _presenter;


  @override
  void dispose() {
    super.dispose();
    ResourceManager().unsubscribeFromUpdates(this);
  }

  @override
  void initState() {
    _presenter = NotificationPresenter(this);
    ResourceManager().signForUpdates(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('notifications page build called');

    return Scaffold(
        body: Container(
            height: double.maxFinite,
            padding: EdgeInsets.all(16),
            decoration: new BoxDecoration(
              image: new DecorationImage(
                  image: AssetImage('assets/background.png'),
                  fit: BoxFit.cover),
            ),
            child: RefreshIndicator(
                child: ListView.separated(
                  shrinkWrap: true,
                  itemCount: ResourceManager().notifications.length,
                  itemBuilder: (context, index) {
                    return NotificationWidget(
                        notification: ResourceManager().notifications[index],
                        presenter: _presenter);
                  },
                  separatorBuilder: (context, index) {
                    return DottedDivider();
                  },
                ),
                onRefresh: () async {
                  await _presenter.refresh();
                  //setState(() { });
                  return null;
                })));
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: new Text(message.title),
          content: new Text(message.message),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Ок',
                style: TextStyle(color: Colors.blue),
              ),
            ),
          ],
        ));
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: new Text(message.title),
          content: new Text(message.message),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Закрыть',
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        ));
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {
    print("NotificationsPage updateState was called");
    setState(() { });
  }

  @override
  void showNotification(NotificationProp notification) async {
    if (!notification.read)
      _presenter.readNotification(notification);
    switch (notification.type) {
      case NotificationType.userAction:
        {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => InformativeNotificationPage(
                      notification: notification)));
          break;
        }
      case NotificationType.reminder:
        {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ConfirmYourVisitPage(
                      notification: notification)));
          break;
        }
      case NotificationType.custom:
        {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => UserActionNotificationPage(
                      notification: notification)));
          break;
        }
    }
  }

  @override
  void onNotificationsLoaded(List<NotificationProp> notifications) {
    setState(() {});
  }

  @override
  void onNotificationRead(NotificationProp notification) {

  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }
}

class NotificationWidget extends StatefulWidget {
  final NotificationProp notification;
  final NotificationPresenterContract presenter;

  const NotificationWidget(
      {@required this.notification, @required this.presenter});

  @override
  State createState() => NotificationWidgetState();
}

class NotificationWidgetState extends State<NotificationWidget> {
  @override
  Widget build(BuildContext context) {

    final FontWeight fontWeight =
        widget.notification.read ? FontWeight.w300 : FontWeight.w600;

    final TextStyle textStyle = TextStyle(
        fontFamily: 'PFDin',
        fontWeight: fontWeight,
        fontSize: 24.0,
        color: Color.fromARGB(255, 102, 102, 102));

    final TextStyle textStyle2 = TextStyle(
        fontFamily: 'PFDin',
        fontWeight: FontWeight.w300,
        fontSize: 18.0,
        color: Color.fromARGB(255, 102, 102, 102));

    return Container(
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.notification.title,
                        style: textStyle, overflow: TextOverflow.ellipsis),
                    Text(widget.notification.subTitle,
                        style: textStyle, overflow: TextOverflow.ellipsis)
                  ],
                ),
              ),
              Expanded(
                child: _buildDateTimeWidget(
                    widget.notification.dateOfReceiving, textStyle2),
              )
            ],
          ),
          onTap: () async {
            widget.presenter.clickOnNotification(widget.notification);
          },
        ),
      ),
    );
  }

  Widget _buildDateTimeWidget(DateTime dateTime, TextStyle style) {
    String _getHours(int hour) {
      return hour > 9 ? hour.toString() : '0' + hour.toString();
    }

    String _getMinutes(int minutes) {
      return minutes > 9 ? minutes.toString() : '0' + minutes.toString();
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            _getHours(dateTime.hour) + ':' + _getMinutes(dateTime.minute),
            style: style,
          ),
          Text(
            dateTime.day.toString() +
                '.' +
                dateTime.month.toString() +
                '.' +
                dateTime.year.toString(),
            style: style,
          )
        ],
      ),
    );
  }
}
