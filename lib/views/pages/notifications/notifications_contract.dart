import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/notificationProp.dart';

abstract class NotificationViewContract implements BaseView {
  void showNotification(NotificationProp notification);
  void onNotificationRead(NotificationProp notification);
  void onNotificationsLoaded(List<NotificationProp> notifications);
}

abstract class NotificationPresenterContract implements BasePresenter {
  void clickOnNotification(NotificationProp notification);
  void readNotification(NotificationProp notificationProp);
  Future<int> refresh();
}