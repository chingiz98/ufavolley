import 'package:flutter/material.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';

class UserActionNotificationPage extends StatelessWidget {

  final NotificationProp notification;

  const UserActionNotificationPage({@required this.notification});

  @override
  Widget build(BuildContext context) {

    final TextStyle titleTextStyle = TextStyle(
        fontFamily: 'PFDin',
        fontWeight: FontWeight.w500,
        fontSize: 30.0,
        color: Colors.black
    );

    final TextStyle messageTextStyle = TextStyle(
        fontFamily: 'PFDin',
        fontWeight: FontWeight.w400,
        fontSize: 24.0,
        color: Colors.black
    );

    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.all(16),
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: AssetImage('assets/background.png'),
              fit: BoxFit.cover
          ),
        ),
        child: ListView(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Text(notification.title, style: titleTextStyle, textAlign: TextAlign.center),
            ),
            Container(
              margin: EdgeInsets.only(top: 16),
              //child: Text(notification.subTitle, style: messageTextStyle, textAlign: TextAlign.center),
              child: Linkify(
                onOpen: (link) {
                  print("Clicked ${link.url}!");
                  _launchURL(link.url);
                },
                text: notification.subTitle,
                textAlign: TextAlign.center,
                style: messageTextStyle
              )
            ),
          ],
        ),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {

      await launch(url);
    } else {
      print("ERROR: link error occured");
    }
  }
}