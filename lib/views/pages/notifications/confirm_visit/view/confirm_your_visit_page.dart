import 'package:flutter/material.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/rules/view/rules_page.dart';
import 'package:ufavolley/views/pages/sign_up_for_training/view/sign_up_for_training_page.dart';

class ConfirmYourVisitPage extends StatefulWidget {

  final NotificationProp notification;

  const ConfirmYourVisitPage({@required this.notification});

  @override
  State createState() => new ConfirmYourVisitState();

}

class ConfirmYourVisitState extends State<ConfirmYourVisitPage> {

  @override
  Widget build(BuildContext context) {

    final TextStyle textStyle = TextStyle(
        fontFamily: 'PFDin',
        fontWeight: FontWeight.w500,
        fontSize: 30.0,
        color: Colors.black
    );

    final Widget notExistTrainingWidget = ListView(
      shrinkWrap: false,
      children: <Widget>[
        Align(
          alignment: Alignment.topCenter,
          child: Text('Тренировка уже завершилась', style: textStyle),
        ),
      ],
    );

    return Scaffold(
      appBar: AppBar(),
      body: FutureBuilder(
          future: APISupport.getTraining(trainingID: widget.notification.trainingID),
          builder: (context, snapshot){

            if (snapshot.hasData) {

              return Container(
                padding: EdgeInsets.all(16),
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: AssetImage('assets/background.png'),
                      fit: BoxFit.cover
                  ),
                ),
                child: snapshot.data is TrainingStub ? notExistTrainingWidget : ListView(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                          'ПОДТВЕРДИТЕ\nСВОЕ ПОСЕЩЕНИЕ!', style: textStyle,
                          textAlign: TextAlign.center),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 24),
                        child: TrainingInfoDashboard(training: snapshot.data,
                            visibleWidgets: [
                              true,
                              true,
                              true,
                              true,
                              false,
                              false,
                              false,
                              false,
                              false
                            ])
                    ),
                    CustomButton(
                        text: 'Да. Приду',
                        relativeWidth: 0.4,
                        onTap: () async {
                          Navigator.of(context).pop();
                        }
                    ),
                    CustomButton(
                        text: 'НЕТ. Не приду',
                        relativeWidth: 0.4,
                        onTap: () async {
                          showLoadingAlert(context);
                          int code = (await APISupport.signOffFromTraining(trainingID: widget.notification.trainingID))[0];
                          Navigator.of(context).pop();
                          String content;
                          if(code == 0){
                            content = 'Вы отписались от занятия!';
                          } else if (code == -1){
                            content = 'Вы уже отписаны от занятия';
                          } else if (code == -2){
                            content = 'Занятие уже прошло';
                          } else if (code == -3){
                            content = 'Вы не можете отписаться от занятия, т.к. оно начнется менее чем через 2 часа!';
                          }

                          showDialog(
                              context: context,
                              builder: (context) => new AlertDialog(
                                title: new Text('Внимание'),
                                content: new Text(content),
                                actions: <Widget>[

                                  new TextButton(
                                    onPressed: ()  {
                                      Navigator.of(context).pop(false);
                                    },
                                    child: new Text('Ок'),
                                  ),
                                ],
                              )
                          );
                        }
                    ),
                    CustomButton(
                        text: 'Правила',
                        relativeWidth: 0.4,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) =>
                                Scaffold(
                                  appBar: AppBar(
                                    backgroundColor: Color.fromARGB(255, 42, 51, 154),
                                    title: Text('Правила'),
                                  ),
                                  body: RulesPage(),
                                )
                            ),
                          );
                        }
                    ),
                  ],
                ),
              );
            }

            return Center(child: CircularProgressIndicator());

      }),
    );
  }

}