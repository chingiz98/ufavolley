import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/notificationProp.dart';

import '../notifications_contract.dart';

class NotificationPresenter implements NotificationPresenterContract {

  NotificationViewContract _view;

  NotificationPresenter(NotificationViewContract view) {
    _view = view;

  }

  @override
  void readNotification(NotificationProp notification) {
    for (int i = 0; i < ResourceManager().notifications.length; i++) {
      if (ResourceManager().notifications[i].id == notification.id) {
        ResourceManager().notifications[i].read = true;
        APISupport.readNotification(notificationId: notification.id);
        break;
      }
    }
    _view.onNotificationRead(notification);
  }

  @override
  Future<int> refresh() async {
    await ResourceManager().refreshNotifications();
    //_view.updateState();
    return 0;
  }

  @override
  void clickOnNotification(NotificationProp notification) {
    _view.showNotification(notification);
  }

}