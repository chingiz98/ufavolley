import 'package:flutter/material.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/constants.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/filter_object.dart';
import 'package:ufavolley/models/cloud_value.dart';

class FilterTrainingPage extends StatefulWidget {

  final String typeFilterLabel = 'Вид тренировки';
  final List<FilterItem> typeFilterItems = [];
  final String titleFilterLabel = 'Тип тренировки';
  final List<FilterItem> titleFilterItems = [];
  final String addressFilterLabel = 'Адрес тренировки';
  final List<FilterItem> addressFilterItems = [];
  final String timeFilterLabel = 'Время тренировки';
  final List<FilterItem> timeFilterItems = [];

  final Function listener;

  FilterTrainingPage({@required this.listener});

  final TextStyle titleStyle = const TextStyle(
      color: Colors.indigo,
      fontFamily: 'PFDin',
      fontWeight: FontWeight.w500,
      fontSize: 24
  );

  final TextStyle labelStyle = const TextStyle(
      color: Colors.grey,
      fontFamily: 'PFDin',
      fontWeight: FontWeight.w400,
      fontSize: 22
  );

  @override
  State createState() => new FilterTrainingPageState();

}

class FilterTrainingPageState extends State<FilterTrainingPage> {

  ResourceManager resourceManager;
  List<Training> levelingTrainings;

  @override
  void initState() {
    resourceManager = ResourceManager();
    initLevelingTrainings();
    _buildFilterItems();
    initWithFilterObject(ResourceManager().filterObject);
    super.initState();
  }

  void initLevelingTrainings() {

    CloudValue undefLevel;
    CloudValue liteLevel;
    CloudValue litePlusLevel;
    CloudValue mediumLevel;
    CloudValue mediumPlusLevel;
    CloudValue hardLevel;

    for (CloudValue level in resourceManager.containers[CloudResource.LEVEL].values) {
      if (level.getValue == 'Неизвестно')
        undefLevel = level;
      if (level.getValue == 'Лайт')
        liteLevel = level;
      if (level.getValue == 'Лайт+')
        litePlusLevel = level;
      if (level.getValue == 'Медиум')
        mediumLevel = level;
      if (level.getValue == C.litePlusProLevelName)
        mediumPlusLevel = level;
      if (level.getValue == 'Хард')
        hardLevel = level;
    }

    User respUserData = resourceManager.currentUser;

    levelingTrainings = [];
    List<Training> trainings = resourceManager.trainings;

    for (Training training in trainings) {
      for (CloudValue trainingLevel in training.levels) {
        if (
        trainingLevel.id == respUserData.level
            || (respUserData.level == undefLevel.id && (trainingLevel.id == liteLevel.id))
            || (respUserData.level == litePlusLevel.id && trainingLevel.id == liteLevel.id)
            || (respUserData.level == mediumLevel.id)
            || (respUserData.level == mediumPlusLevel.id)
            || (respUserData.level == hardLevel.id)
        ) {
          levelingTrainings.add(training);
          break;
        }
      }
    }
  }

  void _buildFilterItems() {
    List<CloudValue> typeCloudValues = resourceManager.containers[CloudResource.TRAINING_TYPE].values;
    for (int i = 0; i < typeCloudValues.length; i++)
      widget.typeFilterItems.add(FilterItem(label: typeCloudValues[i].getValue));
    List<CloudValue> titleCloudValues = resourceManager.containers[CloudResource.TRAINING_TITLE].values;
    for (int i = 0; i < titleCloudValues.length; i++)
      widget.titleFilterItems.add(FilterItem(label: titleCloudValues[i].getValue));
    List<CloudValue> addressCloudValues = resourceManager.containers[CloudResource.ADDRESS].values;
    for (int i = 0; i < addressCloudValues.length; i++)
      widget.addressFilterItems.add(FilterItem(label: addressCloudValues[i].getValue));
    List<String> timesValues = _getActualTimes(levelingTrainings);
    for (int i = 0; i < timesValues.length; i++)
      widget.timeFilterItems.add(FilterItem(label: timesValues[i]));
  }

  List<String> _getActualTimes(List<Training> trainings) {
    print('getActualTimes trainings length ' + trainings.length.toString());
    List<String> timesValues = [];
    for (Training training in trainings) {
      String hours = training.dateTime.hour > 9 ? training.dateTime.hour.toString() : '0' + training.dateTime.hour.toString();
      String minutes = training.dateTime.minute > 9 ? training.dateTime.minute.toString() : '0' + training.dateTime.minute.toString();
      String trainingTime = hours + '-' + minutes;
      if (timesValues.indexOf(trainingTime) < 0)
        timesValues.add(trainingTime);
    }
    //Coll
    timesValues.sort((a, b) {
      return a.toLowerCase().compareTo(b.toLowerCase());
    });
    return timesValues;
  }

  void initWithFilterObject(FilterObject filterObject) {
    if (!filterObject.isEmpty) {
      List<FilterItem> type = filterObject.paramMaps[widget.typeFilterLabel];
      for (int i = 0; i < type.length; i++) {
        if (type[i].isCheck)
          widget.typeFilterItems[i].isCheck = true;
      }
      List<FilterItem> title = filterObject.paramMaps[widget.titleFilterLabel];
      for (int i = 0; i < title.length; i++) {
        if (title[i].isCheck)
          widget.titleFilterItems[i].isCheck = true;
      }
      List<FilterItem> address = filterObject.paramMaps[widget
          .addressFilterLabel];
      for (int i = 0; i < address.length; i++) {
        if (address[i].isCheck)
          widget.addressFilterItems[i].isCheck = true;
      }
      List<FilterItem> time = filterObject.paramMaps[widget.timeFilterLabel];
      for (int i = 0; i < time.length; i++) {
        if (time[i].isCheck)
          widget.timeFilterItems[i].isCheck = true;
      }
    }
  }

  String _getSatisfiedTrainingsCount(FilterObject filterObject, List<Training> inputTrainings) {
    List<Training> trainings = filterObject.applyTo(inputTrainings);

    print('selected count - ' + trainings.length.toString());
    if (trainings.length == 0)
      return '';
    return ' ' + (trainings.length).toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('ФИЛЬТР'),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.clear), onPressed: () {
              _cleanFilter();
            })
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          decoration: new BoxDecoration(
            image: new DecorationImage(
                image: AssetImage('assets/background.png'),
                fit: BoxFit.cover
            ),
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  children: <Widget>[
                    Text(widget.typeFilterLabel, style: widget.titleStyle),
                    Column(
                      children: widget.typeFilterItems.map((FilterItem filterItem) {
                        return FilterItemList(filterItem, () {
                          setState(() {});
                        });
                      }).toList(),
                    ),
                    Text(widget.titleFilterLabel, style: widget.titleStyle),
                    Column(
                      children: widget.titleFilterItems.map((FilterItem filterItem) {
                        return FilterItemList(filterItem, () {
                          setState(() {});
                        });
                      }).toList(),
                    ),
                    Text(widget.addressFilterLabel, style: widget.titleStyle),
                    Column(
                      children: widget.addressFilterItems.map((FilterItem filterItem) {
                        return FilterItemList(filterItem, () {
                          setState(() {});
                        });
                      }).toList(),
                    ),
                    Text(widget.timeFilterLabel, style: widget.titleStyle),
                    Column(
                      children: widget.timeFilterItems.map((FilterItem filterItem) {
                        return FilterItemList(filterItem, () {
                          setState(() {});
                        });
                      }).toList(),
                    ),
                  ],
                ),
              ),
              CustomButton(text: 'Применить' + _getSatisfiedTrainingsCount(_buildFilterObject(), levelingTrainings), onTap: () {
                ResourceManager().filterObject = _buildFilterObject();
                widget.listener();
                Navigator.pop(context);
              })
            ],
          ),
        )
    );

  }

  FilterObject _buildFilterObject() {
    FilterObject filterObject = FilterObject();
    filterObject.addFilterParams(widget.typeFilterLabel, widget.typeFilterItems);
    filterObject.addFilterParams(widget.titleFilterLabel, widget.titleFilterItems);
    filterObject.addFilterParams(widget.addressFilterLabel, widget.addressFilterItems);
    filterObject.addFilterParams(widget.timeFilterLabel, widget.timeFilterItems);
    return filterObject;
  }

  void _cleanFilter() {
    ResourceManager().filterObject = FilterObject();
    widget.listener();
    Navigator.pop(context);
  }

}

class FilterItemList extends StatefulWidget {

  final Function listener;

  final TextStyle labelStyle = const TextStyle(
      color: Colors.black45,
      fontFamily: 'PFDin',
      fontWeight: FontWeight.w500,
      fontSize: 22
  );

  final FilterItem filterItem;

  FilterItemList(FilterItem filterItem, this.listener)
      : filterItem = filterItem,
        super(key: new ObjectKey(filterItem));

  @override
  FilterItemListState createState() {
    return new FilterItemListState(filterItem);
  }
}

class FilterItemListState extends State<FilterItemList> {

  final FilterItem filterItem;

  FilterItemListState(this.filterItem);

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      value: filterItem.isCheck,
      onChanged: (bool value) {
        setState(() {
          filterItem.isCheck = value;
          widget.listener();
        });
      },
      title: Text(filterItem.label, style: widget.labelStyle),
      activeColor: Colors.indigo,
    );
  }
}
