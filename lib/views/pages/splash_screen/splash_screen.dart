import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/ui/splash/splash_background.dart';
import '../../../constants.dart';

class SplashScreen extends StatefulWidget {
  final List<Future<void>> futures;
  final Function dataListener;

  const SplashScreen({
    @required this.futures,
    @required this.dataListener,
  });

  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  Timer timer;
  double _progress = 0.0;
  double _curtainOpacity = 1;
  int stepCounter = 0;

  bool isLoadingComplete = false;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(milliseconds: 10), (Timer t) => emulateProgressStep());
    loadData();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Constants.primaryColorShades.shade500,
        ),
        child: CustomPaint(
          painter: SplashBackground(
            progress: _progress,
            curtainOpacity: _curtainOpacity,
            sizeBias: MediaQuery.of(context).size.height / 1000,
          ),
          child: Stack(
            children: [
              _firstPhaseLogoAnimationWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _firstPhaseLogoAnimationWidget() {
    return Positioned(
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      child: Center(
        child: AnimatedContainer(
          width: !isLoadingComplete ? 100 : 120,
          duration: Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
          child: Center(
            child: _logo(),
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Image(
      image: AssetImage('assets/menu-logo.png'),
      color: Colors.white,
    );
  }

  double fnc(int x) {
    double px = x / 100;
    //print("for x = $px, atan = ${pow(1 + 1 / px, px) / e}");
    return pow(1 + 1 / px, px) / e;
  }

  void emulateProgressStep() {
    setState(() {
      _progress = fnc(stepCounter);
      if (_curtainOpacity > 0)
      _curtainOpacity -= 1/120;
      if (_curtainOpacity <= 0) {
        _curtainOpacity = 0;
      }
      stepCounter++;
   /*   if (_progress > 0.9) {
        _progress = 0;
        stepCounter = 0;
      }*/
      //print(_progress);
    });
  }

  void finishProgress() {
    timer.cancel();
  }

  void _startIntoLogoAnimation() {
    setState(() {
      isLoadingComplete = true;
    });
  }

  void loadData() async {
    Future.wait(widget.futures).then((List responses) async {
      finishProgress();
      //await Future.delayed(const Duration(milliseconds: 300));

      for (var response in responses) {
        if (response is Exception) {
          throw response;
        }
        if (response is bool) {
          bool isOutdated = response;
          if (isOutdated) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('Внимание!'),
                content:
                    Text('Версия вашего приложения устарела! Обновите приложение во избежание возникновения ошибок.'),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: Text('OK'),
                  ),
                ],
              ),
            );
          }
        }
      }

      _startIntoLogoAnimation();
      Future.delayed(Duration(seconds: 1)).then((value) => widget.dataListener(responses));

      //widget.dataListener(responses);
    }).catchError((e) => {showErrorAlert(context, 'Не удалось загрузить данные. Попробуйте позже.')});
  }
}
