import 'package:flutter/material.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/registration/registration_step_1/view/registration_page_step_1.dart';
import 'package:url_launcher/url_launcher.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: [
            Center(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.7,
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: SizedBox(
                        width: double.infinity,
                        height: double.infinity,
                        child: Container(
                          child: Image(
                            image: AssetImage('assets/login-logo.png'),
                          ),
                        ),
                      ),
                    ),
                    CustomButton(
                      text: 'АВТОРИЗАЦИЯ',
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RegistrationPage(),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: GestureDetector(
                  onTap: () {
                    launch('https://chingiz98.github.io/UfaVolley/index.html');
                  },
                  child: Text(
                    'Политика конфиденциальности',
                    style: TextStyle(
                      color: Colors.indigo,
                      fontFamily: 'PFDin',
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
