import 'dart:convert' as json;
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as fa;
import 'package:firebase_messaging/firebase_messaging.dart' hide AuthorizationStatus;
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked_firebase_auth/stacked_firebase_auth.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';

import '../registration_step_1_contract.dart';
import 'package:flutter_login_vk/flutter_login_vk.dart';

class RegistrationStep1Presenter implements RegistrationStep1PresenterContract {
  RegistrationStep1ViewContract _view;

  final String _appleRedirectUri = 'https://ufavolleyreleased.firebaseapp.com/__/auth/handler';

  final vkSignIn = VKLogin();
  final FirebaseAuthenticationService _appleSignInService = FirebaseAuthenticationService();
  final fa.FirebaseAuth _auth = fa.FirebaseAuth.instance;
  bool notSignedIn = true;
  bool codeSent = false;
  String verificationId;

  bool rulesConfirmed = false;
  String phoneNumber = "";
  String code = "";

  bool isTicking = false;

  bool isVkInitialized = false;

  RegistrationStep1Presenter(BaseView view) {
    _view = view;
    _view.showProblemsWithAuthViaPhoneDialog();
  }

  void _saveTokens(String uid, String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('uid', uid);
    await prefs.setString('token', token);
  }

  Future<void> _verifyPhoneNumber(String number) async {
    final fa.PhoneVerificationCompleted verificationCompleted = (fa.PhoneAuthCredential phoneAuthCredential) async {
      notSignedIn = false;

      fa.User user = _auth.currentUser;

      String token = (await user.getIdToken(false));
      _saveTokens(user.uid, token);

      User userData = await APISupport.getUserData();

      if (userData.uid != null && userData.name != '' && userData.lastName != '') {
        (await SharedPreferences.getInstance()).setBool("logedin", true);

        _view.initResourceManager();

        String fcmToken = await FirebaseMessaging.instance.getToken();

        HttpsCallable callable = FirebaseFunctions.instance.httpsCallable('updateUser');
        await callable.call(<String, dynamic>{'fcm_update': true, 'fcm': fcmToken});
      } else {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setBool("logedin", false);
        //TODO ЗДЕСЬ ДОБАВИТЬ БОЛВАНКУ

        String fcmToken = await FirebaseMessaging.instance.getToken();
        User user = User(name: "", lastName: "");
        await APISupport.addNewUser(user: user, token: fcmToken);

        _view.showRegistrationStep2Page();
      }
    };

    final fa.PhoneVerificationFailed verificationFailed = (authException) {
      _view.showError(AlertMessage(title: "Произошла ошибка", message: authException.message.toString()));
    };

    final fa.PhoneCodeSent codeSent = (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
    };

    final fa.PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout = (String verificationId) {
      this.verificationId = verificationId;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: number,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<String> _signInWithPhoneNumber(String smsCode) async {
    _view.showLoading();
    fa.User user;
    try {
      fa.PhoneAuthCredential credential = fa.PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: smsCode,
      );

      // Sign the user in (or link) with the credential
      await _auth.signInWithCredential(credential);

      final fa.User currentUser = _auth.currentUser;

      user = currentUser;
      String token = (await user.getIdToken(false));
      _saveTokens(user.uid, token);
      assert(user.uid == currentUser.uid);
      SharedPreferences prefs = await SharedPreferences.getInstance();

      User userData = await APISupport.getUserData();

      if (userData.uid != null && userData.name != '' && userData.lastName != '') {
        print('open page myApp');
        prefs.setBool("logedin", true);
        //_view.hideLoading();
        await _view.initResourceManager(action: _view.hideLoading);
      } else {
        await prefs.setBool("logedin", false);

        //TODO ЗДЕСЬ ДОБАВИТЬ БОЛВАНКУ
        String fcmToken = await FirebaseMessaging.instance.getToken();
        User user = new User(name: "", lastName: "");
        await APISupport.addNewUser(user: user, token: fcmToken);
        _view.hideLoading();
        _view.showRegistrationStep2Page();
      }
    } catch (e) {
      _view.hideLoading();
      String code = e.code;
      if (code == "ERROR_INVALID_VERIFICATION_CODE") {
        _view.showError(AlertMessage(title: 'Внимание', message: "Вы ввели неверный код. Повторите попытку."));
      }

      print(e.toString());
    }

    return 'signInWithPhoneNumber succeeded: $user';
  }

  Future<http.Response> _fetchPost(String token) {
    return http
        .get(Uri.parse('https://api.vk.com/method/users.get?&v=5.92&access_token=' + token + '&fields=photo_400'));
  }

  Future<fa.User> _handleSignInEmail(String email, String password) async {
    final fa.UserCredential credential = (await _auth.signInWithEmailAndPassword(email: email, password: password));

    assert(credential != null);
    assert(await credential.user.getIdToken() != null);

    final fa.User currentUser = _auth.currentUser;
    assert(credential.user.uid == currentUser.uid);

    print('signInEmail succeeded: $credential');

    return credential.user;
  }

  Future<fa.User> _handleSignUp(email, password) async {
    final fa.UserCredential credential = (await _auth.createUserWithEmailAndPassword(email: email, password: password));

    assert(credential != null);
    assert(await credential.user.getIdToken() != null);

    return credential.user;
  }

  void _loginWithAT(String accessToken) async {
    _view.showLoading();
    var r = await _fetchPost(accessToken);
    var b = r.body;

    var jb = json.jsonDecode(b);
    var res = jb['response'];
    var id = res[0]['id'];
    var photoURL = res[0]['photo_400'];

    try {
      await _handleSignInEmail(id.toString() + "@vk.com", id.toString());

      try {
        await APISupport.uploadVkPhoto(photoURL: photoURL);
      } catch (e) {
        print('uploadVkPhoto throw exception');
      }

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool("logedin", true);

      await FirebaseMessaging.instance.getToken();

      User userData = await APISupport.getUserData();

      if (userData.uid != null && userData.name != '' && userData.lastName != '') {
        prefs.setBool("logedin", true);
        _view.initResourceManager(action: _view.hideLoading);
      } else {
        await prefs.setBool("logedin", false);

        //TODO ЗДЕСЬ ДОБАВИТЬ БОЛВАНКУ
        String fcmToken = await FirebaseMessaging.instance.getToken();
        User user = new User(name: "", lastName: "");
        await APISupport.addNewUser(user: user, token: fcmToken);
        _view.hideLoading();
        _view.showRegistrationStep2Page();
      }
    } catch (e) {
      await _handleSignUp(id.toString() + "@vk.com", id.toString());

      try {
        await APISupport.uploadVkPhoto(photoURL: photoURL);
      } catch (e) {
        print('uploadVkPhoto throw exception');
      }

      String fcmToken = await FirebaseMessaging.instance.getToken();
      //TODO ЗДЕСЬ ДОБАВИТЬ БОЛВАНКУ
      User user = new User(name: "", lastName: "");
      await APISupport.addNewUser(user: user, token: fcmToken);
      _view.hideLoading();
      _view.showRegistrationStep2Page();
    }
  }

  void _checkConfirmCodeButtonAllow() {
    if (code.length == 6 && codeSent)
      _view.setEnabledConfirmCodeButton(true);
    else
      _view.setEnabledConfirmCodeButton(false);
  }

  void _checkRegistrationButtonAllow() {
    if (phoneNumber.length == 10 && rulesConfirmed && !isTicking)
      _view.setEnabledRegistrationButton(true);
    else
      _view.setEnabledRegistrationButton(false);
  }

  @override
  Future<Null> onClickLoginWithVK() async {

    if (!rulesConfirmed) {
      _view.showError(AlertMessage(title: "Внимание!", message: "Вы не согласились с правилами",),);
      return;
    }

    if (!isVkInitialized) {
      final initResult = await vkSignIn.initSdk();

      final isInitSuccess = !initResult.isError && (initResult.asValue?.value ?? false);

      if (!isInitSuccess) {
        _view.showError(
          AlertMessage(
            title: "Ошибка",
            message: 'Не удается продолжить авторизацию',
          ),
        );
        return;
      }

      isVkInitialized = true;
    }

    await vkSignIn.logOut();

    final result = await vkSignIn.logIn(scope: [VKScope.email]);

    if (result.isError) {
      _view.showError(
        AlertMessage(
          title: "Ошибка",
          message: 'Не удалось авторизоваться. Попробуйте еще раз',
        ),
      );
      return;
    }

    final vkLoginResult = result.asValue.value;

    if (vkLoginResult.isCanceled) {
      _view.showError(
        AlertMessage(
          title: "Внимание",
          message: 'Процесс авторизации прерван',
        ),
      );
      return;
    }

    final vkToken = vkLoginResult.accessToken;
    _loginWithAT(vkToken.token);
  }

  @override
  void onClickRegisterWithPhoneButton() async {
    isTicking = true;
    _view.startTicker();
    _view.setEnabledRegistrationButton(false);
    codeSent = true;
    _view.setEnabledCodeField(true);

    await _verifyPhoneNumber('+7' + phoneNumber);
  }

  @override
  void onRulesConfirmationChanged(bool confirmation) {
    this.rulesConfirmed = confirmation;
    _checkRegistrationButtonAllow();
  }

  @override
  void onPhoneFieldChanged(String newValue) {
    phoneNumber = newValue;
    _checkRegistrationButtonAllow();
  }

  @override
  void onClickConfirmCodeButton() async {
    try {
      await _signInWithPhoneNumber(code);
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  void onCodeFieldChanged(String newValue) {
    print('new code ' + newValue);
    code = newValue;
    _checkConfirmCodeButtonAllow();
  }

  @override
  void onTickerFinished() {
    isTicking = false;
    _checkRegistrationButtonAllow();
  }

  @override
  void openOSApplicationSettings() {
    openAppSettings();
  }

  @override
  Future<Null> onClickLoginWithApple() async {
    _view.showLoading();
    try {
      await _appleSignInService.signInWithApple(
        appleRedirectUri: _appleRedirectUri,
        appleClientId: '',
      );

      SharedPreferences prefs = await SharedPreferences.getInstance();
      User userData = await APISupport.getUserData();

      if (userData.uid != null && userData.name.isNotEmpty && userData.lastName.isNotEmpty) {
        print('open page myApp');
        prefs.setBool("logedin", true);
        await _view.initResourceManager(action: _view.hideLoading);
      } else {
        await prefs.setBool("logedin", false);
        //TODO ЗДЕСЬ ДОБАВИТЬ БОЛВАНКУ
        String fcmToken = await FirebaseMessaging.instance.getToken();
        User user = new User(name: "", lastName: "");
        await APISupport.addNewUser(user: user, token: fcmToken);
        _view.hideLoading();
        _view.showRegistrationStep2Page();
      }
    } catch (e) {
      _view.hideLoading();
      _view.showError(AlertMessage(
        title: "Apple ID",
        message: 'Произошла ошибка при входе c помощью Apple ID',
      ));
    }
  }
}
