import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';

abstract class RegistrationStep1ViewContract implements BaseView {
   void showRegistrationStep2Page();
   //void showMain();
   Future<void> initResourceManager({Function action});
   void setEnabledRegistrationButton(bool enabled);
   void setEnabledCodeField(bool enabled);
   void setEnabledConfirmCodeButton(bool enabled);
   void startTicker();
   void showProblemsWithAuthViaPhoneDialog();
}

abstract class RegistrationStep1PresenterContract implements BasePresenter {
  Future<Null> onClickLoginWithVK();
  Future<Null> onClickLoginWithApple();
  void onClickRegisterWithPhoneButton();
  void onClickConfirmCodeButton();
  void onRulesConfirmationChanged(bool confirmation);
  void onPhoneFieldChanged(String newValue);
  void onCodeFieldChanged(String newValue);
  void onTickerFinished();
  void openOSApplicationSettings();
}