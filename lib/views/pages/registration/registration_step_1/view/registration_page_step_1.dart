import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/registration/registration_step_1/presenter/registration_step_1_presenter.dart';
import 'package:ufavolley/views/pages/registration/registration_step_1/widgets/sign_in_with_apple_button.dart';
import 'package:ufavolley/views/pages/registration/registration_step_2/view/registration_page_step_2.dart';
import 'package:ufavolley/views/pages/rules/view/rules_page.dart';

import '../registration_step_1_contract.dart';

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage>
    with TickerProviderStateMixin
    implements RegistrationStep1ViewContract {
  RegistrationStep1Presenter _presenter;

  AnimationController _controller;
  static const int kStartValue = 3 * 60;

  TextEditingController _smsCodeController = TextEditingController();
  final _phoneFieldController = TextEditingController();

  bool registerButtonEnabled = false;
  bool confirmCodeButtonEnabled = false;
  bool codeFieldEnabled = false;
  bool registerPressed = false;

  @override
  void initState() {
    super.initState();

    _presenter = RegistrationStep1Presenter(this);

    _controller = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: kStartValue),
    );

    _phoneFieldController.addListener(() {
      _presenter.onPhoneFieldChanged(_phoneFieldController.text);
    });

    _smsCodeController.addListener(() {
      _presenter.onCodeFieldChanged(_smsCodeController.text);
    });

    _controller.addListener(() {
      if (_controller.isCompleted) _presenter.onTickerFinished();
    });
  }

  @override
  void dispose() {
    _phoneFieldController.dispose();
    _controller.dispose();
    _smsCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 26.0, color: Colors.black);

    Text prefix = new Text('+7', style: style);

    TextField phoneField = new TextField(
      controller: _phoneFieldController,
      style: new TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 26.0, color: Colors.black),
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp("[0-9]")),
        LengthLimitingTextInputFormatter(10),
      ],
      decoration: InputDecoration(
        border: InputBorder.none,
        prefixIcon: SizedBox(
          child: Center(
            widthFactor: 0.0,
            child: prefix,
          ),
        ),
      ),
    );

    Container phoneBox = new Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30.0),
        ),
        padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
        margin: const EdgeInsets.all(6.0),
        width: MediaQuery.of(context).size.width * 0.8,
        child: new Center(child: phoneField));

    Widget rulesChecker = new Container(
        child: Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      alignment: WrapAlignment.center,
      children: <Widget>[
        Checkbox(
            value: _presenter.rulesConfirmed,
            onChanged: (newValue) {
              setState(() {
                _presenter.onRulesConfirmationChanged(newValue);
              });
            },
            activeColor: Colors.indigo),
        Text('Я согласен с ', style: style),
        GestureDetector(
          child: Text('правилами',
              style: TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 26.0, color: Colors.indigo)),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Scaffold(
                        appBar: AppBar(
                          title: Text('Правила'),
                        ),
                        body: RulesPage(),
                      )),
            );
          },
        )
      ],
    ));

    Widget registerButton = CustomButton(
      text: 'ЗАРЕГИСТРИРОВАТЬСЯ',
      size: 24.0,
      enabled: registerButtonEnabled,
      onTap: () {
        registerPressed = true;
        _presenter.onClickRegisterWithPhoneButton();
      },
    );

    TextField smsCodeField = new TextField(
      controller: _smsCodeController,
      enabled: codeFieldEnabled,
      style: new TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 26.0, color: Colors.black),
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp("[0-9]")),
        LengthLimitingTextInputFormatter(6),
      ],
      decoration: InputDecoration(border: InputBorder.none, hintText: ''),
    );

    Container codeBox = new Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30.0),
        ),
        padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
        margin: const EdgeInsets.all(6.0),
        width: MediaQuery.of(context).size.width * 0.5,
        child: new Center(child: smsCodeField));

    var list = ListView(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(horizontal: 20),
      children: <Widget>[
        phoneBox,
        rulesChecker,
        registerButton,
        Container(
          child: new Center(
            child: new CountdownWidget(
              animation: new StepTween(
                begin: kStartValue,
                end: 0,
              ).animate(_controller),
            ),
          ),
        ),
        IgnorePointer(
            ignoring: !registerPressed,
            child: AnimatedOpacity(
                opacity: registerPressed ? 1 : 0,
                duration: Duration(milliseconds: 500),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  child: codeBox,
                ))),
        IgnorePointer(
            ignoring: !registerPressed,
            child: AnimatedOpacity(
                opacity: registerPressed ? 1 : 0,
                duration: Duration(milliseconds: 500),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 60),
                  child: CustomButton(
                    text: 'ОК',
                    enabled: confirmCodeButtonEnabled,
                    onTap: () {
                      _presenter.onClickConfirmCodeButton();
                    },
                  ),
                ))),
        Text('или\nвойти через',
            textAlign: TextAlign.center,
            style:
                new TextStyle(color: Colors.black, fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 28.0)),
        Align(
          alignment: Alignment.center,
          child: Container(
            margin: EdgeInsets.only(top: 8.0),
            alignment: Alignment.center,
            width: 80,
            height: 80,
            child: GestureDetector(
              onTap: () async {
                _presenter.onClickLoginWithVK();
              },
              child: Container(child: Image(image: AssetImage('assets/vk.png'), fit: BoxFit.contain)),
            ),
          ),
        ),
        if (Platform.isIOS) ...[
          const SizedBox(height: 20.0),
          SignInWithAppleButton(
            onPressed: _presenter.onClickLoginWithApple,
          ),
        ],
      ],
    );

    return Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
        ),
        child: Align(
          alignment: Alignment.center,
          child: list,
        ),
      ),
    );
  }

  @override
  void showAlert(AlertMessage message) async {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text(message.title),
              content: new Text(message.message),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Ок',
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ],
            ));
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void showRegistrationStep2Page() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => RegistrationPageStep2()));
  }

  @override
  void hideLoading() {
    Navigator.of(context).pop();
  }

  @override
  void setEnabledCodeField(bool enabled) {
    setState(() {
      codeFieldEnabled = enabled;
    });
  }

  @override
  void setEnabledRegistrationButton(bool enabled) {
    setState(() {
      registerButtonEnabled = enabled;
    });
  }

  @override
  void showError(AlertMessage message) async {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text(message.title),
              content: new Text(message.message),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Закрыть',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ],
            ));
  }

  @override
  void updateState() {
    setState(() {});
  }

  @override
  void setEnabledConfirmCodeButton(bool enabled) {
    print(confirmCodeButtonEnabled.toString());
    setState(() {
      confirmCodeButtonEnabled = enabled;
    });
  }

  @override
  void startTicker() {
    _controller.forward(from: 0.0);
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  Future<void> initResourceManager({Function action}) async {
    print("initResourceManager");

    return await ResourceManager().init().then((r) => {showMain()}).catchError((e) => {
          action == null ? 0 : action(),
          print("ERROR CAUGHT " + e.toString()),
          showErrorAlert(context, 'Не удалось загрузить данные. Попробуйте позже.')
        });
  }

  void showMain() {
    Navigator.of(context).pushNamedAndRemoveUntil("/main", (Route<dynamic> route) => false);
  }

  @override
  void showProblemsWithAuthViaPhoneDialog() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      showDialog(
        context: context,
        builder: (context) => WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: Text('Внимание'),
            content: Text(
                'В данный момент наблюдается проблема с получением смс сообщений с кодом подтверждения на номера операторов Мегафон, Yota и Билайн. При возникновении проблем используйте альтернативные способы авторизации'),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(
                  'Ок',
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}

class CountdownWidget extends AnimatedWidget {
  CountdownWidget({Key key, this.animation}) : super(key: key, listenable: animation);
  final Animation<int> animation;

  @override
  build(BuildContext context) {
    return new Text(getTime(animation.value),
        style: new TextStyle(color: Colors.black, fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 28.0));
  }
}
