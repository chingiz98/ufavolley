import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/registration/registration_step_2/presenter/registration_step_2_presenter.dart';
import 'package:ufavolley/widgets/date_picker/date_picker.dart';
import 'package:ufavolley/widgets/date_picker/date_picker_controller.dart';

import '../registration_step_2_contract.dart';

class RegistrationPageStep2 extends StatefulWidget {
  @override
  RegistrationPageStep2State createState() => RegistrationPageStep2State();
}

class RegistrationPageStep2State extends State<RegistrationPageStep2> implements RegistrationStep2ViewContract {
  Image profileImage;

  TextEditingController _nameFieldController;
  TextEditingController _lastNameFieldController;
  final DatePickerController _datePickerController = DatePickerController();
  RegistrationStep2Presenter _presenter;

  @override
  void initState() {
    _presenter = RegistrationStep2Presenter(this);

    _nameFieldController = TextEditingController();
    _nameFieldController.addListener(() {
      _presenter.onNameFieldValueChanged(_nameFieldController.text);
    });

    _lastNameFieldController = TextEditingController();
    _lastNameFieldController.addListener(() {
      _presenter.onLastNameFieldValueChanged(_lastNameFieldController.text);
    });

    _datePickerController.addListener((date) {
      _presenter.setBirthDate(date);
      setState(() {});
    });

    profileImage = Image(image: AssetImage('assets/default_profile_image_woman.png'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("Registration page step 2 BUILD WAS CALLED");
    var imageWidget = LayoutBuilder(
      builder: (context, constraints) {
        return Container(height: constraints.biggest.width, child: profileImage);
      },
    );

    Container profileImageEditor = new Container(
      margin: EdgeInsets.all(12.0),
      child: new Container(
        //color: Colors.blue,
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              alignment: AlignmentDirectional.bottomCenter,
              width: 25,
              height: 50,
              //color: Colors.green,
            ),
            Expanded(child: imageWidget),
            Container(
              margin: EdgeInsets.only(left: 10),
              width: 25,
              //color: Colors.red,
              child: new Material(
                color: Colors.transparent,
                child: InkWell(
                  child: new Image(image: new AssetImage('assets/edit_profile_photo.png'), fit: BoxFit.cover),
                  onTap: () {
                    _presenter.onClickEditImageButton();
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );

    CustomTextField nameTextField = CustomTextField(
      hint: 'Имя',
      controller: _nameFieldController,
      margin: EdgeInsets.only(bottom: 5),
      isImportant: true,
      formatters: [FilteringTextInputFormatter.allow(RegExp("[a-zA-Zа-яА-Я0-9]"))],
    );

    CustomTextField lastNameTextField = CustomTextField(
      hint: 'Фамилия',
      controller: _lastNameFieldController,
      margin: EdgeInsets.only(bottom: 5),
      isImportant: true,
      formatters: [FilteringTextInputFormatter.allow(RegExp("[a-zA-Zа-яА-Я0-9]"))],
    );

    CustomDropDownButton sexDropDownButton = CustomDropDownButton(
        margin: EdgeInsets.only(bottom: 5),
        onChanged: (String val) {
          _presenter.onSexFieldValueChanged(val);
        });

    CustomButton confirmButton = CustomButton(
        text: 'ГОТОВО',
        size: 24.0,
        margin: EdgeInsets.only(top: 30, bottom: 6, left: 35, right: 35),
        onTap: () async {
          _presenter.onClickConfirmButton();
        });

    return Scaffold(
      //resizeToAvoidBottomPadding: false,
      body: FutureBuilder(
        future: _setupUserImage(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: AssetImage('assets/background.png'), fit: BoxFit.cover, alignment: Alignment.topCenter),
              ),
              child: new Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                child: Center(
                  child: new ListView(
                    children: <Widget>[
                      profileImageEditor,
                      nameTextField,
                      lastNameTextField,
                      sexDropDownButton,
                      DatePicker(
                        controller: _datePickerController,
                      ),
                      confirmButton
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Future<int> _setupUserImage() async {
    Uint8List bytes = await _presenter.downloadUserImage();
    if (bytes != null)
      profileImage = Image.memory(bytes, fit: BoxFit.cover);
    else {
      String path = _presenter.providePathToDefaultUserImage();
      profileImage = Image(image: AssetImage(path));
    }

    return 0;
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void showMain() {
    Navigator.of(context).pushNamedAndRemoveUntil('/main', (Route<dynamic> route) => false);
  }

  @override
  void updateImage(XFile image) {
    setState(() {
      profileImage = Image.file(File(image.path), fit: BoxFit.cover);
    });
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showNeedsPermissionDialog() async {
    await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text("Внимание!"),
        content: new Text("Для корректной работы приложения требуется выдать все права в настройках!"),
        actions: <Widget>[
          new TextButton(
            onPressed: () async {
              _presenter.openOsApplicationSettings();
            },
            child: new Text("Перейти в настройки"),
          ),
        ],
      ),
    );
  }

  @override
  void initResourceManager() async {
    showLoading();
    await ResourceManager().init().then(
      (r) {
        hideLoading();
        showMain();
      },
    ).catchError(
      (e) {
        hideLoading();
        showErrorAlert(context, 'Не удалось загрузить данные. Попробуйте позже.');
      },
    );
  }

  @override
  void showTooLargeFileDialog() {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Внимание'),
        content: new Text('Размер изображения слишком большой. Попробуйте выбрать другое изображение.'),
        actions: <Widget>[
          new TextButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: new Text('Ок'),
          ),
        ],
      ),
    );
  }
}
