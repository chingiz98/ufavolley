import 'dart:io';
import 'dart:math' as Math;
import 'dart:typed_data';
import 'package:firebase_auth/firebase_auth.dart' as fa;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image/image.dart' as Im;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import '../registration_step_2_contract.dart';

class RegistrationStep2Presenter implements RegistrationStep2PresenterContract {
  fa.FirebaseAuth _auth;
  RegistrationStep2ViewContract _view;

  String _sex;
  String _name;
  String _lastName;
  DateTime _birthDate;

  RegistrationStep2Presenter(RegistrationStep2ViewContract view) {
    _view = view;
    _auth = fa.FirebaseAuth.instance;
  }

  Future<Uint8List> downloadUserImage() async {
    //_view.showLoading();
    var bytes;

    try {
      String uid = _auth.currentUser.uid;
      bytes = await FirebaseStorage.instance.ref().child(uid).getData(9999999);
    } catch (e) {
      _view.showError(AlertMessage(
          title: "Ошибка", message: "Не удалось загрузить изображение"));
    }
    //_view.hideLoading();

    return bytes;
  }

  String providePathToDefaultUserImage() {
    String path = 'assets/default_profile_image_woman.png';

    if (_sex != null && _sex != "" && _sex == 'Мужской')
      path = 'assets/boy.jpg';
    else
      path = 'assets/girl.jpg';
    _view.hideLoading();

    return path;
  }

  Future<int> finishRegistration() async {
    _view.showLoading();

    String token = await FirebaseMessaging.instance.getToken();

    var phoneNum = _auth.currentUser.phoneNumber;

    if (phoneNum == null) phoneNum = "";

    bool sex = _sex == 'Мужской' ? true : false;

    User newUser = User(
      name: _name,
      lastName: _lastName,
      dateOfBirth: _birthDate,
      gender: sex,
      phoneNumber: phoneNum,
    );

    await APISupport.updateUser(user: newUser, token: token);
    ResourceManager().currentUser = await APISupport.getUserData();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("logedin", true);

    _view.hideLoading();

    _view.initResourceManager();

    return 0;
  }

  @override
  void setBirthDate(DateTime dateTime) {
    _birthDate = dateTime;
  }

  @override
  void onClickConfirmButton() async {
    if (_name == null || _name.length == 0) {
      _view.showError(
          AlertMessage(title: 'Ошибка', message: 'Имя не может быть пустым'));
      return;
    }

    if (_lastName == null || _lastName.length == 0) {
      _view.showError(AlertMessage(
          title: 'Ошибка', message: 'Фамилия не может быть пустой'));
      return;
    }

    if (_sex == null || _sex != 'Мужской' && _sex != 'Женский') {
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Выберите пол'));
      return;
    }

    await finishRegistration();
  }

  @override
  void onClickEditImageButton() async {
    await ImagePicker().pickImage(source: ImageSource.gallery).then((image) {
      onImagePicked(image);
    }).catchError((error) {
      _view.showNeedsPermissionDialog();
    });

  }

  void onImagePicked(XFile imageFile) async {
    _view.showLoading();
    if (imageFile != null) {
      final tempDir = await getTemporaryDirectory();
      final path = tempDir.path;
      int rand = new Math.Random().nextInt(10000);

      final imageBytes = await imageFile.readAsBytes();

      Im.Image image = Im.decodeImage(imageBytes);

      var compressedImage = new File('$path/img_$rand.jpg')
        ..writeAsBytesSync(Im.encodeJpg(image, quality: 20));

      var size = compressedImage.lengthSync();

      if (size > 600000) {
        _view.showError(AlertMessage(
            title: 'Ошибка',
            message:
            'Размер изображения слишком большой. Попробуйте выбрать другое изображение'));
      } else {
        String uid = _auth.currentUser.uid;
        final Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(uid);
        final UploadTask task =
        firebaseStorageRef.putFile(compressedImage);
        await task;
        _view.updateImage(imageFile);
      }
    }

    _view.hideLoading();
  }

  @override
  void onNameFieldValueChanged(String newValue) {
    _name = newValue;
  }

  @override
  void onLastNameFieldValueChanged(String newValue) {
    _lastName = newValue;
  }

  @override
  void onSexFieldValueChanged(String newValue) {
    _sex = newValue;
  }

  @override
  void openOsApplicationSettings() {
    openAppSettings();
  }
}
