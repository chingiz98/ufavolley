import 'package:image_picker/image_picker.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';

abstract class RegistrationStep2ViewContract implements BaseView {
  void showMain();
  void updateImage(XFile image);
  void showNeedsPermissionDialog();
  void initResourceManager();
  void showTooLargeFileDialog();
}

abstract class RegistrationStep2PresenterContract implements BasePresenter {
  void onClickEditImageButton();
  void onNameFieldValueChanged(String newValue);
  void onLastNameFieldValueChanged(String newValue);
  void onSexFieldValueChanged(String newValue);
  void setBirthDate(DateTime dateTime);
  void onClickConfirmButton();
  void openOsApplicationSettings();
}