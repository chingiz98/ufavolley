import 'package:flutter/material.dart';

class RulesPage extends StatefulWidget {
  @override
  State createState() => new RulesPageState();
}

class RulesPageState extends State<RulesPage> {
  static const _kDuration = const Duration(milliseconds: 300);

  static const _kCurve = Curves.ease;

  static final TextStyle subTitleTextStyle = TextStyle(
    color: Colors.indigo,
    fontFamily: 'PFDin',
    fontWeight: FontWeight.w500,
    fontSize: 20,
  );

  static final TextStyle messageTextStyle = TextStyle(
    color: Color.fromARGB(255, 102, 102, 102),
    fontFamily: 'PFDin',
    fontWeight: FontWeight.w500,
    fontSize: 18,
  );

  final List<Widget> _pages = <Widget>[
    Container(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('ПРИЛОЖЕНИЕ\n', style: subTitleTextStyle),
            Text(
              'ВНИМАНИЕ! \n'
              '\nПрежде чем начать пользоваться приложением UfaVolley для записи на занятия, обязательно проверьте настройки своего телефона!\n'
              '\nЧасовой пояс, который должен быть у вас выставлен "+5 Екатеринбург". '
              'В противном случае время начала занятия отображается неверно и администрация не несёт ответственности за вашу неосведомленность.\n'
              '\nТренировки проходят по разным уровням:\n'
              '- Лайт (новичок);\n'
              '- Лайт + (любитель);\n'
              '- Лайт Pro (продвинутый);\n'
              '- Медиум (опытный);\n'
              '- Хард (полупрофи);\n'
              '\nВ ОДИН ДЕНЬ И НА ОДНО ВРЕМЯ записаться можно ТОЛЬКО НА ОДНО ЗАНЯТИЕ. '
              'В противном случае Приложение выдаст Вам ошибку.\n',
              style: messageTextStyle,
            ),
          ],
        ),
      ),
    ),
    Container(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('КАК ЗАПИСАТЬСЯ НА ТРЕНИРОВКИ?\n', style: subTitleTextStyle),
            Text(
              'Огромная просьба записываться по уровню Вашей игры '
              '(если вы сомневаетесь в вашем уровне игры, '
              'свяжитесь с нами через удобный способ в разделе «Контакты») для того, чтобы разобраться в этом вопросе).\n'
              'Это все необходимо для продуктивной и результативной тренировки всех участников!\n'
              '\nЗаписываться необходимо по расписанию, '
              'представленному в разделе расписание.\n'
              'Для каждой тренировки создана отдельная вкладка.\n'
              '\nНеобходимо выбрать день и время тренировки, '
              'затем записать себя. '
              'Каждый игрок может записать только одного игрока, т.е. себя.\n'
              '\nДля каждой тренировки сушествует ЛИМИТ по колличеству человек!\n'
              'Приходить СВЕРХ ЛИМИТА не следует, '
              'администратор/тренер все равно не пропустит. '
              'Иначе не хватит места в зале и на площадке и Вы сами будете мешаться друг другу.\n'
              '\nЛимит на ИГРОВЫЕ занятия: минимум 8-10 человек, максимум 18 человек!\n'
              '\nЛимит на групповую ТРЕНИРОВКУ С ТРЕНЕРОМ: минимум 8-10 человек, максимум 14 человек в малые залы, 18 человек в большие залы с одним тренером и 24 человека в большие залы с двумя тренерами.',
              style: messageTextStyle,
            ),
          ],
        ),
      ),
    ),
    Container(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'ЕСЛИ ЗАПИСАЛИСЬ И НЕ МОЖЕТЕ ПРИЙТИ?\n',
              style: subTitleTextStyle,
            ),
            Text(
                'Если вдруг Вы НЕ можете прийти, ОТПИСАТЬСЯ НЕОБХОДИМО ЗА 2 ЧАСА ДО ТРЕНИРОВКИ!\n'
                '\nДля чего это делается? '
                'Чтобы другие участники, записавшиеся в резерв (сверх лимита), '
                'успели спланировать свой вечер. '
                'Записаться и не прийти - это безответственно, '
                'тем самым Вы подводите других игроков и тренера/администратора, '
                'а так же людей, которые могли прийти вместо Вас!\n'
                '\nПризываем к тому, '
                'чтобы Вы заблаговременно выписывались самостоятельно с тренировки.'
                'Чтобы отписаться, необходимо в том же списке, '
                'где Вы уже записались, '
                'нажать на кнопку «Отписаться» '
                '(заблаговременно, за 2 часа до начала тренировки).\n'
                '\nВ случае если Вы записались, '
                'не отписались и не пришли на тренировку, '
                'Вы должны оплатить занятие!',
                style: messageTextStyle),
          ],
        ),
      ),
    ),
    Container(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'ЧТО ВЗЯТЬ С СОБОЙ НА ТРЕНИРОВКУ?\n',
              style: subTitleTextStyle,
            ),
            Text(
              '• Спортивная форма\n'
              '(футболка/майка, шорты/спортивное трико)\n'
              '\n• Питьевая вода 0,5-1 л.\n'
              '(во время тренировки следует употреблять воду)\n'
              '\n• Чистая спортивная обувь, хорошо фиксирующая стопу '
              '(желательно для волейбола).\n'
              'Подошва не должна быть черной и не должна оставлять черные полосы на площадке.\n'
              '\nСТОИМОСТЬ 2-УХЧАСОВОЙ ТРЕНИРОВКИ С ТРЕНЕРОМ\n'
              'указана в записа на тренировку\n'
              '(ценовые категории от 200р. до 1500 р.).\n'
              '\nСТОИМОСТЬ 2-УХЧАСОВОЙ ИГРОВОЙ ТРЕНИРОВКИ\n'
              'указана в записи на тренировку\n'
              '(ценовые категории от 150 р. до 300 р.).\n'
              '\nОплата производится по окончанию тренировки, '
              'лично в руки тренеру или администратору\n'
              '(если оплата по карте, '
              'обязательно показать тренеру/администратору данные о переводе средств).',
              style: messageTextStyle,
            ),
          ],
        ),
      ),
    ),
    Container(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'КАК ВЕСТИ СЕБЯ НА ТРЕНИРОВКАХ?\n',
              style: subTitleTextStyle,
            ),
            Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: 'Естественно вести себя с уважением к тренеру или администратору и другим игрокам!\n'
                          '\n Мы все хотим поиграть и весело провести время - '
                          'не создаем негатив во время тренировок!\n'
                          '\nПо всем вопросам всегда можно обратиться к тренеру или администратору.\n'
                          '\n• Относиться бережно к инвентарю и спортзалу, '
                          'где вы занимаетесь: '
                          'не портить, не ломать ничего, ПЕРЕОБУВАТЬСЯ НА ВХОДЕ, '
                          'убирать за собой мусор.\n'
                          '\n• Бережно относиться к волейбольным мячам: '
                          'напоминаем, в волейбол играют ',
                      style: messageTextStyle),
                  TextSpan(
                      text: 'руками',
                      style: TextStyle(
                          color: Color.fromARGB(255, 102, 102, 102),
                          fontFamily: 'PFDin',
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          decoration: TextDecoration.underline)),
                  TextSpan(
                      text: ', но не ногами.\n'
                          '\nИзвиниться, если случайно попал мячом другому игроку в голову или другие части тела.'
                          '\nАдминистрация школы не несет ответственности за жизнь и здоровье клиента!',
                      style: messageTextStyle)
                  // can add more TextSpans here...
                ],
              ),
            ),
          ],
        ),
      ),
    )
  ];

  final controller = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    var pageView = PageView.builder(
      //physics: new NeverScrollableScrollPhysics(),
      controller: controller,
      itemCount: _pages.length,
      itemBuilder: (BuildContext context, int index) {
        return _pages[index];
      },
    );

    var dotsWidget = Container(
      child: new Center(
        child: new DotsIndicator(
          controller: controller,
          itemCount: _pages.length,
          onPageSelected: (int page) {
            controller.animateToPage(
              page,
              duration: _kDuration,
              curve: _kCurve,
            );
          },
        ),
      ),
    );

    var next = SwitchPageWidget(
      controller: controller,
      pagesCount: _pages.length,
      direction: 1,
    );
    var prev = SwitchPageWidget(
      quarterTurns: 2,
      controller: controller,
      pagesCount: _pages.length,
      direction: -1,
    );

    var title = Text(
      'ШКОЛА ВОЛЕЙБОЛА ДЛЯ ВЗРОСЛЫХ (18+)\nГЛАВНОЕ ПРАВИЛО ШКОЛЫ «UFAVOLLEY»\n ПЕРЕОБУВАЕМСЯ НА ВХОДЕ В ЗАЛ!\nПОДОШВА КРОССОВОК\nНЕ ДОЛЖНА БЫТЬ ЧЕРНОЙ И ПАЧКАТЬ ПОЛ!',
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.black,
        fontFamily: 'PFDin',
        fontWeight: FontWeight.w500,
        fontSize: 20,
      ),
    );

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
        ),
        child: Container(
          margin: EdgeInsets.only(top: 16),
          child: Stack(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    title,
                    Expanded(
                      child: pageView,
                    ),
                    Container(
                      margin: EdgeInsets.all(8),
                      child: dotsWidget,
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: prev,
              ),
              Container(
                alignment: Alignment.centerRight,
                child: next,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: const Color.fromARGB(150, 180, 180, 180),
  }) : super(listenable: controller);

  /// The PageController that this DotsIndicator is representing.
  final PageController controller;

  /// The number of items managed by the PageController
  final int itemCount;

  /// Called when a dot is tapped
  final ValueChanged<int> onPageSelected;

  /// The color of the dots.
  ///
  /// Defaults to `Colors.white`.
  final Color color;

  // The base size of the dots
  static const double _kDotSize = 16.0;

  // The distance between the center of each dot
  static const double _kDotSpacing = 25.0;

  Widget _buildDot(int index) {
    int currentIndex = 0;

    if (controller.page != null) currentIndex = controller.page.round();

    return new Container(
      width: _kDotSpacing,
      child: new Center(
        child: new Material(
          color: currentIndex == index ? Color.fromARGB(190, 51, 51, 255) : color,
          type: MaterialType.circle,
          child: new Container(
            width: _kDotSize,
            height: _kDotSize,
            child: new InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: new List<Widget>.generate(itemCount, _buildDot),
    );
  }
}

class SwitchPageWidget extends StatefulWidget {
  final int quarterTurns;
  final PageController controller;
  final int direction;
  final int pagesCount;

  const SwitchPageWidget(
      {this.quarterTurns = 0, @required this.controller, @required this.direction, @required this.pagesCount});

  @override
  State createState() => new SwitchPageWidgetState();
}

class SwitchPageWidgetState extends State<SwitchPageWidget> {
  @override
  Widget build(BuildContext context) {
    double page = 0;

    if (widget.controller.page != null) page = widget.controller.page;

    if (page == widget.pagesCount - 1 || page == 0) return Container();

    return GestureDetector(
      child: RotatedBox(
        quarterTurns: widget.quarterTurns,
        child: Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(30)),
              image: DecorationImage(image: AssetImage('assets/icon_switch_page.png'))),
        ),
      ),
      onTap: () {
        print(widget.controller.page);
        if (widget.direction == 1 && widget.controller.page.round() + 1 < widget.pagesCount) {
          widget.controller.animateToPage((widget.controller.page.round() + 1),
              curve: Curves.ease, duration: Duration(milliseconds: 300));
        }

        if (widget.direction == -1 && widget.controller.page.round() - 1 > -1) {
          widget.controller.animateToPage((widget.controller.page.round() - 1),
              curve: Curves.ease, duration: Duration(milliseconds: 300));
        }
      },
    );
  }
}
