import 'package:flutter/material.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_details.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_group.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/training_leader_board_data.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/presenter/training_leaderboard_settings_presenter.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/widgets/trainig_leaderboard_settings_details.dart';

import '../training_leaderboard_settings_contract.dart';

class TrainingLeaderBoardSettingsView extends StatefulWidget {
  final Training training;

  TrainingLeaderBoardSettingsView(this.training);

  @override
  State<StatefulWidget> createState() {
    return TrainingLeaderBoardSettingsViewState();
  }
}

class TrainingLeaderBoardSettingsViewState extends State<TrainingLeaderBoardSettingsView>
    implements TrainingLeaderBoardSettingsViewContract {
  SelectableTrainingLeaderBoardData _selectableLeaderBoardData;
  TrainingLeaderBoardSettingPresenterContract _presenter;

  bool _isLoading = false;
  bool _isEditMode = false;

  @override
  void initState() {
    super.initState();
    _presenter = TrainingLeaderBoardSettingsPresenter(this, widget.training);
    _presenter.loadLeaderBoard();
  }

  @override
  Widget build(BuildContext context) {
    Widget body;

    if (_isLoading) {
      body = Center(
        child: CircularProgressIndicator(),
      );
    } else {
      if (_selectableLeaderBoardData != null) {
        body = _buildLeaderBoard(_selectableLeaderBoardData);
      } else {
        body = Center(
          child: CircularProgressIndicator(),
        );
      }
    }

    Scaffold scaffold = Scaffold(
      appBar: AppBar(title: Text("Выставление рейтинга")),
      body: body,
      floatingActionButton: FloatingActionButton(
          child: _isEditMode ? Icon(Icons.edit) : Icon(Icons.done),
          onPressed: () {
            _fabClick();
          }),
    );

    return scaffold;
  }

  void _fabClick() {
    if (_isEditMode) {
      _presenter.detailsButtonClick();
    } else {
      _presenter.confirmButtonClick(_selectableLeaderBoardData);
    }
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showAlert(AlertMessage message) {}

  @override
  void showError(AlertMessage message) {}

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void updateState() {}

  @override
  void onLoaded(TrainingLeaderBoardData data) {
    setState(() {
      this._selectableLeaderBoardData = SelectableTrainingLeaderBoardData(data);
    });
  }

  Widget _buildLeaderBoard(SelectableTrainingLeaderBoardData data) {
    List<Widget> items = data.items.map((item) => _buildLeaderBoardItem(item)).toList();
    return ListView(
      children: items,
    );
  }

  Widget _buildLeaderBoardItem(SelectableUserItem item) {
    final double fontSize = 24;

    TextStyle baseStyle = TextStyle(fontSize: fontSize);

    TextStyle ratingTextStyle = TextStyle(fontSize: fontSize, color: Colors.greenAccent);

    final backgroundColor = item.isSelected ? Colors.grey : Colors.transparent;

    String userPlace = "-";

    if (item.ratingDetails.place != 0) {
      userPlace = item.ratingDetails.place.toString();
    }

    return InkWell(
      child: Container(
        color: backgroundColor,
        child: Row(
          children: <Widget>[
            Container(margin: EdgeInsets.all(16), child: Text(userPlace, style: baseStyle)),
            Flexible(
              flex: 1,
              child: Container(
                  margin: EdgeInsets.all(16), child: Text("${item.user.lastName} ${item.user.name}", style: baseStyle)),
            ),
            Container(
                margin: EdgeInsets.all(16), child: Text("+${item.ratingDetails.ratingValue}", style: ratingTextStyle))
          ],
        ),
      ),
      onTap: () {
        _selectItem(item);
      },
    );
  }

  void _selectItem(SelectableUserItem leaderBoardItem) {
    setState(() {
      _selectableLeaderBoardData.items.firstWhere((item) => item.user.uid == leaderBoardItem.user.uid).isSelected =
          !leaderBoardItem.isSelected;
    });
    _presenter.updateLeaderBoard(_selectableLeaderBoardData);
  }

  @override
  void hideDetailsButton() {
    setState(() {
      _isEditMode = false;
    });
  }

  @override
  void showDetailsButton() {
    setState(() {
      _isEditMode = true;
    });
  }

  @override
  void showDetailsDialog() {
    showDialog(
        context: context, builder: (context) => Dialog(child: TrainingLeaderBoardSettingsDetails(_onDetailsConfirmed)));
  }

  void _onDetailsConfirmed(RatingDetails ratingDetails) {
    if (ratingDetails.ratingValue != null) {
      List<SelectableUserItem> selectedUsers =
          _selectableLeaderBoardData.items.where((user) => user.isSelected).toList();
      RatingGroup ratingGroup = RatingGroup(selectedUsers.map((user) => user.user).toList(), ratingDetails);
      _updateRatingGroup(ratingGroup);
      _unSelectAll();
    }
  }

  void _updateRatingGroup(RatingGroup ratingGroup) {
    List<User> ratingGroupUser = ratingGroup.users;
    List<SelectableUserItem> selectableUserItems = _selectableLeaderBoardData.items;
    for (int i = 0; i < ratingGroupUser.length; i++) {
      for (int j = 0; j < selectableUserItems.length; j++) {
        if (ratingGroupUser[i].uid == selectableUserItems[j].user.uid) {
          _selectableLeaderBoardData.items[j].ratingDetails = ratingGroup.ratingDetails;
          break;
        }
      }
    }
    for (int i = 0; i < selectableUserItems.length; i++) {
      if (selectableUserItems[i].ratingDetails.place == ratingGroup.ratingDetails.place) {
        _selectableLeaderBoardData.items[i].ratingDetails = ratingGroup.ratingDetails;
      }
    }
    setState(() {});
  }

  void _unSelectAll() {
    _selectableLeaderBoardData.items.forEach((user) => user.isSelected = false);
    _presenter.updateLeaderBoard(_selectableLeaderBoardData);
    setState(() {});
  }

  @override
  void showLoadingIndicator(bool isLoading) {
    setState(() {
      _isLoading = isLoading;
    });
  }
}

class SelectableTrainingLeaderBoardData {
  List<SelectableUserItem> items = [];

  SelectableTrainingLeaderBoardData(TrainingLeaderBoardData trainingLeaderBoardData) {
    trainingLeaderBoardData.groups.forEach((group) => {
          group.users.forEach((user) => {this.items.add(SelectableUserItem(user, group.ratingDetails))})
        });
  }
}

class SelectableUserItem {
  User user;
  RatingDetails ratingDetails;
  bool isSelected = false;

  SelectableUserItem(this.user, this.ratingDetails, {this.isSelected = false});
}
