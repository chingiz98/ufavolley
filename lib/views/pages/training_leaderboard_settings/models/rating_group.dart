import 'package:json_annotation/json_annotation.dart';
import 'package:ufavolley/models/user.dart';
import 'rating_details.dart';

part 'rating_group.g.dart';

@JsonSerializable(explicitToJson: true)
class RatingGroup {

  List<User> users;
  RatingDetails ratingDetails;

  RatingGroup(this.users, this.ratingDetails);

  factory RatingGroup.fromJson(Map<String, dynamic> json) => _$RatingGroupFromJson(json);
  Map<String, dynamic> toJson() => _$RatingGroupToJson(this);
}
