import 'package:json_annotation/json_annotation.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_group.dart';

part 'training_leader_board_data.g.dart';

@JsonSerializable(explicitToJson: true)
class TrainingLeaderBoardData {
  String trainingId;
  List<RatingGroup> groups;

  TrainingLeaderBoardData(this.trainingId, this.groups);

  factory TrainingLeaderBoardData.fromJson(Map<String, dynamic> json) =>
      _$TrainingLeaderBoardDataFromJson(json);

  Map<String, dynamic> toJson() => _$TrainingLeaderBoardDataToJson(this);
}
