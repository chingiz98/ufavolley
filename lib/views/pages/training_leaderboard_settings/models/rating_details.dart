import 'package:json_annotation/json_annotation.dart';

part 'rating_details.g.dart';

@JsonSerializable(explicitToJson: true)
class RatingDetails {
  int place;
  int ratingValue;

  RatingDetails(this.place, this.ratingValue);

  factory RatingDetails.fromJson(Map<String, dynamic> json) =>
      _$RatingDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$RatingDetailsToJson(this);
}
