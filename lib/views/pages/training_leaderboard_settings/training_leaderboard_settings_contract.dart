import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/view/training_leaderboard_settings_view.dart';
import 'models/training_leader_board_data.dart';

abstract class TrainingLeaderBoardSettingsViewContract implements BaseView {
  void onLoaded(TrainingLeaderBoardData data);
  void showDetailsButton();
  void hideDetailsButton();
  void showDetailsDialog();
  void showLoadingIndicator(bool isLoading);
}

abstract class TrainingLeaderBoardSettingPresenterContract implements BasePresenter {
  void loadLeaderBoard();
  void updateLeaderBoard(SelectableTrainingLeaderBoardData data);
  void detailsButtonClick();
  void confirmButtonClick(SelectableTrainingLeaderBoardData data);
}