import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_details.dart';

class TrainingLeaderBoardSettingsDetails extends StatefulWidget {
  final Function listener;

  TrainingLeaderBoardSettingsDetails(this.listener);

  @override
  State<StatefulWidget> createState() {
    return TrainingLeaderBoardSettingsDetailsState();
  }
}

class TrainingLeaderBoardSettingsDetailsState extends State<TrainingLeaderBoardSettingsDetails> {
  TextEditingController ratingTextEditingController = TextEditingController();
  List<String> places = ["-", "1", "2", "3", "4"];
  String place;

  @override
  void initState() {
    super.initState();
    place = places.first;
  }

  @override
  void dispose() {
    ratingTextEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DropdownButton<String> placeSelector = DropdownButton(
      isExpanded: true,
      value: place,
      items: places.map((String place) {
        return DropdownMenuItem(
          value: place,
          child: new Text(place, style: TextStyle(fontSize: 18)),
        );
      }).toList(),
      hint: Text('Тип', style: TextStyle(fontSize: 18)),
      onChanged: (String newPlace) {
        setState(() {
          place = newPlace;
        });
      },
    );

    Widget content = Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: Wrap(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.symmetric(vertical: 16),
                  child: Text('Рейтинг команды', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600))),
              Text('Призовое место', style: TextStyle(fontSize: 18)),
              Container(
                margin: EdgeInsets.only(top: 16),
                child: placeSelector,
              ),
              TextField(
                controller: ratingTextEditingController,
                decoration: InputDecoration(hintText: "Количество очков"),
                inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[0-9]"))],
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 18),
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('Закрыть'),
                    ),
                    TextButton(
                      onPressed: () {
                        int intPlace = int.tryParse(place);
                        if (intPlace == null) intPlace = 0;
                        int ratingValue = int.tryParse(ratingTextEditingController.text.toString());

                        RatingDetails ratingDetails = RatingDetails(intPlace, ratingValue);

                        widget.listener(ratingDetails);
                        Navigator.of(context).pop();
                      },
                      child: Text('Ок'),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );

    return content;
  }
}
