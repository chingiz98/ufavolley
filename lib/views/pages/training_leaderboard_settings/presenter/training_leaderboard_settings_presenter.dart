import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_details.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/rating_group.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/training_leader_board_data.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/view/training_leaderboard_settings_view.dart';

import '../training_leaderboard_settings_contract.dart';

class TrainingLeaderBoardSettingsPresenter implements TrainingLeaderBoardSettingPresenterContract {
  TrainingLeaderBoardSettingsViewContract _view;
  Training _training;

  TrainingLeaderBoardSettingsPresenter(TrainingLeaderBoardSettingsViewContract view, Training training) {
    this._view = view;
    this._training = training;
  }

  @override
  void updateLeaderBoard(SelectableTrainingLeaderBoardData data) {
    bool isSomeoneSelected = data.items.where((item) => item.isSelected).toList().length > 0;
    if (isSomeoneSelected) {
      _view.showDetailsButton();
    } else {
      _view.hideDetailsButton();
    }
  }

  @override
  void loadLeaderBoard() async {
    _view.showLoadingIndicator(true);
    TrainingLeaderBoardData trainingLeaderBoardData = await APISupport.getLeaderBoardData(_training.trainingID);
    _view.showLoadingIndicator(false);
    _view.onLoaded(trainingLeaderBoardData);
  }

  @override
  void detailsButtonClick() {
    _view.showDetailsDialog();
  }

  @override
  void confirmButtonClick(SelectableTrainingLeaderBoardData selectableTrainingLeaderBoardData) async {
    _view.showLoading();
    await APISupport.setLeaderBoardData(convertToTrainingLeaderBoardData(selectableTrainingLeaderBoardData));
    _view.hideLoading();
  }

  TrainingLeaderBoardData convertToTrainingLeaderBoardData(
      SelectableTrainingLeaderBoardData selectableTrainingLeaderBoardData) {
    List<RatingGroup> groups = [];
    List<SelectableUserItem> users = selectableTrainingLeaderBoardData.items;
    for (int i = 0; i < users.length; i++) {
      RatingDetails currentRatingDetails = users[i].ratingDetails;
      bool isRatingGroupExist = false;
      int ratingGroupIndex = -1;
      for (int j = 0; j < groups.length; j++) {
        if (users[i].ratingDetails.place == groups[j].ratingDetails.place) {
          isRatingGroupExist = true;
          ratingGroupIndex = j;
          break;
        }
      }
      if (isRatingGroupExist) {
        groups[ratingGroupIndex].users.add(users[i].user);
      } else {
        List<User> groupUsers = [];
        groupUsers.add(users[i].user);
        RatingGroup ratingGroup = RatingGroup(groupUsers, currentRatingDetails);
        groups.add(ratingGroup);
      }
    }
    return TrainingLeaderBoardData(_training.trainingID, groups);
  }
}
