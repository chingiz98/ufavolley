import 'package:flutter/widgets.dart';
import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';

abstract class ProfileViewContract implements BaseView {
  void showTooLargeFileDialog();
  void onImageLoaded(Image image);
  void setBirthDate(DateTime dateTime);
  void setName(String name);
  void setLastName(String lastName);
  void setGender(String gender);
  void setRating(int rating);
  void setPhoneNumber(String phoneNumber);
  void openSchedulePage();
  void showNeedsPermissionDialog();
}

abstract class ProfilePresenterContract implements BasePresenter {
  void finishButtonClick();
  void selectImageButtonClick();
  void setBirthDate(DateTime dateTime);
  void setName(String name);
  void setLastName(String lastName);
  void setGender(String gender);
  void openOSApplicationSettings();
  void onPhoneLinked();
}