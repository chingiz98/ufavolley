import 'dart:io';
import 'dart:math' as Math;

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/widgets.dart';
import 'package:image/image.dart' as Im;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/views/pages/profile/profile_contract.dart';

class ProfilePresenter implements ProfilePresenterContract {
  ProfileViewContract _view;

  String _gender = '';
  String _name;
  String _lastName;
  DateTime _birthDate;

  ProfilePresenter(ProfileViewContract view) {
    this._view = view;
    _loadUserImage();
    _loadUserRating();
  }

  void _fillView() {
    User user = ResourceManager().currentUser;
    _view.setName(user.name);
    _view.setLastName(user.lastName);
    _view.setGender(user.gender == true ? 'Мужской' : 'Женский');
    _view.setPhoneNumber(user.phoneNumber);
    //мини-костыль
    _gender = user.gender == true ? 'Мужской' : 'Женский';
    //String stringDate = user.dateOfBirth != null ? _dateToMaskedTextField(user.dateOfBirth) : '';
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _view.setBirthDate(user.dateOfBirth);
    });
  }

  @override
  void finishButtonClick() async {
    if (_name == null || _name == "") {
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Имя не может быть пустым'));
      return;
    }

    if (_lastName == null || _lastName == "") {
      _view.showError(AlertMessage(title: 'Ошибка', message: 'Фамилия не может быть пустой'));
      return;
    }

    if (!ResourceManager().currentUser.isEditable) {
      _view.showError(
          AlertMessage(title: 'Ошибка', message: 'При изменении данных произошла ошибка. Попробуйте позже.'));
      return;
    }

    _view.showLoading();

    String token = await FirebaseMessaging.instance.getToken();

    User userData = _buildUserObject();

    await APISupport.updateUser(user: userData, token: token, byHimself: true);
    ResourceManager().currentUser = await APISupport.getUserData();
    _view.hideLoading();
    _view.openSchedulePage();
  }

  User _buildUserObject() {
    var phoneNum = ResourceManager().currentUser.phoneNumber;
    if (phoneNum == null) phoneNum = "";

    var sex = true;
    if (_gender != 'Мужской') {
      sex = false;
    }

    return User(
      name: _name,
      lastName: _lastName,
      level: ResourceManager().currentUser.level,
      dateOfBirth: _birthDate,
      isAdmin: ResourceManager().currentUser.isAdmin,
      isTrainer: ResourceManager().currentUser.isTrainer,
      uid: ResourceManager().currentUser.uid,
      phoneNumber: phoneNum,
      gender: sex,
      isEditable: ResourceManager().currentUser.isEditable,
      byAdmin: ResourceManager().currentUser.byAdmin,
      isSupervisor: ResourceManager().currentUser.isSupervisor,
      isBanned: ResourceManager().currentUser.isBanned,
      isMainAdmin: ResourceManager().currentUser.isMainAdmin,
    );
  }

  void _loadUserRating() async {
    await Future.delayed(Duration(seconds: 0));
    User u = ResourceManager().currentUser;
    _view.setRating(u.rating);
  }

  void _loadUserImage() async {
    var bytes;

    try {
      bytes = await FirebaseStorage.instance.ref().child(ResourceManager().currentUser.uid).getData(9999999);
    } catch (e) {}

    Image profileImage;

    if (bytes != null) {
      profileImage = Image.memory(bytes, fit: BoxFit.cover);
    } else {
      profileImage = Image(
        image: AssetImage('assets/default_profile_image_woman.png'),
        fit: BoxFit.cover,
      );
    }

    _view.onImageLoaded(profileImage);
    _fillView();
  }

  @override
  void selectImageButtonClick() async {
    await ImagePicker().pickImage(source: ImageSource.gallery).then((image) {
      onImagePicked(image);
    }).catchError((error) {
      _view.showNeedsPermissionDialog();
    });
  }

  void onImagePicked(XFile imageFile) async {
    if (imageFile != null) {
      _view.showLoading();
      final tempDir = await getTemporaryDirectory();
      final path = tempDir.path;
      int rand = new Math.Random().nextInt(10000);

      final bytes = await imageFile.readAsBytes();

      Im.Image image = Im.decodeImage(bytes);

      var compressedImage = new File('$path/img_$rand.jpg')..writeAsBytesSync(Im.encodeJpg(image, quality: 20));

      var size = compressedImage.lengthSync();

      if (size > 600000) {
        _view.hideLoading();
        _view.showTooLargeFileDialog();
      } else {
        String uid = ResourceManager().currentUser.uid;
        final Reference firebaseStorageRef = FirebaseStorage.instance.ref().child(uid);
        final UploadTask task = firebaseStorageRef.putFile(compressedImage);
        await task;
        _view.onImageLoaded(Image.file(File(imageFile.path), fit: BoxFit.cover));
        _view.hideLoading();
      }
    }
    _view.updateState();
  }

  @override
  void setBirthDate(DateTime dateTime) {
    _birthDate = dateTime;
  }

  @override
  void setGender(String gender) {
    _gender = gender;
  }

  @override
  void setLastName(String lastName) {
    _lastName = lastName;
  }

  @override
  void setName(String name) {
    _name = name;
  }

  @override
  void openOSApplicationSettings() {
    openAppSettings();
  }

  @override
  void onPhoneLinked() => _fillView();
}
