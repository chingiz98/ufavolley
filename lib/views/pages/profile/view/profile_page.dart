import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ufavolley/constants.dart';
import 'package:ufavolley/views/pages/phone_linking/view/phone_linking.dart';
import 'package:ufavolley/widgets/date_picker/date_picker.dart';
import 'package:ufavolley/widgets/date_picker/date_picker_controller.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/profile/presenter/profile_presenter.dart';
import 'package:ufavolley/views/pages/profile/profile_contract.dart';

class ProfilePage extends StatefulWidget {
  @override
  State createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> implements ProfileViewContract {
  bool _isImageLoaded = false;
  int _rating;

  TextEditingController _nameFieldController;

  TextEditingController _lastNameFieldController;

  final DatePickerController _datePickerController = DatePickerController();

  CustomDropDownButton sexDropDownButton;
  String sex;

  ProfilePresenterContract _presenter;
  Image profileImage;

  String phoneNumber = '';

  @override
  void initState() {
    _presenter = ProfilePresenter(this);
    _nameFieldController = TextEditingController();
    _nameFieldController.addListener(() {
      setState(() {
        _presenter.setName(_nameFieldController.text);
      });
    });
    _lastNameFieldController = TextEditingController();
    _lastNameFieldController.addListener(() {
      setState(() {
        _presenter.setLastName(_lastNameFieldController.text);
      });
    });
    _datePickerController.addListener((date) {
      _presenter.setBirthDate(date);
      setState(() {

      });
    });
    super.initState();
  }

  Widget _getProfileImage() {
    var defaultImage = Image(image: AssetImage('assets/default_profile_image_woman.png'), fit: BoxFit.cover);

    return profileImage != null
        ? Card(
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
            child: profileImage)
        : defaultImage;
  }

  @override
  Widget build(BuildContext context) {
    print("PROFILE BUILD WAS CALLED");

    var content;

    if (_isImageLoaded)
      content = _buildContent();
    else
      content = Center(
        child: CircularProgressIndicator(),
      );

    return Scaffold(body: content);
  }

  Widget _buildContent() {
    var imageWidget = LayoutBuilder(
      builder: (context, constraints) {
        return AspectRatio(
            aspectRatio: 1, child: Container(height: constraints.biggest.width, child: _getProfileImage()));
      },
    );

    var ratingWidget = Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Constants.primaryColorShades,
        customBorder: StadiumBorder(),
        child: Card(
          shape: StadiumBorder(),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            child: Row(children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 2),
                padding: EdgeInsets.only(top: 1),
                child: new Image(
                  image: new AssetImage('assets/trophy.png'),
                  height: 18,
                  width: 18,
                  fit: BoxFit.cover,
                ),
              ),
              Text(_rating.toString(), style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600))
            ]),
          ),
        ),
        onTap: () {},
      ),
    );

    var ratingLoader = Container(
      margin: EdgeInsets.all(8),
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        strokeWidth: 3,
        valueColor: AlwaysStoppedAnimation(Colors.white),
      ),
    );

    var imageWithRating = Stack(
      children: <Widget>[
        imageWidget,
        Positioned(bottom: 8, right: 8, child: _rating != null ? ratingWidget : ratingLoader)
      ],
    );

    Container profileImageEditorWidget = Container(
      margin: EdgeInsets.all(12.0),
      child: new Container(
          child: new Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(right: 10), alignment: AlignmentDirectional.bottomCenter, width: 25, height: 50),
          Expanded(child: imageWithRating),
          Container(
            margin: EdgeInsets.only(left: 10),
            width: 25,
            child: new Material(
              color: Colors.transparent,
              child: InkWell(
                child: new Image(image: new AssetImage('assets/edit_profile_photo.png'), fit: BoxFit.cover),
                onTap: () {
                  _presenter.selectImageButtonClick();
                },
              ),
            ),
          )
        ],
      )),
    );

    CustomTextField nameTextField = CustomTextField(
      hint: 'Имя',
      controller: _nameFieldController,
      margin: EdgeInsets.only(bottom: 5),
      isImportant: true,
      formatters: [FilteringTextInputFormatter.allow(RegExp("[a-zA-Zа-яА-Я0-9]"))],
    );

    CustomTextField lastNameTextField = CustomTextField(
      hint: 'Фамилия',
      controller: _lastNameFieldController,
      margin: EdgeInsets.only(bottom: 5),
      isImportant: true,
      formatters: [FilteringTextInputFormatter.allow(RegExp("[a-zA-Zа-яА-Я0-9]"))],
    );

    CustomDropDownButton sexDropDownButton = CustomDropDownButton(
      margin: EdgeInsets.only(bottom: 5),
      onChanged: (String val) {
        _presenter.setGender(val);
      },
      initialValue: sex,
    );

    CustomButton finishButton = CustomButton(
        text: 'ГОТОВО',
        size: 24.0,
        margin: EdgeInsets.only(top: 6, bottom: 6, left: 35, right: 35),
        onTap: () async {
          _presenter.finishButtonClick();
        });

    return Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: AssetImage('assets/background.png'), fit: BoxFit.cover, alignment: Alignment.topCenter),
      ),
      child: new Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Center(
          child: new ListView(
            children: <Widget>[
              profileImageEditorWidget,
              nameTextField,
              lastNameTextField,
              sexDropDownButton,
              DatePicker(
                controller: _datePickerController,
              ),
              _buildPhone(),
              finishButton,
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPhone() {
    return phoneNumber.isNotEmpty
        ? Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              'Номер телефона: $phoneNumber',
              textAlign: TextAlign.center,
            ),
          )
        : CustomButton(
            text: 'Привязать номер телефона',
            size: 20.0,
            margin: EdgeInsets.only(top: 6, bottom: 6, left: 35, right: 35),
            onTap: () => _navigateToPhoneLinking(),
          );
  }

  @override
  void hideLoading() {
    hideView();
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(message.title),
        content: new Text(message.message),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void showTooLargeFileDialog() {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Внимание'),
        content: new Text('Размер изображения слишком большой. Попробуйте выбрать другое изображение.'),
        actions: <Widget>[
          new TextButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: new Text('Ок'),
          ),
        ],
      ),
    );
  }

  @override
  void updateState() {
    setState(() {});
  }

  @override
  void setBirthDate(DateTime dateTime) {
    _datePickerController.setValue(dateTime);
  }

  @override
  void setGender(String gender) {
    this.sex = gender;
  }

  @override
  void setLastName(String lastName) {
    _lastNameFieldController.text = lastName;
  }

  @override
  void setName(String name) {
    _nameFieldController.text = name;
  }

  @override
  void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @override
  void onImageLoaded(Image image) {
    this._isImageLoaded = true;
    this.profileImage = image;
    setState(() {});
  }

  @override
  void openSchedulePage() {
    Navigator.of(context).pushNamedAndRemoveUntil('/main', (Route<dynamic> route) => false);
  }

  @override
  void setRating(int rating) {
    setState(() {
      _rating = rating;
    });
  }

  @override
  void showNeedsPermissionDialog() async {
    await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text("Внимание!"),
        content: new Text("Для корректной работы приложения требуется выдать все права в настройках!"),
        actions: <Widget>[
          new TextButton(
            onPressed: () async {
              _presenter.openOSApplicationSettings();
            },
            child: new Text("Перейти в настройки"),
          ),
        ],
      ),
    );
  }

  void _navigateToPhoneLinking() async {
    await Navigator.push(context, MaterialPageRoute(builder: (context) => PhoneLinkingPage()));
    _presenter.onPhoneLinked();
  }
}
