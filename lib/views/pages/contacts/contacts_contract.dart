import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/base/base_presenter.dart';

abstract class ContactsViewContract implements BaseView {

}

abstract class ContactsPresenterContract implements BasePresenter {
  void onClickURLContactButton(String url);
  void onClickPhoneNumber();
}