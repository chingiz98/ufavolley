import 'package:url_launcher/url_launcher.dart';
import 'package:ufavolley/utils/alert_message.dart';

import '../contacts_contract.dart';

class ContactsPresenter extends ContactsPresenterContract {

  ContactsViewContract _view;

  ContactsPresenter(ContactsViewContract view) {
    _view = view;
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      _view.showError(AlertMessage(title: "Ошибка", message: "Хм. Какие-то проблемы, возможно устарела ссылка"));
    }
  }

  @override
  void onClickPhoneNumber() {
    launch('tel:+79899516680');
  }

  @override
  void onClickURLContactButton(String url) {
    _launchURL(url);
  }


}