import 'package:flutter/material.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/views/pages/contacts/presenter/contacts_presenter.dart';

import '../contacts_contract.dart';

class ContactsPage extends StatefulWidget {
  @override
  State createState() => ContactsPageState();
}

class ContactsPageState extends State<ContactsPage> implements ContactsViewContract {
  ContactsPresenter _presenter;

  @override
  void initState() {
    _presenter = ContactsPresenter(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Container contactNumberBlock = Container(
      child: GestureDetector(
        child: Column(
          children: <Widget>[
            Text(
              'По всем вопросам звоните:',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'PFDin',
                fontWeight: FontWeight.w500,
                fontSize: 24.0,
                color: Colors.black,
              ),
            ),
            Text(
              '+79899516680',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'PFDin',
                fontWeight: FontWeight.w500,
                fontSize: 30.0,
                color: Colors.indigo,
              ),
            )
          ],
        ),
        onTap: () {
          _presenter.onClickPhoneNumber();
        },
      ),
    );

    GestureDetector _buildSocialNetworkContact(String path, String url, double sideSize, {String hint}) {
      TextStyle hintStyle = TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w400, color: Colors.blueGrey);

      GestureDetector contact = GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              width: sideSize,
              height: sideSize,
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(path), fit: BoxFit.contain),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 4),
              child: hint != null ? Text(hint, style: hintStyle) : Text('', style: hintStyle),
            )
          ],
        ),
        onTap: () async {
          _presenter.onClickURLContactButton(url);
        },
      );
      return contact;
    }

    Container _buildSocialNetworkContactsBlock(double screenWidth) {
      double sideSize = screenWidth / 6;

      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _buildSocialNetworkContact('assets/vk.png', 'https://vk.com/ufavolleyball', sideSize),
            _buildSocialNetworkContact('assets/icon_whatsapp.png', 'https://it-s.me/ufavolley', sideSize),
          ],
        ),
      );
    }

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(top: 16),
              alignment: Alignment.topCenter,
              child: contactNumberBlock,
            ),
            Container(
              margin: EdgeInsets.only(bottom: 32),
              alignment: Alignment.bottomCenter,
              child: _buildSocialNetworkContactsBlock(MediaQuery.of(context).size.width),
            )
          ],
        ),
      ),
    );
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void showAlert(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Ок',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showError(AlertMessage message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message.title),
        content: Text(message.message),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Закрыть',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    // TODO: implement updateState
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }
}
