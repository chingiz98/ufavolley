import 'package:firebase_auth/firebase_auth.dart' as fa;
import 'package:firebase_messaging/firebase_messaging.dart' hide AuthorizationStatus;
import 'package:flutter/foundation.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/base/base_view.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';

import '../phone_linking_contract.dart';

class PhoneLinkingPresenter implements PhoneLinkingPresenterContract {
  PhoneLinkingContract _view;

  final fa.FirebaseAuth _auth = fa.FirebaseAuth.instance;
  bool codeSent = false;
  String verificationId;

  String phoneNumber = "";
  String code = "";

  bool isTicking = false;

  PhoneLinkingPresenter(BaseView view) {
    _view = view;
  }

  Future<void> _verifyPhoneNumber(String number) async {
    final verificationCompleted = (phoneAuthCredential) => _link(phoneAuthCredential);

    final verificationFailed = (authException) {
      _view.showError(AlertMessage(title: "Произошла ошибка", message: authException.message.toString()));
    };

    final codeSent = (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
    };

    final codeAutoRetrievalTimeout = (String verificationId) {
      this.verificationId = verificationId;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: number,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<bool> _link(fa.PhoneAuthCredential credentials, {VoidCallback onLinkTried}) async {
    try {
      print('link');
      await _auth.currentUser.linkWithCredential(credentials);
      User userData = await APISupport.getUserData();
      String token = await FirebaseMessaging.instance.getToken();
      userData.phoneNumber = '+7' + phoneNumber;
      await APISupport.updateUser(user: userData, token: token, byHimself: true);
      ResourceManager().currentUser = userData;
      onLinkTried?.call();
      return Future.value(true);
    } on fa.FirebaseAuthException catch (e) {
      onLinkTried?.call();
      if (e.code == 'credential-already-in-use') {
        _view.showError(AlertMessage(title: 'Ошибка привязки', message: 'Данный номер уже используется в системе.'));
      } else {
        _view.showError(
            AlertMessage(title: 'Ошибка привязки', message: 'Произошла ошибка во время привязки номера (${e.code})'));
      }
    } catch (e) {
      onLinkTried?.call();
      _view.showError(AlertMessage(title: 'Ошибка привязки', message: 'Произошла ошибка во время привязки номера'));
    } finally {
      print('finally');
      _view.setEnabledCodeField(false);
      _view.resetView();
      phoneNumber = '';
      isTicking = false;
    }
    return Future.value(false);
  }

  Future<String> _linkWithSmsCode(String smsCode) async {
    _view.showLoading();
    fa.User user;
    try {
      fa.PhoneAuthCredential credential = fa.PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: smsCode,
      );

      if (await _link(credential, onLinkTried: _view.hideLoading)) {
        _view.hideView();
      }
    } catch (e) {
      _view.hideLoading();
      String code = e.code;
      if (code == "ERROR_INVALID_VERIFICATION_CODE") {
        _view.showError(AlertMessage(title: 'Внимание', message: "Вы ввели неверный код. Повторите попытку."));
      }

      print(e.toString());
    }

    return '_linkWithSmsCode succeeded: $user';
  }

  void _checkConfirmCodeButtonAllow() {
    if (code.length == 6 && codeSent)
      _view.setEnabledConfirmCodeButton(true);
    else
      _view.setEnabledConfirmCodeButton(false);
  }

  void _checkLinkingButtonAllow() {
    if (phoneNumber.length == 10 && !isTicking)
      _view.setEnabledLinkButton(true);
    else
      _view.setEnabledLinkButton(false);
  }

  @override
  void onClickLinkButton() async {
    isTicking = true;
    _view.startTicker();
    _view.setEnabledLinkButton(false);
    codeSent = true;
    _view.setEnabledCodeField(true);

    await _verifyPhoneNumber('+7' + phoneNumber);
  }

  @override
  void onPhoneFieldChanged(String newValue) {
    phoneNumber = newValue;
    _checkLinkingButtonAllow();
  }

  @override
  void onClickConfirmCodeButton() async {
    try {
      await _linkWithSmsCode(code);
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  void onCodeFieldChanged(String newValue) {
    print('new code ' + newValue);
    code = newValue;
    _checkConfirmCodeButtonAllow();
  }

  @override
  void onTickerFinished() {
    isTicking = false;
    _checkLinkingButtonAllow();
  }
}
