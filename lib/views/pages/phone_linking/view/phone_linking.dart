import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/phone_linking/presenter/phone_linking_presenter.dart';

import '../phone_linking_contract.dart';

class PhoneLinkingPage extends StatefulWidget {
  @override
  _PhoneLinkingPageState createState() => _PhoneLinkingPageState();
}

class _PhoneLinkingPageState extends State<PhoneLinkingPage>
    with TickerProviderStateMixin
    implements PhoneLinkingContract {
  PhoneLinkingPresenter _presenter;

  AnimationController _controller;
  static const int kStartValue = 3 * 60;

  TextEditingController _smsCodeController = TextEditingController();
  final _phoneFieldController = TextEditingController();

  bool registerButtonEnabled = false;
  bool confirmCodeButtonEnabled = false;
  bool codeFieldEnabled = false;
  bool registerPressed = false;

  @override
  void initState() {
    super.initState();

    _presenter = PhoneLinkingPresenter(this);

    _controller = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: kStartValue),
    );

    _phoneFieldController.addListener(() {
      _presenter.onPhoneFieldChanged(_phoneFieldController.text);
    });

    _smsCodeController.addListener(() {
      _presenter.onCodeFieldChanged(_smsCodeController.text);
    });

    _controller.addListener(() {
      if (_controller.isCompleted) _presenter.onTickerFinished();
    });
  }

  @override
  void dispose() {
    _phoneFieldController.dispose();
    _controller.dispose();
    _smsCodeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 26.0, color: Colors.black);

    Text prefix = new Text('+7', style: style);

    TextField phoneField = new TextField(
      controller: _phoneFieldController,
      style: new TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 26.0, color: Colors.black),
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp("[0-9]")),
        LengthLimitingTextInputFormatter(10),
      ],
      decoration: InputDecoration(
        border: InputBorder.none,
        prefixIcon: SizedBox(
          child: Center(
            widthFactor: 0.0,
            child: prefix,
          ),
        ),
      ),
    );

    Container phoneBox = new Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30.0),
        ),
        padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
        margin: const EdgeInsets.all(6.0),
        width: MediaQuery.of(context).size.width * 0.8,
        child: new Center(child: phoneField));

    Widget registerButton = CustomButton(
      text: 'Привязать',
      size: 24.0,
      enabled: registerButtonEnabled,
      onTap: () {
        registerPressed = true;
        _presenter.onClickLinkButton();
      },
    );

    TextField smsCodeField = new TextField(
      controller: _smsCodeController,
      enabled: codeFieldEnabled,
      style: new TextStyle(fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 26.0, color: Colors.black),
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp("[0-9]")),
        LengthLimitingTextInputFormatter(6),
      ],
      decoration: InputDecoration(border: InputBorder.none, hintText: ''),
    );

    Container codeBox = new Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30.0),
        ),
        padding: const EdgeInsets.only(top: 4.0, left: 16.0, right: 16.0),
        margin: const EdgeInsets.all(6.0),
        width: MediaQuery.of(context).size.width * 0.5,
        child: new Center(child: smsCodeField));

    var list = ListView(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(horizontal: 20),
      children: <Widget>[
        phoneBox,
        registerButton,
        Container(
          child: new Center(
            child: new CountdownWidget(
              animation: new StepTween(
                begin: kStartValue,
                end: 0,
              ).animate(_controller),
            ),
          ),
        ),
        IgnorePointer(
            ignoring: !registerPressed,
            child: AnimatedOpacity(
                opacity: registerPressed ? 1 : 0,
                duration: Duration(milliseconds: 500),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  child: codeBox,
                ))),
        IgnorePointer(
            ignoring: !registerPressed,
            child: AnimatedOpacity(
                opacity: registerPressed ? 1 : 0,
                duration: Duration(milliseconds: 500),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 60),
                  child: CustomButton(
                    text: 'ОК',
                    enabled: confirmCodeButtonEnabled,
                    onTap: () {
                      _presenter.onClickConfirmCodeButton();
                    },
                  ),
                )))
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Привязка номера'),
      ),
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.cover),
        ),
        child: Align(
          alignment: Alignment.center,
          child: list,
        ),
      ),
    );
  }

  @override
  void showAlert(AlertMessage message) async {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text(message.title),
              content: new Text(message.message),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Ок',
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ],
            ));
  }

  @override
  void showLoading() {
    showLoadingAlert(context);
  }

  @override
  void hideLoading() {
    Navigator.of(context).pop();
  }

  @override
  void setEnabledCodeField(bool enabled) {
    setState(() {
      codeFieldEnabled = enabled;
    });
  }

  @override
  void setEnabledLinkButton(bool enabled) {
    setState(() {
      registerButtonEnabled = enabled;
    });
  }

  @override
  void showError(AlertMessage message) async {

    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              title: new Text(message.title),
              content: new Text(message.message),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Закрыть',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ],
            ));
  }

  @override
  void updateState() {
    setState(() {});
  }

  @override
  void setEnabledConfirmCodeButton(bool enabled) {
    print(confirmCodeButtonEnabled.toString());
    setState(() {
      confirmCodeButtonEnabled = enabled;
    });
  }

  @override
  void startTicker() {
    _controller.forward(from: 0.0);
  }


  @override
  void resetView() {
    _controller.reset();
    _phoneFieldController.clear();
    _smsCodeController.clear();
    setState(() {
      registerPressed = false;
    });
  }

  @override
  void hideView() {
    Navigator.of(context).pop();
  }

  void showMain() {
    Navigator.of(context).pushNamedAndRemoveUntil("/main", (Route<dynamic> route) => false);
  }
}

class CountdownWidget extends AnimatedWidget {
  CountdownWidget({Key key, this.animation}) : super(key: key, listenable: animation);
  final Animation<int> animation;

  @override
  build(BuildContext context) {
    return new Text(getTime(animation.value),
        style: new TextStyle(color: Colors.black, fontFamily: 'PFDin', fontWeight: FontWeight.w500, fontSize: 28.0));
  }
}
