import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';

abstract class PhoneLinkingContract implements BaseView {
   void setEnabledLinkButton(bool enabled);
   void setEnabledCodeField(bool enabled);
   void setEnabledConfirmCodeButton(bool enabled);
   void startTicker();
   void resetView();
}

abstract class PhoneLinkingPresenterContract implements BasePresenter {
  void onClickLinkButton();
  void onClickConfirmCodeButton();
  void onPhoneFieldChanged(String newValue);
  void onCodeFieldChanged(String newValue);
  void onTickerFinished();
}