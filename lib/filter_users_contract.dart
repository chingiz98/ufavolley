import 'models/user_filter.dart';

abstract class FilterableUsersViewContract {
  void showUsersFilterDialog(UserParamFilter filter);
}

abstract class FilterableUsersPresenterContract {
  void clickOnFilterButton();
  void clickOnConfirmFilterButton(UserParamFilter filter);
  void updateUserFilter(UserParamFilter filter);
}