import 'package:package_info/package_info.dart';
import 'package:ufavolley/repository/app_config/app_config_repository.dart';

class AppConfigRepositoryImpl extends AppConfigRepository {
  @override
  Future<String> getVersion() async {
    final packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }
}
