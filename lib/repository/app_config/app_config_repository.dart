abstract class AppConfigRepository {
  Future<String> getVersion();
}