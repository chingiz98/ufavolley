import 'package:flutter/material.dart';

class Constants {
  static const MaterialColor primaryColorShades = MaterialColor(
    0xFF2a339a,
    <int, Color>{
      50: Color(0xFFE8EAF6),
      100: Color(0xFFC5CAE9),
      200: Color(0xFF9FA8DA),
      300: Color(0xFF7986CB),
      400: Color(0xFF5C6BC0),
      500: Color(0xFF2a339a),
      600: Color(0xFF262f99),
      700: Color(0xFF212a96),
      800: Color(0xFF1c2593),
      900: Color(0xFF1A237E),
    },
  );

  static const Color litePlusProColor = Colors.deepPurple;
}