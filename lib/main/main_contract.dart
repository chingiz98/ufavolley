import 'package:ufavolley/base/base_presenter.dart';
import 'package:ufavolley/base/base_view.dart';

abstract class MainViewContract extends BaseView {
  void showExitDialog();
  void changePage(AppPages page);
}

abstract class MainPresenterContract extends BasePresenter {
  void onBackPressed();
  void setCurrentPage(AppPages page);
}

enum AppPages {
  Schedule,
  Rules,
  Profile,
  Notifications,
  Rating,
  Contacts,
  ControlPanel
}