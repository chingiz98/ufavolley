import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/modules/application_notifications.dart';
import 'package:ufavolley/modules/notifications.dart';
import 'package:ufavolley/presentation/my_flutter_app_icons.dart';
import 'package:ufavolley/utils/alert_message.dart';
import 'package:ufavolley/utils/resource_manager.dart';
import 'package:ufavolley/views/drawer_menu/view/drawer_menu.dart';
import 'package:ufavolley/views/pages/contacts/view/contacts_page.dart';
import 'package:ufavolley/views/pages/control_panel/control_page.dart';
import 'package:ufavolley/views/pages/filter_training/view/filter_training_page.dart';
import 'package:ufavolley/views/pages/notifications/confirm_visit/view/confirm_your_visit_page.dart';
import 'package:ufavolley/views/pages/notifications/informative/view/informative_notification_page.dart';
import 'package:ufavolley/views/pages/notifications/user_action/view/user_action_notification_page.dart';
import 'package:ufavolley/views/pages/notifications/view/notifications_page.dart';
import 'package:ufavolley/views/pages/profile/view/profile_page.dart';
import 'package:ufavolley/views/pages/rules/view/rules_page.dart';
import 'package:ufavolley/views/pages/schedule/view/schedule_page.dart';
import 'package:ufavolley/views/pages/sign_up_for_training/view/sign_up_for_training_page.dart';
import 'package:ufavolley/views/pages/tournament_rating/view/tournament_rating_view.dart';
import 'package:uni_links/uni_links.dart';

import 'main_contract.dart';
import 'main_presenter.dart';

class MainPage extends StatefulWidget {
  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage>
    with TickerProviderStateMixin
    implements MainViewContract, NotificationsListener {
  AppPages currentPage = AppPages.Schedule;
  MainPresenterContract _presenter;
  ApplicationNotifications _applicationNotifications;

  void _updateFilterObject() {
    print('filted object updated');
    setState(() {});
  }

  @override
  void initState() {
    print('initState in main page called');

    super.initState();

//    _applicationNotifications = ApplicationNotifications(this);

    final notifications = Notifications();
    notifications.initialize(this);
    notifications.startListenBackgroundNotifications();

    _presenter = MainPresenter(this);

    ResourceManager().signForUpdates(this);

    _checkInitialDeepLink();
    _listenForDeepLink();
  }

  Widget _buildAppBar(bool showBadge, String text, {List<Widget> actions, Widget bottom}) {
    Widget leading = Builder(
      builder: (context) {
        return IconButton(
          color: Colors.white,
          icon: Stack(
            clipBehavior: Clip.none,
            children: <Widget>[
              Icon(Icons.menu),
              Positioned(
                top: 0.5,
                right: -3.5,
                child: Material(
                  borderRadius: BorderRadius.circular(4.0),
                  color: Colors.transparent,
                  elevation: 4.0,
                  child: Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 10,
                      minHeight: 10,
                    ),
                  ),
                ),
              )
            ],
          ),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
        );
      },
    );

    return AppBar(
      bottom: bottom,
      leading: showBadge ? leading : null,
      title: Text(text),
      actions: (actions != null) ? actions : null,
    );
  }

  @override
  Widget build(BuildContext context) {
    print('main build called');

    if (ResourceManager().currentUser.isBanned)
      return Scaffold(
        body: Center(
          child: Text('Произошла ошибка, попробуйте зайти позже'),
        ),
      );

    bool notificationsFlag = false;
    var notifications = ResourceManager().notifications;
    for (var n in notifications) {
      if (!n.read) {
        notificationsFlag = true;
        break;
      }
    }

    AppBar appBar;
    var body;

    switch (currentPage) {
      case AppPages.Schedule:
        {
          appBar = _buildAppBar(notificationsFlag, 'РАСПИСАНИЕ', actions: <Widget>[
            Builder(
              builder: (context) => IconButton(
                tooltip: 'Фильтр',
                icon: Icon(MyFlutterApp.icon_filter, color: Colors.white),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FilterTrainingPage(listener: _updateFilterObject)),
                  );
                },
                color: Colors.white,
              ),
            )
          ]);

          body = Schedule();
          break;
        }
      case AppPages.Rules:
        appBar = _buildAppBar(notificationsFlag, 'ПРАВИЛА');
        body = RulesPage();
        break;
      case AppPages.Profile:
        appBar = _buildAppBar(notificationsFlag, 'МОЙ ПРОФИЛЬ');
        body = ProfilePage();
        break;
      case AppPages.Notifications:
        appBar = _buildAppBar(notificationsFlag, 'УВЕДОМЛЕНИЯ', actions: <Widget>[
          Builder(
            builder: (context) => IconButton(
              tooltip: 'Фильтр',
              icon: Icon(Icons.mail),
              color: Colors.white,
              onPressed: () async {
                _showReadAllNotificationDialog();
              },
            ),
          )
        ]);
        body = NotificationsPage();
        break;
      case AppPages.Rating:
        var tabController = TabController(length: 3, vsync: this);
        appBar = _buildAppBar(notificationsFlag, 'РЕЙТИНГ',
            bottom: TabBar(
              controller: tabController,
              tabs: [Tab(text: 'Лайт'), Tab(text: 'Лайт+'), Tab(text: 'Медиум')],
            ));

        body = TournamentRatingPage(tabController: tabController);

        if (ResourceManager().currentUser.level == "U6zaxhj6IzaExZqcMkQ3") tabController.animateTo(0);
        if (ResourceManager().currentUser.level == "Y5gzffkscPylzcP8rNkz") tabController.animateTo(1);
        if (ResourceManager().currentUser.level == "ec9tH4CEAVLxfT398nV4") tabController.animateTo(2);

        break;
      case AppPages.Contacts:
        appBar = _buildAppBar(notificationsFlag, 'КОНТАКТЫ');
        body = ContactsPage();
        break;
      case AppPages.ControlPanel:
        appBar = _buildAppBar(notificationsFlag, 'НАСТРОЙКИ');
        body = ControlPage();
        break;
    }

    return Scaffold(
      appBar: appBar,
      drawer: Drawer(
        child: DrawerMenu(listener: changePage),
      ),
      body: WillPopScope(
          child: body,
          onWillPop: () async {
            _presenter.onBackPressed();
            return false;
          }),
    );
  }

  void _showReadAllNotificationDialog() {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Прочитать все уведомления?'),
        content: new Text('Вы уверены?'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Нет'),
          ),
          TextButton(
            onPressed: () async {
              APISupport.readAllNotifications();
              ResourceManager().readAllNotifications();
              Navigator.of(context).pop(true);
            },
            child: Text('Да'),
          ),
        ],
      ),
    );
  }

  @override
  void changePage(AppPages page) {
    print('changePage called');
    _presenter.setCurrentPage(page);
    setState(() {
      currentPage = page;
    });
  }

  @override
  void hideLoading() {
    // TODO: implement hideLoading
  }

  @override
  void hideView() {
    // TODO: implement hideView
  }

  @override
  void showAlert(AlertMessage message) {
    // TODO: implement showAlert
  }

  @override
  void showError(AlertMessage message) {
    // TODO: implement showError
  }

  @override
  void showExitDialog() {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: Text('Закрыть приложение'),
        content: Text('Вы уверены?'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Нет'),
          ),
          TextButton(
            onPressed: () async => await SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop'),
            child: Text('Да'),
          ),
        ],
      ),
    );
  }

  @override
  void showLoading() {
    // TODO: implement showLoading
  }

  @override
  void updateState() {
    print("MainPage updateState was called");
    setState(() {});
  }

  @override
  void onNewNotification(NotificationProp prop, int type) {
    APISupport.readNotification(notificationId: prop.id);
    switch (type) {
      case 0:
        {
          print('---' + prop.id);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => InformativeNotificationPage(notification: prop)));

          break;
        }
      case 1:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmYourVisitPage(notification: prop)));
          break;
        }
      case 2:
        {
          print('---' + prop.id);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => InformativeNotificationPage(notification: prop)));

          break;
        }
      case 3:
        {
          print('---' + prop.id);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => InformativeNotificationPage(notification: prop)));

          break;
        }

      case 4:
        {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => UserActionNotificationPage(notification: prop)));
          break;
        }

      case 5:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmYourVisitPage(notification: prop)));
          break;
        }
    }
  }

  Future<void> _checkInitialDeepLink() async {
    final String link = await getInitialLink();
    if (link != null) {
      _proceedDeepLink(link);
    }
  }

  Future<void> _listenForDeepLink() async {
    linkStream.listen((String link) {
      if (link != null) {
        _proceedDeepLink(link);
      }
    }, onError: (err) {});
  }

  Future<void> _proceedDeepLink(String link) async {
    if (link.contains('/training?id=')) {
      if (ResourceManager().currentUser.uid != null) {
        final String trainingId = link.split('=')[1];
        final training = ResourceManager().trainings.firstWhere(
              (training) => training.trainingID == trainingId,
              orElse: null,
            );
        if (training != null) {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => SignUpForTrainingPage(training)));
        }
      }
    }
  }
}
