import 'dart:io';
import 'package:ufavolley/main/main_contract.dart';

class MainPresenter implements MainPresenterContract {

  MainViewContract _view;
  AppPages currentPage;
  bool isOutdatedMessageShown = false;

  MainPresenter(MainViewContract view) {
    this._view = view;
  }

  @override
  void onBackPressed() {
    if (currentPage != AppPages.Schedule)
      _view.changePage(AppPages.Schedule);
    else if (Platform.isAndroid)
        _view.showExitDialog();
  }

  @override
  void setCurrentPage(AppPages page) {
    currentPage = page;
  }

}