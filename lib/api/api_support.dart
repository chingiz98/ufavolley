import 'dart:convert';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:ufavolley/models/cloud_value.dart';
import 'package:ufavolley/models/notificationProp.dart';
import 'package:ufavolley/models/training.dart';
import 'package:ufavolley/models/user.dart';
import 'package:ufavolley/utils/utils.dart';
import 'package:ufavolley/views/pages/training_leaderboard_settings/models/training_leader_board_data.dart';

class APISupport {
  static Parser _parser = Parser();

  static FirebaseFunctions _getFirebaseFunctionsInstance() {
    //return FirebaseFunctions.instance.useFunctionsEmulator(origin: "http://10.0.2.2:5001");
    return FirebaseFunctions.instance;
  }

  static Future<List<User>> getRatingList(String level) async {
    print('getRatingList');
    HttpsCallable callable = FirebaseFunctions.instance.httpsCallable('getRatingListV2');
    var response = await callable.call({'level': level});

    dynamic usersWrap = response.data;
    final List<User> _users = [];
    for (var userWrap in usersWrap) {
      var wrap = new Map<String, dynamic>.from(userWrap);
      _users.add(User.fromJson(wrap));
    }
    return _users;
  }

  static Future<TrainingLeaderBoardData> getLeaderBoardData(String trainingId) async {
    print('getLeaderBoardData');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getLeaderBoardData');

    var response = await callable.call({'trainingId': trainingId});
    var jsonDecoded = jsonDecode(response.data);
    TrainingLeaderBoardData trainingLeaderBoardData = TrainingLeaderBoardData.fromJson(jsonDecoded);
    return trainingLeaderBoardData;
  }

  static Future<int> setLeaderBoardData(TrainingLeaderBoardData trainingLeaderBoardData) async {
    print('call sendLeaderBoardData');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('setLeaderBoardData');

    var jsonData = jsonEncode(trainingLeaderBoardData.toJson());
    await callable.call({'data': jsonData});
    return 0;
  }

  static Future<int> readAllNotifications() async {
    print('call readAllNotifications');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('readAllNotifications');

    await callable.call();
    return 0;
  }

  static Future<int> uploadVkPhoto({@required String photoURL}) async {
    print('call uploadVkPhoto');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('uploadVkPhoto');

    await callable.call(<String, dynamic>{'url': photoURL}).catchError((error) {
      throw Exception();
    });
    return 0;
  }

  static Future<int> removeTrainer({@required String uid}) async {
    print('call removeTrainer');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('deleteDoc');

    HttpsCallableResult result = await callable.call({'type': "trainer", 'id': uid});

    dynamic response = result.data;
    int code = response['code'];

    return code;
  }

  static Future<List<ArchivedNotificationProp>> getNotificationArchive() async {
    print('call getNotificationArchive');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getNotificationArchive');

    HttpsCallableResult response = await callable.call();
    dynamic archive = response.data['response'];
    final List<ArchivedNotificationProp> notifications = <ArchivedNotificationProp>[];
    for (var notification in archive) {
      NotificationProp notificationProp = _parser.parseNotification(notification);
      dynamic uids = notification['receiverUids'];
      List<String> receiveUids = [];
      for (var uid in uids) {
        receiveUids.add(uid);
      }
      ArchivedNotificationProp archivedNotificationProp = ArchivedNotificationProp(notificationProp, receiveUids);
      notifications.add(archivedNotificationProp);
    }
    return notifications;
  }

  static Future<Map<User, NotificationProp>> getUsersNotifications(String notificationId) async {
    print('call getUsersNotifications');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getUsersNotifications');

    HttpsCallableResult response = await callable.call({'notificationId': notificationId});

    //dynamic resp = response.data['pushes'];

    NotificationProp notificationProp1 = _parser.parseNotification(response.data['notification']);
    List users = response.data['users'];
    notificationProp1.read = false;

    Map<User, NotificationProp> notifications = Map<User, NotificationProp>();
    List readUids = response.data['notification']['readUids'];
    if (readUids == null) readUids = [];

    for (var us in users) {
      NotificationProp np =
          new NotificationProp(title: notificationProp1.title, subTitle: notificationProp1.subTitle, id: null);
      np.dateOfReceiving = notificationProp1.dateOfReceiving;
      np.type = notificationProp1.type;
      User user = _parser.parseUser(us);
      if (readUids.contains(user.uid))
        np.read = true;
      else
        np.read = false;
      notifications[user] = np;
    }

    /*
    for (var responseItem in resp) {
      NotificationProp notificationProp = _parser.parseNotification(responseItem);
      User user = _parser.parseUser(responseItem['user']);
      notifications[user] = notificationProp;
    }
    */
    return notifications;
  }

  static Future<void> readNotification({@required String notificationId}) async {
    print('call readNotification');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('readNotification');

    await callable.call(<String, dynamic>{'id': notificationId});
  }

  static Future<List<User>> getAllUsers({@required String lastID}) async {
    print('call getAllUsers');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getAllUsers');

    HttpsCallableResult response = await callable.call(<String, dynamic>{'lastID': lastID});
    dynamic usersWrap = response.data;

    List<User> _users = [];
    for (var userWrap in usersWrap) _users.add(_parser.parseUser(userWrap));

    return _users;
  }

  static Future<List<User>> getAllUsersV2({@required String lastID}) async {
    print('call getAllUsers');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getAllUsersV2');

    HttpsCallableResult response = await callable.call(<String, dynamic>{'lastID': lastID});
    dynamic usersWrap = response.data;

    List<User> _users = [];
    for (var userWrap in usersWrap) _users.add(_parser.parseUser(userWrap));

    return _users;
  }

  static Future<List<User>> searchUsers({String searchString, @required String startAtUid, String startAtName}) async {
    print('call getAllUsers');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('searchUsers');

    HttpsCallableResult response = await callable
        .call(<String, dynamic>{'searchString': searchString, 'startAtUid': startAtUid, 'startAtName': startAtName});
    dynamic usersWrap = response.data;

    List<User> _users = [];
    for (var userWrap in usersWrap) _users.add(_parser.parseUser(userWrap));

    return _users;
  }

  static Future<List> signForTraining({@required String trainingID}) async {
    print('call signForTraining');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('signForTraining');

    HttpsCallableResult response = await callable.call(<String, dynamic>{'training_id': trainingID});

    dynamic resp = response.data;

    List<dynamic> ret = <dynamic>[];
    Training tr = _parser.parseTraining(resp['training']);
    ret.add(resp['code']);
    ret.add(tr);

    if (resp['cause'] != null) {
      Training conflictingTraining = _parser.parseTraining(resp['cause']);
      ret.add(conflictingTraining);
    }

    return ret;
  }

  static Future<List> signOffFromTraining({@required String trainingID}) async {
    print('call signOffFromTraining');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('signOffFromTraining');

    HttpsCallableResult response = await callable.call(<String, dynamic>{'training_id': trainingID});
    dynamic resp = response.data;

    List<dynamic> ret = [];
    Training tr = _parser.parseTraining(resp['training']);
    ret.add(resp['code']);
    ret.add(tr);

    return ret;
  }

  static Future<List<Training>> getTrainings() async {
    print('call getTrainings');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getTrainings');

    HttpsCallableResult response2 = await callable.call();
    dynamic response = response2.data;

    var trainingsWrap = response['trainings'];

    List<Training> trainings = [];

    for (var trainingWrap in trainingsWrap) trainings.add(_parser.parseTraining(trainingWrap));

    return trainings;
  }

  static Future<List<Training>> getTrainingsV2() async {
    print('call getTrainings');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getTrainingsV2');

    HttpsCallableResult response2 = await callable.call();
    dynamic response = response2.data;

    var trainingsWrap = response['trainings'];

    List<Training> trainings = [];

    for (var trainingWrap in trainingsWrap) trainings.add(_parser.parseTraining(trainingWrap));

    return trainings;
  }

  static Future<Training> getTraining({@required String trainingID}) async {
    print('call getTraining');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getTraining');

    HttpsCallableResult result = await callable.call(<String, dynamic>{'trID': trainingID});
    dynamic response = result.data;

    Training tr = _parser.parseTraining(response);

    return tr;
  }

  static Future<List<User>> getTrainingUsers({@required String trainingID}) async {
    print('call getTrainingUsers');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getTrainingUsersV2');

    HttpsCallableResult result = await callable.call(<String, dynamic>{'id': trainingID});

    dynamic usersWrap = result.data;
    List<User> _users = [];
    for (var userWrap in usersWrap) {
      var wrap = new Map<String, dynamic>.from(userWrap);
      _users.add(User.fromJson(wrap));
    }
    return _users;
  }

  static Future<List<User>> getUsers({@required List<String> uids}) async {
    print('call getUsers');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getUsers');

    HttpsCallableResult result = await callable.call(<String, dynamic>{'uid': uids});

    dynamic usersWrap = result.data;

    List<User> _users = [];
    for (var userWrap in usersWrap) _users.add(_parser.parseUser(userWrap));

    return _users;
  }

  static Future<User> getUserData() async {
    print('call getUserData');
    HttpsCallable callable = FirebaseFunctions.instance.httpsCallable('getUserData');

    HttpsCallableResult result = await callable.call();
    dynamic respUserData = result.data;
    return _parser.parseUser(respUserData);
  }

  static Future<List<User>> getTrainers() async {
    print('call getTrainers');
    List<User> trainers = [];
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getTrainers');

    HttpsCallableResult result = await callable.call();
    dynamic trainersWrap = result.data;
    for (int i = 0; i < trainersWrap.length; i++) trainers.add(_parser.parseUser(trainersWrap[i]));

    return trainers;
  }

  static Future<List<User>> getSupervisors() async {
    print('call getSupervisors');
    List<User> users = [];
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getSupervisors');

    HttpsCallableResult result = await callable.call();
    dynamic trainersWrap = result.data;
    for (int i = 0; i < trainersWrap.length; i++) users.add(_parser.parseUser(trainersWrap[i]));

    return users;
  }

  static Future<Map<String, List<CloudValue>>> getValues({@required String collectionName}) async {
    print('call getValues');
    var params = {'type': collectionName};
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getValues');

    HttpsCallableResult result = await callable.call(params);
    dynamic response = result.data;
    var mappedResponse = response['response'];
    List<CloudValue> cloudValues = [];
    for (int i = 0; i < mappedResponse.length; i++) {
      var cloudValue = mappedResponse[i];
      cloudValues.add(CloudValue(val: cloudValue['val'], id: cloudValue['id']));
    }
    Map<String, List<CloudValue>> result2 = Map();
    result2[collectionName] = cloudValues;

    return result2;
  }

  static Future<String> addNewUser({@required User user, @required String token}) async {
    print('call addNewUser');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('addNewUser');

    HttpsCallableResult result = await callable.call(<String, dynamic>{
      'name': user.name,
      'lastname': user.lastName,
      'age': user.dateOfBirth == null
          ? DateTime.now().toUtc().millisecondsSinceEpoch
          : user.dateOfBirth.toUtc().millisecondsSinceEpoch,
      'sex': user.gender,
      'phone': user.phoneNumber,
      'byAdmin': user.byAdmin,
      'isTrainer': user.isTrainer,
      'fcm': token
    });
    return result.data['uid'];
  }

  static Future<int> updateUser({@required User user, @required String token, bool byHimself = false}) async {
    print('call updateUser');
    Map<String, dynamic> data = {};
    if (user.name != null) data['name'] = user.name;
    if (user.lastName != null) data['lastname'] = user.lastName;
    if (user.gender != null) data['sex'] = user.gender;
    if (user.dateOfBirth != null) data['age'] = user.dateOfBirth.toUtc().millisecondsSinceEpoch;
    if (user.phoneNumber != null) data['phone'] = user.phoneNumber;
    if (user.byAdmin != null) data['byAdmin'] = user.byAdmin;
    if (user.isEditable != null) data['editable'] = user.isEditable;
    if (user.level != null) data['level'] = user.level;
    if (user.isAdmin != null) data['isAdmin'] = user.isAdmin;
    if (user.isTrainer != null) data['isTrainer'] = user.isTrainer;
    if (user.isBanned != null) data['banned'] = user.isBanned;
    if (user.isSupervisor != null) data['supervisor'] = user.isSupervisor;
    if (user.isDebtor != null) data['isDebtor'] = user.isDebtor;
    if (user.uid != null) data['id'] = user.uid;
    data['fcm'] = token;
    data['byHimself'] = byHimself;
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('updateUser');

    await callable.call(data);
    return 0;
  }

  static Future<int> signOut({@required BuildContext context, bool showLoadingWidget = false}) async {
    print('call signOut');
    if (showLoadingWidget) showLoadingAlert(context);

    String fcm = await FirebaseMessaging.instance.getToken();
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('signOut');

    HttpsCallableResult result = await callable.call(<String, dynamic>{'fcm': fcm});
    dynamic resp = result.data;

    if (showLoadingWidget) Navigator.pop(context);

    return resp['code'];
  }

  static Future<List<NotificationProp>> getNotifications() async {
    print('call getNotifications');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getNotifications');

    HttpsCallableResult result = await callable.call();
    dynamic resp = result.data;

    List<NotificationProp> notifications = [];

    for (int i = 0; i < resp.length; i++) notifications.add(_parser.parseNotification((resp[i])));

    return notifications;
  }

  static Future<int> addTraining(
      {@required Training training, @required BuildContext context, bool showLoadingWidget = false}) async {
    print('call addTraining');
    if (showLoadingWidget) showLoadingAlert(context);

    String adr = training.address.id; //id адреса
    var date = training.dateTime.millisecondsSinceEpoch; //timestamp
    List<String> levels = training.levels.map((level) => level.id).toList(); //массив id уровней
    int limit = training.capacity; //размер лимита
    int price = training.price; //цена
    String title = training.title.id; //id тайтла
    List<String> trainerIDs = training.trainerIDs; //массив id тренеров
    String type = training.type.id; //id типа
    List<String> uids = training.participants.map((user) => user.uid).toList(); //id списка записавшихся
    List<String> checked = training.incomingUsersUids;
    bool auto = training.isAutoContinue;
    int durationImMinutes = training.durationImMinutes;
    String comment = training.comment;
    bool isCancelled = training.isCancelled;
    int participantsMinimum = training.participantsMinimum;
    List<String> supervisors = training.supervisors.map((user) => user.uid).toList();

    var data = {
      'adr': adr,
      'date': date,
      'levels': levels,
      'limit': limit,
      'price': price,
      'title': title,
      'trainer_id': trainerIDs,
      'type': type,
      'auto': auto,
      'checked': checked,
      'uids': uids,
      'duration': durationImMinutes,
      'comment': comment,
      'cancelled': isCancelled,
      'participantsMinimum': participantsMinimum,
      'supervisors': supervisors
    };

    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('addTraining');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;

    if (showLoadingWidget) Navigator.pop(context);

    return response['code'];
  }

  static Future<int> updateTraining(
      {@required Training training, @required BuildContext context, bool showLoadingWidget = false}) async {
    print('call updateTraining');
    if (showLoadingWidget) showLoadingAlert(context);

    String adr = training.address.id; //id адреса
    var date = training.dateTime.toUtc().millisecondsSinceEpoch; //timestamp
    List<String> levels = training.levels.map((level) => level.id).toList(); //массив id уровней
    int limit = training.capacity; //размер лимита
    int price = training.price; //цена
    String title = training.title.id; //id тайтла
    List<String> trainerIDs = training.trainerIDs; //массив id тренеров
    String type = training.type.id; //id типа
    List<String> uids = training.participants.map((user) => user.uid).toList(); //id списка записавшихся
    List<String> checked = training.incomingUsersUids;
    bool auto = training.isAutoContinue;
    int durationImMinutes = training.durationImMinutes;
    String comment = training.comment;
    bool isCancelled = training.isCancelled;
    int participantsMinimum = training.participantsMinimum;
    List<String> supervisors = training.supervisors.map((user) => user.uid).toList();

    var data = {
      'id': training.trainingID,
      'adr': adr,
      'date': date,
      'levels': levels,
      'limit': limit,
      'price': price,
      'title': title,
      'trainer_id': trainerIDs,
      'type': type,
      'auto': auto,
      'checked': checked,
      'uids': uids,
      'duration': durationImMinutes,
      'comment': comment,
      'cancelled': isCancelled,
      'participantsMinimum': participantsMinimum,
      'supervisors': supervisors
    };

    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('updateTraining');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;

    if (showLoadingWidget) Navigator.pop(context);

    return response['code'];
  }

  static Future<int> removeTraining(
      {@required String trainingID, @required BuildContext context, bool showLoadingWidget = false}) async {
    print('call removeTraining');
    if (showLoadingWidget) showLoadingAlert(context);
    var data = {'id': trainingID, 'type': 'trainings'};
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('deleteDoc');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;
    if (showLoadingWidget) Navigator.pop(context);
    return response['code'];
  }

  static Future<int> broadcastPushNotification({@required var data}) async {
    print('call broadcastPushNotification');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('broadcastPushNotification');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;
    return response['code'];
  }

  static Future<int> broadcastPushNotificationToAll({@required var data}) async {
    print('call broadcastPushNotification');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('broadcastPushNotificationToAll');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;
    return response['code'];
  }

  static Future<CloudValue> addValue({@required CloudValue cValue, @required String collectionName}) async {
    print('call addValue');
    var data = {'type': collectionName, 'val': cValue.getValue};
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('addValue');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;
    if (response['code'] == 0) return CloudValue(val: cValue.getValue, id: response['id']);
    return null;
  }

  static Future<int> removeValue({@required CloudValue cValue, @required String collectionName}) async {
    print('call removeValue');
    var data = {'type': collectionName, 'id': cValue.id};
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('deleteDoc');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;
    return response['code'];
  }

  static Future<int> updateValue({@required CloudValue cValue, @required String collectionName}) async {
    print('call updateValue');
    var data = {'type': collectionName, 'id': cValue.id, 'val': cValue.getValue};
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('updateValue');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;
    return response['code'];
  }

  static Future<Map<String, String>> getEmailsByUids({@required List<String> uids}) async {
    print('call getEmailsByUids');
    var data = {'uids': uids};

    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getEmailsByUids');

    HttpsCallableResult result = await callable.call(data);
    dynamic response = result.data;
    Map<String, String> resultMap = Map();
    var q = response['response'];
    for (var resp in q) {
      if (resp['email'] != null) resultMap[resp['uid']] = resp['email'];
    }

    return resultMap;
  }

  static Future<bool> checkVersion({@required String version}) async {
    print('call checkVersion');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('checkVersion');

    HttpsCallableResult result = await callable.call(<String, dynamic>{'version': version});
    dynamic response = await result.data;

    if (response['code'] == 0) {
      return false;
    } else {
      return true;
    }
  }

  static Future<List<Training>> getPreviousTrainingsForAdmin() async {
    print('call getPreviousTrainingsForAdmin');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getPreviousTrainingsForAdmin');

    HttpsCallableResult result = await callable.call();
    dynamic response = result.data;
    var trainingsWrap = response['trainings'];

    List trainings = <Training>[];

    for (var trainingWrap in trainingsWrap) trainings.add(_parser.parseTraining(trainingWrap));

    return trainings;
  }

  static Future<List<Training>> getPreviousTrainingsForAdminV2() async {
    print('call getPreviousTrainingsForAdmin');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getPreviousTrainingsForAdminV2');

    HttpsCallableResult result = await callable.call();
    dynamic response = result.data;
    var trainingsWrap = response['trainings'];

    List trainings = <Training>[];

    for (var trainingWrap in trainingsWrap) trainings.add(_parser.parseTraining(trainingWrap));

    return trainings;
  }

  static Future<List<Training>> getUpcomingTrainingsForAdminV2() async {
    print('call getUpcomingTrainingsForAdmin');
    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('getUpcomingTrainingsForAdminV2');

    HttpsCallableResult result = await callable.call();

    dynamic response = result.data;
    var trainingsWrap = response['trainings'];

    List<Training> trainings = [];

    for (var trainingWrap in trainingsWrap) trainings.add(_parser.parseTraining(trainingWrap));

    return trainings;
  }

  static Future<int> cancelTraining({@required String trainingID}) async {
    print('call cancelTraining');
    var data = {
      'training_id': trainingID,
    };

    HttpsCallable callable = _getFirebaseFunctionsInstance().httpsCallable('cancelTraining');

    HttpsCallableResult response = await callable.call(data);

    int code = response.data['code'];
    return code;
  }
}

class Parser {
  Training parseTraining(var trainingWrap) {
    List<User> participants = [];
    List<User> supervisors = [];

    if (trainingWrap['users'] != null) {
      for (var userWrap in trainingWrap['users']) participants.add(parseUser(userWrap));
    } else {
      if (trainingWrap['uids'] != null) {
        for (var userId in trainingWrap['uids']) participants.add(UserStub(userId) /*parseUser(userWrap)*/);
      }
    }

    List<User> trainers = [];

    var trainersWrap = trainingWrap['trainer'];
    if (trainersWrap != null) {
      for (int i = 0; i < trainersWrap.length; i++) trainers.add(parseUser(trainersWrap[i]));
    }

    //List<User> supervisors = List();

    var supervisorsWrap = trainingWrap['supervisors'];
    if (supervisorsWrap != null) {
      for (int i = 0; i < supervisorsWrap.length; i++) supervisors.add(parseUser(supervisorsWrap[i]));
    }

    DateTime date = DateTime.fromMillisecondsSinceEpoch(trainingWrap['date']['_seconds'] * 1000);

    List<String> ids = [];
    var r = trainingWrap['trainer_id'];
    for (int i = 0; i < r.length; i++) {
      var z = r[i];
      ids.add(z);
    }

    List<CloudValue> levels = [];
    var l = trainingWrap['training_levels'];
    for (int i = 0; i < l.length; i++) {
      var z = l[i];
      levels.add(CloudValue.fromMap(z));
    }

    List<String> checked = [];
    var c = trainingWrap['checked'];
    if (c != null) {
      for (int i = 0; i < c.length; i++) {
        var z = c[i];
        checked.add(z);
      }
    }

    int durationInMinutes = trainingWrap['duration'];
    if (durationInMinutes == null) durationInMinutes = 0;

    String comment = trainingWrap['comment'];
    if (comment == null) comment = "";

    bool isCancelled = false;
    if (trainingWrap['cancelled'] != null) isCancelled = trainingWrap['cancelled'];

    int participantsMinimum = 0;
    if (trainingWrap['participantsMinimum'] != null) participantsMinimum = trainingWrap['participantsMinimum'];

    return Training(
        type: CloudValue.fromMap(trainingWrap['type']),
        title: CloudValue.fromMap(trainingWrap['title']),
        levels: levels,
        address: CloudValue.fromMap(trainingWrap['address']),
        trainerIDs: ids,
        trainers: trainers,
        capacity: trainingWrap['limit'],
        reserve: trainingWrap['reserve'],
        price: trainingWrap['price'],
        dateTime: date,
        participants: participants,
        trainingID: trainingWrap['training_id'],
        isAutoContinue: trainingWrap['auto'],
        incomingUsersUids: checked,
        durationImMinutes: durationInMinutes,
        comment: comment,
        isCancelled: isCancelled,
        participantsMinimum: participantsMinimum,
        supervisors: supervisors);
  }

  User parseUser(var userWrap) {
    if (userWrap['uid'] == null) {
      return User();
    }

    bool isMainAdmin = false;
    if (userWrap['main_admin'] != null) isMainAdmin = userWrap['main_admin'];

    bool isBanned = false;
    if (userWrap['banned'] != null) isBanned = userWrap['banned'];

    bool isSupervisor = false;
    if (userWrap['supervisor'] != null) isSupervisor = userWrap['supervisor'];

    bool isDebtor = false;
    if (userWrap['isDebtor'] != null) isDebtor = userWrap['isDebtor'];

    String imgUrl = "";
    if (userWrap['imgUrl'] != null) imgUrl = userWrap['imgUrl'];

    int rating = 0;
    if (userWrap['rating'] != null) rating = userWrap['rating'];

    return User(
        name: userWrap['first_name'],
        lastName: userWrap['last_name'],
        dateOfBirth:
            userWrap['age'] != null ? DateTime.fromMillisecondsSinceEpoch(userWrap['age']['_seconds'] * 1000) : null,
        level: userWrap['level'].toString(),
        phoneNumber: userWrap['phone'],
        gender: userWrap['sex'],
        isAdmin: userWrap['admin'],
        isTrainer: userWrap['trainer'],
        isMainAdmin: isMainAdmin,
        isEditable: userWrap['editable'],
        byAdmin: userWrap['byAdmin'],
        isBanned: isBanned,
        isSupervisor: isSupervisor,
        isDebtor: isDebtor,
        uid: userWrap['uid'],
        imageUrl: imgUrl,
        rating: rating);
  }

  NotificationProp parseNotification(var notificationWrap) {
    NotificationProp notificationProp = NotificationProp(title: '', subTitle: '', id: '');
    notificationProp.trainingID = notificationWrap['training_id'];
    switch (notificationWrap['type']) {
      case 0:
        notificationProp.title = 'Вы записались на занятие';
        notificationProp.subTitle = 'ЛИМИТ';
        notificationProp.type = NotificationType.userAction;
        break;
      case 1:
        notificationProp.title = 'Приходите!';
        notificationProp.subTitle = 'Вы в лимите!';
        notificationProp.type = NotificationType.reminder;
        break;
      case 2:
        notificationProp.title = 'Вы записались на занятие';
        notificationProp.subTitle = 'РЕЗЕРВ';
        notificationProp.type = NotificationType.userAction;
        break;
      case 3:
        notificationProp.title = 'Вы отменили запись на занятие';
        notificationProp.subTitle = '';
        notificationProp.type = NotificationType.userAction;
        break;

      case 4:
        if (notificationWrap['title'] == null) {
          notificationProp.title = "";
        } else {
          notificationProp.title = notificationWrap['title'];
        }

        if (notificationWrap['body'] == null) {
          notificationProp.subTitle = "";
        } else {
          notificationProp.subTitle = notificationWrap['body'];
        }

        notificationProp.type = NotificationType.custom;
        break;
      case 5:
        notificationProp.title = 'Напоминание';
        notificationProp.subTitle = 'Подтвердите свое посещение!';
        notificationProp.type = NotificationType.reminder;
        break;
    }

    notificationProp.id = notificationWrap['notification_id'];
    notificationProp.dateOfReceiving =
        DateTime.fromMillisecondsSinceEpoch(notificationWrap['timestamp']['_seconds'] * 1000);
    notificationProp.read = notificationWrap['read'];
    return notificationProp;
  }
}
