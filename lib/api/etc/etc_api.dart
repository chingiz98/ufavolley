abstract class EtcApi {
  Future<bool> isAppVersionOutdated(String version);
}