import 'package:ufavolley/api/api_support.dart';
import 'package:ufavolley/api/etc/etc_api.dart';

class EtcApiImpl implements EtcApi {
  @override
  Future<bool> isAppVersionOutdated(String version) {
    return APISupport.checkVersion(version: version);
  }
}
